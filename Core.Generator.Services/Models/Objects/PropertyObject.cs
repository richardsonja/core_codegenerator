﻿namespace Core.Generator.Services.Models.Objects;

public class PropertyObject : ICloneable
{
    private const string DuplicateTypesDetected = "Duplicate types detected.";
    private const string InvalidSetupWithTwoTypesOfPrivates = "Invalid setup with two types of privates.";

    public PropertyObject(string name) => 
        Name = name;

    private PropertyObject(
        AccessorType accessorType,
        ImmutableList<GetterSetterObject> getterSetter,
        bool isConstant,
        bool isReadOnly,
        bool isStatic,
        string name,
        string propertyInitiator,
        Type type,
        string typeString)
        : this(name)
    {
        AccessorType = accessorType;
        GetterSetter = getterSetter.Clone().ToImmutableList();
        IsConstant = isConstant;
        IsReadOnly = isReadOnly;
        IsStatic = isStatic;
        PropertyInitiator = propertyInitiator;
        Type = type;
        TypeString = typeString;
    }

    public AccessorType AccessorType { get; private set; } = AccessorType.Public;

    public ImmutableList<GetterSetterObject> GetterSetter { get; private set; } = new List<GetterSetterObject>().ToImmutableList();

    public bool IsConstant { get; private set; }

    public bool IsReadOnly { get; private set; }

    public bool IsStatic { get; private set; }

    public string Name { get; }

    public string PropertyInitiator { get; private set; }

    public Type Type { get; private set; } = typeof(string);

    public string TypeString { get; private set; } = "string";

    public PropertyObject AddGetterSetter(GetterSetterType type) =>
        AddGetterSetter(new GetterSetterObject(type));

    public PropertyObject AddGetterSetter(GetterSetterObject getterSetter)
    {
        if (!GetterSetter.Select(x => x.GetterSetterType).Concat(new List<GetterSetterType> { getterSetter.GetterSetterType }).IsValidCombo())
        {
            throw new IndexOutOfRangeException(InvalidSetupWithTwoTypesOfPrivates);
        }

        if (GetterSetter.Select(x => x.GetterSetterType).Contains(getterSetter.GetterSetterType))
        {
            throw new InvalidOperationException(DuplicateTypesDetected);
        }

        GetterSetter = GetterSetter.AddTo((GetterSetterObject)getterSetter?.Clone()).ToImmutableList();
        return this;
    }

    public PropertyObject AddGetterSetter(IEnumerable<GetterSetterObject> getterSetter)
    {
        var clonedGetterSetter = getterSetter.Clone().ToList();
        if (!GetterSetter.Select(x => x.GetterSetterType).Concat(clonedGetterSetter.Select(x => x.GetterSetterType)).IsValidCombo())
        {
            throw new IndexOutOfRangeException(InvalidSetupWithTwoTypesOfPrivates);
        }

        if (clonedGetterSetter.AddTo(GetterSetter).GroupBy(x => x.GetterSetterType).Any(x => x.Count() > 1))
        {
            throw new InvalidOperationException(DuplicateTypesDetected);
        }

        GetterSetter = GetterSetter.AddTo(clonedGetterSetter).ToImmutableList();

        return this;
    }

    public object Clone() => new PropertyObject(AccessorType, GetterSetter, IsConstant, IsReadOnly, IsStatic, Name, PropertyInitiator, Type, TypeString);

    public PropertyObject SetAccessorType(AccessorType accessorType)
    {
        AccessorType = accessorType;

        return this;
    }

    public PropertyObject SetIsConstant(bool isConstant = true)
    {
        ValidateBoolComboSettings(isConstant, IsReadOnly, "Invalid setup with both readonly and constant being true");

        IsConstant = isConstant;

        return this;
    }

    public PropertyObject SetIsReadOnly(bool isReadOnly = true)
    {
        ValidateBoolComboSettings(IsConstant, isReadOnly, "Invalid setup with both readonly and constant being true");
        IsReadOnly = isReadOnly;

        return this;
    }

    public PropertyObject SetIsStatic(bool isStatic = true)
    {
        ValidateBoolComboSettings(IsConstant, isStatic, "Invalid setup with both static and constant being true");
        IsStatic = isStatic;

        return this;
    }

    public PropertyObject SetPropertyInitiator(string propertyInitiator, bool requiresQuotes = true)
    {
        PropertyInitiator = requiresQuotes ? $"\"{propertyInitiator}\"" : propertyInitiator;

        return this;
    }

    public PropertyObject SetType(Type type, bool isNullable = false)
    {
        Type = type;
        TypeString = Type.ToStringFormat(isNullable);
        return this;
    }

    public PropertyObject SetType(CSharpType cSharpType)
    {
        Type = cSharpType.Type;
        TypeString = cSharpType.TypeString;

        return this;
    }

    public override string ToString() => $"{ToAccessorType()}  {ToConstant()} {ToStatic()}  {ToReadOnly()} {ToType()} {Name} {ToGetterSetter()}{ToInitiation()}{Terminator()}".Trim();

    private static void ValidateBoolComboSettings(bool settingOne, bool settingTwo, string message)
    {
        if (settingTwo && settingOne)
        {
            throw new InvalidOperationException(message);
        }
    }

    private string Terminator() => GetterSetter.IsNullOrEmpty() || !PropertyInitiator.IsNullOrEmpty() ? ";" : string.Empty;

    private string ToAccessorType() => AccessorType.CreateOutput();

    private string ToConstant() => IsConstant
        ? "const"
        : string.Empty;

    private string ToGetterSetter() =>
        GetterSetter.IsNullOrEmpty()
            ? string.Empty
            : $"{{ {GetterSetter.OrderBy(x => x.GetterSetterType).Select(x => x.ToString()).Combine(" ")} }}";

    private string ToInitiation() =>
        PropertyInitiator.IsNullOrEmpty()
            ? string.Empty
            : $" = {PropertyInitiator}";

    private string ToReadOnly() => IsReadOnly
        ? "readonly"
        : string.Empty;

    private string ToStatic() => IsStatic
        ? "static"
        : string.Empty;

    private string ToType() => TypeString;
}