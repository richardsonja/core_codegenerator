﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Sqls;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Sqls;

public class SqlCodeGeneratorServiceTests :
    IClassFixture<SqlCodeGeneratorServiceFixture>,
    IClassFixture<DatabaseTableColumnFixture>,
    IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly SqlCodeGeneratorServiceFixture _service;

    public SqlCodeGeneratorServiceTests(
        SqlCodeGeneratorServiceFixture service,
        DatabaseTableColumnFixture databaseTableColumn,
        ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }

    [Fact]
    public async void SqlCodeGeneratorService_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void SqlCodeGeneratorService_CreateOutput_ShouldBeNone_GivenConfigurationSetToFalse()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.GenerateStoredProcedures = false;

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.CreateComplexTableColumns();
        var table = new Table("BobTable", "Bob")
        {
            Columns = _databaseTableColumn.TableColumns.ToImmutableList()
        };
        var resultOption = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(table));
        resultOption.IsNone.Should().BeTrue();
        resultOption.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void SqlCodeGeneratorService_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.CreateComplexTableColumns();
        var table = new Table("BobTable", "Bob")
        {
            Columns = _databaseTableColumn.TableColumns.ToImmutableList()
        };
        var resultOption = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(table));
        resultOption.IsNone.Should().BeFalse();
        resultOption.IsSome.Should().BeTrue();

        IEnumerable<DisplayResult> result = null;
        resultOption.ShouldBeSome(some => result = some);

        result.Should().NotBeNull();
        result.Should().HaveCount(6);

        // first tab Sql should several tabs

        AssertDisplayResults(
            result.ToList(),
            "SelectService's BOB WAS HERE",
            "SelectByPrimaryKeysService's BOB WAS HERE",
            "SelectActiveListService's BOB WAS HERE",
            "SelectFindService's BOB WAS HERE",
            "DeleteService's BOB WAS HERE",
            "UpdateService's BOB WAS HERE");
    }

    [Fact]
    public async void SqlCodeGeneratorService_CreateOutput_ShouldCreateOutputWithOnly1Result_GivenValidInput()
    {
        _service.HardReset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _service.SelectService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("SelectService's BOB WAS HERE");

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.First().DisplayText.Should().Be("SelectService's BOB WAS HERE"));
    }

    [Fact]
    public async void SqlCodeGeneratorService_CreateOutput_ShouldReturnNone_GivenEmptySqlEmptyTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void SqlCodeGeneratorService_CreateOutput_ShouldReturnNone_GivenEmptySqlNullTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(null));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void SqlCodeGeneratorService_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(null);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    private static void AssertDisplayResults(List<DisplayResult> displayResults, params string[] messages)
    {
        displayResults.Should().HaveCount(messages.Length);
        for (var i = 0; i < messages.Length; i++)
        {
            var message = messages[i];
            displayResults[i].Should().NotBeNull();
            displayResults[i].DisplayText.Should().Be(message);
        }
    }
}