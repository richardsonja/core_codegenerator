﻿using Core.Common.Collections;
using Core.Common.Generics;
using Core.Generator.Services.Models.Databases;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using CollectionExtensions = Castle.Core.Internal.CollectionExtensions;

namespace Core.Generator.Services.Tests.Fixtures.Models.Databases;

public class DatabaseTableColumnFixture : IDisposable, IReset
{
    public Database Database;
    public ImmutableList<TableColumn> TableColumns;

    public DatabaseTableColumnFixture()
    {
        Reset();
    }

    public void AddActive(int counter)
    {
        ValidateSetup();

        var column = CreateTableColumns($"Bob{counter}", $"WasHere{counter}", $"DB{counter}", counter, $"Bob{counter}");
        column.SystemColumns.IsActive = true;
        column.CSharpType = new CSharpType(typeof(bool), "bool");

        TableColumns = TableColumns.AddTo(column).ToImmutableList();
        Database.Tables.First().Columns = Database.Tables.First().Columns.AddTo(column).ToImmutableList();
    }

    public void AddCreateBy(int counter)
    {
        ValidateSetup();

        var column = CreateTableColumns($"Bob{counter}", $"WasHere{counter}", $"DB{counter}", counter, $"Bob{counter}");
        column.SystemColumns.IsCreateBy = true;
        column.CSharpType = new CSharpType(typeof(string), "string");

        TableColumns = TableColumns.AddTo(column).ToImmutableList();
        Database.Tables.First().Columns = Database.Tables.First().Columns.AddTo(column).ToImmutableList();
    }

    public void AddCreateDate(int counter)
    {
        ValidateSetup();

        var column = CreateTableColumns($"Bob{counter}", $"WasHere{counter}", $"DB{counter}", counter, $"Bob{counter}");
        column.SystemColumns.IsCreateDate = true;
        column.CSharpType = new CSharpType(typeof(DateTime), "DateTime");

        TableColumns = TableColumns.AddTo(column).ToImmutableList();
        Database.Tables.First().Columns = Database.Tables.First().Columns.AddTo(column).ToImmutableList();
    }

    public void AddModifyBy(int counter)
    {
        ValidateSetup();

        var column = CreateTableColumns($"Bob{counter}", $"WasHere{counter}", $"DB{counter}", counter, $"Bob{counter}");
        column.SystemColumns.IsModifyBy = true;
        column.CSharpType = new CSharpType(typeof(string), "string");

        TableColumns = TableColumns.AddTo(column).ToImmutableList();
        Database.Tables.First().Columns = Database.Tables.First().Columns.AddTo(column).ToImmutableList();
    }

    public void AddModifyDate(int counter)
    {
        ValidateSetup();

        var column = CreateTableColumns($"Bob{counter}", $"WasHere{counter}", $"DB{counter}", counter, $"Bob{counter}");
        column.SystemColumns.IsModifyDate = true;
        column.CSharpType = new CSharpType(typeof(DateTime), "DateTime");

        TableColumns = TableColumns.AddTo(column).ToImmutableList();
        Database.Tables.First().Columns = Database.Tables.First().Columns.AddTo(column).ToImmutableList();
    }

    public void AddPermanentName(int counter)
    {
        ValidateSetup();

        var column = CreateTableColumns($"Bob{counter}", $"WasHere{counter}", $"DB{counter}", counter, $"Bob{counter}");
        column.SystemColumns.IsPermanentName = true;
        column.CSharpType = new CSharpType(typeof(string), "string");

        TableColumns = TableColumns.AddTo(column).ToImmutableList();
        Database.Tables.First().Columns = Database.Tables.First().Columns.AddTo(column).ToImmutableList();
    }

    public void AddPrimary(int counter)
    {
        ValidateSetup();

        var column = CreateTableColumns($"Bob{counter}", $"WasHere{counter}", $"DB{counter}", counter, $"Bob{counter}");
        column.IsIndex = true;
        column.CSharpType = new CSharpType(typeof(string), "string");

        TableColumns = TableColumns.AddTo(column).ToImmutableList();
        Database.Tables.First().Columns = Database.Tables.First().Columns.AddTo(column).ToImmutableList();
    }

    public void AddTimeStamp(int counter)
    {
        ValidateSetup();

        var column = CreateTableColumns($"Bob{counter}", $"WasHere{counter}", $"DB{counter}", counter, $"Bob{counter}");
        column.SystemColumns.IsTimeStamp = true;
        column.CSharpType = new CSharpType(typeof(DateTime), "DateTime");

        TableColumns = TableColumns.AddTo(column).ToImmutableList();
        Database.Tables.First().Columns = Database.Tables.First().Columns.AddTo(column).ToImmutableList();
    }

    public ImmutableList<TableColumn> CreateComplexTableColumns()
    {
        var column1 = CreateTableColumns("Bob1", "WasHere1", "DB1", 1, "Bob1");
        column1.SystemColumns.IsActive = true;
        column1.CSharpType = new CSharpType(typeof(bool), "bool");

        var column2 = CreateTableColumns("Bob2", "WasHere2", "DB2", 2, "Bob2");
        column2.SystemColumns.IsActive = false;
        column2.CSharpType = new CSharpType(typeof(bool), "bool");

        var column3 = CreateTableColumns("Bob3", "WasHere3", "DB3", 3, "Bob3");
        column3.SystemColumns.IsActive = true;
        column3.CSharpType = new CSharpType(typeof(string), "string");

        var column4 = CreateTableColumns("Bob4", "WasHere4", "DB4", 4, "Bob4");
        column4.SystemColumns.IsActive = false;
        column4.CSharpType = new CSharpType(typeof(bool), "bool");

        var column5 = CreateTableColumns("Bob5", "WasHere5", "DB5", 5, "Bob5");
        column5.SystemColumns.IsActive = true;

        var column6 = CreateTableColumns("Bob6", "WasHere6", "DB6", 6, "Bob6");
        column6.SystemColumns.IsActive = false;

        var column7 = CreateTableColumns("Bob7", "WasHere7", "DB7", 7, "Bob7");
        column7.SystemColumns.IsActive = true;

        var column8 = CreateTableColumns("Bob8", "WasHere8", "DB8", 8, "Bob8");
        column8.SystemColumns.IsActive = false;

        var column9 = CreateTableColumns("Bob9", "WasHere9", "DB9", 9, "Bob9");
        column9.SystemColumns.IsActive = true;
        column9.CSharpType = new CSharpType(typeof(bool), "bool");

        var column10 = CreateTableColumns("Bob10", "WasHere10", "DB10", 10, "Bob10");
        column10.SystemColumns.IsActive = false;
        column10.CSharpType = new CSharpType(typeof(bool), "bool");

        var column11 = CreateTableColumns("Bob11", "WasHere11", "DB11", 11, "Bob11");
        column11.SystemColumns.IsActive = true;
        column11.CSharpType = new CSharpType(typeof(string), "string");

        var column12 = CreateTableColumns("Bob12", "WasHere12", "DB12", 12, "Bob12");
        column12.SystemColumns.IsActive = false;
        column12.CSharpType = new CSharpType(typeof(Guid), "Guid");

        var permanentName = CreateTableColumns("Bob13", "WasHere13", "DB13", 13, "Bob13");
        permanentName.SystemColumns.IsPermanentName = true;
        permanentName.SystemColumns.IsActive = true;

        var inactiveIdentity = CreateTableColumns("Bob14", "WasHere14", "DB14", 14, "Bob14");
        inactiveIdentity.IsIdentity = true;
        inactiveIdentity.SystemColumns.IsActive = false;

        var inactiveIndex = CreateTableColumns("Bob15", "WasHere15", "DB15", 15, "Bob15");
        inactiveIndex.IsIndex = true;
        inactiveIndex.SystemColumns.IsActive = false;

        var inactiveForiegn = CreateTableColumns("Bob16", "WasHere16", "DB16", 16, "Bob16");
        inactiveForiegn.ForeignColumn = "bob2000";
        inactiveForiegn.SystemColumns.IsActive = false;

        var active = CreateTableColumns("Bob17", "WasHere17", "DB17", 17, "Bob17");
        active.SystemColumns.IsActive = true;

        TableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4,
            column7,
            column5,
            column6,
            column8,
            column9,
            column10,
            column11,
            column12,
            permanentName,
            inactiveIdentity,
            inactiveIndex,
            inactiveForiegn,
            active
        }.ToImmutableList();

        return TableColumns.ToImmutableList();
    }

    public TableColumn CreateTableColumns(
        string tableName,
        string tableSchema,
        string databaseName,
        int ordinalPosition,
        string columnName = "Column1") =>
        new()
        {
            TableName = tableName,
            TableSchema = tableSchema,
            DatabaseName = databaseName,
            Name = columnName,
            CamelCaseName = columnName.CamelCase(),
            PascalCaseName = columnName.PascalCase(),
            OrdinalPosition = ordinalPosition,
            SystemColumns = new SystemColumns(),
            CSharpType = new CSharpType(typeof(string), "string"),
            DataType = typeof(string).ToSqlDbType(),
            OriginalDataTypeString = "nvarchar",
            CharacterMaxLength = -1,
            Percision = 10,
            Scale = 11,
            IsNullable = false
        };

    public void Dispose()
    {
        Database = null;
        TableColumns = null;
    }

    public void Reset()
    {
        SetDatabase("Bob");
        SetEmptyTableColumns();
    }

    public void SetDatabase(string databaseName, string tableName = null, string tableSchema = null, string columnName = null)
    {
        Database = new()
        {
            HasPermission = true,
            Id = 5,
            Name = databaseName,
            Tables = new List<Table>().ToImmutableList()
        };

        if (!CollectionExtensions.IsNullOrEmpty(tableName))
        {
            Database.Tables = new List<Table>
            {
                new(tableName, tableSchema)
                {
                    Columns = CollectionExtensions.IsNullOrEmpty(columnName)
                        ? new List<TableColumn>().ToImmutableList()
                        : new List<TableColumn>
                        {
                            new()
                            {
                                Name = columnName,
                                TableName = tableName,
                                TableSchema = tableSchema,
                                CamelCaseName = columnName.CamelCase(),
                                PascalCaseName = columnName.PascalCase(),
                                IsIndex = true,
                                CSharpType = new CSharpType(typeof(bool), "bool"),
                                DatabaseName = databaseName,
                                DataType = typeof(string).ToSqlDbType(),
                                IsNullable = false,
                                HumanReadableFormattedName = columnName.FormatStringWithSpacesAndProperCapitalization(),
                                SystemColumns = new SystemColumns(),
                                OriginalDataTypeString = "nvarchar",
                                CharacterMaxLength = -1,
                                Percision = 10,
                                Scale = 11,
                            }
                        }.ToImmutableList()
                }
            }.ToImmutableList();
        }
    }

    public void SetEmptyTableColumns() => TableColumns = new List<TableColumn>().ToImmutableList();

    public void SetNullDatabase() => Database = null;

    public void SetNullTableColumns() => TableColumns = null;

    public void SetTableColumns(string tableName, string tableSchema, string databaseName) =>
        TableColumns = new List<TableColumn>()
        {
            CreateTableColumns(tableName, tableSchema, databaseName, 2),
            CreateTableColumns(tableName, tableSchema, databaseName, 1)
        }.ToImmutableList();

    private void ValidateSetup()
    {
        TableColumns ??= new List<TableColumn>().ToImmutableList();
        if (Database == null)
        {
            SetDatabase("Bob", "Bob was", "here", "now gone he is");
        }

        Database.Tables ??= new List<Table>().ToImmutableList();
        if (Database.Tables.IsNullOrEmpty())
        {
            Database.Tables = Database.Tables.AddTo(new Table("Bob", "Was Here")).ToImmutableList();
        }

        Database.Tables.First().Columns ??= new List<TableColumn>().ToImmutableList();
    }
}