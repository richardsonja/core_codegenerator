﻿namespace Core.Flux;

public class Fluxs
{
    private static IDispatcher _dispatcher;
    private static IStoreResolver _storeResolver;

    public static IDispatcher Dispatcher
    {
        get => _dispatcher ??= new Dispatcher();
        set => _dispatcher = value;
    }

    public static IStoreResolver StoreResolver
    {
        get => _storeResolver ??= new DefaultStoreResolver();
        set => _storeResolver = value;
    }
}