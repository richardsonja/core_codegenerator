﻿namespace Core.Generator.Services.Services.Configurations;

public class SessionService : ISessionService
{
    public Option<T> Get<T>(string name)
    {
        var result = (T)AppDomain.CurrentDomain.GetData(name);
        return result == null
            ? Option<T>.None
            : result;
    }

    public Option<bool> Save(string name, object value)
    {
        AppDomain.CurrentDomain.SetData(name, value);
        return true;
    }
}