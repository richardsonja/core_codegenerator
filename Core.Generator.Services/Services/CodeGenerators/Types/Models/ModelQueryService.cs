﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public class ModelQueryService : IModelQueryService
{
    private readonly IConfigurationService _configurationService;

    public ModelQueryService(IConfigurationService configurationService)
    {
        _configurationService = configurationService;
    }

    public async Task<Option<string>> CreateOutputAsync(CodeGeneratorRequest request)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<string>.None;
        }

        var configOption = await _configurationService.GetAsync();
        return configOption.Match(
            x => CreateClass(x, request.Table),
            () => Option<string>.None);
    }

    private static Option<string> CreateClass(ConfigurationSettings configuration, Table table)
    {
        var properties = GetProperties(table);
        if(properties.IsNullOrEmpty())
        {
            return Option<string>.None;
        }

        var classObject = new ClassObject(GetClassName(configuration, table), GetNamespace(configuration, table))
            .AddUsingStatements(GetUsingStatements())
            .AddProperties(properties);

        return classObject.ToString();
    }

    private static string GetClassName(ConfigurationSettings configuration, Table table) =>
        $"{table.FormattedName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.QueryModelName}";

    private static string GetNamespace(ConfigurationSettings configuration, Table table) =>
        $"{configuration.ConfigurationDetail.CodeRelatedConfiguration.Namespace}.Models.{table.Schema}.{configuration.ConfigurationDetail.CodeRelatedConfiguration.QueryNamespaceSuffix}";

    private static ImmutableList<PropertyObject> GetProperties(Table table)
    {
        var hasActiveBoolean = !table.Columns
            .GetActiveBooleanColumns()
            .IsNullOrEmpty();
        var properties = new List<PropertyObject>();
        if (hasActiveBoolean)
        {
            properties.Add(new PropertyObject("IncludeInactive")
                .SetType(typeof(bool?), true)
                .AddGetterSetter(new GetterSetterObject(GetterSetterType.Get))
                .AddGetterSetter(new GetterSetterObject(GetterSetterType.Set)));
        }

        var permanentAndOthers = table.Columns
            .GetPermanentNameOrNonActivePrimaryOrForeignColumns()
            .Select(x => new PropertyObject(x.PascalCaseName)
                .SetType(x.CSharpType)
                .AddGetterSetter(new GetterSetterObject(GetterSetterType.Get))
                .AddGetterSetter(new GetterSetterObject(GetterSetterType.Set)))
            .ToList();

        var booleans = table.Columns
            .GetNonActiveBooleansOrActiveNonBooleanColumns()
            .Select(x => new PropertyObject(x.PascalCaseName)
                .SetType(x.CSharpType.Type, true)
                .AddGetterSetter(new GetterSetterObject(GetterSetterType.Get))
                .AddGetterSetter(new GetterSetterObject(GetterSetterType.Set)))
            .ToList();

        return properties
            .AddTo(permanentAndOthers)
            .AddTo(booleans)
            .DistinctByCondition(x=> x.ToString())
            .ToImmutableList();
    }

    private static ImmutableList<string> GetUsingStatements() =>
        new List<string>
        {
            "System",
            "System.Collections.Generic"
        }.ToImmutableList();
}