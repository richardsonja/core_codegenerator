﻿using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public class BulkInsertRequestResponseService :
    RequestResponseBaseService,
    IBulkInsertRequestResponseService
{
    public BulkInsertRequestResponseService(IConfigurationService configurationService)
        : base(configurationService)
    {
    }

    protected override string GetClassName(ConfigurationSettings configuration, Table table, bool isRequestObject) =>
        $"{configuration.ConfigurationDetail.CodeRelatedConfiguration.BulkInsertMethodName}{GetRequestResponseSuffix(configuration, isRequestObject)}";

    protected override ImmutableList<PropertyObject> GetProperties(Table table, bool isRequestObject) =>
        isRequestObject
            ? AddRequestInputListOfTableProperties(table)
            : AddRequestInputListOfPrimaryKeysProperties(table, true, AddResponseErrorAndSuccessfulProperties(table));

    protected override bool ShouldExecute(ConfigurationSettings configuration, Table table) =>
        configuration.ConfigurationDetail.OptionsConfiguration.BulkCreate == true;
}