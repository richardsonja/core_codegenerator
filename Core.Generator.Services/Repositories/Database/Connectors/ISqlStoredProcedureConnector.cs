﻿namespace Core.Generator.Services.Repositories.Database.Connectors;

public interface ISqlStoredProcedureConnector
{
    Task<Option<int>> ExecuteNonQueryStoredProcedureAsync<T>(QueryBase<T> query) where T : IDbDataParameter;

    Task<Option<DataTable>> ExecuteStoredProcedureAsync<T>(QueryBase<T> query) where T : IDbDataParameter;
}