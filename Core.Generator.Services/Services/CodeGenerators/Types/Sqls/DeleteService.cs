﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

public class DeleteService : StatementBaseService, IDeleteService
{
    public DeleteService(IConfigurationService configurationService) : base(configurationService)
    {
    }

    protected override IStatement CreateBody(Table table, IEnumerable<StoredProcedureParameter> whereStatementParameters) =>
        new DeleteStatement(table.Schema, table.Name)
            .AddParameter(whereStatementParameters);

    protected override Option<string> ExecuteTableMessage() => Constants.NotFoundPrimaryKeysMessage;

    protected override string Prefix(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.CodeRelatedConfiguration.DestroyMethodName;

    protected override bool ShouldExecute(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.OptionsConfiguration.Delete == true;

    protected override bool ShouldExecute(Table table) =>
        !table.Columns.GetPrimaryKeyColumns().IsNullOrEmpty();
        
    protected override ImmutableList<StoredProcedureParameter> StoredProcedureParameters(Table table) =>
        table.Columns
            .GetPrimaryKeyColumns()
            .Select(x => new StoredProcedureParameter(x))
            .ToImmutableList();
}