﻿using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using FluentAssertions;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Configurations;

public class ConfigurationSettingsExtensionsTests : IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;

    public ConfigurationSettingsExtensionsTests(ConfigurationFixture configuration)
    {
        _configuration = configuration;
    }

    [Theory]
    [InlineData(null, AuthenticationType.MsSqlWindows, true)]
    [InlineData(null, AuthenticationType.NotSelectedYet, false)]
    [InlineData(null, AuthenticationType.MsSqlServerAccount, false)]
    [InlineData("", AuthenticationType.MsSqlWindows, true)]
    [InlineData("", AuthenticationType.NotSelectedYet, false)]
    [InlineData("", AuthenticationType.MsSqlServerAccount, false)]
    [InlineData("WhyNot", AuthenticationType.MsSqlWindows, true)]
    [InlineData("WhyNot", AuthenticationType.NotSelectedYet, true)]
    [InlineData("WhyNot", AuthenticationType.MsSqlServerAccount, true)]
    public void ConfigurationSettingsExtensions_IsDatabasePasswordSet_ShouldMatchExpected_GivenInput(
        string password,
        AuthenticationType authenticationType,
        bool expected)
    {
        _configuration.Reset();
        _configuration.ConfigurationDetail.ServerConfiguration.AuthenticationType = authenticationType;
        ConfigurationSettings settings = new(password, _configuration.ConfigurationDetail);

        var results = settings.IsDatabasePasswordSet();
        results.Should().Be(expected);
    }

    [Fact]
    public void ConfigurationSettingsExtensions_IsDatabasePasswordSet_ShouldReturnFalse_GivenEmptyInput()
    {
        ConfigurationSettings settings = new(null, new ConfigurationDetail());

        var results = settings.IsDatabasePasswordSet();
        results.Should().BeFalse();
    }

    [Fact]
    public void ConfigurationSettingsExtensions_IsDatabasePasswordSet_ShouldReturnFalse_GivenNullInput()
    {
        ConfigurationSettings settings = null;

        var results = settings.IsDatabasePasswordSet();
        results.Should().BeFalse();
    }
}