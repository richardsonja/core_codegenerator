﻿using Core.Generator.Services.DependencyInjection;
using Core.Generator.Services.Repositories;
using Core.Generator.Services.Services.Configurations;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Core.Generator.Services.Tests.DependencyInjection;

public class BuildServicesTests
{
    [Fact]
    public void BuildServices_Build_ShouldBeValid_GivenSetup()
    {
        var serviceProvider = new ServiceCollection().BuildGeneratorServices().BuildServiceProvider(new ServiceProviderOptions()
        {
            ValidateOnBuild = true,
            ValidateScopes = true
        });

        serviceProvider.GetService<IConfigurationService>().Should().BeOfType<ConfigurationService>();
        serviceProvider.GetService<IFileRepository>().Should().BeOfType<FileRepository>();
        serviceProvider.GetService<ISessionService>().Should().BeOfType<SessionService>();
    }
}