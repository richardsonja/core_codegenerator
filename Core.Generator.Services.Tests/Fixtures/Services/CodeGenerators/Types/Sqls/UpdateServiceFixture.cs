﻿using Core.Generator.Services.Services.CodeGenerators.Types.Sqls;
using Core.Generator.Services.Services.Configurations;
using Moq;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Sqls;

public class UpdateServiceFixture : IDisposable, IReset
{
    public Mock<IConfigurationService> ConfigurationService = new();
    public UpdateService Service;

    public UpdateServiceFixture()
    {
        Reset();
        Service = new UpdateService(ConfigurationService.Object);
    }

    public void Dispose()
    {
        Service = null;
        ConfigurationService = null;
    }

    public void Reset()
    {
        ConfigurationService.Reset();
    }
}