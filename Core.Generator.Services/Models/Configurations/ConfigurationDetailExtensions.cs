﻿namespace Core.Generator.Services.Models.Configurations;

public static class ConfigurationDetailExtensions
{
    public static ConfigurationDetail DefaultSettings() =>
        new()
        {
            ServerConfiguration = new ServerConfiguration
            {
                DatabaseServer = "localhost",
                UserName = string.Empty,
                AuthenticationType = AuthenticationType.MsSqlWindows
            },
            CodeRelatedConfiguration = new CodeRelatedConfiguration()
            {
                BulkDeleteMethodName = "BulkDelete",
                BulkDestroyMethodName = "BulkDestroy",
                BulkInsertMethodName = "BulkInsert",
                BulkUpdateMethodName = "BulkUpdate",
                ControllerLayerName = "Controller",
                DataLayerName = "Repository",
                DeleteMethodName = "Delete",
                DestroyMethodName = "Destroy",
                FindMethodName = "Find",
                GetActiveListMethodName = "GetActiveList",
                GetMethodName = "Get",
                InsertMethodName = "Insert",
                Namespace = string.Empty,
                QueryModelName = "Query",
                QueryNamespaceSuffix = "Queries",
                RequestSuffixModelName = "Request",
                ResponseSuffixModelName = "Response",
                ServiceLayerName = "Service",
                UpdateMethodName = "Update",
            },
            OptionsConfiguration = new OptionsConfiguration
            {
                ActionLog = true,
                ActiveList = true,
                BulkCreate = true,
                BulkDelete = true,
                BulkLogicalDelete = true,
                BulkUpdate = true,
                Delete = true,
                Find = true,
                GetByPrimaryKeys = true,
                Insert = true,
                LogicalDelete = true,
                Update = true,
                GenerateStoredProcedures = true
            }
        };

    public static CodeRelatedConfiguration SetCodeRelatedConfiguration(
        this CodeRelatedConfiguration codeRelatedConfiguration,
        CodeRelatedConfiguration defaultConfiguration)
    {
        if (codeRelatedConfiguration == null)
        {
            return defaultConfiguration;
        }

        var clone = (CodeRelatedConfiguration)codeRelatedConfiguration.Clone();
        clone.BulkDeleteMethodName = clone.BulkDeleteMethodName.LoadConfigValue(defaultConfiguration.BulkDeleteMethodName);
        clone.BulkDestroyMethodName = clone.BulkDestroyMethodName.LoadConfigValue(defaultConfiguration.BulkDestroyMethodName);
        clone.BulkInsertMethodName = clone.BulkInsertMethodName.LoadConfigValue(defaultConfiguration.BulkInsertMethodName);
        clone.BulkUpdateMethodName = clone.BulkUpdateMethodName.LoadConfigValue(defaultConfiguration.BulkUpdateMethodName);
        clone.ControllerLayerName = clone.ControllerLayerName.LoadConfigValue(defaultConfiguration.ControllerLayerName);
        clone.DataLayerName = clone.DataLayerName.LoadConfigValue(defaultConfiguration.DataLayerName);
        clone.DeleteMethodName = clone.DeleteMethodName.LoadConfigValue(defaultConfiguration.DeleteMethodName);
        clone.DestroyMethodName = clone.DestroyMethodName.LoadConfigValue(defaultConfiguration.DestroyMethodName);
        clone.FindMethodName = clone.FindMethodName.LoadConfigValue(defaultConfiguration.FindMethodName);
        clone.GetActiveListMethodName = clone.GetActiveListMethodName.LoadConfigValue(defaultConfiguration.GetActiveListMethodName);
        clone.GetMethodName = clone.GetMethodName.LoadConfigValue(defaultConfiguration.GetMethodName);
        clone.InsertMethodName = clone.InsertMethodName.LoadConfigValue(defaultConfiguration.InsertMethodName);
        clone.Namespace = clone.Namespace.LoadConfigValue(defaultConfiguration.Namespace);
        clone.QueryModelName = clone.QueryModelName.LoadConfigValue(defaultConfiguration.QueryModelName);
        clone.QueryNamespaceSuffix = clone.QueryNamespaceSuffix.LoadConfigValue(defaultConfiguration.QueryNamespaceSuffix);
        clone.RequestSuffixModelName = clone.RequestSuffixModelName.LoadConfigValue(defaultConfiguration.RequestSuffixModelName);
        clone.ResponseSuffixModelName = clone.ResponseSuffixModelName.LoadConfigValue(defaultConfiguration.ResponseSuffixModelName);
        clone.ServiceLayerName = clone.ServiceLayerName.LoadConfigValue(defaultConfiguration.ServiceLayerName);
        clone.UpdateMethodName = clone.UpdateMethodName.LoadConfigValue(defaultConfiguration.UpdateMethodName);

        return clone;
    }

    public static ConfigurationDetail SetConfigurationDetail(
        this ConfigurationDetail configuration,
        ConfigurationDetail defaultSettings = null)
    {
        var defaultSetting = (ConfigurationDetail)defaultSettings?.Clone() ?? DefaultSettings();
        var source = (ConfigurationDetail)(configuration?.Clone() ?? defaultSetting.Clone());
        return new ConfigurationDetail
        {
            ServerConfiguration = source.ServerConfiguration.SetServerConfiguration(defaultSetting.ServerConfiguration),
            CodeRelatedConfiguration = source.CodeRelatedConfiguration.SetCodeRelatedConfiguration(defaultSetting.CodeRelatedConfiguration),
            OptionsConfiguration = source.OptionsConfiguration.SetOptionsConfiguration(defaultSetting.OptionsConfiguration)
        };
    }

    public static OptionsConfiguration SetOptionsConfiguration(
        this OptionsConfiguration optionsConfiguration,
        OptionsConfiguration defaultConfiguration)
    {
        if (optionsConfiguration == null)
        {
            return defaultConfiguration;
        }

        var clone = (OptionsConfiguration)optionsConfiguration.Clone();
        clone.ActionLog = clone.ActionLog.LoadConfigValue(defaultConfiguration.ActionLog);
        clone.ActiveList = clone.ActiveList.LoadConfigValue(defaultConfiguration.ActiveList);
        clone.BulkCreate = clone.BulkCreate.LoadConfigValue(defaultConfiguration.BulkCreate);
        clone.BulkDelete = clone.BulkDelete.LoadConfigValue(defaultConfiguration.BulkDelete);
        clone.BulkLogicalDelete = clone.BulkLogicalDelete.LoadConfigValue(defaultConfiguration.BulkLogicalDelete);
        clone.BulkUpdate = clone.BulkUpdate.LoadConfigValue(defaultConfiguration.BulkUpdate);
        clone.Delete = clone.Delete.LoadConfigValue(defaultConfiguration.Delete);
        clone.Find = clone.Find.LoadConfigValue(defaultConfiguration.Find);
        clone.GetByPrimaryKeys = clone.GetByPrimaryKeys.LoadConfigValue(defaultConfiguration.GetByPrimaryKeys);
        clone.Insert = clone.Insert.LoadConfigValue(defaultConfiguration.Insert);
        clone.LogicalDelete = clone.LogicalDelete.LoadConfigValue(defaultConfiguration.LogicalDelete);
        clone.Update = clone.Update.LoadConfigValue(defaultConfiguration.Update);
        clone.GenerateStoredProcedures = clone.GenerateStoredProcedures.LoadConfigValue(defaultConfiguration.GenerateStoredProcedures);

        return clone;
    }

    public static ServerConfiguration SetServerConfiguration(
        this ServerConfiguration serverConfiguration,
        ServerConfiguration defaultConfiguration)
    {
        if (serverConfiguration == null)
        {
            return defaultConfiguration;
        }

        var clone = (ServerConfiguration)serverConfiguration.Clone();
        clone.DatabaseServer = clone.DatabaseServer.LoadConfigValue(defaultConfiguration.DatabaseServer);
        clone.AuthenticationType =
            clone.AuthenticationType.LoadConfigValue(AuthenticationType.NotSelectedYet, defaultConfiguration.AuthenticationType);

        return clone;
    }

    private static string LoadConfigValue(this string value, string defaultValue) =>
        value.IsNullOrEmpty()
            ? defaultValue
            : value.Trim();

    private static bool? LoadConfigValue(this bool? value, bool? defaultValue) => value ?? defaultValue;

    private static T LoadConfigValue<T>(this T value, T matchingValue, T defaultValue) =>
        value == null || value.Equals(matchingValue)
            ? defaultValue
            : value;
}