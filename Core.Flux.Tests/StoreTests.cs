﻿namespace Core.Flux.Tests;

[Collection("Serial")]
public class StoreTests : IClassFixture<StoreFixture>
{
    private readonly StoreFixture _store;

    public StoreTests(StoreFixture store) => _store = store;

    [Fact]
    public void Store_EmitChange_ShouldTriggerDispatcher()
    {
        _store.Store.EmitChange();

        _store.Dispatcher.Verify(x => x.Dispatch(It.IsAny<ChangePayload>()), Times.Once);
    }
}