﻿using Core.Generator.Services.Models.Sqls;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Sqls;

public class UpdateStatementFixture : IDisposable, IReset
{
    public UpdateStatement UpdateStatement;

    public UpdateStatementFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        UpdateStatement = null;
    }

    public void Reset()
    {
        UpdateStatement = new UpdateStatement("Bob", "Was Here");
    }
}