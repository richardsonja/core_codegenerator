﻿using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using FluentAssertions;
using System;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Databases;

public class DatabaseExtensionsTests : IClassFixture<DatabaseTableColumnFixture>
{
    private readonly DatabaseTableColumnFixture _databaseTableColumn;

    public DatabaseExtensionsTests(DatabaseTableColumnFixture databaseTableColumn)
    {
        _databaseTableColumn = databaseTableColumn;
    }

    [Fact]
    public void DatabaseExtensions_CovertToTableAndColumns_ShouldEmptyDatabaseTableList_GivenEmptyTableColumns()
    {
        _databaseTableColumn.SetDatabase("Bob");
        _databaseTableColumn.SetEmptyTableColumns();

        var results = _databaseTableColumn.Database.CovertToTableAndColumns(_databaseTableColumn.TableColumns);
        results.Should().NotBeNull();
        results.Tables.Should().BeEmpty();
    }

    [Fact]
    public void DatabaseExtensions_CovertToTableAndColumns_ShouldEmptyDatabaseTableList_GivenNullTableColumns()
    {
        _databaseTableColumn.SetDatabase("Bob");
        _databaseTableColumn.SetNullTableColumns();

        var results = _databaseTableColumn.Database.CovertToTableAndColumns(_databaseTableColumn.TableColumns);
        results.Should().NotBeNull();
        results.Tables.Should().BeEmpty();
    }

    [Fact]
    public void DatabaseExtensions_CovertToTableAndColumns_ShouldHaveValidTableAndColumns_GivenDatabaseWithExistingTableANdColumn()
    {
        _databaseTableColumn.SetDatabase("Bob", "Bob was", "here", "now gone he is");
        _databaseTableColumn.SetTableColumns("Bob was", "here", "Bob");
        _databaseTableColumn.Database.Tables.Should().HaveCount(1);
        _databaseTableColumn.Database.Tables.FirstOrDefault().Columns.Should().HaveCount(1);

        var results = _databaseTableColumn.Database.CovertToTableAndColumns(_databaseTableColumn.TableColumns);
        results.Should().NotBeNull();
        results.Tables.Should().HaveCount(1);

        var firstTable = results.Tables.FirstOrDefault();
        firstTable.Should().NotBeNull();
        firstTable.Columns.Should().HaveCount(3);
    }

    [Fact]
    public void DatabaseExtensions_CovertToTableAndColumns_ShouldHaveValidTableAndColumns_GivenNewDatabaseObject()
    {
        _databaseTableColumn.SetDatabase("Bob");
        _databaseTableColumn.SetTableColumns("Bob was", "here", "Bob");
        _databaseTableColumn.Database.Tables.Should().BeEmpty();

        var results = _databaseTableColumn.Database.CovertToTableAndColumns(_databaseTableColumn.TableColumns);
        results.Should().NotBeNull();
        results.Tables.Should().HaveCount(1);

        var firstTable = results.Tables.FirstOrDefault();
        firstTable.Should().NotBeNull();
        firstTable.Columns.Should().HaveCount(2);
    }

    [Fact]
    public void DatabaseExtensions_CovertToTableAndColumns_ShouldThrowException_GivenNullDatabase()
    {
        _databaseTableColumn.SetNullDatabase();

        Action action = () => _databaseTableColumn.Database.CovertToTableAndColumns(_databaseTableColumn.TableColumns);
        action.Should().Throw<ArgumentNullException>();
    }
}