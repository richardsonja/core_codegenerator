﻿using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public class BulkDestroyRequestResponseService :
    RequestResponseBaseService,
    IBulkDestroyRequestResponseService
{
    public BulkDestroyRequestResponseService(IConfigurationService configurationService)
        : base(configurationService)
    {
    }

    protected override string GetClassName(ConfigurationSettings configuration, Table table, bool isRequestObject) =>
        $"{configuration.ConfigurationDetail.CodeRelatedConfiguration.BulkDestroyMethodName}{GetRequestResponseSuffix(configuration, isRequestObject)}";

    protected override ImmutableList<PropertyObject> GetProperties(Table table, bool isRequestObject) =>
        isRequestObject
            ? AddRequestInputListOfPrimaryKeysProperties(table)
            : AddResponseErrorAndSuccessfulProperties(table);

    protected override bool ShouldExecute(ConfigurationSettings configuration, Table table) =>
        configuration.ConfigurationDetail.OptionsConfiguration.BulkDelete == true;
}