﻿namespace Core.Generator.Services.Models.Configurations;

public static class ServerConfigurationExtensions
{
    public static ServerType GetServerType(this ServerConfiguration config) =>
        config.AuthenticationType switch
        {
            AuthenticationType.MsSqlWindows => ServerType.MicrosoftSql,
            AuthenticationType.MsSqlServerAccount => ServerType.MicrosoftSql,
            AuthenticationType.MySqlServerAccount => ServerType.MySqlSql,
            _ => ServerType.Unknown
        };

    public static bool IsLoginBasedConnection(this ServerConfiguration config) =>
        config?.AuthenticationType != AuthenticationType.MsSqlWindows;
}