﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

public class SelectActiveListService : SelectService, ISelectActiveListService
{
    public SelectActiveListService(IConfigurationService configurationService) : base(configurationService)
    {
    }

    protected override Option<string> ExecuteTableMessage() => Constants.NotFoundActiveColumnsMessage;

    protected override string Prefix(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.CodeRelatedConfiguration.GetActiveListMethodName;

    protected override bool ShouldExecute(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.OptionsConfiguration.ActiveList == true;

    protected override bool ShouldExecute(Table table) =>
        !table.Columns.GetIsActiveColumns().IsNullOrEmpty();

    protected override ImmutableList<StoredProcedureParameter> StoredProcedureParameters(Table table) =>
        null;

    protected override ImmutableList<StoredProcedureParameter> WhereStatementParameters(Table table) =>
        table.Columns
            .GetIsActiveColumns()
            .Select(x => new StoredProcedureParameter(x)
                .SetIsWhereStatement()
                .SetOverriddenWhereStatementValue(x.GetDefaultSqlValue()))
            .ToImmutableList();
}