﻿using Core.Flux;
using Core.Flux.Extensions;
using Core.Generator.Stores.Database;
using Core.Generator.Stores.Database.Actions;
using Core.Generator.Stores.Tests.Fixtures;
using FluentAssertions;
using LanguageExt;
using Moq;
using System.Collections.Generic;
using System.Collections.Immutable;
using Xunit;

namespace Core.Generator.Stores.Tests.Stores;

public class DatabaseStoreTests : IFluxViewFor<DatabaseStore>, IClassFixture<DatabaseStoreFixture>
{
    private readonly DatabaseStoreFixture _databaseStore;

    public DatabaseStoreTests(DatabaseStoreFixture databaseStore)
    {
        _databaseStore = databaseStore;
    }

    [Fact]
    public void DatabaseStore_LoadDatabasesAction_ShouldHaveError_GivenServiceReturningNone()
    {
        _databaseStore.Reset();
        _databaseStore.DatabaseService
            .Setup(x => x.GetAsync())
            .ReturnsAsync(Option<ImmutableList<Services.Models.Databases.Database>>.None);
        this.Dispatch(new LoadDatabasesAction());

        this.OnChange(
            t =>
            {
                t.Exceptions.Should().NotBeEmpty();
                t.Databases.Should().BeNull();
            });
    }

    [Fact]
    public void DatabaseStore_LoadDatabasesAction_ShouldHaveSome_GivenSetup()
    {
        _databaseStore.Reset();
        _databaseStore.DatabaseService
            .Setup(x => x.GetAsync())
            .ReturnsAsync(new List<Services.Models.Databases.Database>().ToImmutableList());

        this.Dispatch(new LoadDatabasesAction());

        this.OnChange(
            t =>
            {
                t.Exceptions.Should().BeEmpty();
                t.Databases.Should().NotBeNull();
            });
    }
}