﻿using System.Security.Cryptography;

namespace Core.Common.Collections;

public static class EnumerableExtensions
{
    public static IEnumerable<T> AddTo<T>(this IEnumerable<T> list, T value) where T : ICloneable
    {
        var clone = list.Clone().ToList();
        clone.Add(value);
        return clone;
    }

    public static IEnumerable<T> AddTo<T>(this IEnumerable<T> list, IEnumerable<T> values) where T : ICloneable
    {
        var clone = list.Clone().ToList();
        clone.AddRange(values.Clone().ToList());
        return clone;
    }

    public static IEnumerable<T> Clone<T>(this IEnumerable<T> source) where T : ICloneable =>
        source?.Select(item => (T)item?.Clone())?.ToList();

    public static IEnumerable<T> DistinctByCondition<T, TU>(this IEnumerable<T> source, Func<T, TU> selector) where TU : IComparable<TU>
    {
        if (source == null)
        {
            throw new ArgumentNullException(nameof(source));
        }

        if (selector == null)
        {
            throw new ArgumentNullException(nameof(selector));
        }

        return source
            .GroupBy(selector)
            .Select(x => x.First());
    }

    public static IEnumerable<T> Duplicates<T, TU>(this IEnumerable<T> source, Func<T, TU> selector) where TU : IComparable<TU> =>
        source.Duplicates(selector, 1);

    public static IEnumerable<T> Duplicates<T, TU>(this IEnumerable<T> source, Func<T, TU> selector, int moreThanCount)
        where TU : IComparable<TU>
    {
        if (source == null)
        {
            throw new ArgumentNullException(nameof(source));
        }

        if (selector == null)
        {
            throw new ArgumentNullException(nameof(selector));
        }

        if (moreThanCount < 1)
        {
            throw new ArgumentNullException("The count should be greater than 0");
        }

        return source.GroupBy(selector).Where(x => x.Count() > moreThanCount).Select(x => x.First());
    }

    public static bool IsNullOrEmpty<T>(this IEnumerable<T> source) => source?.Any() != true;

    public static T MaxObject<T, TU>(this IEnumerable<T> source, Func<T, TU> selector) where TU : IComparable<TU> =>
        MinOrMaxObject(source, selector, false);

    public static T MinObject<T, TU>(this IEnumerable<T> source, Func<T, TU> selector) where TU : IComparable<TU> =>
        MinOrMaxObject(source, selector, true);

    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source) where T : ICloneable
    {
        var list = source?.Clone()?.ToList();
        if (list.IsNullOrEmpty())
        {
            return new List<T>();
        }

        using var provider = new RNGCryptoServiceProvider();
        var n = list.Count;
        while (n > 1)
        {
            var box = new byte[1];
            do
            {
                provider.GetBytes(box);
            }
            while (!(box[0] < n * (byte.MaxValue / n)));

            var k = box[0] % n;
            n--;

            (list[k], list[n]) = (list[n], list[k]);
        }

        return list;
    }

    private static T MinOrMaxObject<T, TU>(this IEnumerable<T> source, Func<T, TU> selector, bool minSelector) where TU : IComparable<TU>
    {
        var fullList = source.ToList();
        if (fullList.IsNullOrEmpty())
        {
            throw new ArgumentNullException(nameof(source));
        }

        if (selector == null)
        {
            throw new ArgumentNullException(nameof(selector));
        }

        var firstElement = fullList.First();
        if (fullList.Count == 1)
        {
            return firstElement;
        }

        var maxObj = firstElement;
        var maxKey = selector(firstElement);

        var enumerable = fullList.Skip(1).ToList();
        foreach (var item in enumerable!)
        {
            var currentKey = selector(item);
            if (minSelector && currentKey.CompareTo(maxKey) < 0
                || !minSelector && currentKey.CompareTo(maxKey) > 0)
            {
                maxKey = currentKey;
                maxObj = item;
            }
        }

        return maxObj;
    }
}