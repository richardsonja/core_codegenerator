﻿namespace Core.Generator.Services.Models.Sqls;

public class DeleteStatement : ICloneable, IStatement
{
    public DeleteStatement(string schema, string name)
    {
        Schema = schema.IsNullOrEmpty() ? "dbo" : schema;
        Name = name;
        TableAbbreviation = name.CreateAbbreviation();
    }

    private DeleteStatement(
        string schema,
        string name,
        bool removeBrackets,
        ImmutableList<StoredProcedureParameter> whereStatementParameters)
        : this(schema, name)
    {
        RemoveBrackets = removeBrackets;
        WhereStatementParameters = whereStatementParameters.Clone().ToImmutableList();
    }

    public string Name { get; }

    public bool RemoveBrackets { get; private set; }

    public string Schema { get; }

    public ImmutableList<StoredProcedureParameter> WhereStatementParameters { get; private set; } = new List<StoredProcedureParameter>().ToImmutableList();

    public string TableAbbreviation { get; }

    public IStatement AddParameter(StoredProcedureParameter parameter)
    {
        if (parameter == null)
        {
            return this;
        }
        var clone = parameter.Clone() as StoredProcedureParameter;
        SetIsWhereStatement(clone);
        WhereStatementParameters = WhereStatementParameters.AddTo(clone).ToImmutableList();

        return this;
    }

    public IStatement AddParameter(IEnumerable<StoredProcedureParameter> parameter)
    {
        if (parameter.IsNullOrEmpty())
        {
            return this;
        }

        var clone = parameter.Clone().Select(SetIsWhereStatement).ToList();
        WhereStatementParameters = WhereStatementParameters.AddTo(clone).ToImmutableList();

        return this;
    }

    public object Clone() => new DeleteStatement(Schema, Name, RemoveBrackets, WhereStatementParameters);

    public DeleteStatement SetRemoveBrackets(bool removeBrackets = true)
    {
        RemoveBrackets = removeBrackets;
        WhereStatementParameters = WhereStatementParameters
            .Clone()
            .Select(x=> x.SetRemoveBrackets(RemoveBrackets))
            .ToImmutableList();
        return this;
    }

    public override string ToString() => $"{Environment.NewLine}DELETE{Environment.NewLine}{ToFromStatement()}{Environment.NewLine}{ToWhereStatement()}";

    private static StoredProcedureParameter SetIsWhereStatement(StoredProcedureParameter parameter) => parameter.SetIsWhereStatement(true);

    private string AddBrackets(string value) => 
        value.FormatBracketedValue(RemoveBrackets);

    private string ToFromStatement() =>
        $"FROM {ToFullName()} as {TableAbbreviation}";

    private string ToFullName() =>
        $"{AddBrackets(Schema)}.{AddBrackets(Name)}";

    private string ToWhereStatement() =>
        WhereStatementParameters.ToWhereStatement();
}