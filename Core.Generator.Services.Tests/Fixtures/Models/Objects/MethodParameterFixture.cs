﻿using Core.Generator.Services.Models.Objects;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Objects;

public class MethodParameterFixture : IDisposable, IReset
{
    public MethodParameter MethodParameter;

    public MethodParameterFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        MethodParameter = null;
    }

    public void Reset()
    {
        MethodParameter = Create("Bob", null, 0);
    }

    public MethodParameter Create(string type, string name, int order)
    {
        MethodParameter = new MethodParameter(type, name, order);
        return MethodParameter;
    }

    public MethodParameter Create(string name, int order)
    {
        MethodParameter = new MethodParameter(name, order);
        return MethodParameter;
    }
}