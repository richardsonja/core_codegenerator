﻿namespace Core.Generator.Stores.CodeGenerator.Actions;

public class LoadGeneratorCodeAction
{
    public LoadGeneratorCodeAction(Table table)
    {
        Table = table;
    }

    public Table Table { get; set; }
}