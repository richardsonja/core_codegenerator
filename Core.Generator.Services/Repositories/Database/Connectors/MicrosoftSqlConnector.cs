﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Services.Configurations;
using System.Data.SqlClient;

namespace Core.Generator.Services.Repositories.Database.Connectors;

public class MicrosoftSqlConnector : IDisposable, IMicrosoftSqlConnector
{
    private readonly IConfigurationService _configurationService;
    private SqlConnection _connection;
    private bool _disposed;

    public MicrosoftSqlConnector(IConfigurationService configurationService)
    {
        _configurationService = configurationService;
    }

    public void Dispose()
    {
        Dispose(true);

        GC.SuppressFinalize(this);
    }

    public async Task<Option<int>> ExecuteNonQuerySqlAsync<T>(QueryBase<T> query) where T : IDbDataParameter
    {
        if (query.IsEmpty())
        {
            return Option<int>.None;
        }

        try
        {
            await LoadConnection();

            await using var command = query.CreateCommand(_connection);
            await command.ExecuteNonQueryAsync();
        }
        finally
        {
            await _connection.CloseConnection();
        }

        return 1;
    }

    public async Task<Option<int>> ExecuteNonQueryStoredProcedureAsync<T>(QueryBase<T> query) where T : IDbDataParameter
    {
        if (query.IsEmpty())
        {
            return Option<int>.None;
        }

        try
        {
            await LoadConnection();

            await using var command = query.CreateCommand(_connection);
            command.CommandType = CommandType.StoredProcedure;
            await command.ExecuteScalarAsync();
        }
        finally
        {
            await _connection.CloseConnection();
        }

        return 1;
    }

    public async Task<Option<DataTable>> ExecuteSqlAsync<T>(QueryBase<T> query) where T : IDbDataParameter
    {
        if (query.IsEmpty())
        {
            return Option<DataTable>.None;
        }

        var results = new DataTable();
        try
        {
            await LoadConnection();

            using var command = query.CreateDataAdapter(_connection);

            command.Fill(results);
        }
        finally
        {
            await _connection.CloseConnection();
        }

        return results;
    }

    public async Task<Option<DataTable>> ExecuteStoredProcedureAsync<T>(QueryBase<T> query) where T : IDbDataParameter
    {
        if (query.IsEmpty())
        {
            return Option<DataTable>.None;
        }

        var results = new DataTable();
        try
        {
            await LoadConnection();

            using var command = query.CreateDataAdapter(_connection);
            command.SelectCommand.CommandType = CommandType.StoredProcedure;

            command.Fill(results);
        }
        finally
        {
            await _connection.CloseConnection();
        }

        return results;
    }

    protected virtual void Dispose(bool disposing)
    {
        lock (this)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _connection?.Dispose();
            }

            // Remember that the object has been disposed of.
            _disposed = true;
        }
    }

    private static SqlConnectionStringBuilder GetPasswordBasedConnectionString(ConfigurationSettings settings) =>
        new()
        {
            DataSource = settings.ConfigurationDetail.ServerConfiguration.DatabaseServer,
            UserID = settings.ConfigurationDetail.ServerConfiguration.UserName,
            Password = settings.DatabasePassword,
            InitialCatalog = Constants.MicrosoftSqlServerMasterDatabaseName
        };

    private static SqlConnectionStringBuilder GetWindowsAuthenticationConnectionString(ConfigurationSettings settings) =>
        new()
        {
            DataSource = settings.ConfigurationDetail.ServerConfiguration.DatabaseServer,
            InitialCatalog = Constants.MicrosoftSqlServerMasterDatabaseName,
            IntegratedSecurity = true
        };

    private async Task<string> GetConnectionString()
    {
        var settings = await _configurationService.GetAsync().Match(s => s, () => null);

        if (settings is null)
        {
            throw new ArgumentNullException("Configuration Settings are missing");
        }

        var builder = settings.ConfigurationDetail.ServerConfiguration.IsLoginBasedConnection()
            ? GetPasswordBasedConnectionString(settings)
            : GetWindowsAuthenticationConnectionString(settings);

        return builder.ConnectionString;
    }

    private async Task LoadConnection()
    {
        _connection ??= new SqlConnection(await GetConnectionString());
        await _connection.OpenAsync();
    }
}