﻿namespace Core.Generator.Stores.Storage.Actions;

public class SaveConfigurationSettingsAction
{
    public SaveConfigurationSettingsAction(ConfigurationSettings configurationSettings)
    {
        ConfigurationSettings = configurationSettings;
    }

    public ConfigurationSettings ConfigurationSettings { get; }
}