﻿namespace Core.Generator.Stores.Database;

public class DatabaseStore : Store, IActionException
{
    private readonly IDatabaseService _databaseService;

    public DatabaseStore(IDatabaseService databaseService)
    {
        _databaseService = databaseService;

        Dispatcher.Register<LoadDatabasesAction>(LoadDatabasesAndTables);
    }

    public ImmutableList<Services.Models.Databases.Database> Databases { get; private set; }

    public List<ActionException> Exceptions { get; private set; }

    public override bool IsLoaded => Exceptions.IsNullOrEmpty() || Databases is not null;

    private async void LoadDatabasesAndTables(LoadDatabasesAction action)
    {
        Exceptions = new();
        var resultOption = await _databaseService.GetAsync();

        resultOption.IfNone(() => Exceptions.Add(new ActionException(nameof(LoadDatabasesAction))));
        resultOption.IfSome(x => Databases = x);

        EmitChange();
    }
}