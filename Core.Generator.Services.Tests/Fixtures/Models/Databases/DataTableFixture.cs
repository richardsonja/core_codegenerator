﻿using Core.Generator.Services.Models.Databases;
using System;
using System.Data;
using System.Linq;

namespace Core.Generator.Services.Tests.Fixtures.Models.Databases;

public class DataTableFixture : IDisposable, IReset
{
    public DataTable DataTable;

    public DataTableFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        DataTable = null;
    }

    public DataRow GetDataRow() => DataTable.AsEnumerable().First();

    public void Reset()
    {
        DataTable = new DataTable();
        DataTable.Clear();
    }

    public void SetDatabase()
    {
        DataTable = new DataTable();
        DataTable.Clear();
        DataTable.Columns.Add(DatabaseColumnConstants.DatabaseId);
        DataTable.Columns.Add(DatabaseColumnConstants.DatabaseName);
        var dataRow = DataTable.NewRow();
        dataRow[DatabaseColumnConstants.DatabaseId] = "11";
        dataRow[DatabaseColumnConstants.DatabaseName] = "SomeTable";
        DataTable.Rows.Add(dataRow);
    }

    public void SetTableColumn()
    {
        DataTable = new DataTable();
        DataTable.Clear();
        DataTable.Columns.Add(DatabaseColumnConstants.TableDatabaseName);
        DataTable.Columns.Add(DatabaseColumnConstants.TableName);
        DataTable.Columns.Add(DatabaseColumnConstants.TableSchema);
        DataTable.Columns.Add(DatabaseColumnConstants.ColumnName);
        DataTable.Columns.Add(DatabaseColumnConstants.OrdinalPosition);
        DataTable.Columns.Add(DatabaseColumnConstants.DataType);
        DataTable.Columns.Add(DatabaseColumnConstants.NumericPrecision);
        DataTable.Columns.Add(DatabaseColumnConstants.NumericScale);
        DataTable.Columns.Add(DatabaseColumnConstants.IsNullable);
        DataTable.Columns.Add(DatabaseColumnConstants.CharacterMaximumLength);
        DataTable.Columns.Add(DatabaseColumnConstants.IsIdentity);
        DataTable.Columns.Add(DatabaseColumnConstants.IsIndex);
        DataTable.Columns.Add(DatabaseColumnConstants.ForeignTable);
        DataTable.Columns.Add(DatabaseColumnConstants.ForeignColumn);
        DataTable.Columns.Add(DatabaseColumnConstants.ForeignSchema);

        var dataRow = DataTable.NewRow();
        dataRow[DatabaseColumnConstants.TableDatabaseName] = "Testing1";
        dataRow[DatabaseColumnConstants.TableName] = "Testing2";
        dataRow[DatabaseColumnConstants.TableSchema] = "Testing3";
        dataRow[DatabaseColumnConstants.ColumnName] = "TestingSomething4";
        dataRow[DatabaseColumnConstants.OrdinalPosition] = "5";
        dataRow[DatabaseColumnConstants.DataType] = "NVarChar";
        dataRow[DatabaseColumnConstants.NumericPrecision] = "7";
        dataRow[DatabaseColumnConstants.NumericScale] = "8";
        dataRow[DatabaseColumnConstants.IsNullable] = "0";
        dataRow[DatabaseColumnConstants.CharacterMaximumLength] = "10";
        dataRow[DatabaseColumnConstants.IsIdentity] = "1";
        dataRow[DatabaseColumnConstants.IsIndex] = "1";
        dataRow[DatabaseColumnConstants.ForeignTable] = "Testing13";
        dataRow[DatabaseColumnConstants.ForeignColumn] = "Testing14";
        dataRow[DatabaseColumnConstants.ForeignSchema] = "Testing15";

        DataTable.Rows.Add(dataRow);
    }

    public void SetServerVersion()
    {
        DataTable = new DataTable();
        DataTable.Clear();
        DataTable.Columns.Add(DatabaseColumnConstants.ServerVersion);
        var dataRow = DataTable.NewRow();
        dataRow[DatabaseColumnConstants.ServerVersion] = "1.1.1";
        DataTable.Rows.Add(dataRow);
    }
}