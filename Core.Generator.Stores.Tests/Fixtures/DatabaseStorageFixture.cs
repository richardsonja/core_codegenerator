﻿using Core.Flux;
using Core.Generator.Services.Services.Databases;
using Core.Generator.Stores.Database;
using Moq;
using System;

namespace Core.Generator.Stores.Tests.Fixtures;

public class DatabaseStoreFixture : IDisposable, IReset
{
    public Mock<IDatabaseService> DatabaseService;
    public DatabaseStore Store;

    public DatabaseStoreFixture()
    {
        DatabaseService = new Mock<IDatabaseService>();

        Fluxs.Dispatcher = new AsyncDispatcher();
        Reset();
    }

    public void Dispose()
    {
        Store = null;
        DatabaseService = null;
    }

    public void Reset()
    {
        DatabaseService.Reset();
        Store = new DatabaseStore(DatabaseService.Object);
        Fluxs.StoreResolver.Register(Store);
    }
}