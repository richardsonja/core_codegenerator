﻿namespace Core.Generator.Services.Services.Databases;

public interface IDatabaseService
{
    Task<Option<ImmutableList<Database>>> GetAsync();
}