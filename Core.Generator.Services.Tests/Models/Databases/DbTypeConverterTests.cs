﻿using Core.Generator.Services.Models.Databases;
using FluentAssertions;
using System.Data;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Databases;

public class DbTypeConverterTests
{
    [Theory]
    [InlineData("bigint", SqlDbType.BigInt)]
    [InlineData("binary", SqlDbType.Binary)]
    [InlineData("bit", SqlDbType.Bit)]
    [InlineData("bool", SqlDbType.TinyInt)]
    [InlineData("boolean", SqlDbType.TinyInt)]
    [InlineData("char", SqlDbType.Char)]
    [InlineData("char2", SqlDbType.Char)]
    [InlineData("date", SqlDbType.Date)]
    [InlineData("datetime", SqlDbType.DateTime)]
    [InlineData("datetime2", SqlDbType.DateTime2)]
    [InlineData("datetimeoffset", SqlDbType.DateTimeOffset)]
    [InlineData("dec", SqlDbType.Decimal)]
    [InlineData("decimal", SqlDbType.Decimal)]
    [InlineData("decmial", SqlDbType.Decimal)]
    [InlineData("double", SqlDbType.Float)]
    [InlineData("float", SqlDbType.Float)]
    [InlineData("int", SqlDbType.Int)]
    [InlineData("integer", SqlDbType.Int)]
    [InlineData("money", SqlDbType.Money)]
    [InlineData("nchar", SqlDbType.NChar)]
    [InlineData("ntext", SqlDbType.NText)]
    [InlineData("numeric", SqlDbType.Decimal)]
    [InlineData("nvarchar", SqlDbType.NVarChar)]
    [InlineData("nvarchar2", SqlDbType.NVarChar)]
    [InlineData("real", SqlDbType.Real)]
    [InlineData("smalldatetime", SqlDbType.SmallDateTime)]
    [InlineData("smallint", SqlDbType.SmallInt)]
    [InlineData("smallmoney", SqlDbType.SmallMoney)]
    [InlineData("structured", SqlDbType.Structured)]
    [InlineData("text", SqlDbType.Text)]
    [InlineData("time", SqlDbType.Time)]
    [InlineData("timestamp", SqlDbType.Timestamp)]
    [InlineData("tinyint", SqlDbType.TinyInt)]
    [InlineData("udt", SqlDbType.Udt)]
    [InlineData("uniqueidentifier", SqlDbType.UniqueIdentifier)]
    [InlineData("varchar", SqlDbType.VarChar)]
    [InlineData("varchar2", SqlDbType.VarChar)]
    [InlineData("variant", SqlDbType.Variant)]
    [InlineData("xml", SqlDbType.Xml)]
    [InlineData("bob", SqlDbType.Variant)]
    public void DbTypeConverter_GetSqlDbType_ShouldMatchExpect_GivenInput(
        string value,
        SqlDbType expected)
    {
        var results = value.GetSqlDbType();

        results.Should().NotBeNull();
        results.Should().Be(expected);
    }
}