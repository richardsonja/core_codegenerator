﻿namespace Core.Generator.Services.Models.Databases;

public class TableColumn : ICloneable, IEquatable<TableColumn>, IBaseColumnRow
{
    public string CamelCaseName { get; set; }

    public int? CharacterMaxLength { get; set; }

    public CSharpType CSharpType { get; set; }

    public string DatabaseName { get; set; }

    public SqlDbType DataType { get; set; }

    public string ForeignColumn { get; set; }

    public string ForeignSchema { get; set; }

    public string ForeignTable { get; set; }

    public string HumanReadableFormattedName { get; set; }

    public bool IsForeignKey => !string.IsNullOrEmpty(ForeignColumn);

    public bool IsIdentity { get; set; }

    public bool IsIdentityOrIndex => IsIdentity || IsIndex;

    public bool IsIndex { get; set; }

    public bool IsNullable { get; set; }

    public string Name { get; set; }

    public int OrdinalPosition { get; set; }

    public string OriginalDataTypeString { get; set; }

    public string PascalCaseName { get; set; }

    public int? Percision { get; set; }

    public int? Scale { get; set; }

    public SystemColumns SystemColumns { get; set; }

    public string TableName { get; set; }

    public string TableSchema { get; set; }

    public object Clone() =>
        new TableColumn
        {
            DatabaseName = DatabaseName,
            TableName = TableName,
            TableSchema = TableSchema,
            CamelCaseName = CamelCaseName,
            CharacterMaxLength = CharacterMaxLength,
            CSharpType = CSharpType?.Clone() as CSharpType,
            DataType = DataType,
            ForeignColumn = ForeignColumn,
            ForeignSchema = ForeignSchema,
            ForeignTable = ForeignTable,
            HumanReadableFormattedName = HumanReadableFormattedName,
            IsIdentity = IsIdentity,
            IsIndex = IsIndex,
            IsNullable = IsNullable,
            Name = Name,
            OrdinalPosition = OrdinalPosition,
            OriginalDataTypeString = OriginalDataTypeString,
            PascalCaseName = PascalCaseName,
            Percision = Percision,
            Scale = Scale,
            SystemColumns = SystemColumns?.Clone() as SystemColumns
        };

    public bool Equals(TableColumn other) =>
        other != null
        && Name.Equals(other.Name, StringComparison.InvariantCultureIgnoreCase)
        && CSharpType.TypeString.Equals(other.CSharpType.TypeString, StringComparison.InvariantCultureIgnoreCase);

    public override bool Equals(object obj) => obj is TableColumn other && Equals(other);

    public override int GetHashCode() => HashCode.Combine(CSharpType, Name);
}