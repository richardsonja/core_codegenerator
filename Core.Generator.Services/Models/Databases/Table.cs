﻿namespace Core.Generator.Services.Models.Databases;

public class Table : ICloneable
{
    public Table()
    {
    }

    public Table(string name, string schema) => (Name, Schema) = (name, schema);

    public string CamelCaseFormattedName => FormattedName.CamelCase();

    public ImmutableList<TableColumn> Columns { get; set; }

    public string FormattedName =>
                Name.Equals(Schema, StringComparison.InvariantCultureIgnoreCase)
            ? Name + "s"
            : Name;

    public string Name { get; set; }

    public string Schema { get; set; }

    public object Clone() =>
        new Table
        {
            Name = Name,
            Schema = Schema,
            Columns = Columns?.Clone().ToImmutableList()
        };
}