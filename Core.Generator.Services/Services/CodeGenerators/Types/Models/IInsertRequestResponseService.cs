﻿namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public interface IInsertRequestResponseService : ITypeService<bool>
{
}