﻿using Core.Generator.Services.Services.CodeGenerators.Types.Models;
using Core.Generator.Services.Services.Configurations;
using Moq;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Models;

public class ModelQueryServiceFixture : IDisposable, IReset
{
    public Mock<IConfigurationService> ConfigurationService = new();
    public ModelQueryService Service;

    public ModelQueryServiceFixture()
    {
        Reset();
        Service = new ModelQueryService(ConfigurationService.Object);
    }

    public void Dispose()
    {
        Service = null;
        ConfigurationService = null;
    }

    public void Reset()
    {
        ConfigurationService.Reset();
    }
}