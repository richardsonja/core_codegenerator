﻿namespace Core.Generator.Services.Models.CodeGenerators;

public class DisplayResult : ICloneable
{
    public DisplayResult(int displayOrder, string name, string displayText) =>
        (DisplayOrder, Name, DisplayText, DisplayResults, IsEmpty) =
        (displayOrder, name, displayText, new List<DisplayResult>().ToImmutableList(), false);

    public DisplayResult(int displayOrder, string name, IEnumerable<DisplayResult> displayResults) =>
        (DisplayOrder, Name, DisplayResults, IsEmpty) =
        (displayOrder, name, displayResults?.Clone().ToImmutableList(), false);

    public DisplayResult() => IsEmpty = true;

    private DisplayResult(int displayOrder, string name, string displayText, IEnumerable<DisplayResult> displayResults, bool isEmpty) =>
        (DisplayOrder, Name, DisplayText, DisplayResults, IsEmpty) =
        (displayOrder, name, displayText, displayResults?.Clone().ToImmutableList(), isEmpty);

    public int DisplayOrder { get; private set; }

    public ImmutableList<DisplayResult> DisplayResults { get; private set; }

    public string DisplayText { get; }

    public bool IsEmpty { get; }

    public string Name { get; }

    public DisplayResult AddDisplayResult(DisplayResult displayResult)
    {
        DisplayResults = DisplayResults.AddTo(displayResult).ToImmutableList();

        return this;
    }

    public DisplayResult AddDisplayResult(IEnumerable<DisplayResult> displayResults)
    {
        DisplayResults = DisplayResults.AddTo(displayResults).ToImmutableList();

        return this;
    }

    public object Clone() => new DisplayResult(DisplayOrder, Name, DisplayText, DisplayResults, IsEmpty);

    public DisplayResult OrderList()
    {
        if (DisplayResults.IsNullOrEmpty())
        {
            return this;
        }

        DisplayResults = DisplayResults?
            .Select(x => x?.OrderList()).OrderList() 
                         ?? new List<DisplayResult>().ToImmutableList();

        return this;
    }

    public DisplayResult ResetDisplayOrder(int displayOrder)
    {
        DisplayOrder = displayOrder;
        return this;
    }
}