﻿global using Core.Common;
global using Core.Common.Collections;
global using Core.Common.Generics;
global using Core.Generator.Services.Models.Configurations;
global using Core.Generator.Services.Models.Databases;
global using System.Collections.Immutable;
global using LanguageExt;
global using System.Data;