﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.Files;
using Core.Generator.Services.Repositories;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Core.Generator.Services.Services.Configurations;

public class ConfigurationService : IConfigurationService
{
    private readonly string _directoryPath;
    private readonly string _fileName;
    private readonly IFileRepository _fileRepository;
    private readonly JsonSerializerOptions _options;
    private readonly ISessionService _sessionService;

    public ConfigurationService(IFileRepository fileRepository, ISessionService sessionService, string directoryPath, string fileName)
    {
        _fileRepository = fileRepository;
        _sessionService = sessionService;
        _directoryPath = directoryPath;
        _fileName = fileName;

        _options = new JsonSerializerOptions
        {
            WriteIndented = true,
            Converters =
            {
                new JsonStringEnumConverter(JsonNamingPolicy.CamelCase)
            }
        };
    }

    public async Task<Option<ConfigurationSettings>> GetAsync()
    {
        return await _sessionService
            .Get<string>(Constants.PasswordSessionName)
            .MatchAsync(
                LoadConfigurationSettings,
                () => LoadConfigurationSettings(null));
    }

    public async Task<Option<ConfigurationDetail>> SaveConfigurationDetailAsync(ConfigurationDetail configuration)
    {
        var jsonString = JsonSerializer.Serialize(configuration, _options);
        var fileDetails = new FileDetails(jsonString, _fileName, _directoryPath);

        return await _fileRepository.SaveAsync(fileDetails)
            .Match(
                _ => (ConfigurationDetail)configuration.Clone(),
                () => Option<ConfigurationDetail>.None);
    }

    private async Task<Option<ConfigurationSettings>> LoadConfigurationSettings(string password) =>
        await RetrieveAsync()
            .Match(
                configurationDetail => new ConfigurationSettings(password, configurationDetail),
                () => Option<ConfigurationSettings>.None);

    private async Task<Option<ConfigurationDetail>> RetrieveAsync() =>
        await _fileRepository.GetAsync(new FileDetails(_fileName, _directoryPath))
            .MatchAsync(
                s => JsonSerializer.Deserialize<ConfigurationDetail>(s.FileString, _options).SetConfigurationDetail(),
                () => SaveConfigurationDetailAsync(new ConfigurationDetail().SetConfigurationDetail()));
}