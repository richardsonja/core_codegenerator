﻿using AutoMapper;

namespace Core.Generator.Services.Models.Databases;

public class DatabaseMappingProfile : Profile
{
    public DatabaseMappingProfile()
    {
        CreateMap<DataTable, ServerVersion>()
            .ForMember(
                dest => dest.FullVersion,
                opt => opt.MapFrom(src => src.AsEnumerable().FirstOrDefault()[DatabaseColumnConstants.ServerVersion]));

        CreateMap<DataRow, Database>()
            .ForMember(
                dest => dest.Id,
                opt => opt.MapFrom(src => ParseInt(src[DatabaseColumnConstants.DatabaseId])))
            .ForMember(
                dest => dest.Name,
                opt => opt.MapFrom(src => src[DatabaseColumnConstants.DatabaseName]))
            .ForMember(
                dest => dest.HasPermission,
                opt => opt.Ignore())
            .ForMember(
                dest => dest.Tables,
                opt => opt.Ignore());

        CreateMap<DataRow, TableColumn>()
            .ForMember(
                dest => dest.SystemColumns,
                opt => opt.MapFrom(src => src))
            .ForMember(
                dest => dest.CSharpType,
                opt => opt.MapFrom(src => src))
            .ForMember(
                dest => dest.CamelCaseName,
                opt => opt.MapFrom(src => ParseCamelCase(src[DatabaseColumnConstants.ColumnName])))
            .ForMember(
                dest => dest.HumanReadableFormattedName,
                opt => opt.MapFrom(src => ParseHumanReadableFormattedName(src[DatabaseColumnConstants.ColumnName])))
            .ForMember(
                dest => dest.PascalCaseName,
                opt => opt.MapFrom(src => ParsePascalCase(src[DatabaseColumnConstants.ColumnName])))
            .ForMember(
                dest => dest.CharacterMaxLength,
                opt => opt.MapFrom(src => ParseIntNullable(src[DatabaseColumnConstants.CharacterMaximumLength])))
            .ForMember(
                dest => dest.DataType,
                opt => opt.MapFrom(src => ParseSqlDbType(src[DatabaseColumnConstants.DataType])))
            .ForMember(
                dest => dest.ForeignColumn,
                opt => opt.MapFrom(src => src[DatabaseColumnConstants.ForeignColumn]))
            .ForMember(
                dest => dest.ForeignSchema,
                opt => opt.MapFrom(src => src[DatabaseColumnConstants.ForeignSchema]))
            .ForMember(
                dest => dest.ForeignTable,
                opt => opt.MapFrom(src => src[DatabaseColumnConstants.ForeignTable]))
            .ForMember(
                dest => dest.IsIdentity,
                opt => opt.MapFrom(src => ParseBoolean(src[DatabaseColumnConstants.IsIdentity])))
            .ForMember(
                dest => dest.IsIndex,
                opt => opt.MapFrom(src => ParseBoolean(src[DatabaseColumnConstants.IsIndex])))
            .ForMember(
                dest => dest.IsNullable,
                opt => opt.MapFrom(src => ParseBoolean(src[DatabaseColumnConstants.IsNullable])))
            .ForMember(
                dest => dest.Name,
                opt => opt.MapFrom(src => src[DatabaseColumnConstants.ColumnName]))
            .ForMember(
                dest => dest.OrdinalPosition,
                opt => opt.MapFrom(src => ParseInt(src[DatabaseColumnConstants.OrdinalPosition])))
            .ForMember(
                dest => dest.OriginalDataTypeString,
                opt => opt.MapFrom(src => src[DatabaseColumnConstants.DataType]))
            .ForMember(
                dest => dest.Percision,
                opt => opt.MapFrom(src => ParseIntNullable(src[DatabaseColumnConstants.NumericPrecision])))
            .ForMember(
                dest => dest.Scale,
                opt => opt.MapFrom(src => ParseIntNullable(src[DatabaseColumnConstants.NumericScale])))
            .ForMember(
                dest => dest.DatabaseName,
                opt => opt.MapFrom(src => src[DatabaseColumnConstants.TableDatabaseName]))
            .ForMember(
                dest => dest.TableName,
                opt => opt.MapFrom(src => src[DatabaseColumnConstants.TableName]))
            .ForMember(
                dest => dest.TableSchema,
                opt => opt.MapFrom(src => src[DatabaseColumnConstants.TableSchema]));

        CreateMap<DataRow, SystemColumns>()
            .ForMember(
                dest => dest.IsActive,
                opt => opt.MapFrom(src => ParseIsActive(src[DatabaseColumnConstants.ColumnName])))
            .ForMember(
                dest => dest.IsCreateBy,
                opt => opt.MapFrom(src => ParseIsCreateBy(src[DatabaseColumnConstants.ColumnName])))
            .ForMember(
                dest => dest.IsCreateDate,
                opt => opt.MapFrom(src => ParseIsCreateDate(src[DatabaseColumnConstants.ColumnName])))
            .ForMember(
                dest => dest.IsModifyBy,
                opt => opt.MapFrom(src => ParseIsModifyBy(src[DatabaseColumnConstants.ColumnName])))
            .ForMember(
                dest => dest.IsModifyDate,
                opt => opt.MapFrom(src => ParseIsModifyDate(src[DatabaseColumnConstants.ColumnName])))
            .ForMember(
                dest => dest.IsPermanentName,
                opt => opt.MapFrom(src => ParseIsPermanentName(src[DatabaseColumnConstants.ColumnName])))
            .ForMember(
                dest => dest.IsTimeStamp,
                opt => opt.MapFrom(src => ParseIsTimeStamp(src[DatabaseColumnConstants.ColumnName])));

        CreateMap<DataRow, CSharpType>()
            .ForMember(
                dest => dest.Type,
                opt => opt.MapFrom(src => ParseCSharpType(src)))
            .ForMember(
                dest => dest.TypeString,
                opt => opt.MapFrom(src => ParseCSharpTypeString(src)));
    }

    private static bool ParseBoolean(object value) => value.ToBoolean();

    private static bool? ParseBooleanNullable(object value) => value.ToBooleanNullable();

    private static string ParseCamelCase(object value) => value.ToString().CamelCase();

    private static Type ParseCSharpType(DataRow src)
    {
        var isNullable = ParseBoolean(src[DatabaseColumnConstants.IsNullable]);
        var dataType = ParseSqlDbType(src[DatabaseColumnConstants.DataType]);

        return dataType.ToNetType(isNullable);
    }

    private static string ParseCSharpTypeString(DataRow src)
    {
        var isNullable = ParseBoolean(src[DatabaseColumnConstants.IsNullable]);
        var dataType = ParseSqlDbType(src[DatabaseColumnConstants.DataType]);

        return dataType.ToStringFormat(isNullable);
    }

    private static string ParseHumanReadableFormattedName(object value) =>
        value.ToString().FormatStringWithSpacesAndProperCapitalization();

    private static int ParseInt(object value) => value.ToInt();

    private static int? ParseIntNullable(object value) => value.ToIntNullable();

    private static bool ParseIsActive(object value) => value.IsActive();

    private static bool ParseIsCreateBy(object value) => value.IsCreateBy();

    private static bool ParseIsCreateDate(object value) => value.IsCreateDate();

    private static bool ParseIsModifyBy(object value) => value.IsModifyBy();

    private static bool ParseIsModifyDate(object value) => value.IsModifyDate();

    private static bool ParseIsPermanentName(object value) => value.IsPermanentName();

    private static bool ParseIsTimeStamp(object value) => value.IsTimeStamp();

    private static string ParsePascalCase(object value) => value.ToString().PascalCase();

    private static SqlDbType ParseSqlDbType(object value) => value.GetSqlDbType();
}