﻿using Core.Generator.Services.Models.Sqls;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Sqls;

public class InsertStatementFixture : IDisposable, IReset
{
    public InsertStatement InsertStatement;

    public InsertStatementFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        InsertStatement = null;
    }

    public void Reset()
    {
        InsertStatement = new InsertStatement("Bob", "Was Here");
    }
}