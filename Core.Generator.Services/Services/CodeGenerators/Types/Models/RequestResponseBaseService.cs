﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public abstract class RequestResponseBaseService
{
    protected readonly IConfigurationService ConfigurationService;

    protected RequestResponseBaseService(IConfigurationService configurationService)
    {
        ConfigurationService = configurationService;
    }

    public async Task<Option<string>> CreateOutputAsync(
        CodeGeneratorRequest request,
        bool isRequestObject)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<string>.None;
        }

        var configOption = await ConfigurationService.GetAsync();
        return configOption.Match(
            x => CreateClass(x, request.Table, isRequestObject),
            () => Option<string>.None);
    }

    protected virtual ImmutableList<PropertyObject> AddRequestInputListOfPrimaryKeysProperties(
        Table table,
        bool isList = true,
        IEnumerable<PropertyObject> properties = null)
    {
        properties ??= new List<PropertyObject>();
        return properties.AddTo(table.Columns
                .GetPrimaryKeyColumns()
                .Select(
                    x => new PropertyObject($"{x.PascalCaseName}{(isList ? "s": string.Empty)}")
                        .SetType(new CSharpType(null, isList ? $"IEnumerable<{x.CSharpType.TypeString}>" : x.CSharpType.TypeString))
                        .AddGetterSetter(GetterSetterType.Get)
                        .AddGetterSetter(GetterSetterType.Set)))
            .ToImmutableList();
    }

    protected virtual ImmutableList<PropertyObject> AddRequestInputListOfTableProperties(
        Table table,
        bool isList = true,
        IEnumerable<PropertyObject> properties = null)
    {
        properties ??= new List<PropertyObject>();
        return properties.AddTo(new PropertyObject($"{table.FormattedName}{(isList ? "s": string.Empty)}")
                .SetType(new CSharpType(null, isList ? $"IEnumerable<{table.FormattedName}>" : table.FormattedName))
                .AddGetterSetter(GetterSetterType.Get)
                .AddGetterSetter(GetterSetterType.Set))
            .ToImmutableList();
    }

    protected virtual ImmutableList<PropertyObject> AddResponseErrorAndSuccessfulProperties(
        Table table,
        IEnumerable<PropertyObject> properties = null)
    {
        properties ??= new List<PropertyObject>();
        return properties
            .AddTo(new PropertyObject($"IsSuccessful")
                .AddGetterSetter(GetterSetterType.Get)
                .AddGetterSetter(GetterSetterType.Set)
                .SetType(typeof(bool)))
            .AddTo(
                new PropertyObject($"Errors")
                    .SetType(new CSharpType(null, $"IEnumerable<string>"))
                    .AddGetterSetter(GetterSetterType.Get)
                    .AddGetterSetter(GetterSetterType.Set))
            .ToImmutableList();
    }

    protected Option<string> CreateClass(ConfigurationSettings configuration, Table table, bool isRequestObject)
    {
        if (!ShouldExecute(configuration, table))
        {
            return Option<string>.None;
        }

        var properties = GetProperties(table, isRequestObject);
        if (properties.IsNullOrEmpty())
        {
            return Option<string>.None;
        }

        var classObject = new ClassObject(
                GetClassName(configuration, table, isRequestObject),
                GetNamespace(configuration, table))
            .AddUsingStatements(GetUsingStatements())
            .AddProperties(properties);

        return classObject.ToString();
    }

    protected abstract string GetClassName(ConfigurationSettings configuration, Table table, bool isRequestObject);

    protected abstract ImmutableList<PropertyObject> GetProperties(Table table, bool isRequestObject);

    protected string GetRequestResponseSuffix(ConfigurationSettings configuration, bool isRequestObject) =>
        isRequestObject
            ? configuration.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName
            : configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName;

    protected abstract bool ShouldExecute(ConfigurationSettings configuration, Table table);

    private static string GetNamespace(ConfigurationSettings configuration, Table table) =>
        $"{configuration.ConfigurationDetail.CodeRelatedConfiguration.Namespace}.Models.{table.Schema}";

    private static ImmutableList<string> GetUsingStatements() =>
        new List<string>
        {
            "System",
            "System.Collections.Generic"
        }.ToImmutableList();
}