﻿using Core.Generator.Services.Repositories;
using Core.Generator.Services.Services.Configurations;
using Moq;
using System;
using System.IO;

namespace Core.Generator.Services.Tests.Fixtures.Services;

public class ConfigurationServiceFixture : IDisposable, IReset
{
    public Mock<IFileRepository> FileRepository = new();
    public ConfigurationService Service;
    public Mock<ISessionService> SessionService = new();

    public ConfigurationServiceFixture()
    {
        Reset();
        var directory = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            "CodeGenerator_UnitTests");
        const string fileName = "TestSettings.json";
        Service = new ConfigurationService(FileRepository.Object, SessionService.Object, directory, fileName);
    }

    public void Dispose()
    {
        Service = null;
        FileRepository = null;
        SessionService = null;
    }

    public void Reset()
    {
        FileRepository.Reset();
        SessionService.Reset();
    }
}