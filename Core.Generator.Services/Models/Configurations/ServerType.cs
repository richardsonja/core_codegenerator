﻿namespace Core.Generator.Services.Models.Configurations;

public enum ServerType
{
    Unknown = 0,
    MicrosoftSql = 1,
    MySqlSql = 2
}