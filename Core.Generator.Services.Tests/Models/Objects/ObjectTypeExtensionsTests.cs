﻿using Core.Generator.Services.Models.Objects;
using FluentAssertions;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Objects;

public class ObjectTypeExtensionsTests
{
    [Theory]
    [InlineData(ObjectType.Class, "class")]
    [InlineData(ObjectType.Interface, "interface")]
    [InlineData(ObjectType.Enum, "enum")]
    public void ObjectTypeExtensions_CreateOutput_ShouldMatchExpected_GivenInput(ObjectType value, string expected) =>
        value.CreateOutput().Should().Be(expected);
}