﻿namespace Core.Flux;

public interface IAsyncDispatcher : IDispatcher
{
    SynchronizationContext SynchronizationContext { get; set; }
}