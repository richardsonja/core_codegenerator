﻿namespace Core.Generator.Services.Models.Files;

public class FileDetails : ICloneable
{
    public FileDetails(string fileString, string fileName, string directoryPath) : this(fileName, directoryPath)
    {
        FileString = fileString;
    }

    public FileDetails(string fileName, string directoryPath)
    {
        FilePathAndName = Path.Combine(directoryPath, fileName);
        FileName = fileName;
        DirectoryPath = directoryPath;
    }

    public string DirectoryPath { get; }

    public string FileName { get; }

    public string FilePathAndName { get; }

    public string FileString { get; }

    public object Clone() => new FileDetails(FileString, FileName, DirectoryPath);
}