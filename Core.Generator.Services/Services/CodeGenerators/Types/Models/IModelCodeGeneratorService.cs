﻿using Core.Generator.Services.Models.CodeGenerators;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public interface IModelCodeGeneratorService : ICodeGeneratorServiceType<ImmutableList<DisplayResult>>
{
}