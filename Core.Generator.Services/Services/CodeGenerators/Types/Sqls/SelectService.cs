﻿using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

public class SelectService : StatementBaseService, ISelectService
{
    public SelectService(IConfigurationService configurationService) : base(configurationService)
    {
    }

    protected override IStatement CreateBody(Table table, IEnumerable<StoredProcedureParameter> whereStatementParameters) =>
        new SelectStatement(table.Schema, table.Name)
            .AddSelectStatement(SelectValues(table))
            .AddParameter(whereStatementParameters);

    protected override string Prefix(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.CodeRelatedConfiguration.GetMethodName;

    protected virtual ImmutableList<string> SelectValues(Table table) =>
        table.Columns
            .Select(x => x.Name)
            .ToImmutableList();
}