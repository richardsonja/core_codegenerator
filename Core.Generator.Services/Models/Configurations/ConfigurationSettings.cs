﻿namespace Core.Generator.Services.Models.Configurations;

public class ConfigurationSettings : ICloneable
{
    public ConfigurationSettings(string databasePassword, ConfigurationDetail configurationDetail) =>
        (DatabasePassword, ConfigurationDetail) = (databasePassword, configurationDetail);

    public ConfigurationDetail ConfigurationDetail { get; }

    public string DatabasePassword { get; }

    public object Clone() => new ConfigurationSettings(DatabasePassword, ConfigurationDetail.Clone() as ConfigurationDetail);
}