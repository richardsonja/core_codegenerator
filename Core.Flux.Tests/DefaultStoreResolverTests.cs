﻿using Core.Flux.Exceptions;
using Core.Flux.Tests.Mocks;

namespace Core.Flux.Tests;

[Collection("Serial")]
public class DefaultStoreResolverTests : IClassFixture<DefaultStoreResolverFixture>
{
    private readonly DefaultStoreResolverFixture _storeResolver;

    public DefaultStoreResolverTests(DefaultStoreResolverFixture storeResolver) => 
        _storeResolver = storeResolver;

    [Fact]
    public void DefaultStoreResolver_GetStore_ShouldReturnLastRegisteredStore()
    {
        _storeResolver.Reset();
        var store1 = new TestStore();
        var store2 = new TestStore();

        _storeResolver.StoreResolver.Register(store1);
        _storeResolver.StoreResolver.Register(store2);

        var result = _storeResolver.StoreResolver.GetStore<TestStore>();

        result.Should().BeSameAs(store2);
    }

    [Fact]
    public void DefaultStoreResolver_GetStore_ShouldReturnRegisteredStore()
    {
        _storeResolver.Reset();
        var store = new TestStore();

        _storeResolver.StoreResolver.Register(store);

        var result = _storeResolver.StoreResolver.GetStore<TestStore>();
        result.Should().BeSameAs(store);
    }

    [Fact]
    public void DefaultStoreResolver_GetStore_ShouldThrowStoreNotRegisteredException_GivenUnregisteredStore()
    {
        _storeResolver.Reset();
        Action action = () => _storeResolver.StoreResolver.GetStore<TestStore>();
        action.Should().Throw<NullStoreException>();
    }

    [Fact]
    public void DefaultStoreResolver_Register_ShouldNotThrowException()
    {
        _storeResolver.Reset();
        var store = new TestStore();
        Action action = () => _storeResolver.StoreResolver.Register(store);
        action.Should().NotThrow();
    }
}