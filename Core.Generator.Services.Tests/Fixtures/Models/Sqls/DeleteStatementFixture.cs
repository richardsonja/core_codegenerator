﻿using Core.Generator.Services.Models.Sqls;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Sqls;

public class DeleteStatementFixture : IDisposable, IReset
{
    public DeleteStatement DeleteStatement;

    public DeleteStatementFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        DeleteStatement = null;
    }

    public void Reset()
    {
        DeleteStatement = new DeleteStatement("Bob", "Was Here");
    }
}