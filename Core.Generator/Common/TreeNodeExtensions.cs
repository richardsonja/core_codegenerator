﻿namespace Core.Generator.Common;

public static class TreeNodeExtensions
{
    public static TreeNode ConvertDatabaseToTreeNode(this Database database)
    {
        if (database.Tables.IsNullOrEmpty())
        {
            return new TreeNode(database.Name);
        }

        var childrenNodes = database.Tables
            .OrderBy(x => x.Schema)
            .ThenBy(x => x.Name)
            .Select(x => new TreeNode($"{x.Schema}.{x.Name}", 1, 1))
            .ToList();

        return new TreeNode(database.Name, childrenNodes.ToArray());
    }

    public static ImmutableList<TreeNode> ConvertToTreeNodes(this ImmutableList<Database> databases) =>
        !databases.IsNullOrEmpty()
            ? databases.OrderBy(x => x.Name).Select(ConvertDatabaseToTreeNode).ToImmutableList()
            : new List<TreeNode>().ToImmutableList();
}