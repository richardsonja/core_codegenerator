﻿namespace Core.Generator.Services.Models.Configurations;

public class ServerConfiguration : ICloneable, IEquatable<ServerConfiguration>
{
    public AuthenticationType AuthenticationType { get; set; } = AuthenticationType.NotSelectedYet;

    public string DatabaseServer { get; set; }

    public string UserName { get; set; }

    public object Clone() =>
        new ServerConfiguration
        {
            DatabaseServer = DatabaseServer,
            AuthenticationType = AuthenticationType,
            UserName = UserName
        };

    public bool Equals(ServerConfiguration other)
    {
        if (ReferenceEquals(null, other))
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return AuthenticationType == other.AuthenticationType && DatabaseServer == other.DatabaseServer && UserName == other.UserName;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }

        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj.GetType() == this.GetType() && Equals((ServerConfiguration)obj);
    }

    public override int GetHashCode() => HashCode.Combine((int)AuthenticationType, DatabaseServer, UserName);
}