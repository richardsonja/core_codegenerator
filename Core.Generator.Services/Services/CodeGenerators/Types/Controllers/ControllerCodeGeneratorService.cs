﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Controllers;

public class ControllerCodeGeneratorService : IControllerCodeGeneratorService
{
    private readonly IConfigurationService _configurationService;
    private readonly IWebApiControllerService _webApiControllerService;

    public ControllerCodeGeneratorService(
        IConfigurationService configurationService,
        IWebApiControllerService webApiControllerService)
    {
        _configurationService = configurationService;
        _webApiControllerService = webApiControllerService;
    }

    public async Task<Option<ImmutableList<DisplayResult>>> CreateOutputAsync(CodeGeneratorRequest request)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<ImmutableList<DisplayResult>>.None;
        }

        var configOption = await _configurationService.GetAsync();
        var displayResults = await configOption
            .MatchAsync(
                x => CreateTabAsync(x, request),
                () => new List<DisplayResult>().ToImmutableList());

        return displayResults.IsNullOrEmpty()
            ? Option<ImmutableList<DisplayResult>>.None
            : displayResults.ToImmutableList();
    }

    private async Task<ImmutableList<DisplayResult>> CreateTabAsync(
        ConfigurationSettings configuration,
        CodeGeneratorRequest request)
    {
        var counter = 1;
        var webApiController = await _webApiControllerService
            .CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabWebApiController, x),
                () => new DisplayResult());

        return webApiController.ToFullImmutableList();
    }
}