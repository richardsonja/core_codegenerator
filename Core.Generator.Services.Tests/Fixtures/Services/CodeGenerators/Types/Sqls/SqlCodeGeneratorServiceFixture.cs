﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Services.CodeGenerators.Types.Sqls;
using Core.Generator.Services.Services.Configurations;
using Moq;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Sqls;

public class SqlCodeGeneratorServiceFixture : IDisposable, IReset
{
    public Mock<IConfigurationService> ConfigurationService = new();
    public Mock<ISelectService> SelectService = new ();
    public Mock<ISelectByPrimaryKeysService> SelectByPrimaryKeysService = new ();
    public Mock<ISelectActiveListService> SelectActiveListService = new ();
    public Mock<ISelectFindService> SelectFindService = new ();
    public Mock<IDeleteService> DeleteService = new ();
    public Mock<IUpdateService> UpdateService = new ();
    public SqlCodeGeneratorService Service;

    public SqlCodeGeneratorServiceFixture()
    {
        Reset();
        Service = new SqlCodeGeneratorService(
            ConfigurationService.Object,
            SelectService.Object,
            SelectByPrimaryKeysService.Object,
            SelectActiveListService.Object,
            SelectFindService.Object,
            DeleteService.Object,
            UpdateService.Object);
    }

    public void Dispose()
    {
        Service = null;
        ConfigurationService = null;
        SelectService = null;
        SelectByPrimaryKeysService = null;
        SelectActiveListService = null;
        SelectFindService = null;
        DeleteService = null;
        UpdateService = null;
    }

    public void HardReset()
    {
        ConfigurationService.Reset();
        SelectService.Reset();
        SelectByPrimaryKeysService.Reset();
        SelectActiveListService.Reset();
        SelectFindService.Reset();
        DeleteService.Reset();
        UpdateService.Reset();
    }

    public void Reset()
    {
        HardReset();

        SelectService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("SelectService's BOB WAS HERE");

        SelectByPrimaryKeysService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("SelectByPrimaryKeysService's BOB WAS HERE");

        SelectActiveListService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("SelectActiveListService's BOB WAS HERE");

        SelectFindService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("SelectFindService's BOB WAS HERE");

        DeleteService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("DeleteService's BOB WAS HERE");

        UpdateService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("UpdateService's BOB WAS HERE");
    }
}