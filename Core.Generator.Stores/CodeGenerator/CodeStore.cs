﻿namespace Core.Generator.Stores.CodeGenerator;

public class CodeStore : Store, IActionException
{
    private readonly ICodeGeneratorService _codeGeneratorService;

    public CodeStore(ICodeGeneratorService codeGeneratorService)
    {
        _codeGeneratorService = codeGeneratorService;
        Dispatcher.Register<LoadGeneratorCodeAction>(LoadGeneratedCode);
    }

    public CodeGeneratorResult CodeGeneratorResult { get; private set; }

    public List<ActionException> Exceptions { get; private set; }

    public override bool IsLoaded => Exceptions.IsNullOrEmpty() || CodeGeneratorResult is not null;

    private async void LoadGeneratedCode(LoadGeneratorCodeAction action)
    {
        Exceptions = new();
        var results = await _codeGeneratorService
            .CreateOutputAsync(new CodeGeneratorRequest(action?.Table))
            .Match(x => x, () => null);

        if (results == null)
        {
            Exceptions.Add(new ActionException(nameof(_codeGeneratorService.CreateOutputAsync)));
        }
        else
        {
            CodeGeneratorResult = results;
        }

        EmitChange();
    }
}