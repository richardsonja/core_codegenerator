﻿namespace Core.Generator.Stores.Tests.Fixtures;

public interface IReset
{
    void Reset();
}