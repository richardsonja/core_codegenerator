﻿using Core.Generator.Services.Repositories;
using Core.Generator.Services.Repositories.Database;
using Core.Generator.Services.Repositories.Database.Connectors;
using Core.Generator.Services.Services.CodeGenerators;
using Core.Generator.Services.Services.CodeGenerators.Types.Controllers;
using Core.Generator.Services.Services.CodeGenerators.Types.Models;
using Core.Generator.Services.Services.CodeGenerators.Types.Sqls;
using Core.Generator.Services.Services.Configurations;
using Core.Generator.Services.Services.Databases;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Generator.Services.DependencyInjection;

public static class BuildServices
{
    public static IServiceCollection BuildGeneratorServices(this IServiceCollection serviceCollection) =>
        serviceCollection
            .AddMappers()
            .AddServices()
            .AddRepositories();

    private static IServiceCollection AddMappers(this IServiceCollection serviceCollection) =>
        serviceCollection
            .AddAutoMapper(typeof(DatabaseMappingProfile));

    private static IServiceCollection AddRepositories(this IServiceCollection serviceCollection) =>
        serviceCollection
            .AddTransient<IFileRepository, FileRepository>()
            .AddTransient<IMicrosoftSqlConnector, MicrosoftSqlConnector>()
            .AddTransient<IMySqlConnector, MySqlConnector>()
            .AddTransient<IMicrosoftSqlRepository, MicrosoftSqlRepository>()
            .AddTransient<IMySqlRepository, MySqlRepository>();

    private static IServiceCollection AddServices(this IServiceCollection serviceCollection) =>
            serviceCollection
            .AddTransient<IConfigurationService>(
                x => new ConfigurationService(
                    x.GetRequiredService<IFileRepository>(),
                    x.GetRequiredService<ISessionService>(),
                    GetFIleDirectory(),
                    GetFileName()))
            .AddTransient<ISessionService, SessionService>()
            .AddTransient<IDatabaseService, DatabaseService>()

            // code generator services
            .AddTransient<IModelService, ModelService>()
            .AddTransient<IModelExtensionsService, ModelExtensionsService>()
            .AddTransient<IModelQueryService, ModelQueryService>()
            .AddTransient<IBulkDeleteRequestResponseService, BulkDeleteRequestResponseService>()
            .AddTransient<IBulkDestroyRequestResponseService, BulkDestroyRequestResponseService>()
            .AddTransient<IBulkInsertRequestResponseService, BulkInsertRequestResponseService>()
            .AddTransient<IBulkUpdateRequestResponseService, BulkUpdateRequestResponseService>()
            .AddTransient<IDeleteRequestResponseService, DeleteRequestResponseService>()
            .AddTransient<IDestroyRequestResponseService, DestroyRequestResponseService>()
            .AddTransient<IInsertRequestResponseService, InsertRequestResponseService>()
            .AddTransient<IUpdateRequestResponseService, UpdateRequestResponseService>()
            .AddTransient<IGetRequestResponseService, GetRequestResponseService>()
            .AddTransient<ISelectService, SelectService>()
            .AddTransient<ISelectByPrimaryKeysService, SelectByPrimaryKeysService>()
            .AddTransient<ISelectActiveListService, SelectActiveListService>()
            .AddTransient<ISelectFindService, SelectFindService>()
            .AddTransient<IDeleteService, DeleteService>()
            .AddTransient<IUpdateService, UpdateService>()
            .AddTransient<IInsertService, InsertService>()
            .AddTransient<ICodeGeneratorService, CodeGeneratorService>()
            .AddTransient<IModelCodeGeneratorService, ModelCodeGeneratorService>()
            .AddTransient<IControllerCodeGeneratorService, ControllerCodeGeneratorService>()
            .AddTransient<ISqlCodeGeneratorService, SqlCodeGeneratorService>()
            .AddTransient<IWebApiControllerService, WebApiControllerService>();

    private static string GetFIleDirectory() =>
        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CodeGenerator");

    private static string GetFileName() => "Settings.json";
}