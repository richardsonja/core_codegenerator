﻿namespace Core.Flux;

public abstract class Store : IStore
{
    protected Store() => Dispatcher = Fluxs.Dispatcher;

    public IDispatcher Dispatcher { get; }

    public virtual bool IsLoaded => false;

    public void EmitChange() => Dispatcher.Dispatch(new ChangePayload());
}