﻿namespace Core.Generator.Services.Services.Configurations;

public interface ISessionService
{
    Option<T> Get<T>(string name);

    Option<bool> Save(string name, object value);
}