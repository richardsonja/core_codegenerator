﻿namespace Core.Flux;

public interface IDispatcher
{
    void Clear();

    void Dispatch<T>(T payload);

    void Register<T>(Action<T> callback);
}