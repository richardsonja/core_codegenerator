﻿namespace Core.Generator.Services.Models.Configurations;

public static class ConfigurationSettingsExtensions
{
    public static bool IsDatabasePasswordSet(this ConfigurationSettings settings) =>
        settings != null
        && (settings.DatabasePassword.IsNullOrEmpty() == false
            || settings.ConfigurationDetail?.ServerConfiguration?.AuthenticationType == AuthenticationType.MsSqlWindows);
}