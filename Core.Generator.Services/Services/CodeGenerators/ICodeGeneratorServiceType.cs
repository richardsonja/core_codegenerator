﻿using Core.Generator.Services.Models.CodeGenerators;

namespace Core.Generator.Services.Services.CodeGenerators;

public interface ICodeGeneratorServiceType<T>
{
    Task<Option<T>> CreateOutputAsync(CodeGeneratorRequest request);
}