﻿namespace Core.Generator.Services.Models.Databases;

public class QueryBase<T> where T : IDbDataParameter
{
    public QueryBase()
    {
    }

    public QueryBase(string sqlOrStoredProcedureName, IEnumerable<T> parameters) =>
        (Sql, Parameters) =
        (sqlOrStoredProcedureName, parameters.ToImmutableList());

    public ImmutableList<T> Parameters { get; protected set; }

    public string Sql { get; protected set; }
}