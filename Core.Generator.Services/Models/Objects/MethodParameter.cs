﻿namespace Core.Generator.Services.Models.Objects;

public class MethodParameter : ICloneable
{
    public MethodParameter(string type, string name, int order = 0) =>
        (Type, Name, Order) = (type, name, order);

    public MethodParameter(string name, int order = 0) : this(null, name, order)
    {
    }

    private MethodParameter(string type, string name, int order, IEnumerable<Method> attributes) : this(type, name, order) => 
        Attributes = attributes?.Clone().ToImmutableList();

    public ImmutableList<Method> Attributes { get; private set; } = new List<Method>().ToImmutableList();

    public string Name { get; }

    public int Order { get; }

    public string Type { get; }

    public MethodParameter AddAttributes(Method parameter)
    {
        Attributes ??= new List<Method>().ToImmutableList();
        Attributes = Attributes.AddTo(parameter).ToImmutableList();

        return this;
    }

    public MethodParameter AddAttributes(IEnumerable<Method> parameters)
    {
        Attributes ??= new List<Method>().ToImmutableList();
        Attributes = Attributes.AddTo(parameters).ToImmutableList();

        return this;
    }

    public object Clone() => new MethodParameter(Type, Name, Order, Attributes);

    public override string ToString() => $"{ToAttribute()} {ToBody()}"?.Trim();

    private string ToAttribute() => Attributes.ToAttribute();

    private string ToBody() =>
        Type.IsNullOrEmpty()
            ? Name
            : $"{Type} {Name}";
}