﻿using Core.Generator.Services.Models.CodeGenerators;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

public interface ISqlCodeGeneratorService : ICodeGeneratorServiceType<ImmutableList<DisplayResult>>
{
}