﻿using Core.Generator.Services.Models.CodeGenerators;

namespace Core.Generator.Services.Services.CodeGenerators.Types;

public interface ITypeService
{
    Task<Option<string>> CreateOutputAsync(CodeGeneratorRequest request);
}
public interface ITypeService<in T>
{
    Task<Option<string>> CreateOutputAsync(CodeGeneratorRequest request, T extraOption);
}