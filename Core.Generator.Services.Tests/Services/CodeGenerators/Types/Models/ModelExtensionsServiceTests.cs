﻿using Core.Common.Generics;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Models;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Models;

public class ModelExtensionsServiceTests : IClassFixture<ModelExtensionsServiceFixture>, IClassFixture<DatabaseTableColumnFixture>, IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly ModelExtensionsServiceFixture _service;

    public ModelExtensionsServiceTests(ModelExtensionsServiceFixture service, DatabaseTableColumnFixture databaseTableColumn, ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }

    [Fact]
    public async void ModelExtensionsService_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelExtensionsService_CreateOutput_ShouldCreateOutput_GivenModifyInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        var counter = 1;
        _databaseTableColumn.AddPrimary(counter++);
        _databaseTableColumn.AddPrimary(counter++);
        _databaseTableColumn.AddActive(counter++);
        _databaseTableColumn.AddCreateBy(counter++);
        _databaseTableColumn.AddCreateDate(counter++);
        _databaseTableColumn.AddModifyBy(counter++);
        _databaseTableColumn.AddModifyDate(counter++);
        _databaseTableColumn.AddPermanentName(counter++);
        _databaseTableColumn.AddTimeStamp(counter++);
        _configuration.SetData();

        var table = new Table("bob", "was here")
        {
            Columns = _databaseTableColumn.TableColumns
        };
        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(table));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using System; using System.Collections.Generic; namespace Bob.Is.Here.Testing.Models.was here { public static class bobExtensions { static string DeletedAction(bob bob) => $\"bob1 => {bob.bob1} bob2 => {bob.bob2} was deleted by bob6 => {bob.bob6} on bob7 => {bob.bob7}\"; static string DestroyedAction(bob bob) => $\"bob1 => {bob.bob1} bob2 => {bob.bob2} was destroyed by bob6 => {bob.bob6} on bob7 => {bob.bob7}\"; static string InsertedAction(bob bob) => $\"bob1 => {bob.bob1} bob2 => {bob.bob2} was created by bob4 => {bob.bob4} on bob5 => {bob.bob5}\"; static string UpdatedAction(bob bob) => $\"bob1 => {bob.bob1} bob2 => {bob.bob2} was updated by bob6 => {bob.bob6} on bob7 => {bob.bob7}\"; } }"));
    }

    [Fact]
    public async void ModelExtensionsService_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using System; using System.Collections.Generic; namespace Bob.Is.Here.Testing.Models.bob { public static class BobTableExtensions { static string DeletedAction(BobTable bobTable) => $\"bobColumn => {bobTable.bobColumn} was deleted on {DateTime.Now}\"; static string DestroyedAction(BobTable bobTable) => $\"bobColumn => {bobTable.bobColumn} was destroyed on {DateTime.Now}\"; static string InsertedAction(BobTable bobTable) => $\"bobColumn => {bobTable.bobColumn} was created on {DateTime.Now}\"; static string UpdatedAction(BobTable bobTable) => $\"bobColumn => {bobTable.bobColumn} was updated on {DateTime.Now}\"; } }"));
    }

    [Fact]
    public async void ModelExtensionsService_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelExtensionsService_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(null));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelExtensionsService_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(null);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }
}