﻿using Core.Common.Generics;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Models;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Collections.Immutable;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Models;

public class ModelQueryServiceTests : IClassFixture<ModelQueryServiceFixture>, IClassFixture<DatabaseTableColumnFixture>, IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly ModelQueryServiceFixture _service;

    public ModelQueryServiceTests(ModelQueryServiceFixture service, DatabaseTableColumnFixture databaseTableColumn, ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }

    [Fact]
    public async void ModelQueryService_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelQueryService_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.Reset();
        _databaseTableColumn.CreateComplexTableColumns();
        var table = new Table("BobTable", "Bob")
        {
            Columns = _databaseTableColumn.TableColumns.ToImmutableList()
        };

        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(table));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using System; using System.Collections.Generic; namespace Bob.Is.Here.Testing.Models.Bob.Queries { public class BobTableQuery { public bool? Bob10 { get; set; } public string Bob11 { get; set; } public string Bob13 { get; set; } public string Bob14 { get; set; } public string Bob15 { get; set; } public string Bob16 { get; set; } public string Bob17 { get; set; } public bool? Bob2 { get; set; } public string Bob3 { get; set; } public bool? Bob4 { get; set; } public string Bob5 { get; set; } public string Bob7 { get; set; } public bool? IncludeInactive { get; set; } } }"));
    }

    [Fact]
    public async void ModelQueryService_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelQueryService_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(null));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelQueryService_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(null);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }
}