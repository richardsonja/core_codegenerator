﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Services.Configurations;
using Method = Core.Generator.Services.Models.Objects.Method;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Controllers;

public class WebApiControllerService : IWebApiControllerService
{
    private readonly IConfigurationService _configurationService;

    public WebApiControllerService(IConfigurationService configurationService)
    {
        _configurationService = configurationService;
    }

    public async Task<Option<string>> CreateOutputAsync(CodeGeneratorRequest request)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<string>.None;
        }

        var configOption = await _configurationService.GetAsync();
        return configOption.Match(
            x => CreateClass(x, request.Table),
            () => Option<string>.None);
    }

    protected static Option<string> CreateClass(
        ConfigurationSettings configuration,
        Table table)
    {
        var classObject = new ClassObject(GetClassName(configuration, table), GetNamespace(configuration, table))
            .AddUsingStatements(GetUsingStatements())
            .AddInterfacesAndExtension(GetInterfacesAndExtension(table))
            .AddMethods(CreateBulkDeleteMethod(configuration, table))
            .AddMethods(CreateBulkDestroyMethod(configuration, table))
            .AddMethods(CreateBulkInsertMethod(configuration, table))
            .AddMethods(CreateBulkUpdateMethod(configuration, table))
            .AddMethods(CreateConstructorMethod(configuration, table))
            .AddMethods(CreateDeleteMethod(configuration, table))
            .AddMethods(CreateDestroyMethod(configuration, table))
            .AddMethods(CreateFindMethod(configuration, table))
            .AddMethods(CreateGetActiveListMethod(configuration, table))
            .AddMethods(CreateGetMethod(configuration, table))
            .AddMethods(CreateInsertMethod(configuration, table))
            .AddMethods(CreateUpdateMethod(configuration, table))
            .AddProperties(GetProperties(configuration, table));

        return classObject.ToString();
    }

    private static ImmutableList<Method> CreateActionVerbMethods(string actionVerb, string path) =>
        new Method(actionVerb)
            .SetAttributeMethod()
            .AddParameters(new MethodParameter($"\"{path}\""))
            .ToFullImmutableList();

    private static Method CreateBulkDeleteMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.BulkLogicalDelete != true
            || !table.Columns.GetIsActiveColumns().Any())
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.BulkDeleteMethodName;
        const string actionVerb = Constants.AttributeHttpDelete;
        var parameterType = $"{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName}";
        var parameterName = parameterType.CamelCase();
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";

        var parameters = new MethodParameter(parameterType, parameterName)
            .AddAttributes(new Method(Constants.AttributeFromBody).SetAttributeMethod())
            .ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static Method CreateBulkDestroyMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.BulkDelete != true)
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.BulkDestroyMethodName;
        const string actionVerb = Constants.AttributeHttpDelete;
        var parameterType = $"{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName}";
        var parameterName = parameterType.CamelCase();
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";

        var parameters = new MethodParameter(parameterType, parameterName)
            .AddAttributes(new Method(Constants.AttributeFromBody).SetAttributeMethod())
            .ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static Method CreateBulkInsertMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.BulkCreate != true)
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.BulkInsertMethodName;
        const string actionVerb = Constants.AttributeHttpPost;
        var parameterType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName}";
        var parameterName = parameterType.CamelCase();
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";
        var parameters = new MethodParameter(parameterType, parameterName)
            .AddAttributes(new Method(Constants.AttributeFromBody).SetAttributeMethod())
            .ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static Method CreateBulkUpdateMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.BulkUpdate != true)
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.BulkUpdateMethodName;
        const string actionVerb = Constants.AttributeHttpPut;
        var parameterType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName}";
        var parameterName = parameterType.CamelCase();
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";

        var parameters = new MethodParameter(parameterType, parameterName)
            .AddAttributes(new Method(Constants.AttributeFromBody).SetAttributeMethod())
            .ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static Method CreateConstructorMethod(ConfigurationSettings configuration, Table table)
    {
        var serviceName = configuration.ConfigurationDetail.CodeRelatedConfiguration.ServiceLayerName;
        var parameterType = $"I{table.FormattedName}{serviceName}";
        var parameterName = $"{table.CamelCaseFormattedName}{serviceName}";
        return new Method(GetClassName(configuration, table))
            .SetConstructorMethod()
            .AddParameters(
                new MethodParameter(parameterType, parameterName))
            .AddBody($"_{table.CamelCaseFormattedName}{serviceName} = {table.CamelCaseFormattedName}{serviceName};");
    }

    private static Method CreateDeleteMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.LogicalDelete != true)
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.DeleteMethodName;
        const string actionVerb = Constants.AttributeHttpDelete;
        var parameterType = $"{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName}";
        var parameterName = parameterType.CamelCase();
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";

        var parameters = new MethodParameter(parameterType, parameterName)
            .AddAttributes(new Method(Constants.AttributeFromBody).SetAttributeMethod())
            .ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static Method CreateDestroyMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.Delete != true)
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.DestroyMethodName;
        const string actionVerb = Constants.AttributeHttpDelete;
        var parameterType = $"{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName}";
        var parameterName = parameterType.CamelCase();
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";

        var parameters = new MethodParameter(parameterType, parameterName)
            .AddAttributes(new Method(Constants.AttributeFromBody).SetAttributeMethod())
            .ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static Method CreateExecuteMethod(
        ConfigurationSettings configuration,
        Table table,
        string methodName,
        string parametersToServiceLayer,
        string returnType,
        IEnumerable<MethodParameter> parameters,
        IEnumerable<Method> attributes)
    {
        var serviceName = configuration.ConfigurationDetail.CodeRelatedConfiguration.ServiceLayerName;
        return new Method(methodName)
            .AddParameters(parameters)
            .AddReturnType(returnType)
            .AddAttributes(attributes)
            .AddBody($"var results = _{table.CamelCaseFormattedName}{serviceName}.{methodName}({parametersToServiceLayer});")
            .AddBody("return results;");
    }

    private static Method CreateFindMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.Find != true)
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.FindMethodName;
        const string actionVerb = Constants.AttributeHttpPost;
        var parameterType = $"{table.FormattedName}Query";
        var parameterName = parameterType.CamelCase();
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";

        var parameters = new MethodParameter(parameterType, parameterName)
            .AddAttributes(new Method(Constants.AttributeFromBody).SetAttributeMethod())
            .ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static Method CreateGetActiveListMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.ActiveList != true
            || table.Columns.GetIsActiveColumns().IsNullOrEmpty())
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.GetActiveListMethodName;
        const string actionVerb = Constants.AttributeHttpGet;
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            null,
            returnType,
            null,
            attributes);
    }

    private static Method CreateGetMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.GetByPrimaryKeys != true)
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.GetMethodName;
        const string actionVerb = Constants.AttributeHttpGet;
        var parameterType = GetParameterKeyType(table);
        var parameterName = "id";
        var returnType = $"{table.FormattedName}";
        var path = "{id}";

        var parameters = new MethodParameter(parameterType, parameterName).ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, path);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static Method CreateInsertMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.Insert != true)
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.InsertMethodName;
        const string actionVerb = Constants.AttributeHttpPost;
        var parameterType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName}";
        var parameterName = parameterType.CamelCase();
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";

        var parameters = new MethodParameter(parameterType, parameterName)
            .AddAttributes(new Method(Constants.AttributeFromBody).SetAttributeMethod())
            .ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static Method CreateUpdateMethod(ConfigurationSettings configuration, Table table)
    {
        if (configuration.ConfigurationDetail.OptionsConfiguration.Update != true)
        {
            return null;
        }

        var methodName = configuration.ConfigurationDetail.CodeRelatedConfiguration.UpdateMethodName;
        const string actionVerb = Constants.AttributeHttpPut;
        var parameterType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName}";
        var parameterName = parameterType.CamelCase();
        var returnType = $"{table.FormattedName}{methodName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName}";

        var parameters = new MethodParameter(parameterType, parameterName)
            .AddAttributes(new Method(Constants.AttributeFromBody).SetAttributeMethod())
            .ToFullImmutableList();
        var attributes = CreateActionVerbMethods(actionVerb, methodName);

        return CreateExecuteMethod(
            configuration,
            table,
            methodName,
            parameterName,
            returnType,
            parameters,
            attributes);
    }

    private static string GetClassName(ConfigurationSettings configuration, Table table) =>
        $"{table.FormattedName}Controller";

    private static ImmutableList<string> GetInterfacesAndExtension(Table table) => "Controller".ToFullImmutableList();

    private static string GetNamespace(
        ConfigurationSettings configuration,
        Table table) =>
        $"{configuration.ConfigurationDetail.CodeRelatedConfiguration.Namespace}.{configuration.ConfigurationDetail.CodeRelatedConfiguration.ControllerLayerName}";

    private static string GetParameterKeyType(Table table)
    {
        return table.Columns
                   .GetPrimaryKeyColumns()
                   .OrderBy(x => x.OrdinalPosition)
                   .FirstOrDefault()
                   ?.CSharpType?.TypeString
               ?? "int";
    }

    private static ImmutableList<PropertyObject> GetProperties(
        ConfigurationSettings configuration,
        Table table) =>
        new PropertyObject($"_{table.CamelCaseFormattedName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ServiceLayerName}")
            .SetAccessorType(AccessorType.Private)
            .SetIsReadOnly()
            .SetType(new CSharpType(null, $"I{table.FormattedName}{configuration.ConfigurationDetail.CodeRelatedConfiguration.ServiceLayerName}"))
            .ToFullImmutableList();

    private static ImmutableList<string> GetUsingStatements() =>
        new List<string>
        {
            "System",
            "System.Collections.Generic",
            "System.Threading.Tasks",
            "Microsoft.AspNetCore.Mvc",
            "Microsoft.AspNetCore.Authorization"
        }.ToImmutableList();
}