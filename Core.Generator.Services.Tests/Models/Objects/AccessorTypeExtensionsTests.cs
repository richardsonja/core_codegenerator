﻿using Core.Generator.Services.Models.Objects;
using FluentAssertions;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Objects;

public class AccessorTypeExtensionsTests
{
    [Theory]
    [InlineData(AccessorType.Internal, "internal")]
    [InlineData(AccessorType.Public, "public")]
    [InlineData(AccessorType.Private, "private")]
    [InlineData(AccessorType.Protected, "protected")]
    public void AccessorTypeExtensions_CreateOutput_ShouldMatchExpected_GivenInput(AccessorType value, string expected) =>
        value.CreateOutput().Should().Be(expected);
}