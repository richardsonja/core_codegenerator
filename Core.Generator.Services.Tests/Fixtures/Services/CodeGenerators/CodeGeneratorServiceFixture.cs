﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Services.CodeGenerators;
using Core.Generator.Services.Services.CodeGenerators.Types.Controllers;
using Core.Generator.Services.Services.CodeGenerators.Types.Models;
using Core.Generator.Services.Services.CodeGenerators.Types.Sqls;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;

public class CodeGeneratorServiceFixture : IDisposable, IReset
{
    public Mock<IControllerCodeGeneratorService> ControllerCodeGeneratorService = new();
    public Mock<IModelCodeGeneratorService> ModelCodeGeneratorService = new();
    public CodeGeneratorService Service;
    public Mock<ISqlCodeGeneratorService> SqlCodeGeneratorService = new();

    public CodeGeneratorServiceFixture()
    {
        Reset();
        Service = new CodeGeneratorService(
            ModelCodeGeneratorService.Object,
            ControllerCodeGeneratorService.Object,
            SqlCodeGeneratorService.Object);
    }

    public void Dispose()
    {
        Service = null;
        ModelCodeGeneratorService = null;
        ControllerCodeGeneratorService = null;
        SqlCodeGeneratorService = null;
    }

    public void HardReset()
    {
        ModelCodeGeneratorService.Reset();
        ControllerCodeGeneratorService.Reset();
        SqlCodeGeneratorService.Reset();
    }

    public void Reset()
    {
        HardReset();

        ModelCodeGeneratorService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync(new List<DisplayResult> { new(1, "BOB WAS HERE", "ModelCodeGeneratorService's BOB WAS HERE") }.ToImmutableList());

        ControllerCodeGeneratorService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync(new List<DisplayResult> { new(1, "BOB WAS HERE", "ControllerCodeGeneratorService's BOB WAS HERE") }.ToImmutableList());

        SqlCodeGeneratorService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync(new List<DisplayResult> { new(1, "BOB WAS HERE", "SqlCodeGeneratorService's BOB WAS HERE") }.ToImmutableList());
    }
}