﻿using Core.Flux.Tests.Mocks;

namespace Core.Flux.Tests.Fixtures;

public class FluxViewFixture : IDisposable, IReset
{
    public IFluxViewFor<TestStore> View;

    public FluxViewFixture() => Reset();

    public void Dispose() => View = null;

    public void Reset()
    {
        var store = new TestStore();

        Fluxs.StoreResolver.Register(store);

        View = new TestView();
    }
}