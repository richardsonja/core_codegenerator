﻿namespace Core.Generator.Services.Models.Configurations;

public enum AuthenticationType
{
    NotSelectedYet = 0,
    MsSqlWindows = 1,
    MsSqlServerAccount = 2,
    MySqlServerAccount = 3
}