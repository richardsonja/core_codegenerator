﻿namespace Core.Common.Tests.Fixtures;

public class TestClass : ICloneable
{
    public TestClass()
    {
    }

    public TestClass(int value, Guid id, DateTime dateTime, string name) =>
        (Value, Id, DateTime, Name) = (value, id, dateTime, name);

    public DateTime DateTime { get; }

    public Guid Id { get; }

    public string Name { get; }

    public int Value { get; }

    public object Clone() => new TestClass(Value, Id, DateTime, Name);
}