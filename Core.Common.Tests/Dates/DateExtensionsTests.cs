﻿using Core.Common.Dates;

namespace Core.Common.Tests.Dates;

public class DateExtensionsTests
{
    [Fact]
    public void DateExtensions_IsNullOrEmpty_ShouldBeFalse_GivenValue()
    {
        // Arrange
        DateTime? testValue = default(DateTime).AddSeconds(1);

        // Act
        var result = testValue.IsNullOrEmpty();

        // Assert
        result.Should().BeFalse();
    }

    [Fact]
    public void DateExtensions_IsNullOrEmpty_ShouldBeTrue_GivenDefaultValue()
    {
        DateTime? testValue = default(DateTime);

        testValue.IsNullOrEmpty().Should().BeTrue();
    }

    [Fact]
    public void DateExtensions_IsNullOrEmpty_ShouldBeTrue_GivenNull()
    {
        DateTime? testValue = null;

        testValue.IsNullOrEmpty().Should().BeTrue();
    }
}