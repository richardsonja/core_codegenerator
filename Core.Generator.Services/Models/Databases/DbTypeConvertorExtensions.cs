﻿namespace Core.Generator.Services.Models.Databases;

public static class DbTypeConvertorExtensions
{
    public static SqlDbType GetSqlDbType(this object type)
    {
        if (type == null)
        {
            return SqlDbType.Variant;
        }

        return type.ToString()?.ToLower() switch
        {
            "bigint" => SqlDbType.BigInt,
            "binary" => SqlDbType.Binary,
            "bit" => SqlDbType.Bit,
            "bool" => SqlDbType.TinyInt,
            "boolean" => SqlDbType.TinyInt,
            "char" => SqlDbType.Char,
            "char2" => SqlDbType.Char,
            "date" => SqlDbType.Date,
            "datetime" => SqlDbType.DateTime,
            "datetime2" => SqlDbType.DateTime2,
            "datetimeoffset" => SqlDbType.DateTimeOffset,
            "dec" => SqlDbType.Decimal,
            "decimal" => SqlDbType.Decimal,
            "decmial" => SqlDbType.Decimal,
            "double" => SqlDbType.Float,
            "float" => SqlDbType.Float,
            "int" => SqlDbType.Int,
            "integer" => SqlDbType.Int,
            "money" => SqlDbType.Money,
            "nchar" => SqlDbType.NChar,
            "ntext" => SqlDbType.NText,
            "numeric" => SqlDbType.Decimal,
            "nvarchar" => SqlDbType.NVarChar,
            "nvarchar2" => SqlDbType.NVarChar,
            "real" => SqlDbType.Real,
            "smalldatetime" => SqlDbType.SmallDateTime,
            "smallint" => SqlDbType.SmallInt,
            "smallmoney" => SqlDbType.SmallMoney,
            "structured" => SqlDbType.Structured,
            "text" => SqlDbType.Text,
            "time" => SqlDbType.Time,
            "timestamp" => SqlDbType.Timestamp,
            "tinyint" => SqlDbType.TinyInt,
            "udt" => SqlDbType.Udt,
            "uniqueidentifier" => SqlDbType.UniqueIdentifier,
            "varchar" => SqlDbType.VarChar,
            "varchar2" => SqlDbType.VarChar,
            "variant" => SqlDbType.Variant,
            "xml" => SqlDbType.Xml,
            _ => SqlDbType.Variant
        };
    }
}