﻿namespace Core.Generator.Services.Models.Sqls;

public interface IStatement
{
    IStatement AddParameter(IEnumerable<StoredProcedureParameter> parameter);

    IStatement AddParameter(StoredProcedureParameter parameter);

    string ToString();
}