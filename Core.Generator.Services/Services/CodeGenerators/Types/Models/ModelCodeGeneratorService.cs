﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public class ModelCodeGeneratorService : IModelCodeGeneratorService
{
    private readonly IBulkDeleteRequestResponseService _bulkDeleteRequestResponseService;
    private readonly IBulkDestroyRequestResponseService _bulkDestroyRequestResponseService;
    private readonly IBulkInsertRequestResponseService _bulkInsertRequestResponseService;
    private readonly IBulkUpdateRequestResponseService _bulkUpdateRequestResponseService;
    private readonly IDeleteRequestResponseService _deleteRequestResponseService;
    private readonly IDestroyRequestResponseService _destroyRequestResponseService;
    private readonly IInsertRequestResponseService _insertRequestResponseService;
    private readonly IUpdateRequestResponseService _updateRequestResponseService;
    private readonly IGetRequestResponseService _getRequestResponseService;
    private readonly IConfigurationService _configurationService;
    private readonly IModelExtensionsService _modelExtensionsService;
    private readonly IModelQueryService _modelQueryService;
    private readonly IModelService _modelService;

    public ModelCodeGeneratorService(
        IConfigurationService configurationService,
        IModelService modelService,
        IModelExtensionsService modelExtensionsService,
        IModelQueryService modelQueryService,
        IBulkDeleteRequestResponseService bulkDeleteRequestResponseService,
        IBulkDestroyRequestResponseService bulkDestroyRequestResponseService,
        IBulkInsertRequestResponseService bulkInsertRequestResponseService,
        IBulkUpdateRequestResponseService bulkUpdateRequestResponseService,
        IDeleteRequestResponseService deleteRequestResponseService,
        IDestroyRequestResponseService destroyRequestResponseService,
        IInsertRequestResponseService insertRequestResponseService,
        IUpdateRequestResponseService updateRequestResponseService,
        IGetRequestResponseService getRequestResponseService)
    {
        _configurationService = configurationService;
        _modelService = modelService;
        _modelExtensionsService = modelExtensionsService;
        _modelQueryService = modelQueryService;
        _bulkDeleteRequestResponseService = bulkDeleteRequestResponseService;
        _bulkDestroyRequestResponseService = bulkDestroyRequestResponseService;
        _bulkInsertRequestResponseService = bulkInsertRequestResponseService;
        _bulkUpdateRequestResponseService = bulkUpdateRequestResponseService;
        _deleteRequestResponseService = deleteRequestResponseService;
        _destroyRequestResponseService = destroyRequestResponseService;
        _insertRequestResponseService = insertRequestResponseService;
        _updateRequestResponseService = updateRequestResponseService;
        _getRequestResponseService = getRequestResponseService;
    }

    public async Task<Option<ImmutableList<DisplayResult>>> CreateOutputAsync(CodeGeneratorRequest request)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<ImmutableList<DisplayResult>>.None;
        }

        var configOption = await _configurationService.GetAsync();
        var displayResults = await configOption
            .MatchAsync(
                x => CreateMethodTabAsync(x, request),
                () => new List<DisplayResult>().ToImmutableList());

        return displayResults.IsNullOrEmpty()
            ? Option<ImmutableList<DisplayResult>>.None
            : displayResults.ToImmutableList();
    }

    private static async Task<DisplayResult> CreateRequestResponse(
        ITypeService<bool> service,
        CodeGeneratorRequest request,
        string methodName,
        string tabName,
        CodeRelatedConfiguration codeRelatedConfig,
        int resultCounter,
        int responseCounter)
    {
        var bulkDeleteResult = await service
            .CreateOutputAsync(request, true)
            .Match(
                x => new DisplayResult(
                    resultCounter,
                    $"{methodName}{codeRelatedConfig.RequestSuffixModelName}",
                    x),
                () => new DisplayResult());

        var bulkDeleteResponse = await service
            .CreateOutputAsync(request, false)
            .Match(
                x => new DisplayResult(
                    responseCounter,
                    $"{methodName}{codeRelatedConfig.ResponseSuffixModelName}",
                    x),
                () => new DisplayResult());

        var displayResults = bulkDeleteResult
            .ToFullList()
            .AddTo(bulkDeleteResponse)
            .ToList();

        var list = displayResults?.Where(x => x != null && !x.IsEmpty).OrderList()?.ToList();
        return list.IsNullOrEmpty()
            ? new DisplayResult()
            : tabName.CreateDisplayResult(resultCounter, displayResults.ToArray());
    }

    private async Task<ImmutableList<DisplayResult>> CreateMethodTabAsync(
        ConfigurationSettings configuration,
        CodeGeneratorRequest request)
    {
        var counter = 1;
        var modelResult = await _modelService.CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabModels, x),
                () => new DisplayResult());

        var modelExtensionsResult = await _modelExtensionsService.CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabModelExtensions, x),
                () => new DisplayResult());

        var modelQueryResult = await _modelQueryService.CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabModelQueries, x),
                () => new DisplayResult());

        var codeRelatedConfig = configuration.ConfigurationDetail.CodeRelatedConfiguration;

        var bulkDeleteResults = await CreateRequestResponse(
            _bulkDeleteRequestResponseService,
            request,
            codeRelatedConfig.BulkDeleteMethodName,
            Constants.TabBulkDelete,
            codeRelatedConfig,
            counter++,
            counter++);

        var bulkDestroyResults = await CreateRequestResponse(
            _bulkDestroyRequestResponseService,
            request,
            codeRelatedConfig.BulkDestroyMethodName,
            Constants.TabBulkDestroy,
            codeRelatedConfig,
            counter++,
            counter++);

        var bulkInsertResults = await CreateRequestResponse(
            _bulkInsertRequestResponseService,
            request,
            codeRelatedConfig.BulkInsertMethodName,
            Constants.TabBulkInsert,
            codeRelatedConfig,
            counter++,
            counter++);

        var bulkUpdateResults = await CreateRequestResponse(
            _bulkUpdateRequestResponseService,
            request,
            codeRelatedConfig.BulkUpdateMethodName,
            Constants.TabBulkUpdate,
            codeRelatedConfig,
            counter++,
            counter++);
            
        var deleteResults = await CreateRequestResponse(
            _deleteRequestResponseService,
            request,
            codeRelatedConfig.DeleteMethodName,
            Constants.TabDelete,
            codeRelatedConfig,
            counter++,
            counter++);

        var destroyResults = await CreateRequestResponse(
            _destroyRequestResponseService,
            request,
            codeRelatedConfig.DestroyMethodName,
            Constants.TabDestroy,
            codeRelatedConfig,
            counter++,
            counter++);

        var insertResults = await CreateRequestResponse(
            _insertRequestResponseService,
            request,
            codeRelatedConfig.InsertMethodName,
            Constants.TabInsert,
            codeRelatedConfig,
            counter++,
            counter++);

        var updateResults = await CreateRequestResponse(
            _updateRequestResponseService,
            request,
            codeRelatedConfig.UpdateMethodName,
            Constants.TabUpdate,
            codeRelatedConfig,
            counter++,
            counter++);
            

        var getResults = await CreateRequestResponse(
            _getRequestResponseService,
            request,
            codeRelatedConfig.GetMethodName,
            Constants.TabGet,
            codeRelatedConfig,
            counter++,
            counter++);
            
        return modelResult.ToFullList()
            .AddTo(modelExtensionsResult)
            .AddTo(modelQueryResult)
            .AddTo(bulkDeleteResults)
            .AddTo(bulkDestroyResults)
            .AddTo(bulkInsertResults)
            .AddTo(bulkUpdateResults)
            .AddTo(deleteResults)
            .AddTo(destroyResults)
            .AddTo(insertResults)
            .AddTo(updateResults)
            .AddTo(getResults)
            .ToImmutableList();
    }
}