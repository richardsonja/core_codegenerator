﻿namespace Core.Generator.Services.Models.Sqls;

public static class StatementExtensions
{
    public static string FormatBracketedValue(this string value, bool removeBrackets) =>
        value.IsNullOrEmpty() || removeBrackets && !value.Contains(" ")
            ? value
            : $"[{value}]";
}