﻿using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public class DeleteRequestResponseService :
    RequestResponseBaseService,
    IDeleteRequestResponseService
{
    public DeleteRequestResponseService(IConfigurationService configurationService)
        : base(configurationService)
    {
    }

    protected override string GetClassName(ConfigurationSettings configuration, Table table, bool isRequestObject) =>
        $"{configuration.ConfigurationDetail.CodeRelatedConfiguration.DeleteMethodName}{GetRequestResponseSuffix(configuration, isRequestObject)}";

    protected override ImmutableList<PropertyObject> GetProperties(Table table, bool isRequestObject) =>
        isRequestObject
            ? AddRequestInputListOfPrimaryKeysProperties(table, false)
            : AddResponseErrorAndSuccessfulProperties(table);

    protected override bool ShouldExecute(ConfigurationSettings configuration, Table table) =>
        configuration.ConfigurationDetail.OptionsConfiguration.LogicalDelete == true
        && table.Columns.GetIsActiveColumns().Any();
}