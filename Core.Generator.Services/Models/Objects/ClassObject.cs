﻿namespace Core.Generator.Services.Models.Objects;

public class ClassObject : ICloneable
{
    public ClassObject(string name, string namespaceName) => 
        (Name, NamespaceName) = (name, namespaceName);

    private ClassObject(
        AccessorType accessorType,
        ImmutableList<Method> attributes,
        ImmutableList<string> interfacesAndExtension,
        bool isSealed,
        bool isStatic,
        ImmutableList<Method> methods,
        string name,
        string namespaceName,
        ObjectType objectType,
        ImmutableList<PropertyObject> properties,
        ImmutableList<string> usingStatements)
        : this(name, namespaceName)
    {
        AccessorType = accessorType;
        Attributes = attributes.Clone().ToImmutableList();
        InterfacesAndExtension = interfacesAndExtension.Clone().ToImmutableList();
        IsSealed = isSealed;
        IsStatic = isStatic;
        Methods = methods.Clone().ToImmutableList();
        ObjectType = objectType;
        Properties = properties.Clone().ToImmutableList();
        UsingStatements = usingStatements.Clone().ToImmutableList();
    }

    public AccessorType AccessorType { get; private set; } = AccessorType.Public;

    public ImmutableList<Method> Attributes { get; private set; } = new List<Method>().ToImmutableList();

    public ImmutableList<string> InterfacesAndExtension { get; private set; } = new List<string>().ToImmutableList();

    public bool IsSealed { get; private set; }

    public bool IsStatic { get; private set; }

    public ImmutableList<Method> Methods { get; private set; } = new List<Method>().ToImmutableList();

    public string Name { get; }

    public string NamespaceName { get; }

    public ObjectType ObjectType { get; private set; }

    public ImmutableList<PropertyObject> Properties { get; set; } = new List<PropertyObject>().ToImmutableList();

    public ImmutableList<string> UsingStatements { get; private set; } = new List<string>().ToImmutableList();

    public ClassObject AddAttributes(Method parameter)
    {
        if (parameter == null)
        {
            return this;
        }

        Attributes = Attributes.AddTo(parameter).ToImmutableList();

        return this;
    }

    public ClassObject AddAttributes(IEnumerable<Method> parameters)
    {
        if (parameters.IsNullOrEmpty())
        {
            return this;
        }

        Attributes = Attributes.AddTo(parameters).ToImmutableList();

        return this;
    }

    public ClassObject AddInterfacesAndExtension(IEnumerable<string> interfacesAndExtensions)
    {
        if (interfacesAndExtensions.IsNullOrEmpty())
        {
            return this;
        }

        InterfacesAndExtension = InterfacesAndExtension.AddTo(interfacesAndExtensions).ToImmutableList();

        return this;
    }

    public ClassObject AddInterfacesAndExtension(string interfacesAndExtensions)
    {
        if (interfacesAndExtensions.IsNullOrEmpty())
        {
            return this;
        }

        InterfacesAndExtension = InterfacesAndExtension.AddTo(interfacesAndExtensions).ToImmutableList();

        return this;
    }

    public ClassObject AddMethods(Method methods)
    {
        if (methods == null)
        {
            return this;
        }

        Methods = Methods.AddTo(methods).ToImmutableList();

        return this;
    }

    public ClassObject AddMethods(IEnumerable<Method> methods)
    {
        if (methods.IsNullOrEmpty())
        {
            return this;
        }

        Methods = Methods.AddTo(methods).ToImmutableList();

        return this;
    }

    public ClassObject AddProperties(PropertyObject properties)
    {
        if (properties == null)
        {
            return this;
        }

        Properties = Properties.AddTo(properties).ToImmutableList();

        return this;
    }

    public ClassObject AddProperties(IEnumerable<PropertyObject> properties)
    {
        if (properties.IsNullOrEmpty())
        {
            return this;
        }

        Properties = Properties.AddTo(properties).ToImmutableList();

        return this;
    }

    public ClassObject AddUsingStatements(IEnumerable<string> usingStatements)
    {
        if (usingStatements.IsNullOrEmpty())
        {
            return this;
        }

        UsingStatements = UsingStatements.AddTo(usingStatements).ToImmutableList();

        return this;
    }

    public ClassObject AddUsingStatements(string usingStatements)
    {
        if (usingStatements.IsNullOrEmpty())
        {
            return this;
        }

        UsingStatements = UsingStatements.AddTo(usingStatements).ToImmutableList();

        return this;
    }

    public object Clone() => new ClassObject(
        AccessorType,
        Attributes,
        InterfacesAndExtension,
        IsSealed,
        IsStatic,
        Methods,
        Name,
        NamespaceName,
        ObjectType,
        Properties,
        UsingStatements);

    public ClassObject SetAccessorType(AccessorType accessorType)
    {
        AccessorType = accessorType;

        return this;
    }

    public ClassObject SetClassObject(ObjectType objectType)
    {
        ObjectType = objectType;

        return this;
    }

    public ClassObject SetIsSealed(bool isSealed = true)
    {
        IsSealed = isSealed;

        return this;
    }

    public ClassObject SetIsStatic(bool isStatic = true)
    {
        IsStatic = isStatic;

        return this;
    }

    public override string ToString() =>
        $"{CreateUsingStatements()}{CreateNamespace()} {{ {Environment.NewLine} {CreateSignature()}{CreateInterfaces()} {Environment.NewLine} {{{Environment.NewLine}  {CreateProperties()} {Environment.NewLine} {CreateMethods()} {Environment.NewLine} }} {Environment.NewLine} }}";

    private string CreateInterfaces() =>
        InterfacesAndExtension.IsNullOrEmpty()
            ? string.Empty
            : $" : {InterfacesAndExtension.Combine(", ")}";

    private string CreateMethods() => Methods.ToMethod();

    private string CreateNamespace() =>
        $"namespace {(NamespaceName.IsNullOrEmpty() ? "UPDATE.SETTINGS.MISSING.NAMESPACE" : NamespaceName)}{Environment.NewLine}";

    private string CreateProperties() => Properties.ToPropertyObject();

    private string CreateSignature() => $"{ToAttributes()} {ToAccessorType()}  {ToStatic()} {ToSealed()} {ToObjectType()} {Name}".TrimInternalExtraSpaces();

    private string CreateUsingStatements() =>
        UsingStatements.IsNullOrEmpty()
            ? string.Empty
            : UsingStatements
                .OrderBy(x => x)
                .Select(x => $"using {x}; ")
                .AddTo(Environment.NewLine)
                .Combine(Environment.NewLine);

    private string ToAccessorType() => AccessorType.CreateOutput();

    private string ToAttributes() => Attributes.ToAttribute();

    private string ToObjectType() => ObjectType.CreateOutput();

    private string ToSealed() => IsSealed
        ? "sealed"
        : string.Empty;

    private string ToStatic() => IsStatic
        ? "static"
        : string.Empty;
}