﻿using Core.Flux.Extensions;
using Core.Flux.Tests.Mocks;

namespace Core.Flux.Tests;

[Collection("Serial")]
public class FluxViewForExtensionsTests : IClassFixture<FluxViewFixture>
{
    private readonly FluxViewFixture _view;

    public FluxViewForExtensionsTests(FluxViewFixture view) => _view = view;

    [Fact]
    public void FluxViewForExtensions_Dispatch_ShouldCallOnChangeAndUpdateTestResult()
    {
        _view.Reset();
        var payload = new TestAction { Result = "Changed" };

        _view.View.Dispatch(payload);
        ((TestView)_view.View).TestResult.Should().Be(payload.Result);
    }

    [Fact]
    public void FluxViewForExtensions_EmitChange_ShouldDoNothing()
    {
        _view.Reset();
        _view.View.EmitChange();
        ((TestView)_view.View).TestResult.Should().BeNull();
    }
}