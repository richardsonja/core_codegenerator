﻿namespace Core.Generator.Services.Models.Objects;

public static class GetterSetterTypeExtensions
{
    private static readonly List<GetterSetterType> InvalidCombo = 
        new() { GetterSetterType.Set, GetterSetterType.Init };

    public static string CreateOutput(this GetterSetterType getterSetterType) => getterSetterType switch
    {
        GetterSetterType.Set => "set",
        GetterSetterType.Init => "init",
        _ => "get"
    };

    public static bool IsValidCombo(this IEnumerable<GetterSetterType> getterSetterTypes) =>
        getterSetterTypes.IsNullOrEmpty()
        || getterSetterTypes.Count() < 2
        || InvalidCombo.Except(getterSetterTypes).Any();
}