﻿namespace Core.Generator.Services.Models.Objects;

public class Method : ICloneable
{
    public Method(string name) => 
        Name = name;

    public Method(string name, IEnumerable<MethodParameter> parameters) : this(name) => 
        Parameters = parameters.Clone()?.ToImmutableList() ?? new List<MethodParameter>().ToImmutableList();

    private Method(
        AccessorType accessorType,
        ImmutableList<Method> attributes,
        ImmutableList<string> body,
        bool hasNoBody,
        bool isAsync,
        bool isShortHandMethod,
        bool isStatic,
        bool isConstructor,
        bool isOverride,
        string name,
        ImmutableList<MethodParameter> parameters,
        string returnType) : this(name, parameters)
    {
        AccessorType = accessorType;
        Attributes = attributes.Clone()?.ToImmutableList() ?? new List<Method>().ToImmutableList();
        Body = body.Clone()?.ToImmutableList() ?? new List<string>().ToImmutableList();
        HasNoBody = hasNoBody;
        IsAsync = isAsync;
        IsShortHandMethod = isShortHandMethod;
        IsStatic = isStatic;
        IsConstructor = isConstructor;
        IsOverride = isOverride;
        ReturnType = returnType;
    }

    public AccessorType AccessorType { get; private set; } = AccessorType.Public;

    public ImmutableList<Method> Attributes { get; private set; } = new List<Method>().ToImmutableList();

    public ImmutableList<string> Body { get; private set; } = new List<string>().ToImmutableList();

    public bool HasNoBody { get; private set; }

    public bool IsAsync { get; private set; }

    public bool IsConstructor { get; private set; }

    public bool IsOverride { get; private set; }

    public bool IsShortHandMethod { get; private set; }

    public bool IsStatic { get; private set; }

    public string Name { get; }

    public ImmutableList<MethodParameter> Parameters { get; private set; } = new List<MethodParameter>().ToImmutableList();

    public string ReturnType { get; private set; }

    public Method AddAttributes(Method attribute)
    {
        if (attribute == null)
        {
            return this;
        }

        Attributes = Attributes.AddTo(attribute).ToImmutableList();

        return this;
    }

    public Method AddAttributes(IEnumerable<Method> attributes)
    {
        if (attributes.IsNullOrEmpty())
        {
            return this;
        }

        Attributes = Attributes.AddTo(attributes).ToImmutableList();

        return this;
    }

    public Method AddBody(string body)
    {
        if (body.IsNullOrEmpty())
        {
            return this;
        }

        if (HasNoBody)
        {
            throw new InvalidOperationException($"{nameof(HasNoBody)} is set to true, body is not allowed");
        }

        Body = Body.AddTo(body?.Trim()).ToImmutableList();

        return this;
    }

    public Method AddBody(IEnumerable<string> body)
    {
        if (body.IsNullOrEmpty())
        {
            return this;
        }

        if (HasNoBody)
        {
            throw new InvalidOperationException($"{nameof(HasNoBody)} is set to true, body is not allowed");
        }

        Body = Body.AddTo(body.Clone()?.Select(x => x?.Trim())).ToImmutableList();

        return this;
    }

    public Method AddParameters(MethodParameter parameter)
    {
        if (parameter == null)
        {
            return this;
        }

        Parameters = Parameters.AddTo(parameter).ToImmutableList();

        return this;
    }

    public Method AddParameters(IEnumerable<MethodParameter> parameters)
    {
        if (parameters.IsNullOrEmpty())
        {
            return this;
        }

        Parameters = Parameters.AddTo(parameters).ToImmutableList();

        return this;
    }

    public Method AddReturnType(Type returnType)
    {
        return AddReturnType(returnType.ToStringFormat());
    }

    public Method AddReturnType(string returnType)
    {
        if (HasNoBody)
        {
            throw new InvalidOperationException($"{nameof(HasNoBody)} is set to true, return type is not allowed");
        }

        ReturnType = returnType?.Trim();

        return this;
    }

    public object Clone() => new Method(
        AccessorType,
        Attributes,
        Body,
        HasNoBody,
        IsAsync,
        IsShortHandMethod,
        IsStatic,
        IsConstructor,
        IsOverride,
        Name,
        Parameters,
        ReturnType);

    public Method SetAccessorType(AccessorType accessorType)
    {
        AccessorType = accessorType;

        return this;
    }

    public Method SetAttributeMethod()
    {
        HasNoBody = true;
        IsShortHandMethod = true;

        return this;
    }

    public Method SetConstructorMethod()
    {
        IsConstructor = true;
        HasNoBody = false;
        IsShortHandMethod = false;

        return this;
    }

    public Method SetHasNoBody(bool hasNoBody = true)
    {
        HasNoBody = hasNoBody;

        return this;
    }

    public Method SetIsAsync(bool isAsync = true)
    {
        IsAsync = isAsync;

        return this;
    }

    public Method SetIsOverride(bool isOverride = true)
    {
        IsOverride = isOverride;

        return this;
    }

    public Method SetIsShortHandMethod(bool isShortHandMethod = true)
    {
        IsShortHandMethod = isShortHandMethod;

        return this;
    }

    public Method SetIsStatic(bool isStatic = true)
    {
        IsStatic = isStatic;

        return this;
    }

    public override string ToString() => $"{Environment.NewLine}{CreateSignature()} {ToBody()}".Trim();

    private string CreateSignature() => $"{ToAttribute()} {ToAccessorType()} {ToStatic()} {ToOverride()}  {ToAsync()} {ToReturnType()} {Name}{ToParameter()}".TrimInternalExtraSpaces();

    private string ToAccessorType() =>
        IsShortHandMethod
            ? string.Empty
            : AccessorType.CreateOutput();

    private string ToAsync() => IsAsync
        ? "async"
        : string.Empty;

    private string ToAsyncReturnType() =>
        HasNoBody || IsConstructor
            ? string.Empty
            : ReturnType.IsNullOrEmpty()
                ? "Task"
                : $"Task<{ReturnType}>";

    private string ToAttribute() =>
        Attributes.IsNullOrEmpty()
            ? string.Empty
            : Attributes.ToAttribute() + Environment.NewLine;

    private string ToBody() =>
        HasNoBody
            ? string.Empty
            : Body.Count() == 1
                ? $" => {ToExpressionBody()};"
                : $@"
{{
    {Body.Combine(Environment.NewLine)}
}}";

    private string ToExpressionBody() =>
        Body
            .First()
            .Replace("return ", string.Empty)
            .Replace(";", string.Empty);

    private string ToNonAsyncReturnType() =>
        HasNoBody || IsConstructor
            ? string.Empty
            : ReturnType.IsNullOrEmpty()
                ? "void"
                : ReturnType;

    private string ToOverride() =>
        IsOverride
            ? "override"
            : string.Empty;

    private string ToParameter() => IsShortHandMethod && Parameters.IsNullOrEmpty()
        ? string.Empty
        : $"({Parameters.ToParameter()})";

    private string ToReturnType() =>
        HasNoBody || IsConstructor
            ? string.Empty
            : IsAsync
                ? ToAsyncReturnType()
                : ToNonAsyncReturnType();

    private string ToStatic() => IsStatic
        ? "static"
        : string.Empty;
}