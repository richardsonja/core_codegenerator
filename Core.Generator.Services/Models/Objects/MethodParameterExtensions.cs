﻿namespace Core.Generator.Services.Models.Objects;

public static class MethodParameterExtensions
{
    public static string ToParameter(this IEnumerable<MethodParameter> methodParameters) =>
        methodParameters
            ?.OrderBy(x => x.Order)
            .ThenBy(x => x.Name)
            .ThenBy(x => x.Type)
            .Select(x => x.ToString())
            .Combine(", ");
}