﻿using Core.Flux;
using Core.Generator.Services.Services.CodeGenerators;
using Core.Generator.Stores.CodeGenerator;
using Moq;
using System;

namespace Core.Generator.Stores.Tests.Fixtures;

public class CodeStoreFixture : IDisposable, IReset
{
    public CodeStore Store;
    public Mock<ICodeGeneratorService> CodeGeneratorService = new();

    public CodeStoreFixture()
    {
        Fluxs.Dispatcher = new AsyncDispatcher();
        Reset();
    }

    public void Dispose()
    {
        Store = null;
    }

    public void Reset()
    {
        CodeGeneratorService.Reset();
        Store = new CodeStore(CodeGeneratorService.Object);
        Fluxs.StoreResolver.Register(Store);
    }
}