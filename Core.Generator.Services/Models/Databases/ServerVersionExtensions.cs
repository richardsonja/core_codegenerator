﻿namespace Core.Generator.Services.Models.Databases;

public static class ServerVersionExtensions
{
    public static int GetMajorVersion(this ServerVersion serverVersion) => 
        (serverVersion?
            .FullVersion?
            .Split('.')
            .ToList()
            .FirstOrDefault())
        .ToInt(-1);
}