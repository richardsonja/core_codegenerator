﻿using Core.Common.Collections;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using FluentAssertions;
using System.Collections.Immutable;
using Xunit;

namespace Core.Generator.Services.Tests.Models.CodeGenerators;

public class CodeGeneratorRequestTests : IClassFixture<DatabaseTableColumnFixture>
{
    private readonly DatabaseTableColumnFixture _databaseTableColumn;

    public CodeGeneratorRequestTests(DatabaseTableColumnFixture databaseTableColumn)
    {
        _databaseTableColumn = databaseTableColumn;
    }

    [Fact]
    public void CodeGeneratorRequest_Clone_ShouldHaveTwoResultsWithoutTouchingOriginal_GivenListInput()
    {
        _databaseTableColumn.Reset();

        var model = new CodeGeneratorRequest(new Table("Bob", "Was Here") { Columns = _databaseTableColumn.TableColumns.AddTo(new TableColumn()).ToImmutableList() });
        var newModel = (model.Clone() as CodeGeneratorRequest);
        newModel.Table.Columns = newModel.Table.Columns.AddTo(new TableColumn()).ToImmutableList();

        model.Table.Columns.Should().HaveCount(1);
        newModel.Table.Columns.Should().HaveCount(2);
    }
}