﻿namespace Core.Generator;

public partial class FormConnect : Form, IFluxViewFor<StorageStore>
{
    private volatile bool _isUpdating;

    public FormConnect()
    {
        InitializeComponent();
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();

        this.OnChange(OnStorageStoreChange);
        this.Dispatch(new LoadConfigurationAction());

        ddlAuthenticationMode.SelectedIndexChanged += (_, _) => OnServerConfigurationChangeCommand();
        txtPassword.TextChanged += (_, _) => OnServerConfigurationChangeCommand();
        txtUserName.TextChanged += (_, _) => OnServerConfigurationChangeCommand();
        txtDatabaseName.TextChanged += (_, _) => OnDatabaseNameChangeCommand();
        btnCancel.Click += (_, _) => OnCancelClickCommand();
        btnSave.Click += (_, _) => OnSaveClickCommand();
    }

    private bool IsAuthenticationTypeWindows =>
        ddlAuthenticationMode.SelectedItem.ToEnum<AuthenticationType>() == AuthenticationType.MsSqlWindows;

    private bool IsInvalidDatabaseName => txtDatabaseName.Text.IsNullOrWhiteSpace();

    private bool IsInvalidLoginData => !IsAuthenticationTypeWindows && (IsInvalidUserName || IsInvalidPassword);

    private bool IsInvalidPassword => txtPassword.Text.IsNullOrWhiteSpace();

    private bool IsInvalidUserName => txtUserName.Text.IsNullOrWhiteSpace();

    private void OnCancelClickCommand() => Close();

    private void OnDatabaseNameChangeCommand() => SetDatabaseRequired();

    private void OnSaveClickCommand()
    {
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();
        // Required so we don't end up dispatching again
        // when changing the check state of an inserted item
        if (_isUpdating)
        {
            return;
        }

        if (IsInvalidDatabaseName || IsInvalidLoginData)
        {
            MessageBox.Show("Required Data Missing", "DB Settings - Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        _isUpdating = true;
        var store = new Lazy<StorageStore>(FluxViewForExtensions.GetStore<StorageStore>).Value;

        var configurationDetail = store.ConfigurationSettings.ConfigurationDetail.Clone() as ConfigurationDetail;
        configurationDetail.ServerConfiguration.DatabaseServer = txtDatabaseName.Text;
        configurationDetail.ServerConfiguration.AuthenticationType = ddlAuthenticationMode.SelectedItem.ToEnum<AuthenticationType>();
        configurationDetail.ServerConfiguration.UserName = txtUserName.Text;

        var payload = new SaveConfigurationSettingsAction(new ConfigurationSettings(txtPassword.Text, configurationDetail));
        this.Dispatch(payload);

        store.ExceptionMessaging();

        _isUpdating = false;
        Close();
    }

    private void OnServerConfigurationChangeCommand() => SetLoginDataRequired(!IsAuthenticationTypeWindows);

    private void OnStorageStoreChange(StorageStore store)
    {
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();
        if (!store.IsLoaded)
        {
            return;
        }

        _isUpdating = true;
        store.ExceptionMessaging();

        var databaseName = store.ConfigurationSettings?.ConfigurationDetail?.ServerConfiguration?.DatabaseServer;
        var userName = store.ConfigurationSettings?.ConfigurationDetail?.ServerConfiguration?.UserName;
        var password = store.ConfigurationSettings?.DatabasePassword;
        var authenticationType = store.ConfigurationSettings?.ConfigurationDetail?.ServerConfiguration?.AuthenticationType;

        txtDatabaseName.Text = databaseName;
        txtPassword.Text = password;
        txtUserName.Text = userName;

        ddlAuthenticationMode.DataSource = Enum.GetValues(typeof(AuthenticationType));
        ddlAuthenticationMode.SelectedItem = authenticationType;

        var loginInfoIsRequired = store.ConfigurationSettings?.ConfigurationDetail?.ServerConfiguration?.IsLoginBasedConnection() != false;
        SetLoginDataRequired(loginInfoIsRequired);

        SetDatabaseRequired();

        _isUpdating = false;
    }

    private void SetDatabaseRequired() => lblDatabaseRequired.Visible = IsInvalidDatabaseName;

    private void SetLoginDataRequired(bool loginInfoIsRequired)
    {
        // TODO: the logic for visible is messed up
        lblPasswordRequired.Visible = loginInfoIsRequired && IsInvalidPassword;
        lblUserNameRequired.Visible = loginInfoIsRequired && IsInvalidUserName;
        txtPassword.Enabled = loginInfoIsRequired;
        txtUserName.Enabled = loginInfoIsRequired;
    }
}