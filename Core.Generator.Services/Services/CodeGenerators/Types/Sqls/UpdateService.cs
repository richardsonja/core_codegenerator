﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

public class UpdateService : StatementBaseService, IUpdateService
{
    public UpdateService(IConfigurationService configurationService) : base(configurationService)
    {
    }
    protected override Option<string> ExecuteTableMessage() => Constants.NotFoundPrimaryKeysMessage;

    protected override IStatement CreateBody(Table table, IEnumerable<StoredProcedureParameter> whereStatementParameters) =>
        new UpdateStatement(table.Schema, table.Name)
            .AddSetValue(SelectSetValues(table))
            .AddParameter(whereStatementParameters);
        
    protected override ImmutableList<StoredProcedureParameter> WhereStatementParameters(Table table) =>
        table.Columns
            .GetPrimaryKeyColumns()
            .Select(x => new StoredProcedureParameter(x))
            .ToImmutableList();

    protected override ImmutableList<StoredProcedureParameter> StoredProcedureParameters(Table table) =>
        table.Columns
            .Select(x => new StoredProcedureParameter(x))
            .ToImmutableList();

    protected override string Prefix(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.CodeRelatedConfiguration.UpdateMethodName;

    protected override bool ShouldExecute(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.OptionsConfiguration.Update == true;

    protected override bool ShouldExecute(Table table) =>
        !table.Columns.GetPrimaryKeyColumns().IsNullOrEmpty();

    protected virtual ImmutableList<StoredProcedureParameter> SelectSetValues(Table table) =>
        table.Columns
            .Except(table.Columns.GetPrimaryKeyColumns())
            .Select(x => new StoredProcedureParameter(x))
            .ToImmutableList();
}