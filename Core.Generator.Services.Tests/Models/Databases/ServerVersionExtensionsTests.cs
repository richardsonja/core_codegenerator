﻿using Core.Generator.Services.Models.Databases;
using FluentAssertions;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Databases;

public class ServerVersionExtensionsTests
{
    [Theory]
    [InlineData(null, -1)]
    [InlineData("", -1)]
    [InlineData("1.1.1.1.1", 1)]
    [InlineData("2.3.4.5", 2)]
    [InlineData("a.2.3.4", -1)]
    [InlineData("12", 12)]
    [InlineData("2,3,4.5", -1)]
    [InlineData("2.a.4.5", 2)]
    public void ServerVersionExtensions_Version_ShouldMatchExpected_GivenInput(string testValue, int expected)
    {
        var results = new ServerVersion
        {
            FullVersion = testValue
        };

        results.GetMajorVersion().Should().Be(expected);
    }
}