﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Sqls;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Sqls;

public class InsertStatementExtensionsTests : IClassFixture<InsertStatementFixture>, IClassFixture<DatabaseTableColumnFixture>
{
    private readonly InsertStatementFixture _insertStatement;
    private readonly DatabaseTableColumnFixture _database;

    public InsertStatementExtensionsTests(InsertStatementFixture insertStatement, DatabaseTableColumnFixture database)
    {
        _insertStatement = insertStatement;
        _database = database;
    }

        

    [Fact]
    public void InsertStatement_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _insertStatement.Reset();
        var item = new StoredProcedureParameter(_database.CreateTableColumns("bob", "was here", "and here", 1));
        var results = _insertStatement.InsertStatement.AddParameter(item).ToString();
        results.TrimInternalExtraSpaces().Should().Be("INSERT INTO [Bob].[Was Here] ([Column1]) VALUES (@Column1)");
    }

    [Fact]
    public void InsertStatement_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenListInput()
    {
        _insertStatement.Reset();

        var tsil = new List<StoredProcedureParameter>
        {
            new (_database.CreateTableColumns("bob", "was here", "and here", 2, "Bob1")),
            new (_database.CreateTableColumns("bob", "was here", "and here", 1, "Bob2"))
        };

        var results = _insertStatement.InsertStatement.AddParameter(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("INSERT INTO [Bob].[Was Here] ([Bob1], [Bob2]) VALUES (@Bob1, @Bob2)");
    }

    [Fact]
    public void InsertStatement_AddParameter_ToString_ShouldNotThrow_GivenEmptyList()
    {
        var body = new List<StoredProcedureParameter>();
        _insertStatement.Reset();

        Action results = () => _insertStatement.InsertStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void InsertStatement_AddParameter_ToString_ShouldNotThrow_GivenNullList()
    {
        List<StoredProcedureParameter> body = null;
        _insertStatement.Reset();

        Action results = () => _insertStatement.InsertStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void InsertStatement_AddParameter_ToString_ShouldNotThrow_GivenNullSingle()
    {
        StoredProcedureParameter body = null;
        _insertStatement.Reset();

        Action results = () => _insertStatement.InsertStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void InsertStatement_Clone_ShouldHaveAllAttributes_GivenSetup()
    {
        _insertStatement.Reset();
        var parameter = new StoredProcedureParameter(_database.CreateTableColumns("bob", "was here", "and here", 1));

        var results = (_insertStatement
                .InsertStatement
                .AddParameter(parameter) as InsertStatement)
            .SetRemoveBrackets(true)
            .Clone() as InsertStatement;

        results.Should().NotBeNull();
        results.Schema.Should().Be("Bob");
        results.Name.Should().Be("Was Here");
        results.ColumnParameters.Should().HaveCount(1);
        results.ColumnParameters.First().ToString().Should().Contain("Column1 = @Column1");
        results.RemoveBrackets.Should().BeTrue();

        results.ToString().TrimInternalExtraSpaces().Should().Be("INSERT INTO Bob.[Was Here] (Column1) VALUES (@Column1)");
    }

    [Theory]
    [InlineData("Bob", "WasHere", "INSERT INTO Bob.WasHere () VALUES ()")]
    [InlineData("Bob", "Was Here", "INSERT INTO Bob.[Was Here] () VALUES ()")]
    [InlineData("Bob Was", "WasHere", "INSERT INTO [Bob Was].WasHere () VALUES ()")]
    [InlineData("Bob Was", "Was Here", "INSERT INTO [Bob Was].[Was Here] () VALUES ()")]
    public void InsertStatement_SetRemoveBrackets_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        string schemaName,
        string tableName,
        string expected)
    {
        _insertStatement.Reset();
        _insertStatement.InsertStatement = new InsertStatement(schemaName, tableName);

        var results = _insertStatement.InsertStatement.SetRemoveBrackets().ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void InsertStatement_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _insertStatement.Reset();

        var results = _insertStatement.InsertStatement.ToString();
        results.TrimInternalExtraSpaces().Should().Be("INSERT INTO [Bob].[Was Here] () VALUES ()");
    }
}