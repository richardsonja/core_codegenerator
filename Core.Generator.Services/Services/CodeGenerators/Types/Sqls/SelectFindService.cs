﻿using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

public class SelectFindService : SelectService, ISelectFindService
{
    public SelectFindService(IConfigurationService configurationService) : base(configurationService)
    {
    }

    protected override string Prefix(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.CodeRelatedConfiguration.FindMethodName;

    protected override bool ShouldExecute(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.OptionsConfiguration.Find == true;

    protected override bool ShouldExecute(Table table) =>
        !table.Columns.GetNonSystemColumns().IsNullOrEmpty();

    protected override ImmutableList<StoredProcedureParameter> StoredProcedureParameters(Table table) =>
        table.Columns
            .GetNonSystemColumns()
            .Select(x => new StoredProcedureParameter(x).SetIsNullable().SetIsNullableWhereFormat())
            .ToImmutableList();
}