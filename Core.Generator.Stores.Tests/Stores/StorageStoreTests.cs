﻿using Core.Flux;
using Core.Flux.Extensions;
using Core.Generator.Services.Models;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Stores.Storage;
using Core.Generator.Stores.Storage.Actions;
using Core.Generator.Stores.Tests.Fixtures;
using FluentAssertions;
using LanguageExt;
using Moq;
using Xunit;

namespace Core.Generator.Stores.Tests.Stores;

public class StorageStorageStoreTests : IFluxViewFor<StorageStore>, IClassFixture<StorageStoreFixture>
{
    private readonly StorageStoreFixture _storageStore;

    public StorageStorageStoreTests(StorageStoreFixture storageStore)
    {
        _storageStore = storageStore;
    }

    [Fact]
    public void StorageStore_LoadConfigurationAction_ShouldHaveError_GivenServiceReturningNone()
    {
        _storageStore.Reset();
        _storageStore.SessionService
            .Setup(x => x.Get<string>(It.IsAny<string>()))
            .Returns(Option<string>.None);
        _storageStore.ConfigurationService
            .Setup(x => x.GetAsync())
            .ReturnsAsync(Option<ConfigurationSettings>.None);
        this.Dispatch(new LoadConfigurationAction());

        this.OnChange(
            t =>
            {
                t.Exceptions.Should().NotBeEmpty();
                t.ConfigurationSettings.Should().BeNull();
            });
    }

    [Fact]
    public void StorageStore_LoadConfigurationAction_ShouldHaveSome_GivenSetup()
    {
        _storageStore.Reset();
        _storageStore.ConfigurationService
            .Setup(x => x.GetAsync())
            .ReturnsAsync(new ConfigurationSettings("madeUp", new ConfigurationDetail()));

        this.Dispatch(new LoadConfigurationAction());

        this.OnChange(
            t =>
            {
                t.Exceptions.Should().BeEmpty();
                t.ConfigurationSettings.Should().NotBeNull();
            });
    }

    [Fact]
    public void StorageStore_SaveConfigurationSettingsAction_ShouldHaveError_GivenPasswordReturningNone()
    {
        _storageStore.Reset();
        _storageStore.ConfigurationService
            .Setup(x => x.SaveConfigurationDetailAsync(It.IsAny<ConfigurationDetail>()))
            .ReturnsAsync(new ConfigurationDetail());

        _storageStore.SessionService
            .Setup(x => x.Save(Constants.PasswordSessionName, "bob"))
            .Returns(Option<bool>.None);
        this.Dispatch(new SaveConfigurationSettingsAction(new ConfigurationSettings("", new ConfigurationDetail())));

        this.OnChange(t => { t.Exceptions.Should().NotBeEmpty(); });
    }

    [Fact]
    public void StorageStore_SaveConfigurationSettingsAction_ShouldHaveError_GivenServiceReturningNone()
    {
        _storageStore.Reset();
        _storageStore.ConfigurationService
            .Setup(x => x.SaveConfigurationDetailAsync(It.IsAny<ConfigurationDetail>()))
            .ReturnsAsync(Option<ConfigurationDetail>.None);

        _storageStore.SessionService
            .Setup(x => x.Save(Constants.PasswordSessionName, "bob"))
            .Returns(Option<bool>.None);
        this.Dispatch(new SaveConfigurationSettingsAction(new ConfigurationSettings("", new ConfigurationDetail())));

        this.OnChange(t => { t.Exceptions.Should().NotBeEmpty(); });
    }

    [Fact]
    public void StorageStore_SaveConfigurationSettingsAction_ShouldHaveSome_GivenSetup()
    {
        _storageStore.Reset();
        _storageStore.ConfigurationService
            .Setup(x => x.SaveConfigurationDetailAsync(It.IsAny<ConfigurationDetail>()))
            .ReturnsAsync(new ConfigurationDetail());

        _storageStore.SessionService
            .Setup(x => x.Save(Constants.PasswordSessionName, "bob"))
            .Returns(true);

        this.OnChange(t => { t.Exceptions.Should().NotBeEmpty(); });

        this.Dispatch(new SaveConfigurationSettingsAction(new ConfigurationSettings("", new ConfigurationDetail())));
        this.OnChange(
            t =>
            {
                t.Exceptions.Should().BeEmpty();
                t.ConfigurationSettings.Should().NotBeNull();
            });
    }
}