﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Services.CodeGenerators.Types.Controllers;
using Core.Generator.Services.Services.Configurations;
using Moq;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Controllers;

public class ControllerCodeGeneratorServiceFixture : IDisposable, IReset
{
    public Mock<IConfigurationService> ConfigurationService = new();
    public ControllerCodeGeneratorService Service;
    public Mock<IWebApiControllerService> WebApiControllerService = new();

    public ControllerCodeGeneratorServiceFixture()
    {
        Reset();
        Service = new ControllerCodeGeneratorService(
            ConfigurationService.Object,
            WebApiControllerService.Object);
    }

    public void Dispose()
    {
        Service = null;
        ConfigurationService = null;
        WebApiControllerService = null;
    }

    public void HardReset()
    {
        ConfigurationService.Reset();
        WebApiControllerService.Reset();
    }

    public void Reset()
    {
        HardReset();

        WebApiControllerService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("WebApiControllerService's BOB WAS HERE");
    }
}