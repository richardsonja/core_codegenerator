﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.Files;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Files;
using Core.Generator.Services.Tests.Fixtures.Services;
using FluentAssertions;
using LanguageExt;
using Moq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.Configurations;

public class ConfigurationServiceTests : IClassFixture<ConfigurationServiceFixture>, IClassFixture<ConfigurationFixture>, IClassFixture<FileDetailsFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly ConfigurationServiceFixture _configurationService;
    private readonly FileDetailsFixture _fileDetails;

    public ConfigurationServiceTests(ConfigurationServiceFixture configurationService, ConfigurationFixture configuration, FileDetailsFixture fileDetails)
    {
        _configurationService = configurationService;
        _configuration = configuration;
        _fileDetails = fileDetails;
    }

    [Fact]
    public async void ConfigurationService_GetAsync_ShouldNotSave_GivenRetrievalBeingValid()
    {
        _configurationService.Reset();
        _configuration.Reset();
        _fileDetails.Reset(_configuration.ConfigurationDetail);

        _configurationService.FileRepository
            .Setup(x => x.GetAsync(It.IsAny<FileDetails>()))
            .ReturnsAsync(_fileDetails.FileDetails);

        _configurationService.SessionService
            .Setup(x => x.Get<string>(It.Is<string>(t => t == Constants.PasswordSessionName)))
            .Returns("YESSSSSS");

        var resultOptions = await _configurationService.Service.GetAsync();
        resultOptions.IsNone.Should().BeFalse();
        resultOptions.IsSome.Should().BeTrue();
        resultOptions.Some(detail =>
        {
            detail.DatabasePassword.Should().Be("YESSSSSS");
            detail.ConfigurationDetail.ServerConfiguration.Should().NotBeNull();
            detail.ConfigurationDetail.ServerConfiguration.DatabaseServer.Should().Be("CG1");
            detail.ConfigurationDetail.ServerConfiguration.UserName.Should().Be("CG2");
        });

        _configurationService.SessionService.Verify(x => x.Get<string>(It.IsAny<string>()), Times.Once());
        _configurationService.FileRepository.Verify(x => x.GetAsync(It.IsAny<FileDetails>()), Times.Once());
        _configurationService.FileRepository.Verify(x => x.SaveAsync(It.IsAny<FileDetails>()), Times.Never);
    }

    [Fact]
    public async void ConfigurationService_GetAsync_ShouldReturnDefaultSettings_GivenRetrievalBeingNone()
    {
        _configurationService.Reset();
        _configuration.Reset();
        _fileDetails.Reset(_configuration.ConfigurationDetail);

        _configurationService.SessionService
            .Setup(x => x.Get<string>(It.IsAny<string>()))
            .Returns("bob");

        _configurationService.FileRepository
            .Setup(x => x.GetAsync(It.IsAny<FileDetails>()))
            .ReturnsAsync(Option<FileDetails>.None);

        _configurationService.FileRepository
            .Setup(x => x.SaveAsync(It.IsAny<FileDetails>()))
            .ReturnsAsync(Option<FileDetails>.None);

        var resultOptions = await _configurationService.Service.GetAsync();
        resultOptions.IsNone.Should().BeTrue();
        resultOptions.IsSome.Should().BeFalse();

        _configurationService.FileRepository.Verify(x => x.GetAsync(It.IsAny<FileDetails>()), Times.Once());
        _configurationService.FileRepository.Verify(x => x.SaveAsync(It.IsAny<FileDetails>()), Times.Once());
    }

    [Fact]
    public async void ConfigurationService_GetAsync_ShouldReturnNone_GivenAllNone()
    {
        _configurationService.Reset();
        _configuration.Reset();
        _fileDetails.Reset(_configuration.ConfigurationDetail);

        _configurationService.SessionService
            .Setup(x => x.Get<string>(It.IsAny<string>()))
            .Returns("bob");

        _configurationService.FileRepository
            .Setup(x => x.GetAsync(It.IsAny<FileDetails>()))
            .ReturnsAsync(Option<FileDetails>.None);

        _configurationService.FileRepository
            .Setup(x => x.SaveAsync(It.IsAny<FileDetails>()))
            .ReturnsAsync(Option<FileDetails>.None);

        var resultOptions = await _configurationService.Service.GetAsync();
        resultOptions.IsNone.Should().BeTrue();
        resultOptions.IsSome.Should().BeFalse();

        _configurationService.FileRepository.Verify(x => x.GetAsync(It.IsAny<FileDetails>()), Times.Once());
        _configurationService.FileRepository.Verify(x => x.SaveAsync(It.IsAny<FileDetails>()), Times.Once());
    }

    [Fact]
    public async void ConfigurationService_GetAsync_ShouldSaveNewFile_GivenRetrievalBeingNone()
    {
        _configurationService.Reset();
        _configuration.Reset();
        _fileDetails.Reset(_configuration.ConfigurationDetail);

        _configurationService.SessionService
            .Setup(x => x.Get<string>(It.IsAny<string>()))
            .Returns("bob");

        _configurationService.FileRepository
            .Setup(x => x.GetAsync(It.IsAny<FileDetails>()))
            .ReturnsAsync(Option<FileDetails>.None);

        _configurationService.FileRepository
            .Setup(x => x.SaveAsync(It.IsAny<FileDetails>()))
            .ReturnsAsync(_fileDetails.FileDetails);

        var resultOptions = await _configurationService.Service.GetAsync();
        resultOptions.IsNone.Should().BeFalse();
        resultOptions.IsSome.Should().BeTrue();
        resultOptions.Some(detail =>
        {
            detail.ConfigurationDetail.ServerConfiguration.Should().NotBeNull();
            detail.ConfigurationDetail.ServerConfiguration.DatabaseServer.Should().Be("CG1");
            detail.ConfigurationDetail.ServerConfiguration.UserName.Should().Be("CG2");
        });

        _configurationService.FileRepository.Verify(x => x.GetAsync(It.IsAny<FileDetails>()), Times.Once());
        _configurationService.FileRepository.Verify(x => x.SaveAsync(It.IsAny<FileDetails>()), Times.Once());
    }
}