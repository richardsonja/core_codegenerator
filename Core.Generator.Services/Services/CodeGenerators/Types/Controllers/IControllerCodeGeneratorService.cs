﻿using Core.Generator.Services.Models.CodeGenerators;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Controllers;

public interface IControllerCodeGeneratorService : ICodeGeneratorServiceType<ImmutableList<DisplayResult>>
{
}