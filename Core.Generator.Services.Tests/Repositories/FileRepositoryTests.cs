﻿using Core.Generator.Services.Models.Files;
using Core.Generator.Services.Tests.Fixtures;
using Core.Generator.Services.Tests.Fixtures.Models.Files;
using Core.Generator.Services.Tests.Fixtures.Repositories;
using FluentAssertions;
using System;
using System.IO;
using Xunit;

namespace Core.Generator.Services.Tests.Repositories;

public class FileRepositoryTests : IClassFixture<FileRepositoryFixture>, IClassFixture<FileDetailsFixture>
{
    private readonly FileDetailsFixture _fileDetails;

    private readonly FileRepositoryFixture _fileRepository;

    public FileRepositoryTests(FileRepositoryFixture fileRepository, FileDetailsFixture fileDetails)
    {
        _fileRepository = fileRepository;
        _fileDetails = fileDetails;
    }

    [Fact]
    public async void FileRepository_GetAsync_ShouldReturnNothing_GivenMissingSetting()
    {
        _fileRepository.Reset();
        _fileDetails.Reset();
        _fileDetails.FileDetails = new FileDetails("Bob.txt", FindEmptyFolder());

        var result = await _fileRepository.Repository.GetAsync(_fileDetails.FileDetails);
        result.IsNone.Should().BeTrue();
        result.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void FileRepository_GetAsync_ShouldReturnValue_GivenValidSetting()
    {
        _fileRepository.Reset();
        _fileDetails.Reset(new TestClass("Bob"));
        _fileDetails.FileDetails = new FileDetails(_fileDetails.FileDetails.FileString, "Bob.txt", FindEmptyFolder());

        var setupResult = await _fileRepository.Repository.SaveAsync(_fileDetails.FileDetails);
        setupResult.IsNone.Should().BeFalse();
        setupResult.IsSome.Should().BeTrue();

        var result = await _fileRepository.Repository.GetAsync(_fileDetails.FileDetails);
        result.IsNone.Should().BeFalse();
        result.IsSome.Should().BeTrue();
        result.Some(x => x.FileString.Should().Contain("Bob"));
    }

    private static string FindEmptyFolder(
        string fileName = "Bob.txt",
        string filePathBase = "C:\\\\GeneratorUnitTests\\",
        int numberOfLoops = 0)
    {
        var filePath = Path.Combine(filePathBase, new Random().Next(int.MaxValue).ToString());
        var filePathAndName = Path.Combine(filePath, fileName);
        if (numberOfLoops > 25)
        {
            throw new OverflowException($"Unable to find empty file location.  Manually empty {filePathBase}");
        }

        return File.Exists(filePathAndName)
            ? FindEmptyFolder(fileName, filePathBase, ++numberOfLoops)
            : filePath;
    }
}