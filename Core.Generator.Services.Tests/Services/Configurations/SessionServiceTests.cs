﻿using Core.Generator.Services.Tests.Fixtures.Services;
using FluentAssertions;
using System;
using Xunit;

namespace Core.Generator.Services.Tests.Services.Configurations;

public class SessionServiceTests : IClassFixture<SessionServiceFixture>
{
    private readonly SessionServiceFixture _sessionService;

    public SessionServiceTests(SessionServiceFixture sessionService)
    {
        _sessionService = sessionService;
    }

    [Fact]
    public void SessionService_Get_ShouldReturnNothing_GivenMissingSetting()
    {
        _sessionService.Reset();
        var sessionName = $"MadeUp{new Random().Next(100000)}";
        var result = _sessionService.Service.Get<string>(sessionName);
        result.IsNone.Should().BeTrue();
        result.IsSome.Should().BeFalse();
    }

    [Fact]
    public void SessionService_Get_ShouldReturnValue_GivenValidSetting()
    {
        _sessionService.Reset();
        var sessionName = "MadeUp";
        var value = "asdfasdasdf";
        var setupResult = _sessionService.Service.Save(sessionName, value);
        setupResult.IsNone.Should().BeFalse();
        setupResult.IsSome.Should().BeTrue();

        var result = _sessionService.Service.Get<string>(sessionName);
        result.IsNone.Should().BeFalse();
        result.IsSome.Should().BeTrue();
        result.Some(x => x.Should().Be(value));
    }
}