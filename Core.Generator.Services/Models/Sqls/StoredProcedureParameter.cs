﻿namespace Core.Generator.Services.Models.Sqls;

public class StoredProcedureParameter : ICloneable, IBaseColumnRow
{
    public StoredProcedureParameter(IBaseColumnRow baseColumnRow)
    {
        TableSchema = baseColumnRow?.TableSchema;
        Name = baseColumnRow?.Name;
        DataType = baseColumnRow?.DataType ?? SqlDbType.NVarChar;
        TableName = baseColumnRow?.TableName;
        TableAbbreviation = baseColumnRow?.TableName.CreateAbbreviation();
        OriginalDataTypeString = baseColumnRow?.OriginalDataTypeString;
        CharacterMaxLength = baseColumnRow?.CharacterMaxLength;
        Percision = baseColumnRow?.Percision;
        Scale = baseColumnRow?.Scale;
        IsNullable = baseColumnRow?.IsNullable ?? false;
        IsNullableWhereFormat = false;
    }

    private StoredProcedureParameter(
        string columnName,
        string tableSchema,
        string tableName,
        string tableAbbreviation,
        bool isTableAbbreviationDisabled,
        string overriddenWhereStatementValue,
        SqlDbType dataType,
        string originalDataTypeString,
        int? characterMaxLength,
        int? percision,
        int? scale,
        bool isNullable,
        bool isNullableWhereFormat,
        bool removeBrackets,
        bool isWhereStatement)
    {
        Name = columnName;
        TableSchema = tableSchema;
        TableName = tableName;
        TableAbbreviation = tableAbbreviation;
        IsTableAbbreviationDisabled = isTableAbbreviationDisabled;
        OverriddenWhereStatementValue = overriddenWhereStatementValue;
        DataType = dataType;
        OriginalDataTypeString = originalDataTypeString;
        CharacterMaxLength = characterMaxLength;
        Percision = percision;
        Scale = scale;
        IsNullable = isNullable;
        IsNullableWhereFormat = isNullableWhereFormat;
        RemoveBrackets = removeBrackets;
        IsWhereStatement = isWhereStatement;
    }

    public int? CharacterMaxLength { get; }

    public SqlDbType DataType { get; }

    public string FormattedColumnName => AddBrackets(Name);

    public string FormattedParameterName =>
        $"@{Name?.Replace(" ", string.Empty)}";

    public bool IsNullable { get; private set; }

    public bool IsNullableWhereFormat { get; private set; }

    public bool IsTableAbbreviationDisabled { get; private set; }

    public bool IsWhereStatement { get; private set; }

    public string Name { get; }

    public string OriginalDataTypeString { get; }

    public string OverriddenWhereStatementValue { get; private set; }

    public int? Percision { get; }

    public bool RemoveBrackets { get; private set; }

    public int? Scale { get; }

    public string TableAbbreviation { get; private set; }

    public string TableName { get; }

    public string TableSchema { get; }

    public object Clone() => new StoredProcedureParameter(
        Name,
        TableSchema,
        TableName,
        TableAbbreviation,
        IsTableAbbreviationDisabled,
        OverriddenWhereStatementValue,
        DataType,
        OriginalDataTypeString,
        CharacterMaxLength,
        Percision,
        Scale,
        IsNullable,
        IsNullableWhereFormat,
        RemoveBrackets,
        IsWhereStatement);

    public StoredProcedureParameter SetIsNullable(bool isNullable = true)
    {
        IsNullable = isNullable;
        return this;
    }

    public StoredProcedureParameter SetIsNullableWhereFormat(bool isNullableWhereFormat = true)
    {
        IsNullableWhereFormat = isNullableWhereFormat;
        return this;
    }

    public StoredProcedureParameter SetIsTableAbbreviationDisabled(bool isTableAbbreviationDisabled = true)
    {
        IsTableAbbreviationDisabled = isTableAbbreviationDisabled;
        return this;
    }

    public StoredProcedureParameter SetIsWhereStatement(bool isWhereStatement = true)
    {
        IsWhereStatement = isWhereStatement;
        return this;
    }

    public StoredProcedureParameter SetOverriddenWhereStatementValue(string overriddenWhereStatementValue)
    {
        OverriddenWhereStatementValue = overriddenWhereStatementValue;
        return this;
    }

    public StoredProcedureParameter SetRemoveBrackets(bool removeBrackets = true)
    {
        RemoveBrackets = removeBrackets;
        return this;
    }

    public StoredProcedureParameter SetTableAbbreviation(string tableAbbreviation)
    {
        TableAbbreviation = tableAbbreviation;
        return this;
    }

    public override string ToString() =>
        IsWhereStatement
            ? $"{ToFullName()} = {ToWhereStatementValue()}"
            : $"{FormattedParameterName} {ConvertDataType()}{ConvertNullable()}";

    private string AddBrackets(string value) =>
        value.FormatBracketedValue(RemoveBrackets);

    private string ConvertDataType()
    {
        switch (DataType)
        {
            case SqlDbType.Char
                or SqlDbType.NChar
                or SqlDbType.NVarChar
                or SqlDbType.VarChar:
                {
                    var length = CharacterMaxLength is null or -1
                        ? "MAX"
                        : CharacterMaxLength.ToString();
                    return $"{OriginalDataTypeString.ToUpper()}({length})";
                }
            case SqlDbType.Decimal:
                return $"{OriginalDataTypeString.ToUpper()}({Percision}, {Scale})";

            default:
                return OriginalDataTypeString.ToUpper();
        }
    }

    private string ConvertNullable() =>
        IsNullable
            ? " = NULL"
            : string.Empty;

    private string ToFullName() => $"{ToTableAbbreviation()}{FormattedColumnName}";

    private string ToTableAbbreviation() =>
        IsTableAbbreviationDisabled
            ? string.Empty
            : $"{TableAbbreviation}.";

    private string ToWhereStatementValue() =>
        OverriddenWhereStatementValue.IsNullOrEmpty()
            ? IsNullableWhereFormat
                ? $" ISNULL({FormattedParameterName}, {ToFullName()}) "
                : FormattedParameterName
            : OverriddenWhereStatementValue.Replace(" ", string.Empty);
}