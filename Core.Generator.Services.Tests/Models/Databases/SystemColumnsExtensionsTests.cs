﻿using Core.Generator.Services.Models.Databases;
using FluentAssertions;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Databases;

public class SystemColumnsExtensionsTests
{
    [Theory]
    [InlineData("active_ind", true)]
    [InlineData("activeind", true)]
    [InlineData("active", true)]
    [InlineData("isactive", true)]
    [InlineData("is_active", true)]
    [InlineData("void", true)]
    [InlineData("isvoid", true)]
    [InlineData("is_void", true)]
    [InlineData("isdeleted", true)]
    [InlineData("isdelete", true)]
    [InlineData("isdeleteByBob", true)]
    [InlineData("BobisdeleteByBob", true)]
    [InlineData("", false)]
    [InlineData("bob", false)]
    [InlineData(null, false)]
    public void SystemColumnsExtensions_IsActive_ShouldMatchExpected_GivenInput(string testValue, bool expected)
    {
        testValue.IsActive().Should().Be(expected);
    }

    [Theory]
    [InlineData("create_by,", true)]
    [InlineData("createby,", true)]
    [InlineData("created_by,", true)]
    [InlineData("createdby,", true)]
    [InlineData("createdbyid", true)]
    [InlineData("createdbyidBob", true)]
    [InlineData("BobcreatedbyidBob", true)]
    [InlineData("", false)]
    [InlineData("bob", false)]
    [InlineData(null, false)]
    public void SystemColumnsExtensions_IsCreateBy_ShouldMatchExpected_GivenInput(string testValue, bool expected)
    {
        testValue.IsCreateBy().Should().Be(expected);
    }

    [Theory]
    [InlineData("create_date,", true)]
    [InlineData("createdate,", true)]
    [InlineData("created_date,", true)]
    [InlineData("createddate,", true)]
    [InlineData("createdon,", true)]
    [InlineData("creationdate", true)]
    [InlineData("creationdateBob", true)]
    [InlineData("BobcreationdateBob", true)]
    [InlineData("", false)]
    [InlineData("bob", false)]
    [InlineData(null, false)]
    public void SystemColumnsExtensions_IsCreateDate_ShouldMatchExpected_GivenInput(string testValue, bool expected)
    {
        testValue.IsCreateDate().Should().Be(expected);
    }

    [Theory]
    [InlineData("modify_by,", true)]
    [InlineData("modifyby,", true)]
    [InlineData("modifybyid,", true)]
    [InlineData("modified_by,", true)]
    [InlineData("modifiedby,", true)]
    [InlineData("modifiedbyid,", true)]
    [InlineData("modificationby,", true)]
    [InlineData("modificationbyid,", true)]
    [InlineData("updatedby,", true)]
    [InlineData("updatebyBob", true)]
    [InlineData("BobupdatebyBob", true)]
    [InlineData("", false)]
    [InlineData("bob", false)]
    [InlineData(null, false)]
    public void SystemColumnsExtensions_IsModifyBy_ShouldMatchExpected_GivenInput(string testValue, bool expected)
    {
        testValue.IsModifyBy().Should().Be(expected);
    }

    [Theory]
    [InlineData("modify_date,", true)]
    [InlineData("modifydate,", true)]
    [InlineData("modified_date,", true)]
    [InlineData("modifieddate,", true)]
    [InlineData("modificationdate,", true)]
    [InlineData("modifiedon,", true)]
    [InlineData("updateddate,", true)]
    [InlineData("updatedate", true)]
    [InlineData("updatedateBob", true)]
    [InlineData("BobupdatedateBob", true)]
    [InlineData("", false)]
    [InlineData("bob", false)]
    [InlineData(null, false)]
    public void SystemColumnsExtensions_IsModifyDate_ShouldMatchExpected_GivenInput(string testValue, bool expected)
    {
        testValue.IsModifyDate().Should().Be(expected);
    }

    [Theory]
    [InlineData("permanentname,", true)]
    [InlineData("permname,", true)]
    [InlineData("permanent", true)]
    [InlineData("permanentBob", true)]
    [InlineData("BobpermanentBob", true)]
    [InlineData("", false)]
    [InlineData("bob", false)]
    [InlineData(null, false)]
    public void SystemColumnsExtensions_IsPermanentName_ShouldMatchExpected_GivenInput(string testValue, bool expected)
    {
        testValue.IsPermanentName().Should().Be(expected);
    }

    [Theory]
    [InlineData("timestamp", true)]
    [InlineData("tstamp,", true)]
    [InlineData("time_stamp", true)]
    [InlineData("timestampBob", true)]
    [InlineData("BobtimestampBob", true)]
    [InlineData("", false)]
    [InlineData("bob", false)]
    [InlineData(null, false)]
    public void SystemColumnsExtensions_IsTimeStamp_ShouldMatchExpected_GivenInput(string testValue, bool expected)
    {
        testValue.IsTimeStamp().Should().Be(expected);
    }
}