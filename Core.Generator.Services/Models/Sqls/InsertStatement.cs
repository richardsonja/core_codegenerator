﻿namespace Core.Generator.Services.Models.Sqls;

public class InsertStatement : ICloneable, IStatement
{
    public InsertStatement(string schema, string name)
    {
        Schema = schema.IsNullOrEmpty() ? "dbo" : schema;
        Name = name;
    }

    private InsertStatement(
        string schema,
        string name,
        bool removeBrackets,
        ImmutableList<StoredProcedureParameter> whereParameters)
        : this(schema, name)
    {
        RemoveBrackets = removeBrackets;
        ColumnParameters = whereParameters.Clone().ToImmutableList();
    }

    public string Name { get; }

    public bool RemoveBrackets { get; private set; }

    public string Schema { get; }
        
    public ImmutableList<StoredProcedureParameter> ColumnParameters { get; private set; } = new List<StoredProcedureParameter>().ToImmutableList();

    public IStatement AddParameter(StoredProcedureParameter parameter)
    {
        if (parameter == null)
        {
            return this;
        }
        var clone = parameter.Clone() as StoredProcedureParameter;
        SetIsWhereStatement(clone);
        ColumnParameters = ColumnParameters.AddTo(clone).ToImmutableList();

        return this;
    }

    public IStatement AddParameter(IEnumerable<StoredProcedureParameter> parameter)
    {
        if (parameter.IsNullOrEmpty())
        {
            return this;
        }

        var clone = parameter.Clone().Select(SetIsWhereStatement).ToList();
        ColumnParameters = ColumnParameters.AddTo(clone).ToImmutableList();

        return this;
    }


    public object Clone() => new InsertStatement(Schema, Name, RemoveBrackets, ColumnParameters);

    public InsertStatement SetRemoveBrackets(bool removeBrackets = true)
    {
        RemoveBrackets = removeBrackets;
        ColumnParameters = ColumnParameters
            .Clone()
            .Select(x=> x.SetRemoveBrackets(RemoveBrackets))
            .ToImmutableList();
        return this;
    }

    public override string ToString() => $"{Environment.NewLine}INSERT INTO {ToFullName()}{Environment.NewLine}({ToColumnName()}){Environment.NewLine}VALUES ({ToValueName()})";

    private StoredProcedureParameter SetIsWhereStatement(StoredProcedureParameter parameter) => 
        parameter
            .SetIsWhereStatement(true)
            .SetIsTableAbbreviationDisabled(true)
            .SetRemoveBrackets(RemoveBrackets);

    private string AddBrackets(string value) => 
        value.FormatBracketedValue(RemoveBrackets);

    private string ToFullName() =>
        $"{AddBrackets(Schema)}.{AddBrackets(Name)}";

    private string ToColumnName() =>
        ColumnParameters.ToColumnNames();

    private string ToValueName() =>
        ColumnParameters.ToParameterNames();
}