﻿using Core.Generator.Services.Models.Objects;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Objects;

public class GetterSetterObjectFixture : IDisposable, IReset
{
    public GetterSetterObject GetterSetterObject;

    public GetterSetterObjectFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        GetterSetterObject = null;
    }

    public void Reset()
    {
        GetterSetterObject = new GetterSetterObject(GetterSetterType.Get);
    }
}