﻿namespace Core.Generator.Stores.Storage;

public class StorageStore : Store, IActionException
{
    private readonly IConfigurationService _configurationService;
    private readonly ISessionService _sessionService;

    public StorageStore(IConfigurationService configurationService, ISessionService sessionService)
    {
        _configurationService = configurationService;
        _sessionService = sessionService;

        Dispatcher.Register<LoadConfigurationAction>(LoadConfiguration);
        Dispatcher.Register<SaveConfigurationSettingsAction>(SaveConfigurationSettings);
    }

    public ConfigurationSettings ConfigurationSettings { get; private set; }

    public List<ActionException> Exceptions { get; private set; }

    public override bool IsLoaded => Exceptions.IsNullOrEmpty() || ConfigurationSettings is not null;

    private async void LoadConfiguration(LoadConfigurationAction action)
    {
        Exceptions = new();
        var resultOption = await _configurationService.GetAsync();

        resultOption.IfNone(() => Exceptions.Add(new ActionException(nameof(LoadConfigurationAction))));
        resultOption.IfSome(settings => ConfigurationSettings = settings);

        EmitChange();
    }

    private async void SaveConfigurationSettings(SaveConfigurationSettingsAction action)
    {
        Exceptions = new();
        if (action?.ConfigurationSettings.Clone() is not ConfigurationSettings newCopy)
        {
            Exceptions.Add(new ActionException($"{nameof(SaveConfigurationSettingsAction)} is empty"));
            return;
        }

        await _configurationService.SaveConfigurationDetailAsync(newCopy.ConfigurationDetail.Clone() as ConfigurationDetail)
            .Match(
                _ => SavePassword(action.ConfigurationSettings.DatabasePassword),
                () => Exceptions.Add(new ActionException(nameof(SaveConfigurationSettingsAction))));
    }

    private void SavePassword(string password)
    {
        _sessionService.Save(Constants.PasswordSessionName, password)
            .Some(_ => LoadConfiguration(new LoadConfigurationAction()))
            .None(
                () =>
                {
                    LoadConfiguration(new LoadConfigurationAction());
                    Exceptions.Add(new ActionException(nameof(password)));
                });

        EmitChange();
    }
}