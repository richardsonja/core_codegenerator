﻿namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public interface IDeleteRequestResponseService : ITypeService<bool>
{
}