﻿namespace Core.Generator.Services.Models.CodeGenerators;

public class CodeGeneratorResult : ICloneable
{
    public CodeGeneratorResult(ImmutableList<TableColumn> columns)
    {
        Columns = columns?.Clone().ToImmutableList();
        DisplayResults = new List<DisplayResult>().ToImmutableList();
    }

    private CodeGeneratorResult(
        ImmutableList<TableColumn> columns, 
        ImmutableList<DisplayResult> displayResults) : this(columns) => 
        DisplayResults = displayResults?.Clone().ToImmutableList();

    public ImmutableList<TableColumn> Columns { get; }

    public ImmutableList<DisplayResult> DisplayResults { get; private set; }

    public CodeGeneratorResult AddDisplayResult(DisplayResult displayResult)
    {
        DisplayResults = DisplayResults.AddTo(displayResult).ToImmutableList();

        return this;
    }

    public CodeGeneratorResult AddDisplayResult(IEnumerable<DisplayResult> displayResults)
    {
        DisplayResults = DisplayResults.AddTo(displayResults).ToImmutableList();

        return this;
    }

    public object Clone() => new CodeGeneratorResult(Columns, DisplayResults);

    public CodeGeneratorResult OrderList()
    {
        if (DisplayResults.IsNullOrEmpty())
        {
            return this;
        }

        DisplayResults = DisplayResults.Select(x => x.OrderList()).OrderList();
        return this;
    }
}