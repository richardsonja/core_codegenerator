﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public class ModelExtensionsService : IModelExtensionsService
{
    private readonly IConfigurationService _configurationService;

    public ModelExtensionsService(IConfigurationService configurationService)
    {
        _configurationService = configurationService;
    }

    public async Task<Option<string>> CreateOutputAsync(CodeGeneratorRequest request)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<string>.None;
        }

        var configOption = await _configurationService.GetAsync();
        return configOption.Match(
            x => CreateClass(x, request.Table),
            () => Option<string>.None);
    }

    private static Option<string> CreateClass(ConfigurationSettings configuration, Table table)
    {
        var methods = GetMethods(table).ToImmutableList();
        if (methods.IsNullOrEmpty())
        {
            return Option<string>.None;
        }

        var classObject = new ClassObject(GetClassName(configuration, table), GetNamespace(configuration, table))
            .SetIsStatic()
            .AddUsingStatements(GetUsingStatements())
            .AddMethods(methods);

        return classObject.ToString();
    }

    private static string CreatedBy(Table table)
    {
        var keys = table.Columns
            .GetIsCreateByColumns()
            .Select(x => $"{x.CamelCaseName} => {{{table.CamelCaseFormattedName}.{x.CamelCaseName}}}")
            .Combine(";");
        return keys.IsNullOrEmpty()
            ? string.Empty
            : $"by {keys}";
    }

    private static string CreatedDate(Table table)
    {
        var keys = table.Columns
            .GetIsCreateDateColumns()
            .Select(x => $"{x.CamelCaseName} => {{{table.CamelCaseFormattedName}.{x.CamelCaseName}}}")
            .Combine(";");
        return keys.IsNullOrEmpty()
            ? "on {DateTime.Now}"
            : $"on {keys}";
    }

    private static string GetClassName(ConfigurationSettings configuration, Table table) =>
        $"{table.FormattedName}Extensions";

    private static ImmutableList<Method> GetMethods(Table table)
    {
        var methods = new List<Method>();
        var primaryKeys = table.Columns
            .GetPrimaryKeyColumns()
            .Select(x => $"{x.CamelCaseName} => {{{table.CamelCaseFormattedName}.{x.CamelCaseName}}}")
            .Combine("; ");

        var modifyBy = ModifyBy(table);
        var modifyDate = ModifyDate(table);
        var createdBy = CreatedBy(table);
        var createdDate = CreatedDate(table);

        var deleted = SetExpressionAction(Constants.MethodDeleteAction, table)
            .AddBody($"$\"{primaryKeys} was deleted {modifyBy} {modifyDate}\"");

        var destroyed = SetExpressionAction(Constants.MethodDestroyAction, table)
            .AddBody($"$\"{primaryKeys} was destroyed {modifyBy} {modifyDate}\"");

        var updated = SetExpressionAction(Constants.MethodUpdateAction, table)
            .AddBody($"$\"{primaryKeys} was updated {modifyBy} {modifyDate}\"");

        var inserted = SetExpressionAction(Constants.MethodInsertAction, table)
            .AddBody($"$\"{primaryKeys} was created {createdBy} {createdDate}\"");

        return methods
            .AddTo(deleted)
            .AddTo(destroyed)
            .AddTo(updated)
            .AddTo(inserted)
            .ToImmutableList();
    }

    private static string GetNamespace(ConfigurationSettings configuration, Table table) =>
        $"{configuration.ConfigurationDetail.CodeRelatedConfiguration.Namespace}.Models.{table.Schema}";

    private static ImmutableList<string> GetUsingStatements() =>
        new List<string>
        {
            "System",
            "System.Collections.Generic"
        }.ToImmutableList();

    private static string ModifyBy(Table table)
    {
        var keys = table.Columns
            .GetIsModifyByColumns()
            .Select(x => $"{x.CamelCaseName} => {{{table.CamelCaseFormattedName}.{x.CamelCaseName}}}")
            .Combine(";");
        return keys.IsNullOrEmpty()
            ? string.Empty
            : $"by {keys}";
    }

    private static string ModifyDate(Table table)
    {
        var keys = table.Columns
            .GetIsModifyDateColumns()
            .Select(x => $"{x.CamelCaseName} => {{{table.CamelCaseFormattedName}.{x.CamelCaseName}}}")
            .Combine(";");
        return keys.IsNullOrEmpty()
            ? "on {DateTime.Now}"
            : $"on {keys}";
    }

    private static Method SetExpressionAction(string methodName, Table table) =>
        new Method(methodName)
            .AddParameters(new MethodParameter(table.FormattedName, table.CamelCaseFormattedName, 1))
            .AddReturnType(typeof(string))
            .SetIsShortHandMethod()
            .SetIsStatic();
}