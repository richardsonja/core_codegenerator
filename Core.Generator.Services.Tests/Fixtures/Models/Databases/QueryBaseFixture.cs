﻿using Core.Generator.Services.Models.Databases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Core.Generator.Services.Tests.Fixtures.Models.Databases;

public class QueryBaseFixture : IDisposable, IReset
{
    public QueryBase<SqlParameter> MicrosoftQueryBase;
    public QueryBase<MySqlParameter> MySqlQueryBase;

    public QueryBaseFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        MicrosoftQueryBase = null;
        MySqlQueryBase = null;
    }

    public void Reset()
    {
        MicrosoftQueryBase = new QueryBase<SqlParameter>("select * from 42", new List<SqlParameter>());
        MySqlQueryBase = new QueryBase<MySqlParameter>("select * from 42", new List<MySqlParameter>());
    }

    public void SetEmpty()
    {
        MicrosoftQueryBase = new QueryBase<SqlParameter>();
        MySqlQueryBase = new QueryBase<MySqlParameter>();
    }

    public void SetNull()
    {
        MicrosoftQueryBase = null;
        MySqlQueryBase = null;
    }

    public void SetParameters()
    {
        MicrosoftQueryBase = new QueryBase<SqlParameter>(
            "select * from 42",
            new List<SqlParameter>
            {
                new("string", SqlDbType.NVarChar),
                new("string2", SqlDbType.NVarChar)
            });
        MySqlQueryBase = new QueryBase<MySqlParameter>(
            "select * from 42",
            new List<MySqlParameter>
            {
                new("string", SqlDbType.NVarChar),
                new("string2", SqlDbType.NVarChar)
            });
    }
}