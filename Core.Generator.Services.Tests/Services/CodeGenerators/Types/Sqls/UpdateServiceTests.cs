﻿using Core.Common.Generics;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Sqls;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Collections.Immutable;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Sqls;

public class UpdateServiceTests :
    IClassFixture<UpdateServiceFixture>,
    IClassFixture<DatabaseTableColumnFixture>,
    IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly UpdateServiceFixture _service;

    public UpdateServiceTests(UpdateServiceFixture service, DatabaseTableColumnFixture databaseTableColumn, ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }
        
    [Fact]
    public async void UpdateService_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void UpdateService_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.AddCreateBy(2);
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Update_BobTable]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) DROP PROCEDURE [bob].[Update_BobTable] CREATE PROCEDURE [bob].[Update_BobTable] @Bob2 NVARCHAR(MAX), @BobColumn NVARCHAR(MAX) AS BEGIN UPDATE [bob].[BobTable] SET [Bob2] = @Bob2 WHERE [BobColumn] = @BobColumn END GO"));
    }

    [Fact]
    public async void UpdateService_CreateOutput_ShouldReturnNone_GivenConfigIsFalse()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Update = false;
        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void UpdateService_CreateOutput_ShouldReturnNone_GivenEmptySelectEmptyTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void UpdateService_CreateOutput_ShouldReturnNone_GivenEmptySelectNullTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(null));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void UpdateService_CreateOutput_ShouldReturnNone_GivenNoPrimaryKeys()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var model = _databaseTableColumn.Database.Tables.First();
        model.Columns = model.Columns
            .Select(x => { x.IsIdentity = false; x.IsIndex = false; return x; })
            .ToImmutableList();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(model));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("-- NO PRIMARY KEYS FOUND --"));
    }

    [Fact]
    public async void UpdateService_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(null);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }
}