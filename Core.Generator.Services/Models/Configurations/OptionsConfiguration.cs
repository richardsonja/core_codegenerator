﻿namespace Core.Generator.Services.Models.Configurations;

public class OptionsConfiguration : ICloneable
{
    public bool? ActionLog { get; set; }

    public bool? ActiveList { get; set; }

    public bool? BulkCreate { get; set; }

    public bool? BulkDelete { get; set; }

    public bool? BulkLogicalDelete { get; set; }

    public bool? BulkUpdate { get; set; }

    public bool? Delete { get; set; }

    public bool? Find { get; set; }

    public bool? GenerateStoredProcedures { get; set; }

    public bool? GetByPrimaryKeys { get; set; }

    public bool? Insert { get; set; }

    public bool? LogicalDelete { get; set; }

    public bool? Update { get; set; }

    public object Clone() =>
        new OptionsConfiguration
        {
            ActionLog = ActionLog,
            ActiveList = ActiveList,
            BulkCreate = BulkCreate,
            BulkDelete = BulkDelete,
            BulkLogicalDelete = BulkLogicalDelete,
            BulkUpdate = BulkUpdate,
            Delete = Delete,
            Find = Find,
            GetByPrimaryKeys = GetByPrimaryKeys,
            Insert = Insert,
            LogicalDelete = LogicalDelete,
            Update = Update,
            GenerateStoredProcedures = GenerateStoredProcedures
        };
}