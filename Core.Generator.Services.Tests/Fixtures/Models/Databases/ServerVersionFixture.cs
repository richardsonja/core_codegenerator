﻿using Core.Generator.Services.Models.Databases;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Databases;

public class ServerVersionFixture : IDisposable, IReset
{
    public ServerVersion ServerVersion;

    public ServerVersionFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        ServerVersion = null;
    }

    public void Reset()
    {
        SetServerVersion("18.1.1.1");
    }

    public void SetEmptyServerVersion()
    {
        ServerVersion = new();
    }

    public void SetNullServerVersion()
    {
        ServerVersion = null;
    }

    public void SetServerVersion(string fullVersion)
    {
        ServerVersion = new()
        {
            FullVersion = fullVersion
        };
    }
}