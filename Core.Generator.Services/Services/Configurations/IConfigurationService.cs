﻿namespace Core.Generator.Services.Services.Configurations;

public interface IConfigurationService
{
    Task<Option<ConfigurationSettings>> GetAsync();

    Task<Option<ConfigurationDetail>> SaveConfigurationDetailAsync(ConfigurationDetail configuration);
}