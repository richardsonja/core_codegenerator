﻿namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public interface IDestroyRequestResponseService : ITypeService<bool>
{
}