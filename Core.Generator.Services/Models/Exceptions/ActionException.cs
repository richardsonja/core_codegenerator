﻿using System.Runtime.CompilerServices;

namespace Core.Generator.Services.Models.Exceptions;

public class ActionException : Exception
{
    private const string ErrorMessage = "Error During Processing Action";

    public ActionException(
        string actionName,
        [CallerMemberName] string memberName = null,
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0)
        : base(ErrorMessage)
    {
        ActionName = actionName;
        MemberName = memberName;
        SourceFilePath = sourceFilePath;
        SourceLineNumber = sourceLineNumber;
    }

    public ActionException(
        string actionName,
        Exception innerException,
        [CallerMemberName] string memberName = null,
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0)
        : base(ErrorMessage, innerException)
    {
        ActionName = actionName;
        MemberName = memberName;
        SourceFilePath = sourceFilePath;
        SourceLineNumber = sourceLineNumber;
    }

    public string ActionName { get; }

    public string MemberName { get; }

    public string SourceFilePath { get; }

    public int SourceLineNumber { get; }
}