﻿using AutoMapper;
using Core.Common.Data;
using Core.Generator.Services.Repositories.Database.Connectors;

namespace Core.Generator.Services.Repositories.Database;

public class MySqlRepository : IMySqlRepository
{
    private readonly IMapper _mapper;
    private readonly IMySqlConnector _sqlConnector;

    public MySqlRepository(IMySqlConnector sqlConnector, IMapper mapper)
    {
        _sqlConnector = sqlConnector;
        _mapper = mapper;
    }

    public async Task<Option<ImmutableList<Models.Databases.Database>>> GetDatabaseListFromServerAsync()
    {
        var sql = @$"SELECT
                    0 AS {DatabaseColumnConstants.DatabaseId},
                    SCHEMA_NAME AS {DatabaseColumnConstants.DatabaseName}
                    FROM INFORMATION_SCHEMA.SCHEMATA
                    ORDER BY SCHEMA_NAME";
        var results = await _sqlConnector.ExecuteSqlAsync(new MySqlQuery(sql));
        if (results.IsNone)
        {
            return Option<ImmutableList<Models.Databases.Database>>.None;
        }

        var converted = results.Match(
            s => s.IsNullOrEmpty()
                ? null
                // Lesson learn is to use select so the mapping error will show up correctly in error logs
                : results.AsEnumerable()
                    .SelectMany(x => x.AsEnumerable().Select(y => _mapper.Map<Models.Databases.Database>(y)))
                    .ToList(),
            () => null);
        return converted.IsNullOrEmpty() || converted.FirstOrDefault().Name.IsNullOrEmpty()
            ? Option<ImmutableList<Models.Databases.Database>>.None
            : converted.ToImmutableList();
    }

    public async Task<Option<ImmutableList<TableColumn>>> GetTableAndColumnsAsync(IEnumerable<string> listOfDatabases)
    {
        var listOfTableAndColumnSql = listOfDatabases.Select(TableAndColumnSql).ToList();
        var sql = listOfTableAndColumnSql.Combine(" UNION ");
        var results = await _sqlConnector.ExecuteSqlAsync(new MySqlQuery(sql));
        if (results.IsNone)
        {
            return Option<ImmutableList<TableColumn>>.None;
        }

        var converted = results.Match(
            s => s.IsNullOrEmpty()
                ? null
                // Lesson learn is to use select so the mapping error will show up correctly in error logs
                : results.AsEnumerable()
                    .SelectMany(x => x.AsEnumerable().Select(y => _mapper.Map<TableColumn>(y)))
                    .ToList(),
            () => null);
        return converted.IsNullOrEmpty() || converted.FirstOrDefault().Name.IsNullOrEmpty()
            ? Option<ImmutableList<TableColumn>>.None
            : converted.ToImmutableList();
    }

    private string TableAndColumnSql(string databaseName)
    {
        string sql = @"SELECT
                                    '{0:G}' AS {1},
									`TABLE_SCHEMA` AS  {2},
                                    `TABLE_NAME` AS {3},
                                    `COLUMN_NAME` AS {4},
                                    `ORDINAL_POSITION` AS {5},
                                    `DATA_TYPE` AS  {6},
                                    `NUMERIC_PRECISION` AS {7},
                                    `NUMERIC_SCALE` AS {8},
                                    `IS_NULLABLE` AS {9},
                                    `CHARACTER_MAXIMUM_LENGTH` AS {10},
                                    (SELECT count(p.`COLUMN_NAME`)
                                            FROM `information_schema`.`columns` AS p
                                            WHERE (p.`TABLE_SCHEMA` = '{0:G}')
                                              AND (p.`TABLE_NAME` = `TABLE_NAME`)
                                              AND (p.`COLUMN_NAME` = columns.COLUMN_NAME)
                                              AND (p.`COLUMN_KEY` = 'PRI')) AS {11},
                                    (SELECT count(p.`COLUMN_NAME`)
                                            FROM `information_schema`.`columns` AS p
                                            WHERE (p.`TABLE_SCHEMA` = '{0:G}')
                                              AND (p.`TABLE_NAME` = `TABLE_NAME`)
                                              AND (p.`COLUMN_NAME` = columns.COLUMN_NAME)
                                              AND (p.`COLUMN_KEY` = 'PRI')) AS {12},
                                   NULL AS {13},
                                   NULL AS {14},
                                   NULL AS {15}
                                    FROM `columns` AS columns
                                    WHERE `TABLE_SCHEMA` = '{0:G}' ";

        return string.Format(
            sql,
            databaseName,
            DatabaseColumnConstants.TableDatabaseName,
            DatabaseColumnConstants.TableName,
            DatabaseColumnConstants.TableSchema,
            DatabaseColumnConstants.ColumnName,
            DatabaseColumnConstants.OrdinalPosition,
            DatabaseColumnConstants.DataType,
            DatabaseColumnConstants.NumericPrecision,
            DatabaseColumnConstants.NumericScale,
            DatabaseColumnConstants.IsNullable,
            DatabaseColumnConstants.CharacterMaximumLength,
            DatabaseColumnConstants.IsIdentity,
            DatabaseColumnConstants.IsIndex,
            DatabaseColumnConstants.ForeignTable,
            DatabaseColumnConstants.ForeignColumn,
            DatabaseColumnConstants.ForeignSchema);
    }
}