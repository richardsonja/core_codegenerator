﻿using Core.Generator.Services.Models.Objects;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Objects;

public class PropertyObjectFixture : IDisposable, IReset
{
    public PropertyObject PropertyObject;

    public PropertyObjectFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        PropertyObject = null;
    }

    public void Reset()
    {
        PropertyObject = new PropertyObject("Bob");
    }
}