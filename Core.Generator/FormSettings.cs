﻿namespace Core.Generator;

public partial class FormSettings : Form, IFluxViewFor<StorageStore>
{
    private volatile bool _isUpdating;

    public FormSettings()
    {
        InitializeComponent();
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();

        this.OnChange(OnStorageStoreChange);
        this.Dispatch(new LoadConfigurationAction());

        btnCancel.Click += (_, _) => OnCancelClickCommand();
        btnSave.Click += (_, _) => OnSaveClickCommand();

        cbOptionGet.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionGet, txtOptionGetName);
        txtOptionGetName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionGet, txtOptionGetName);
        cbOptionFind.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionFind, txtOptionFindName);
        txtOptionFindName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionFind, txtOptionFindName);
        cbOptionActiveList.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionActiveList, txtOptionActiveListName);
        txtOptionActiveListName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionActiveList, txtOptionActiveListName);

        cbOptionUpdate.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionUpdate, txtOptionUpdateName);
        txtOptionUpdateName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionUpdate, txtOptionUpdateName);
        cbOptionInsert.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionInsert, txtOptionInsertName);
        txtOptionInsertName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionInsert, txtOptionInsertName);
        cbOptionDestroy.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionDestroy, txtOptionDestroyName);
        txtOptionDestroyName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionDestroy, txtOptionDestroyName);
        cbOptionLogicalDelete.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionLogicalDelete, txtOptionLogicalDeleteName);
        txtOptionLogicalDeleteName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionLogicalDelete, txtOptionLogicalDeleteName);

        cbOptionBulkUpdate.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionBulkUpdate, txtOptionBulkUpdateName);
        txtOptionBulkUpdateName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionBulkUpdate, txtOptionBulkUpdateName);
        cbOptionBulkInsert.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionBulkInsert, txtOptionBulkInsertName);
        txtOptionBulkInsertName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionBulkInsert, txtOptionBulkInsertName);
        cbOptionBulkDestroy.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionBulkDestroy, txtOptionBulkDestroyName);
        txtOptionBulkDestroyName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionBulkDestroy, txtOptionBulkDestroyName);
        cbOptionBulkLogicalDelete.CheckedChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionBulkLogicalDelete, txtOptionBulkLogicalDeleteName);
        txtOptionBulkLogicalDeleteName.TextChanged += (_, _) => ToggleCheckBoxTextBoxChange(cbOptionBulkLogicalDelete, txtOptionBulkLogicalDeleteName);

        txtNamespace.TextChanged += (_, _) => CheckValidity(true, txtNamespace);
        txtQueryNamespace.TextChanged += (_, _) => CheckValidity(true, txtQueryNamespace);
        txtControllerLayer.TextChanged += (_, _) => CheckValidity(true, txtControllerLayer);
        txtServiceLayer.TextChanged += (_, _) => CheckValidity(true, txtServiceLayer);
        txtDatabaseLayer.TextChanged += (_, _) => CheckValidity(true, txtDatabaseLayer);
    }

    private static void CheckValidity(CheckBox checkBox, Control textBox) => CheckValidity(checkBox.Checked, textBox);

    private static void CheckValidity(bool required, Control textBox) => textBox.BackColor = IsInvalid(required, textBox) ? Color.LightSalmon : Color.White;

    private static bool IsInvalid(CheckBox checkBox, Control textBox) => IsInvalid(checkBox.Checked, textBox);

    private static bool IsInvalid(bool required, Control textBox) => (required && textBox.Text.IsNullOrEmpty());

    private void ConnectCheckBoxAndTextbox(CheckBox checkBox, Control textBox, bool? checkedValue, string textValue)
    {
        checkBox.Checked = checkedValue ?? true;
        textBox.Text = textValue;

        ToggleCheckBoxTextBoxChange(checkBox, textBox);
    }

    private void OnCancelClickCommand() => Close();

    private void OnSaveClickCommand()
    {
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();
        // Required so we don't end up dispatching again
        // when changing the check state of an inserted item
        if (_isUpdating)
        {
            return;
        }

        if (IsInvalid(cbOptionGet, txtOptionGetName)
            || IsInvalid(cbOptionGet, txtOptionGetName)
            || IsInvalid(cbOptionFind, txtOptionFindName)
            || IsInvalid(cbOptionFind, txtOptionFindName)
            || IsInvalid(cbOptionActiveList, txtOptionActiveListName)
            || IsInvalid(cbOptionActiveList, txtOptionActiveListName)

            || IsInvalid(cbOptionUpdate, txtOptionUpdateName)
            || IsInvalid(cbOptionUpdate, txtOptionUpdateName)
            || IsInvalid(cbOptionInsert, txtOptionInsertName)
            || IsInvalid(cbOptionInsert, txtOptionInsertName)
            || IsInvalid(cbOptionDestroy, txtOptionDestroyName)
            || IsInvalid(cbOptionDestroy, txtOptionDestroyName)
            || IsInvalid(cbOptionLogicalDelete, txtOptionLogicalDeleteName)
            || IsInvalid(cbOptionLogicalDelete, txtOptionLogicalDeleteName)

            || IsInvalid(cbOptionBulkUpdate, txtOptionBulkUpdateName)
            || IsInvalid(cbOptionBulkUpdate, txtOptionBulkUpdateName)
            || IsInvalid(cbOptionBulkInsert, txtOptionBulkInsertName)
            || IsInvalid(cbOptionBulkInsert, txtOptionBulkInsertName)
            || IsInvalid(cbOptionBulkDestroy, txtOptionBulkDestroyName)
            || IsInvalid(cbOptionBulkDestroy, txtOptionBulkDestroyName)
            || IsInvalid(cbOptionBulkLogicalDelete, txtOptionBulkLogicalDeleteName)
            || IsInvalid(cbOptionBulkLogicalDelete, txtOptionBulkLogicalDeleteName)

            || IsInvalid(true, txtNamespace)
            || IsInvalid(true, txtQueryNamespace)
            || IsInvalid(true, txtControllerLayer)
            || IsInvalid(true, txtServiceLayer)
            || IsInvalid(true, txtDatabaseLayer))
        {
            MessageBox.Show("Required Data Missing", "Settings - Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        _isUpdating = true;

        var store = new Lazy<StorageStore>(FluxViewForExtensions.GetStore<StorageStore>).Value;

        var configurationSettings = store.ConfigurationSettings.Clone() as ConfigurationSettings;
        configurationSettings.ConfigurationDetail.OptionsConfiguration.GetByPrimaryKeys = cbOptionGet.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.GetMethodName = txtOptionGetName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.Find = cbOptionFind.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.FindMethodName = txtOptionFindName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.ActiveList = cbOptionActiveList.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.GetActiveListMethodName = txtOptionActiveListName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.Update = cbOptionUpdate.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.UpdateMethodName = txtOptionUpdateName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.Insert = cbOptionInsert.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.InsertMethodName = txtOptionInsertName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.Delete = cbOptionDestroy.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.DestroyMethodName = txtOptionDestroyName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.LogicalDelete = cbOptionLogicalDelete.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.DeleteMethodName = txtOptionLogicalDeleteName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.BulkUpdate = cbOptionBulkUpdate.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.BulkUpdateMethodName = txtOptionBulkUpdateName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.BulkCreate = cbOptionBulkInsert.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.BulkInsertMethodName = txtOptionBulkInsertName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.BulkDelete = cbOptionBulkDestroy.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.BulkDestroyMethodName = txtOptionBulkDestroyName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.BulkLogicalDelete = cbOptionBulkLogicalDelete.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.BulkDeleteMethodName = txtOptionBulkLogicalDeleteName.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.ActionLog = cbActionLog.Checked;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.Namespace = txtNamespace.Text;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.QueryModelName = txtQueryNamespace.Text;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.ControllerLayerName = txtControllerLayer.Text;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.ServiceLayerName = txtServiceLayer.Text;
        configurationSettings.ConfigurationDetail.CodeRelatedConfiguration.DataLayerName = txtDatabaseLayer.Text;

        configurationSettings.ConfigurationDetail.OptionsConfiguration.GenerateStoredProcedures = cbGenStoredProcedures.Checked;

        var payload = new SaveConfigurationSettingsAction(configurationSettings);
        this.Dispatch(payload);

        _isUpdating = false;
        Close();
    }

    private void OnStorageStoreChange(StorageStore store)
    {
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();
        if (!store.IsLoaded)
        {
            return;
        }

        _isUpdating = true;
        store.ExceptionMessaging();

        var options = store.ConfigurationSettings?.ConfigurationDetail?.OptionsConfiguration;
        var codeRelated = store.ConfigurationSettings?.ConfigurationDetail?.CodeRelatedConfiguration;

        ConnectCheckBoxAndTextbox(cbOptionGet, txtOptionGetName, options?.GetByPrimaryKeys, codeRelated?.GetMethodName);
        ConnectCheckBoxAndTextbox(cbOptionFind, txtOptionFindName, options?.Find, codeRelated?.FindMethodName);
        ConnectCheckBoxAndTextbox(cbOptionActiveList, txtOptionActiveListName, options?.ActiveList, codeRelated?.GetActiveListMethodName);

        ConnectCheckBoxAndTextbox(cbOptionUpdate, txtOptionUpdateName, options?.Update, codeRelated?.UpdateMethodName);
        ConnectCheckBoxAndTextbox(cbOptionInsert, txtOptionInsertName, options?.Insert, codeRelated?.InsertMethodName);
        ConnectCheckBoxAndTextbox(cbOptionDestroy, txtOptionDestroyName, options?.Delete, codeRelated?.DestroyMethodName);
        ConnectCheckBoxAndTextbox(cbOptionLogicalDelete, txtOptionLogicalDeleteName, options?.LogicalDelete, codeRelated?.DeleteMethodName);

        ConnectCheckBoxAndTextbox(cbOptionBulkUpdate, txtOptionBulkUpdateName, options?.BulkUpdate, codeRelated?.BulkUpdateMethodName);
        ConnectCheckBoxAndTextbox(cbOptionBulkInsert, txtOptionBulkInsertName, options?.BulkCreate, codeRelated?.BulkInsertMethodName);
        ConnectCheckBoxAndTextbox(cbOptionBulkDestroy, txtOptionBulkDestroyName, options?.BulkDelete, codeRelated?.BulkDestroyMethodName);
        ConnectCheckBoxAndTextbox(cbOptionBulkLogicalDelete, txtOptionBulkLogicalDeleteName, options?.BulkLogicalDelete, codeRelated?.BulkDeleteMethodName);

        cbActionLog.Checked = options?.ActionLog ?? true;
        txtNamespace.Text = codeRelated?.Namespace;
        txtQueryNamespace.Text = codeRelated?.QueryModelName;
        txtControllerLayer.Text = codeRelated?.ControllerLayerName;
        txtServiceLayer.Text = codeRelated?.ServiceLayerName;
        txtDatabaseLayer.Text = codeRelated?.DataLayerName;

        cbGenStoredProcedures.Checked = options?.GenerateStoredProcedures ?? true;

        CheckValidity(true, txtNamespace);
        CheckValidity(true, txtQueryNamespace);
        CheckValidity(true, txtControllerLayer);
        CheckValidity(true, txtServiceLayer);
        CheckValidity(true, txtDatabaseLayer);

        _isUpdating = false;
    }

    private void ToggleCheckBoxTextBoxChange(CheckBox checkBox, Control textBox)
    {
        textBox.Enabled = checkBox.Checked;
        CheckValidity(checkBox, textBox);
    }
}