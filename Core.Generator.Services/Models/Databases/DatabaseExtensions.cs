﻿namespace Core.Generator.Services.Models.Databases;

public static class DatabaseExtensions
{
    public static Database CovertToTableAndColumns(
        this Database database,
        IEnumerable<TableColumn> tableColumns)
    {
        if (database is null)
        {
            throw new ArgumentNullException(nameof(database));
        }

        var tables = tableColumns
                         ?.Where(x => x.DatabaseName.Equals(database.Name, StringComparison.InvariantCultureIgnoreCase))
                         .Clone()
                         ?.ToList()
                     ?? new List<TableColumn>();

        var clonedDatabase = database.Clone() as Database;
        clonedDatabase.Tables ??= new List<Table>().ToImmutableList();

        if (tables.IsNullOrEmpty())
        {
            return clonedDatabase;
        }

        var tableGroups = tables
            .GroupBy(x => new { x.TableName, x.TableSchema })
            .Select(x => new { x.Key.TableName, x.Key.TableSchema, columns = x });
        var updatedTables = new List<Table>();
        foreach (var tableGroup in tableGroups)
        {
            var table = clonedDatabase
                            .Tables
                            .FirstOrDefault(x => x.Name == tableGroup.TableName && x.Schema == tableGroup.TableSchema)
                        ?? new Table(tableGroup.TableName, tableGroup.TableSchema);

            table.Columns ??= new List<TableColumn>().ToImmutableList();
            table.Columns = table.Columns.AddTo(tableGroup.columns.ToList()).ToImmutableList();

            updatedTables.Add(table);
        }

        clonedDatabase.Tables = updatedTables.ToImmutableList();
        return clonedDatabase;
    }
}