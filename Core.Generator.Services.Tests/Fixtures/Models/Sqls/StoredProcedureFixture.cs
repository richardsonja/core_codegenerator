﻿using Core.Generator.Services.Models.Sqls;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Sqls;

public class StoredProcedureFixture : IDisposable, IReset
{
    public StoredProcedure StoredProcedure;

    public StoredProcedureFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        StoredProcedure = null;
    }

    public void Reset()
    {
        StoredProcedure = new StoredProcedure("Bob", "Was Here", "AndHere", "andThere");
    }
}