﻿namespace Core.Generator.Services.Repositories.Database;

public interface IMySqlRepository
{
    Task<Option<ImmutableList<Models.Databases.Database>>> GetDatabaseListFromServerAsync();
    Task<Option<ImmutableList<TableColumn>>> GetTableAndColumnsAsync(IEnumerable<string> listOfDatabases);
}