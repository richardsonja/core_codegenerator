﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

public abstract class StatementBaseService
{
    protected readonly IConfigurationService ConfigurationService;

    protected StatementBaseService(IConfigurationService configurationService)
    {
        ConfigurationService = configurationService;
    }

    public async Task<Option<string>> CreateOutputAsync(CodeGeneratorRequest request)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<string>.None;
        }

        var configOption = await ConfigurationService.GetAsync();
        return configOption.Match(
            x => CreateClass(x, request.Table),
            () => Option<string>.None);
    }

    protected virtual IStatement CreateBody(
        Table table,
        IEnumerable<StoredProcedureParameter> whereStatementParameters) => null;

    protected virtual Option<string> ExecuteConfigurationMessage() => Option<string>.None;

    protected virtual Option<string> ExecuteTableMessage() => Option<string>.None;

    protected virtual string Prefix(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.CodeRelatedConfiguration.GetMethodName;

    protected virtual bool ShouldExecute(ConfigurationSettings configuration) =>
        true;

    protected virtual bool ShouldExecute(Table table) =>
        true;

    protected virtual bool ShouldHaveDropStatement() => true;

    protected virtual ImmutableList<StoredProcedureParameter> StoredProcedureParameters(Table table) =>
        new List<StoredProcedureParameter>().ToImmutableList();

    protected virtual string Suffix(ConfigurationSettings configuration) =>
        null;

    protected virtual ImmutableList<StoredProcedureParameter> WhereStatementParameters(Table table) =>
                StoredProcedureParameters(table);

    private Option<string> CreateClass(
        ConfigurationSettings configuration,
        Table table)
    {
        if (!ShouldExecute(configuration))
        {
            return ExecuteConfigurationMessage();
        }

        if (!ShouldExecute(table))
        {
            return ExecuteTableMessage();
        }

        var storedProcedureParameters = StoredProcedureParameters(table)?.ToList();
        var whereStatementParameters = WhereStatementParameters(table)?.ToList();

        var prefix = Prefix(configuration);
        var suffix = Suffix(configuration);
        return new StoredProcedure(table.Schema, table.FormattedName, prefix, suffix)
            .AddParameter(storedProcedureParameters)
            .AddBody(CreateBody(table, whereStatementParameters))
            .SetDropStatement(ShouldHaveDropStatement())
            .ToString();
    }
}