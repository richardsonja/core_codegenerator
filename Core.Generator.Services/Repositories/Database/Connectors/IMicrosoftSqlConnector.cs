﻿namespace Core.Generator.Services.Repositories.Database.Connectors;

public interface IMicrosoftSqlConnector : ISqlConnector, ISqlStoredProcedureConnector
{
}