﻿using System.Data;

namespace Core.Common.Data;

public static class DataTableExtensions
{
    public static bool IsNullOrEmpty(this DataTable dataTable) =>
        dataTable is null
        || dataTable.AsEnumerable().IsNullOrEmpty();
}