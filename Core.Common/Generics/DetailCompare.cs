﻿namespace Core.Common.Generics;

public static class DetailCompare
{
    private const string EmptyLabel = "NULL";
    private const string MissingLabel = "MISSING";

    public static List<Variance> DetailedCompare<T>(
        this T source,
        T target, 
        IEnumerable<string> listOfColumnsToExclude = null)
    {
        Validate(source, target);

        var propertyList = source.GetType().GetProperties().ToList();
        var variances = propertyList
            .Select(
                property => new Variance(
                    property.Name,
                    GetValue(source, property.Name),
                    GetValue(target, property.Name)))
            .Where(variance => variance.IsDifferent())
            .ApplyExcludeList(listOfColumnsToExclude?.ToList());

        return variances.ToList();
    }

    public static List<Variance> DetailedCompareWith<T>(
        this T source, 
        T target,
        IEnumerable<string> listOfPropertiesRetrieveAndCompare)
    {
        Validate(source, target);
        var ofPropertiesRetrieveAndCompare = listOfPropertiesRetrieveAndCompare?.ToList();
        if (ofPropertiesRetrieveAndCompare.IsNullOrEmpty())
        {
            throw new ArgumentNullException(nameof(listOfPropertiesRetrieveAndCompare));
        }

        var variances = ofPropertiesRetrieveAndCompare!
            .Select(property => new Variance(property, GetValue(source, property), GetValue(target, property)))
            .Where(variance => variance.IsDifferent());

        return variances.ToList();
    }

    private static IEnumerable<Variance> ApplyExcludeList(
        this IEnumerable<Variance> variances,
        ICollection<string> listOfColumnsToExclude) =>
        listOfColumnsToExclude.IsNullOrEmpty()
            ? variances
            : variances.Where(v => !listOfColumnsToExclude.Contains(v.PropertyName));

    private static string GetValue<T>(T source, string individualPropertyName)
    {
        if (source == null)
        {
            return MissingLabel;
        }

        var value = source
            .GetType()
            .GetProperties()
            .FirstOrDefault(x => x.Name == individualPropertyName)
            ?.GetValue(source);

        return value == null
            ? EmptyLabel
            : value.ToString();
    }

    private static void Validate<T>(T source, T target)
    {
        if (source == null)
        {
            throw new ArgumentNullException(nameof(source));
        }

        if (target == null)
        {
            throw new ArgumentNullException(nameof(target));
        }
    }
}