﻿using Core.Flux.Tests.Mocks;

namespace Core.Flux.Tests.Fixtures;

public class StoreFixture : IDisposable, IReset
{
    public Mock<IDispatcher> Dispatcher;
    public IStore Store;

    public StoreFixture() => Reset();

    public void Dispose() => (Store, Dispatcher) = (null, null);

    public void Reset()
    {
        Dispatcher = new Mock<IDispatcher>();
        Fluxs.Dispatcher = Dispatcher.Object;

        Store = new TestStore();
    }
}