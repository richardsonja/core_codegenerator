﻿using Core.Generator.Services.Models.CodeGenerators;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.CodeGenerators;

public class DisplayResultFixture : IDisposable, IReset
{
    public DisplayResult DisplayResult;

    public DisplayResultFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        DisplayResult = null;
    }

    public void Reset()
    {
        DisplayResult = new DisplayResult(1, "Bob", "Was Here");
    }
}