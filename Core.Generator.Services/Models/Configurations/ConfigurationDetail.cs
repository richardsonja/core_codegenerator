﻿namespace Core.Generator.Services.Models.Configurations;

public class ConfigurationDetail : ICloneable
{
    public CodeRelatedConfiguration CodeRelatedConfiguration { get; set; }

    public OptionsConfiguration OptionsConfiguration { get; set; }

    public ServerConfiguration ServerConfiguration { get; set; }

    public object Clone() => new ConfigurationDetail
    {
        ServerConfiguration = ServerConfiguration?.Clone() as ServerConfiguration,
        CodeRelatedConfiguration = CodeRelatedConfiguration?.Clone() as CodeRelatedConfiguration,
        OptionsConfiguration = OptionsConfiguration?.Clone() as OptionsConfiguration
    };
}