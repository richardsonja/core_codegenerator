﻿using Core.Generator.Services.Models.Objects;
using FluentAssertions;
using System.Collections.Generic;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Objects;

public class GetterSetterTypeExtensionsTests
{
    [Theory]
    [InlineData(GetterSetterType.Get, "get")]
    [InlineData(GetterSetterType.Set, "set")]
    [InlineData(GetterSetterType.Init, "init")]
    public void GetterSetterTypeExtensions_CreateOutput_ShouldMatchExpected_GivenInput(GetterSetterType value, string expected) =>
        value.CreateOutput().Should().Be(expected);

    [Fact]
    public void GetterSetterTypeExtensions_IsValidCombo_ShouldBeFalse_GivenAllInputs() =>
        new List<GetterSetterType> { GetterSetterType.Get, GetterSetterType.Set, GetterSetterType.Init }.IsValidCombo().Should().BeFalse();

    [Fact]
    public void GetterSetterTypeExtensions_IsValidCombo_ShouldBeTrue_GivenEmptyList() =>
        new List<GetterSetterType>().IsValidCombo().Should().BeTrue();

    [Fact]
    public void GetterSetterTypeExtensions_IsValidCombo_ShouldBeTrue_GivenNullList()
    {
        List<GetterSetterType> list = null;
        list.IsValidCombo().Should().BeTrue();
    }

    [Fact]
    public void GetterSetterTypeExtensions_IsValidCombo_ShouldBeTrue_GivenOnlyOneInList() =>
        new List<GetterSetterType> { GetterSetterType.Set }.IsValidCombo().Should().BeTrue();

    [Theory]
    [InlineData(GetterSetterType.Get, GetterSetterType.Set, true)]
    [InlineData(GetterSetterType.Get, GetterSetterType.Init, true)]
    [InlineData(GetterSetterType.Set, GetterSetterType.Init, false)]
    public void GetterSetterTypeExtensions_IsValidCombo_ShouldMatchExpected_GivenInput(GetterSetterType value1, GetterSetterType value2, bool expected) =>
        new List<GetterSetterType> { value1, value2 }.IsValidCombo().Should().Be(expected);
}