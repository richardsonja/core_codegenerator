﻿using Core.Flux;
using Core.Flux.Extensions;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Stores.CodeGenerator;
using Core.Generator.Stores.CodeGenerator.Actions;
using Core.Generator.Stores.Tests.Fixtures;
using FluentAssertions;
using Xunit;

namespace Core.Generator.Stores.Tests.Stores;

public class CodeStoreTests : IFluxViewFor<CodeStore>, IClassFixture<CodeStoreFixture>
{
    private readonly CodeStoreFixture _codeStore;

    public CodeStoreTests(CodeStoreFixture codeStore)
    {
        _codeStore = codeStore;
    }

    [Fact]
    public void CodeStore_LoadGeneratorCodeAction_ShouldHaveSome_GivenSetup()
    {
        _codeStore.Reset();
        this.Dispatch(new LoadGeneratorCodeAction(new Table()));

        this.OnChange(
            t =>
            {
                t.Exceptions.Should().BeEmpty();
                t.CodeGeneratorResult.Should().NotBeNull();
            });
    }
}