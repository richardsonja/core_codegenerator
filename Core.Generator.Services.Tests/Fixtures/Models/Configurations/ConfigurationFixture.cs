﻿using Core.Generator.Services.Models.Configurations;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Configurations;

public class ConfigurationFixture : IDisposable, IReset
{
    public ConfigurationDetail ConfigurationDetail;
    public ConfigurationSettings ConfigurationSettings;

    public ConfigurationFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        ConfigurationDetail = null;
        ConfigurationSettings = null;
    }

    public void Reset()
    {
        ConfigurationDetail = new ConfigurationDetail
        {
            ServerConfiguration = new ServerConfiguration
            {
                DatabaseServer = "CG1",
                UserName = "CG2",
                AuthenticationType = AuthenticationType.MsSqlServerAccount
            },
            CodeRelatedConfiguration = new CodeRelatedConfiguration
            {
                Namespace = "CG3",
                QueryModelName = "CG4",
                QueryNamespaceSuffix = "CG5",
                ControllerLayerName = "CG6",
                ServiceLayerName = "CG7",
                DataLayerName = "CG8",
                BulkDeleteMethodName = "CG13",
                BulkDestroyMethodName = "CG14",
                BulkUpdateMethodName = "CG15",
                BulkInsertMethodName = "CG16",
                DeleteMethodName = "CG17",
                DestroyMethodName = "CG18",
                UpdateMethodName = "CG19",
                InsertMethodName = "CG20",
                FindMethodName = "CG21",
                GetMethodName = "CG22",
                GetActiveListMethodName = "CG23",
                RequestSuffixModelName = "CG24",
                ResponseSuffixModelName = "CG25"
            },
            OptionsConfiguration = new OptionsConfiguration
            {
                ActionLog = true,
                ActiveList = true,
                BulkCreate = true,
                BulkDelete = true,
                BulkLogicalDelete = true,
                BulkUpdate = true,
                Delete = true,
                Find = true,
                GetByPrimaryKeys = true,
                Insert = true,
                LogicalDelete = true,
                Update = true,
                GenerateStoredProcedures = true,
            }
        };

        ConfigurationSettings = new ConfigurationSettings("password", ConfigurationDetail);
    }

    public void SetData(bool optionSetting = true)
    {
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.BulkDeleteMethodName = "BulkDelete";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.BulkDestroyMethodName = "BulkDestroy";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.BulkInsertMethodName = "BulkInsert";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.BulkUpdateMethodName = "BulkUpdate";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.ControllerLayerName = "BobController";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.DataLayerName = "BobRepository";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.DeleteMethodName = "Delete";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.DestroyMethodName = "Destroy";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.FindMethodName = "Find";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.GetActiveListMethodName = "GetActiveList";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.GetMethodName = "Get";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.InsertMethodName = "Insert";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.Namespace = "Bob.Is.Here.Testing";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.QueryModelName = "Query";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.QueryNamespaceSuffix = "Queries";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.RequestSuffixModelName = "Request";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.ResponseSuffixModelName = "Response";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.ServiceLayerName = "BobService";
        ConfigurationSettings.ConfigurationDetail.CodeRelatedConfiguration.UpdateMethodName = "Update";

        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.ActionLog = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.ActiveList = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkCreate = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkDelete = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkLogicalDelete = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkUpdate = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Delete = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Find = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.GetByPrimaryKeys = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Insert = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.LogicalDelete = optionSetting;
        ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Update = optionSetting;


    }
}