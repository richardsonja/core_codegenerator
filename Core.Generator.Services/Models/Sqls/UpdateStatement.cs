﻿namespace Core.Generator.Services.Models.Sqls;

public class UpdateStatement : ICloneable, IStatement
{
    public UpdateStatement(string schema, string name) =>
        (Schema, Name) = (schema.IsNullOrEmpty() ? "dbo" : schema, name);

    private UpdateStatement(
        string schema,
        string name,
        bool removeBrackets,
        ImmutableList<StoredProcedureParameter> storedProcedureParameters,
        ImmutableList<StoredProcedureParameter> whereParameters)
        : this(schema, name)
    {
        RemoveBrackets = removeBrackets;
        SetValues = storedProcedureParameters.Clone().ToImmutableList();
        WhereParameters = whereParameters.Clone().ToImmutableList();
    }

    public string Name { get; }

    public bool RemoveBrackets { get; private set; }

    public string Schema { get; }

    public ImmutableList<StoredProcedureParameter> SetValues { get; private set; } = new List<StoredProcedureParameter>().ToImmutableList();

    public ImmutableList<StoredProcedureParameter> WhereParameters { get; private set; } = new List<StoredProcedureParameter>().ToImmutableList();

    public IStatement AddParameter(StoredProcedureParameter parameter)
    {
        if (parameter == null)
        {
            return this;
        }
        var clone = parameter.Clone() as StoredProcedureParameter;
        SetIsWhereStatement(clone);
        WhereParameters = WhereParameters.AddTo(clone).ToImmutableList();

        return this;
    }

    public IStatement AddParameter(IEnumerable<StoredProcedureParameter> parameter)
    {
        if (parameter.IsNullOrEmpty())
        {
            return this;
        }

        var clone = parameter.Clone().Select(SetIsWhereStatement).ToList();
        WhereParameters = WhereParameters.AddTo(clone).ToImmutableList();

        return this;
    }

    public UpdateStatement AddSetValue(StoredProcedureParameter parameter)
    {
        if (parameter == null)
        {
            return this;
        }
        var clone = parameter.Clone() as StoredProcedureParameter;
        clone = SetIsWhereStatement(clone).SetRemoveBrackets(RemoveBrackets);
        SetValues = SetValues.AddTo(clone).ToImmutableList();

        return this;
    }

    public UpdateStatement AddSetValue(IEnumerable<StoredProcedureParameter> parameter)
    {
        if (parameter.IsNullOrEmpty())
        {
            return this;
        }

        var clone = parameter.Clone().Select(SetIsWhereStatement).ToList();
        SetValues = SetValues.AddTo(clone).ToImmutableList();

        return this;
    }

    public object Clone() => new UpdateStatement(Schema, Name, RemoveBrackets, SetValues, WhereParameters);

    public UpdateStatement SetRemoveBrackets(bool removeBrackets = true)
    {
        RemoveBrackets = removeBrackets;
        SetValues = SetValues
            .Clone()
            .Select(x => x.SetRemoveBrackets(RemoveBrackets))
            .ToImmutableList();
        WhereParameters = WhereParameters
            .Clone()
            .Select(x => x.SetRemoveBrackets(RemoveBrackets))
            .ToImmutableList();
        return this;
    }

    public override string ToString() => $"{Environment.NewLine}UPDATE {ToFullName()}{Environment.NewLine}{ToSetStatement()}{Environment.NewLine}{ToWhereStatement()}";

    private string AddBrackets(string value) =>
        value.FormatBracketedValue(RemoveBrackets);

    private StoredProcedureParameter SetIsWhereStatement(StoredProcedureParameter parameter) =>
            parameter
            .SetIsWhereStatement(true)
            .SetIsTableAbbreviationDisabled(true)
            .SetRemoveBrackets(RemoveBrackets);

    private string ToFullName() =>
        $"{AddBrackets(Schema)}.{AddBrackets(Name)}";

    private string ToSetStatement() =>
        SetValues.ToSetStatement();

    private string ToWhereStatement() =>
        WhereParameters.ToWhereStatement();
}