﻿namespace Core.Generator.Services.Models.Sqls;

public class StoredProcedure : ICloneable
{
    public StoredProcedure(string schema, string name, string prefixName, string suffixName)
    {
        Schema = schema.IsNullOrEmpty() ? "dbo" : schema;
        Name = name;
        PrefixName = prefixName;
        SuffixName = suffixName;
    }

    private StoredProcedure(
        string schema,
        string name,
        string prefixName,
        string suffixName,
        ImmutableList<string> body,
        IStatement statement,
        ImmutableList<StoredProcedureParameter> storedProcedureParameters,
        bool hasDropStatement)
        : this(schema, name, prefixName, suffixName)
    {
        Body = body.Clone().ToImmutableList();
        Statement = statement;
        HasDropStatement = hasDropStatement;
        StoredProcedureParameters = storedProcedureParameters.Clone().ToImmutableList();
    }

    public ImmutableList<string> Body { get; private set; } = new List<string>().ToImmutableList();

    public bool HasDropStatement { get; private set; }

    public string Name { get; }

    public string PrefixName { get; }

    public string Schema { get; }

    public IStatement Statement { get; private set; }

    public ImmutableList<StoredProcedureParameter> StoredProcedureParameters { get; private set; } = new List<StoredProcedureParameter>().ToImmutableList();

    public string SuffixName { get; }

    public StoredProcedure AddBody(IStatement statement)
    {
        if (statement == null)
        {
            return this;
        }

        Statement = statement;

        return this;
    }

    public StoredProcedure AddBody(string body)
    {
        if (body.IsNullOrEmpty())
        {
            return this;
        }

        Body = Body.AddTo(body?.Trim()).ToImmutableList();

        return this;
    }

    public StoredProcedure AddBody(IEnumerable<string> body)
    {
        if (body.IsNullOrEmpty())
        {
            return this;
        }

        Body = Body.AddTo(body.Clone()?.Select(x => x?.Trim())).ToImmutableList();

        return this;
    }

    public StoredProcedure AddParameter(StoredProcedureParameter parameter)
    {
        if (parameter == null)
        {
            return this;
        }
        var clone = parameter.Clone() as StoredProcedureParameter;
        SetIsWhereStatement(clone);
        StoredProcedureParameters = StoredProcedureParameters.AddTo(clone).ToImmutableList();

        return this;
    }

    public StoredProcedure AddParameter(IEnumerable<StoredProcedureParameter> parameter)
    {
        if (parameter.IsNullOrEmpty())
        {
            return this;
        }

        var clone = parameter.Clone().Select(SetIsWhereStatement).ToList();
        StoredProcedureParameters = StoredProcedureParameters.AddTo(clone).ToImmutableList();

        return this;
    }

    public object Clone() =>
        new StoredProcedure(
            Schema,
            Name,
            PrefixName,
            SuffixName,
            Body,
            Statement,
            StoredProcedureParameters,
            HasDropStatement);

    public StoredProcedure SetDropStatement(bool hasDropStatement = true)
    {
        HasDropStatement = hasDropStatement;
        return this;
    }

    public override string ToString() => $"{Environment.NewLine}{ToDropStatement()}{Environment.NewLine}{ToStatement()}{Environment.NewLine}{ToParameters()}{Environment.NewLine}AS {Environment.NewLine}BEGIN {Environment.NewLine}{ToBody()}{Environment.NewLine}END {Environment.NewLine}GO";

    private StoredProcedureParameter SetIsWhereStatement(StoredProcedureParameter parameter) => parameter.SetIsWhereStatement(false);

    private void SetParametersAndToBody()
    {
        if (Statement != null)
        {
            Body = Body.AddTo(Statement.ToString()).ToImmutableList();
        }
    }

    private string ToBody()
    {
        SetParametersAndToBody();
        return Body.Combine(Environment.NewLine);
    }

    private string ToDropStatement() =>
        HasDropStatement
            ? $"IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[{ToStoredProcedureName()}]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)"
                .ToFullList()
                .AddTo($"DROP PROCEDURE {ToFullName()}")
                .AddTo(Environment.NewLine)
                .Combine(Environment.NewLine)
            : string.Empty;

    private string ToFullName() => $"[{Schema}].[{ToStoredProcedureName()}]";

    private string ToParameters() =>
        StoredProcedureParameters.ToStoredProcedureParameter();

    private string ToPrefixName() =>
        PrefixName.IsNullOrEmpty()
            ? string.Empty
            : $"{PrefixName.Trim()}_";

    private string ToStatement() => $"CREATE PROCEDURE {ToFullName()}";

    private string ToStoredProcedureName() => $"{ToPrefixName()}{Name?.Trim()}{ToSuffixName()}"?.Replace(" ", string.Empty);

    private string ToSuffixName() =>
        SuffixName.IsNullOrEmpty()
            ? string.Empty
            : $"_{SuffixName.Trim()}";
}