﻿namespace Core.Generator.Services.Models.CodeGenerators;

public static class DisplayResultExtensions
{
    public static DisplayResult CreateDisplayResult(
        this string tabName,
        int displayOrder,
        params DisplayResult[] displayResults) =>
        displayResults.CreateDisplayResult(displayOrder, tabName);

    public static DisplayResult CreateDisplayResult(
        this IEnumerable<DisplayResult> displayResults,
        int displayOrder,
        string tabName)
    {
        var list = displayResults?.Where(x => x != null && !x.IsEmpty).OrderList()?.ToList();
        if (list.IsNullOrEmpty())
        {
            return new DisplayResult(displayOrder, tabName, new List<DisplayResult>());
        }

        if (list.Count > 1)
        {
            return new DisplayResult(displayOrder, tabName, list).OrderList();
        }

        var displayResult = list.First();
        return displayResult.DisplayResults.IsNullOrEmpty()
            ? new DisplayResult(displayOrder, tabName, displayResult.DisplayText)
            : new DisplayResult(displayOrder, tabName, displayResult.DisplayResults).OrderList();
    }

    public static IEnumerable<DisplayResult> FilterNullOrEmpty(this IEnumerable<DisplayResult> source) =>
        source?.Where(x => x != null && !x.IsEmpty && (!x.DisplayText.IsNullOrEmpty() || !x.DisplayResults.IsNullOrEmpty()));

    public static bool IsNullOrEmpty(this IEnumerable<DisplayResult> source) =>
            EnumerableExtensions.IsNullOrEmpty(source?.FilterNullOrEmpty());

    public static ImmutableList<DisplayResult> OrderList(this IEnumerable<DisplayResult> displayResults) =>
        displayResults
            ?.Where(x => x != null)
            .OrderBy(x => x.DisplayOrder)
            .ThenBy(x => x.DisplayText)
            .ThenBy(x => x.DisplayResults?.Count ?? 0)
            .ToImmutableList();
}