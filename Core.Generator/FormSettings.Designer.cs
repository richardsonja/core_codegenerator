﻿
namespace Core.Generator
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbActionLog = new System.Windows.Forms.CheckBox();
            this.txtOptionBulkDestroyName = new System.Windows.Forms.TextBox();
            this.txtOptionBulkLogicalDeleteName = new System.Windows.Forms.TextBox();
            this.txtOptionBulkInsertName = new System.Windows.Forms.TextBox();
            this.txtOptionBulkUpdateName = new System.Windows.Forms.TextBox();
            this.txtOptionDestroyName = new System.Windows.Forms.TextBox();
            this.txtOptionLogicalDeleteName = new System.Windows.Forms.TextBox();
            this.txtOptionInsertName = new System.Windows.Forms.TextBox();
            this.txtOptionUpdateName = new System.Windows.Forms.TextBox();
            this.txtOptionActiveListName = new System.Windows.Forms.TextBox();
            this.txtOptionFindName = new System.Windows.Forms.TextBox();
            this.txtOptionGetName = new System.Windows.Forms.TextBox();
            this.cbOptionBulkDestroy = new System.Windows.Forms.CheckBox();
            this.cbOptionBulkLogicalDelete = new System.Windows.Forms.CheckBox();
            this.cbOptionBulkInsert = new System.Windows.Forms.CheckBox();
            this.cbOptionBulkUpdate = new System.Windows.Forms.CheckBox();
            this.cbOptionDestroy = new System.Windows.Forms.CheckBox();
            this.cbOptionActiveList = new System.Windows.Forms.CheckBox();
            this.cbOptionLogicalDelete = new System.Windows.Forms.CheckBox();
            this.cbOptionInsert = new System.Windows.Forms.CheckBox();
            this.cbOptionUpdate = new System.Windows.Forms.CheckBox();
            this.cbOptionFind = new System.Windows.Forms.CheckBox();
            this.cbOptionGet = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblQueryNamespace = new System.Windows.Forms.Label();
            this.lblDatabaseLayer = new System.Windows.Forms.Label();
            this.lblServiceLayer = new System.Windows.Forms.Label();
            this.lblNamespace = new System.Windows.Forms.Label();
            this.lblControllerLayerName = new System.Windows.Forms.Label();
            this.txtDatabaseLayer = new System.Windows.Forms.TextBox();
            this.txtServiceLayer = new System.Windows.Forms.TextBox();
            this.txtControllerLayer = new System.Windows.Forms.TextBox();
            this.txtQueryNamespace = new System.Windows.Forms.TextBox();
            this.txtNamespace = new System.Windows.Forms.TextBox();
            this.cbGenStoredProcedures = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(589, 569);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(140, 29);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(735, 569);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(140, 29);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbActionLog);
            this.groupBox1.Controls.Add(this.txtOptionBulkDestroyName);
            this.groupBox1.Controls.Add(this.txtOptionBulkLogicalDeleteName);
            this.groupBox1.Controls.Add(this.txtOptionBulkInsertName);
            this.groupBox1.Controls.Add(this.txtOptionBulkUpdateName);
            this.groupBox1.Controls.Add(this.txtOptionDestroyName);
            this.groupBox1.Controls.Add(this.txtOptionLogicalDeleteName);
            this.groupBox1.Controls.Add(this.txtOptionInsertName);
            this.groupBox1.Controls.Add(this.txtOptionUpdateName);
            this.groupBox1.Controls.Add(this.txtOptionActiveListName);
            this.groupBox1.Controls.Add(this.txtOptionFindName);
            this.groupBox1.Controls.Add(this.txtOptionGetName);
            this.groupBox1.Controls.Add(this.cbOptionBulkDestroy);
            this.groupBox1.Controls.Add(this.cbOptionBulkLogicalDelete);
            this.groupBox1.Controls.Add(this.cbOptionBulkInsert);
            this.groupBox1.Controls.Add(this.cbOptionBulkUpdate);
            this.groupBox1.Controls.Add(this.cbOptionDestroy);
            this.groupBox1.Controls.Add(this.cbOptionActiveList);
            this.groupBox1.Controls.Add(this.cbOptionLogicalDelete);
            this.groupBox1.Controls.Add(this.cbOptionInsert);
            this.groupBox1.Controls.Add(this.cbOptionUpdate);
            this.groupBox1.Controls.Add(this.cbOptionFind);
            this.groupBox1.Controls.Add(this.cbOptionGet);
            this.groupBox1.Location = new System.Drawing.Point(12, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 438);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options";
            // 
            // cbActionLog
            // 
            this.cbActionLog.AutoSize = true;
            this.cbActionLog.Location = new System.Drawing.Point(6, 396);
            this.cbActionLog.Name = "cbActionLog";
            this.cbActionLog.Size = new System.Drawing.Size(103, 24);
            this.cbActionLog.TabIndex = 22;
            this.cbActionLog.Text = "Action Log";
            this.cbActionLog.UseVisualStyleBackColor = true;
            // 
            // txtOptionBulkDestroyName
            // 
            this.txtOptionBulkDestroyName.Location = new System.Drawing.Point(204, 350);
            this.txtOptionBulkDestroyName.Name = "txtOptionBulkDestroyName";
            this.txtOptionBulkDestroyName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionBulkDestroyName.TabIndex = 21;
            // 
            // txtOptionBulkLogicalDeleteName
            // 
            this.txtOptionBulkLogicalDeleteName.Location = new System.Drawing.Point(204, 320);
            this.txtOptionBulkLogicalDeleteName.Name = "txtOptionBulkLogicalDeleteName";
            this.txtOptionBulkLogicalDeleteName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionBulkLogicalDeleteName.TabIndex = 20;
            // 
            // txtOptionBulkInsertName
            // 
            this.txtOptionBulkInsertName.Location = new System.Drawing.Point(204, 290);
            this.txtOptionBulkInsertName.Name = "txtOptionBulkInsertName";
            this.txtOptionBulkInsertName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionBulkInsertName.TabIndex = 19;
            // 
            // txtOptionBulkUpdateName
            // 
            this.txtOptionBulkUpdateName.Location = new System.Drawing.Point(204, 259);
            this.txtOptionBulkUpdateName.Name = "txtOptionBulkUpdateName";
            this.txtOptionBulkUpdateName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionBulkUpdateName.TabIndex = 18;
            // 
            // txtOptionDestroyName
            // 
            this.txtOptionDestroyName.Location = new System.Drawing.Point(204, 214);
            this.txtOptionDestroyName.Name = "txtOptionDestroyName";
            this.txtOptionDestroyName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionDestroyName.TabIndex = 17;
            // 
            // txtOptionLogicalDeleteName
            // 
            this.txtOptionLogicalDeleteName.Location = new System.Drawing.Point(204, 184);
            this.txtOptionLogicalDeleteName.Name = "txtOptionLogicalDeleteName";
            this.txtOptionLogicalDeleteName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionLogicalDeleteName.TabIndex = 16;
            // 
            // txtOptionInsertName
            // 
            this.txtOptionInsertName.Location = new System.Drawing.Point(204, 154);
            this.txtOptionInsertName.Name = "txtOptionInsertName";
            this.txtOptionInsertName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionInsertName.TabIndex = 15;
            // 
            // txtOptionUpdateName
            // 
            this.txtOptionUpdateName.Location = new System.Drawing.Point(204, 123);
            this.txtOptionUpdateName.Name = "txtOptionUpdateName";
            this.txtOptionUpdateName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionUpdateName.TabIndex = 14;
            // 
            // txtOptionActiveListName
            // 
            this.txtOptionActiveListName.Location = new System.Drawing.Point(204, 84);
            this.txtOptionActiveListName.Name = "txtOptionActiveListName";
            this.txtOptionActiveListName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionActiveListName.TabIndex = 13;
            // 
            // txtOptionFindName
            // 
            this.txtOptionFindName.Location = new System.Drawing.Point(204, 54);
            this.txtOptionFindName.Name = "txtOptionFindName";
            this.txtOptionFindName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionFindName.TabIndex = 12;
            // 
            // txtOptionGetName
            // 
            this.txtOptionGetName.Location = new System.Drawing.Point(204, 23);
            this.txtOptionGetName.Name = "txtOptionGetName";
            this.txtOptionGetName.Size = new System.Drawing.Size(216, 27);
            this.txtOptionGetName.TabIndex = 11;
            // 
            // cbOptionBulkDestroy
            // 
            this.cbOptionBulkDestroy.AutoSize = true;
            this.cbOptionBulkDestroy.Location = new System.Drawing.Point(6, 350);
            this.cbOptionBulkDestroy.Name = "cbOptionBulkDestroy";
            this.cbOptionBulkDestroy.Size = new System.Drawing.Size(181, 24);
            this.cbOptionBulkDestroy.TabIndex = 10;
            this.cbOptionBulkDestroy.Text = "Bulk Permanent Delete";
            this.cbOptionBulkDestroy.UseVisualStyleBackColor = true;
            // 
            // cbOptionBulkLogicalDelete
            // 
            this.cbOptionBulkLogicalDelete.AutoSize = true;
            this.cbOptionBulkLogicalDelete.Location = new System.Drawing.Point(6, 320);
            this.cbOptionBulkLogicalDelete.Name = "cbOptionBulkLogicalDelete";
            this.cbOptionBulkLogicalDelete.Size = new System.Drawing.Size(159, 24);
            this.cbOptionBulkLogicalDelete.TabIndex = 9;
            this.cbOptionBulkLogicalDelete.Text = "Bulk Logical Delete";
            this.cbOptionBulkLogicalDelete.UseVisualStyleBackColor = true;
            // 
            // cbOptionBulkInsert
            // 
            this.cbOptionBulkInsert.AutoSize = true;
            this.cbOptionBulkInsert.Location = new System.Drawing.Point(6, 290);
            this.cbOptionBulkInsert.Name = "cbOptionBulkInsert";
            this.cbOptionBulkInsert.Size = new System.Drawing.Size(99, 24);
            this.cbOptionBulkInsert.TabIndex = 8;
            this.cbOptionBulkInsert.Text = "Bulk Insert";
            this.cbOptionBulkInsert.UseVisualStyleBackColor = true;
            // 
            // cbOptionBulkUpdate
            // 
            this.cbOptionBulkUpdate.AutoSize = true;
            this.cbOptionBulkUpdate.Location = new System.Drawing.Point(6, 259);
            this.cbOptionBulkUpdate.Name = "cbOptionBulkUpdate";
            this.cbOptionBulkUpdate.Size = new System.Drawing.Size(112, 24);
            this.cbOptionBulkUpdate.TabIndex = 7;
            this.cbOptionBulkUpdate.Text = "Bulk Update";
            this.cbOptionBulkUpdate.UseVisualStyleBackColor = true;
            // 
            // cbOptionDestroy
            // 
            this.cbOptionDestroy.AutoSize = true;
            this.cbOptionDestroy.Location = new System.Drawing.Point(6, 214);
            this.cbOptionDestroy.Name = "cbOptionDestroy";
            this.cbOptionDestroy.Size = new System.Drawing.Size(149, 24);
            this.cbOptionDestroy.TabIndex = 6;
            this.cbOptionDestroy.Text = "Permanent Delete";
            this.cbOptionDestroy.UseVisualStyleBackColor = true;
            // 
            // cbOptionActiveList
            // 
            this.cbOptionActiveList.AutoSize = true;
            this.cbOptionActiveList.Location = new System.Drawing.Point(6, 84);
            this.cbOptionActiveList.Name = "cbOptionActiveList";
            this.cbOptionActiveList.Size = new System.Drawing.Size(98, 24);
            this.cbOptionActiveList.TabIndex = 5;
            this.cbOptionActiveList.Text = "Active List";
            this.cbOptionActiveList.UseVisualStyleBackColor = true;
            // 
            // cbOptionLogicalDelete
            // 
            this.cbOptionLogicalDelete.AutoSize = true;
            this.cbOptionLogicalDelete.Location = new System.Drawing.Point(6, 184);
            this.cbOptionLogicalDelete.Name = "cbOptionLogicalDelete";
            this.cbOptionLogicalDelete.Size = new System.Drawing.Size(127, 24);
            this.cbOptionLogicalDelete.TabIndex = 4;
            this.cbOptionLogicalDelete.Text = "Logical Delete";
            this.cbOptionLogicalDelete.UseVisualStyleBackColor = true;
            // 
            // cbOptionInsert
            // 
            this.cbOptionInsert.AutoSize = true;
            this.cbOptionInsert.Location = new System.Drawing.Point(6, 154);
            this.cbOptionInsert.Name = "cbOptionInsert";
            this.cbOptionInsert.Size = new System.Drawing.Size(67, 24);
            this.cbOptionInsert.TabIndex = 3;
            this.cbOptionInsert.Text = "Insert";
            this.cbOptionInsert.UseVisualStyleBackColor = true;
            // 
            // cbOptionUpdate
            // 
            this.cbOptionUpdate.AutoSize = true;
            this.cbOptionUpdate.Location = new System.Drawing.Point(6, 123);
            this.cbOptionUpdate.Name = "cbOptionUpdate";
            this.cbOptionUpdate.Size = new System.Drawing.Size(80, 24);
            this.cbOptionUpdate.TabIndex = 2;
            this.cbOptionUpdate.Text = "Update";
            this.cbOptionUpdate.UseVisualStyleBackColor = true;
            // 
            // cbOptionFind
            // 
            this.cbOptionFind.AutoSize = true;
            this.cbOptionFind.Location = new System.Drawing.Point(6, 54);
            this.cbOptionFind.Name = "cbOptionFind";
            this.cbOptionFind.Size = new System.Drawing.Size(112, 24);
            this.cbOptionFind.TabIndex = 1;
            this.cbOptionFind.Text = "Find (Query)";
            this.cbOptionFind.UseVisualStyleBackColor = true;
            // 
            // cbOptionGet
            // 
            this.cbOptionGet.AutoSize = true;
            this.cbOptionGet.Location = new System.Drawing.Point(6, 23);
            this.cbOptionGet.Name = "cbOptionGet";
            this.cbOptionGet.Size = new System.Drawing.Size(135, 24);
            this.cbOptionGet.TabIndex = 0;
            this.cbOptionGet.Text = "Get (Primary Id)";
            this.cbOptionGet.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbGenStoredProcedures);
            this.groupBox2.Controls.Add(this.lblQueryNamespace);
            this.groupBox2.Controls.Add(this.lblDatabaseLayer);
            this.groupBox2.Controls.Add(this.lblServiceLayer);
            this.groupBox2.Controls.Add(this.lblNamespace);
            this.groupBox2.Controls.Add(this.lblControllerLayerName);
            this.groupBox2.Controls.Add(this.txtDatabaseLayer);
            this.groupBox2.Controls.Add(this.txtServiceLayer);
            this.groupBox2.Controls.Add(this.txtControllerLayer);
            this.groupBox2.Controls.Add(this.txtQueryNamespace);
            this.groupBox2.Controls.Add(this.txtNamespace);
            this.groupBox2.Location = new System.Drawing.Point(474, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(439, 260);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Code Related";
            // 
            // lblQueryNamespace
            // 
            this.lblQueryNamespace.AutoSize = true;
            this.lblQueryNamespace.Location = new System.Drawing.Point(68, 55);
            this.lblQueryNamespace.Name = "lblQueryNamespace";
            this.lblQueryNamespace.Size = new System.Drawing.Size(130, 20);
            this.lblQueryNamespace.TabIndex = 22;
            this.lblQueryNamespace.Text = "Query Namespace";
            // 
            // lblDatabaseLayer
            // 
            this.lblDatabaseLayer.AutoSize = true;
            this.lblDatabaseLayer.Location = new System.Drawing.Point(87, 146);
            this.lblDatabaseLayer.Name = "lblDatabaseLayer";
            this.lblDatabaseLayer.Size = new System.Drawing.Size(111, 20);
            this.lblDatabaseLayer.TabIndex = 21;
            this.lblDatabaseLayer.Text = "Database Layer";
            // 
            // lblServiceLayer
            // 
            this.lblServiceLayer.AutoSize = true;
            this.lblServiceLayer.Location = new System.Drawing.Point(103, 117);
            this.lblServiceLayer.Name = "lblServiceLayer";
            this.lblServiceLayer.Size = new System.Drawing.Size(95, 20);
            this.lblServiceLayer.TabIndex = 20;
            this.lblServiceLayer.Text = "Service Layer";
            // 
            // lblNamespace
            // 
            this.lblNamespace.AutoSize = true;
            this.lblNamespace.Location = new System.Drawing.Point(111, 23);
            this.lblNamespace.Name = "lblNamespace";
            this.lblNamespace.Size = new System.Drawing.Size(87, 20);
            this.lblNamespace.TabIndex = 19;
            this.lblNamespace.Text = "Namespace";
            // 
            // lblControllerLayerName
            // 
            this.lblControllerLayerName.AutoSize = true;
            this.lblControllerLayerName.Location = new System.Drawing.Point(84, 87);
            this.lblControllerLayerName.Name = "lblControllerLayerName";
            this.lblControllerLayerName.Size = new System.Drawing.Size(114, 20);
            this.lblControllerLayerName.TabIndex = 18;
            this.lblControllerLayerName.Text = "Controller Layer";
            // 
            // txtDatabaseLayer
            // 
            this.txtDatabaseLayer.Location = new System.Drawing.Point(204, 143);
            this.txtDatabaseLayer.Name = "txtDatabaseLayer";
            this.txtDatabaseLayer.Size = new System.Drawing.Size(216, 27);
            this.txtDatabaseLayer.TabIndex = 15;
            // 
            // txtServiceLayer
            // 
            this.txtServiceLayer.Location = new System.Drawing.Point(204, 114);
            this.txtServiceLayer.Name = "txtServiceLayer";
            this.txtServiceLayer.Size = new System.Drawing.Size(216, 27);
            this.txtServiceLayer.TabIndex = 14;
            // 
            // txtControllerLayer
            // 
            this.txtControllerLayer.Location = new System.Drawing.Point(204, 84);
            this.txtControllerLayer.Name = "txtControllerLayer";
            this.txtControllerLayer.Size = new System.Drawing.Size(216, 27);
            this.txtControllerLayer.TabIndex = 13;
            // 
            // txtQueryNamespace
            // 
            this.txtQueryNamespace.Location = new System.Drawing.Point(204, 54);
            this.txtQueryNamespace.Name = "txtQueryNamespace";
            this.txtQueryNamespace.Size = new System.Drawing.Size(216, 27);
            this.txtQueryNamespace.TabIndex = 12;
            // 
            // txtNamespace
            // 
            this.txtNamespace.Location = new System.Drawing.Point(204, 23);
            this.txtNamespace.Name = "txtNamespace";
            this.txtNamespace.Size = new System.Drawing.Size(216, 27);
            this.txtNamespace.TabIndex = 11;
            // 
            // cbGenStoredProcedures
            // 
            this.cbGenStoredProcedures.AutoSize = true;
            this.cbGenStoredProcedures.Location = new System.Drawing.Point(204, 184);
            this.cbGenStoredProcedures.Name = "cbGenStoredProcedures";
            this.cbGenStoredProcedures.Size = new System.Drawing.Size(216, 24);
            this.cbGenStoredProcedures.TabIndex = 23;
            this.cbGenStoredProcedures.Text = "Generate Stored Procedures";
            this.cbGenStoredProcedures.UseVisualStyleBackColor = true;
            // 
            // FormSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 631);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox2);
            this.Name = "FormSettings";
            this.Text = "FormSettings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbOptionBulkDestroy;
        private System.Windows.Forms.CheckBox cbOptionBulkLogicalDelete;
        private System.Windows.Forms.CheckBox cbOptionBulkInsert;
        private System.Windows.Forms.CheckBox cbOptionBulkUpdate;
        private System.Windows.Forms.CheckBox cbOptionDestroy;
        private System.Windows.Forms.CheckBox cbOptionActiveList;
        private System.Windows.Forms.CheckBox cbOptionLogicalDelete;
        private System.Windows.Forms.CheckBox cbOptionInsert;
        private System.Windows.Forms.CheckBox cbOptionUpdate;
        private System.Windows.Forms.CheckBox cbOptionFind;
        private System.Windows.Forms.CheckBox cbOptionGet;
        private System.Windows.Forms.TextBox txtOptionFindName;
        private System.Windows.Forms.TextBox txtOptionGetName;
        private System.Windows.Forms.TextBox txtOptionDestroyName;
        private System.Windows.Forms.TextBox txtOptionLogicalDeleteName;
        private System.Windows.Forms.TextBox txtOptionInsertName;
        private System.Windows.Forms.TextBox txtOptionUpdateName;
        private System.Windows.Forms.TextBox txtOptionActiveListName;
        private System.Windows.Forms.TextBox txtOptionBulkDestroyName;
        private System.Windows.Forms.TextBox txtOptionBulkLogicalDeleteName;
        private System.Windows.Forms.TextBox txtOptionBulkInsertName;
        private System.Windows.Forms.TextBox txtOptionBulkUpdateName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDatabaseLayer;
        private System.Windows.Forms.TextBox txtServiceLayer;
        private System.Windows.Forms.TextBox txtControllerLayer;
        private System.Windows.Forms.TextBox txtQueryNamespace;
        private System.Windows.Forms.TextBox txtNamespace;
        private System.Windows.Forms.Label lblControllerLayerName;
        private System.Windows.Forms.Label lblNamespace;
        private System.Windows.Forms.Label lblDatabaseLayer;
        private System.Windows.Forms.Label lblServiceLayer;
        private System.Windows.Forms.Label lblQueryNamespace;
        private System.Windows.Forms.CheckBox cbActionLog;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox cbGenStoredProcedures;
    }
}