﻿namespace Core.Common.Generics;

public class Variance
{
    public Variance(
        string propertyName, 
        string valueFromOriginalObject,
        string valueFromTargetObject) =>
            (PropertyName, ValueFromOriginalObject, ValueFromTargetObject) = 
            (propertyName, valueFromOriginalObject, valueFromTargetObject);

    public string PropertyName { get; }

    public object ValueFromOriginalObject { get; }

    public object ValueFromTargetObject { get; }

    public bool IsDifferent() => !ValueFromOriginalObject.Equals(ValueFromTargetObject);

    public override string ToString() => $"Property: {PropertyName}; From: {ValueFromOriginalObject}; To: {ValueFromTargetObject};";
}