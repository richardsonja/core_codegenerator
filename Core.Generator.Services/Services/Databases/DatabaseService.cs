﻿using Core.Generator.Services.Repositories.Database;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.Databases;

public class DatabaseService : IDatabaseService
{
    private readonly IConfigurationService _configurationService;
    private readonly IMicrosoftSqlRepository _microsoftSqlRepository;
    private readonly IMySqlRepository _mySqlRepository;

    public DatabaseService(IConfigurationService configurationService, IMicrosoftSqlRepository microsoftSqlRepository, IMySqlRepository mySqlRepository) =>
        (_configurationService, _microsoftSqlRepository, _mySqlRepository) = (configurationService, microsoftSqlRepository, mySqlRepository);

    public async Task<Option<ImmutableList<Database>>> GetAsync()
    {
        var config = await _configurationService.GetAsync()
            .Match(s => s, () => new ConfigurationSettings(null, null));

        if (config.ConfigurationDetail == null || !config.IsDatabasePasswordSet())
        {
            return Option<ImmutableList<Database>>.None;
        }

        var databases = await GetDatabasesAsync(config)
            .MatchAsync(
                databases => GetDatabaseAccessPermissionsAsync(databases, config),
                () => new List<Database>().ToImmutableList());

        if (databases.IsNullOrEmpty())
        {
            return Option<ImmutableList<Database>>.None;
        }

        var updatedDatabases = (await GetDatabaseAccessPermissionsAsync(databases, config)).ToList();

        var databasesWithAccessPermission = updatedDatabases
            .Where(x => x.HasPermission)
            .Select(x => x.Name)
            .ToList();

        var tableAndColumns = await GetTableAndColumns(config, databasesWithAccessPermission);

        var mergedDatabases = updatedDatabases
            .Select(x => x.CovertToTableAndColumns(tableAndColumns))
            .ToList();
        return mergedDatabases.ToImmutableList();
    }

    private async Task<ImmutableList<Database>> GetDatabaseAccessPermissionsAsync(IEnumerable<Database> databases, ConfigurationSettings config)
    {
        if (databases.IsNullOrEmpty())
        {
            return null;
        }

        if (config.ConfigurationDetail.ServerConfiguration.GetServerType() != ServerType.MicrosoftSql)
        {
            return databases.ToImmutableList();
        }

        var clones = databases.Clone().ToList();
        foreach (var x in clones)
        {
            x.HasPermission = await _microsoftSqlRepository.HasAccessToDatabaseAsync(x.Name);
        }

        return clones.ToImmutableList();
    }

    private async Task<Option<ImmutableList<Database>>> GetDatabasesAsync(ConfigurationSettings config)
    {
        if (config == null)
        {
            return Option<ImmutableList<Database>>.None;
        }

        return config.ConfigurationDetail.ServerConfiguration.GetServerType() switch
        {
            ServerType.MicrosoftSql => await GetMicrosoftDatabaseListAsync(),
            ServerType.MySqlSql => await _mySqlRepository.GetDatabaseListFromServerAsync(),
            _ => Option<ImmutableList<Database>>.None
        };
    }

    private async Task<Option<ImmutableList<Database>>> GetMicrosoftDatabaseListAsync()
    {
        return await _microsoftSqlRepository.GetServerVersionAsync()
            .MatchAsync(
                GetMicrosoftDatabaseListAsync,
                () => Option<ImmutableList<Database>>.None);
    }

    private Task<Option<ImmutableList<Database>>> GetMicrosoftDatabaseListAsync(ServerVersion serverVersion) =>
        serverVersion.GetMajorVersion() <= 8
            ? _microsoftSqlRepository.GetDatabaseListFromServerVersionLessThan2008Async()
            : _microsoftSqlRepository.GetDatabaseListFromServerVersionGreaterThan2008Async();

    private async Task<ImmutableList<TableColumn>> GetTableAndColumns(ConfigurationSettings config, List<string> databasesWithAccessPermission)
    {
        if (databasesWithAccessPermission.IsNullOrEmpty())
        {
            return new List<TableColumn>().ToImmutableList();
        }

        var resultOption = config.ConfigurationDetail.ServerConfiguration.GetServerType() switch
        {
            ServerType.MicrosoftSql => await _microsoftSqlRepository.GetTableAndColumnsAsync(databasesWithAccessPermission),
            ServerType.MySqlSql => await _mySqlRepository.GetTableAndColumnsAsync(databasesWithAccessPermission),
            _ => Option<ImmutableList<TableColumn>>.None
        };

        return resultOption.Match(s =>
            s?.ToImmutableList(),
            () => new List<TableColumn>().ToImmutableList());
    }
}