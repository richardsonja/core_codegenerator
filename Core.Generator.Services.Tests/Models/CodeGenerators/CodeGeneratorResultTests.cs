﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Models.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using FluentAssertions;
using System.Collections.Generic;
using System.Collections.Immutable;
using Xunit;

namespace Core.Generator.Services.Tests.Models.CodeGenerators;

public class CodeGeneratorResultTests : IClassFixture<DatabaseTableColumnFixture>, IClassFixture<DisplayResultFixture>
{
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly DisplayResultFixture _displayResult;

    public CodeGeneratorResultTests(DatabaseTableColumnFixture databaseTableColumn, DisplayResultFixture displayResult)
    {
        _databaseTableColumn = databaseTableColumn;
        _displayResult = displayResult;
    }

    [Fact]
    public void CodeGeneratorResult_AddDisplayResult_ShouldMatchExpected_GivenListInput()
    {
        _databaseTableColumn.Reset();
        _displayResult.Reset();

        var tsil = new List<DisplayResult>
        {
            _displayResult.DisplayResult
        };

        var model = new CodeGeneratorResult(_databaseTableColumn.TableColumns.ToImmutableList())
            .AddDisplayResult(tsil);
        model.DisplayResults.Should().HaveCount(1);
        model.Columns.Should().BeEmpty();
    }

    [Fact]
    public void CodeGeneratorResult_AddDisplayResult_ShouldMatchExpected_GivenSingleInput()
    {
        _databaseTableColumn.Reset();
        _displayResult.Reset();
        var model = new CodeGeneratorResult(_databaseTableColumn.TableColumns.ToImmutableList())
            .AddDisplayResult(_displayResult.DisplayResult);
        model.DisplayResults.Should().HaveCount(1);
        model.Columns.Should().BeEmpty();
    }

    [Fact]
    public void CodeGeneratorResult_Clone_ShouldHaveTwoResultsWithoutTouchingOriginal_GivenListInput()
    {
        _databaseTableColumn.Reset();
        _displayResult.Reset();

        var tsil = new List<DisplayResult>
        {
            _displayResult.DisplayResult
        };

        var model = new CodeGeneratorResult(_databaseTableColumn.TableColumns.ToImmutableList())
            .AddDisplayResult(tsil);
        var newModel = (model.Clone() as CodeGeneratorResult).AddDisplayResult(tsil);

        model.DisplayResults.Should().HaveCount(1);
        newModel.DisplayResults.Should().HaveCount(2);
    }

    [Fact]
    public void CodeGeneratorResult_OrderList_ShouldReOrderAsExpected_GivenListInput()
    {
        _databaseTableColumn.Reset();
        _displayResult.Reset();

        var list = new List<DisplayResult>
        {
            new (3, "Bob", "bob was here"),
            new (2, "Bob", "bob is here"),
            new (1, "Bob", "Bob is coming")
        };

        var list2 = new List<DisplayResult>
        {
            new (3, "Bob", list),
            new (2, "Bob", "bob is here"),
            new (1, "Bob", "Bob is coming")
        };

        var model = new CodeGeneratorResult(_databaseTableColumn.TableColumns.ToImmutableList())
            .AddDisplayResult(list2)
            .OrderList();
        model.DisplayResults.Should().HaveCount(3);
        model.DisplayResults.Should().BeInAscendingOrder(x=> x.DisplayOrder);
        model.Columns.Should().BeEmpty();
    }

    [Fact]
    public void CodeGeneratorResult_OrderList_ShouldReturnSelf_GivenEmptyListInput()
    {
        _databaseTableColumn.Reset();
        _displayResult.Reset();

        var model = new CodeGeneratorResult(_databaseTableColumn.TableColumns.ToImmutableList())
            .OrderList();
        model.DisplayResults.Should().HaveCount(0);
        model.Columns.Should().BeEmpty();
    }
}