﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Sqls;
using FluentAssertions;
using System;
using System.Collections.Generic;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Sqls;

public class StoredProcedureExtensionsTests : IClassFixture<StoredProcedureFixture>, IClassFixture<DatabaseTableColumnFixture>
{
    private readonly StoredProcedureFixture _storedProcedure;
    private readonly DatabaseTableColumnFixture _database;

    public StoredProcedureExtensionsTests(StoredProcedureFixture storedProcedure, DatabaseTableColumnFixture database)
    {
        _storedProcedure = storedProcedure;
        _database = database;
    }
    [Fact]
    public void StoredProcedure_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _storedProcedure.Reset();
        var item = new StoredProcedureParameter(_database.CreateTableColumns("bob", "was here", "and here", 1));
        var results = _storedProcedure.StoredProcedure.AddParameter(item).ToString();
        results.TrimInternalExtraSpaces().Should().Be("CREATE PROCEDURE [Bob].[AndHere_WasHere_andThere] @Column1 NVARCHAR(MAX) AS BEGIN END GO");
    }

    [Fact]
    public void StoredProcedure_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenListInput()
    {
        _storedProcedure.Reset();

        var tsil = new List<StoredProcedureParameter>
        {
            new (_database.CreateTableColumns("bob", "was here", "and here", 2, "Bob1")),
            new (_database.CreateTableColumns("bob", "was here", "and here", 1, "Bob2"))
        };

        var results = _storedProcedure.StoredProcedure.AddParameter(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("CREATE PROCEDURE [Bob].[AndHere_WasHere_andThere] @Bob1 NVARCHAR(MAX), @Bob2 NVARCHAR(MAX) AS BEGIN END GO");
    }

    [Fact]
    public void StoredProcedure_AddParameter_ToString_ShouldNotThrow_GivenEmptyList()
    {
        var body = new List<StoredProcedureParameter>();
        _storedProcedure.Reset();

        Action results = () => _storedProcedure.StoredProcedure.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void StoredProcedure_AddParameter_ToString_ShouldNotThrow_GivenNullList()
    {
        List<StoredProcedureParameter> body = null;
        _storedProcedure.Reset();

        Action results = () => _storedProcedure.StoredProcedure.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void StoredProcedure_AddParameter_ToString_ShouldNotThrow_GivenNullSingle()
    {
        StoredProcedureParameter body = null;
        _storedProcedure.Reset();

        Action results = () => _storedProcedure.StoredProcedure.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void StoredProcedure_AddBody_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _storedProcedure.Reset();

        var results = _storedProcedure.StoredProcedure.AddBody("Select * from Nothing;").ToString();
        results.TrimInternalExtraSpaces().Should().Be("CREATE PROCEDURE [Bob].[AndHere_WasHere_andThere] AS BEGIN Select * from Nothing; END GO");
    }

    [Fact]
    public void StoredProcedure_AddBody_ToString_ShouldDisplayBasicMethodWithBody_GivenIStatement()
    {
        _storedProcedure.Reset();
        IStatement select = new SelectStatement("bob", "was here");

        var results = _storedProcedure.StoredProcedure.AddBody(select).ToString();
        results.TrimInternalExtraSpaces().Should().StartWith("CREATE PROCEDURE [Bob].[AndHere_WasHere_andThere] AS BEGIN SELECT");
        results.TrimInternalExtraSpaces().Should().EndWith("END GO");
    }

    [Fact]
    public void StoredProcedure_AddBody_ToString_ShouldDisplayBasicMethodWithBody_GivenListInput()
    {
        _storedProcedure.Reset();

        var tsil = new List<string>
        {
            "Select * from Nothing;"
        };

        var results = _storedProcedure.StoredProcedure.AddBody(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("CREATE PROCEDURE [Bob].[AndHere_WasHere_andThere] AS BEGIN Select * from Nothing; END GO");
    }

    [Fact]
    public void StoredProcedure_AddBody_ToString_ShouldNotThrow_GivenEmptyList()
    {
        List<string> body = new List<string>();
        _storedProcedure.Reset();

        Action results = () => _storedProcedure.StoredProcedure.AddBody(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void StoredProcedure_AddBody_ToString_ShouldNotThrow_GivenNullIStatement()
    {
        _storedProcedure.Reset();
        IStatement select = null;

        Action results = () => _storedProcedure.StoredProcedure.AddBody(select);
        results.Should().NotThrow();
    }

    [Fact]
    public void StoredProcedure_AddBody_ToString_ShouldNotThrow_GivenNullList()
    {
        List<string> body = null;
        _storedProcedure.Reset();

        Action results = () => _storedProcedure.StoredProcedure.AddBody(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void StoredProcedure_AddBody_ToString_ShouldNotThrow_GivenNullSingle()
    {
        string body = null;
        _storedProcedure.Reset();

        Action results = () => _storedProcedure.StoredProcedure.AddBody(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void StoredProcedure_Clone_ShouldHaveAllAttributes_GivenSetup()
    {
        _storedProcedure.Reset();

        var results = _storedProcedure
            .StoredProcedure
            .AddBody("Testing")
            .SetDropStatement(true)
            .Clone() as StoredProcedure;

        results.Should().NotBeNull();
        results.Schema.Should().Be("Bob");
        results.Name.Should().Be("Was Here");
        results.Body.Should().Contain("Testing");
        results.HasDropStatement.Should().BeTrue();

        results.ToString().TrimInternalExtraSpaces().Should().Be("IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AndHere_WasHere_andThere]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) DROP PROCEDURE [Bob].[AndHere_WasHere_andThere] CREATE PROCEDURE [Bob].[AndHere_WasHere_andThere] AS BEGIN Testing END GO");
    }

    [Fact]
    public void StoredProcedure_SetDropStatement_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _storedProcedure.Reset();

        var results = _storedProcedure.StoredProcedure.SetDropStatement().ToString();
        results.TrimInternalExtraSpaces().Should().Be("IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AndHere_WasHere_andThere]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) DROP PROCEDURE [Bob].[AndHere_WasHere_andThere] CREATE PROCEDURE [Bob].[AndHere_WasHere_andThere] AS BEGIN END GO");
    }

    [Fact]
    public void StoredProcedure_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _storedProcedure.Reset();

        var results = _storedProcedure.StoredProcedure.ToString();
        results.TrimInternalExtraSpaces().Should().Be("CREATE PROCEDURE [Bob].[AndHere_WasHere_andThere] AS BEGIN END GO");
    }
}