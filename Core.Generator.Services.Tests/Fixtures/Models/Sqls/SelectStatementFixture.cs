﻿using Core.Generator.Services.Models.Sqls;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Sqls;

public class SelectStatementFixture : IDisposable, IReset
{
    public SelectStatement SelectStatement;

    public SelectStatementFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        SelectStatement = null;
    }

    public void Reset()
    {
        SelectStatement = new SelectStatement("Bob", "Was Here");
    }
}