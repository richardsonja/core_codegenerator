﻿namespace Core.Flux.Tests.Fixtures;

public class DispatcherFixture : IDisposable, IReset
{
    public IDispatcher Dispatcher;

    public DispatcherFixture() => Reset();

    public void Dispose() => Dispatcher = null;

    public void Reset() => Dispatcher = new Dispatcher();
}