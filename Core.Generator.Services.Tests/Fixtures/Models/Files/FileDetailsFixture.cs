﻿using Core.Generator.Services.Models.Files;
using System;
using System.Text.Json;

namespace Core.Generator.Services.Tests.Fixtures.Models.Files;

public class FileDetailsFixture : IDisposable, IReset
{
    public FileDetails FileDetails;

    public FileDetailsFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        FileDetails = null;
    }

    public void Reset()
    {
        FileDetails = new FileDetails(@"Bob", "Bob.Txt", "C:\\");
    }

    public void Reset(object value)
    {
        var jsonString = JsonSerializer.Serialize(value);
        FileDetails = new FileDetails(jsonString, "Bob.Txt", "C:\\");
    }
}