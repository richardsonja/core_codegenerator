﻿using Core.Common.Generics;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Controllers;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Controllers;

public class WebApiControllerServiceTests : IClassFixture<WebApiControllerServiceFixture>, IClassFixture<DatabaseTableColumnFixture>, IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly WebApiControllerServiceFixture _service;

    public WebApiControllerServiceTests(WebApiControllerServiceFixture service, DatabaseTableColumnFixture databaseTableColumn, ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }

    [Fact]
    public async void WebApiControllerService_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void WebApiControllerService_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        var counter = 2;
        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.AddActive(counter++);
        _databaseTableColumn.AddCreateBy(counter++);
        _databaseTableColumn.AddCreateDate(counter++);
        _databaseTableColumn.AddModifyBy(counter++);
        _databaseTableColumn.AddModifyDate(counter++);
        _databaseTableColumn.AddPermanentName(counter++);
        _databaseTableColumn.AddTimeStamp(counter++);
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using Microsoft.AspNetCore.Authorization; using Microsoft.AspNetCore.Mvc; using System; using System.Collections.Generic; using System.Threading.Tasks; namespace Bob.Is.Here.Testing.BobController { public class BobTableController : Controller { private readonly IBobTableBobService _bobTableBobService ; public BobTableController(IBobTableBobService bobTableBobService) => _bobTableBobService = bobTableBobService; [HttpDelete(\"BulkDelete\")] public BobTableBulkDeleteResponse BulkDelete([FromBody] BulkDeleteRequest bulkDeleteRequest) { var results = _bobTableBobService.BulkDelete(bulkDeleteRequest); return results; } [HttpDelete(\"BulkDestroy\")] public BobTableBulkDestroyResponse BulkDestroy([FromBody] BulkDestroyRequest bulkDestroyRequest) { var results = _bobTableBobService.BulkDestroy(bulkDestroyRequest); return results; } [HttpPost(\"BulkInsert\")] public BobTableBulkInsertResponse BulkInsert([FromBody] BobTableBulkInsertRequest bobTableBulkInsertRequest) { var results = _bobTableBobService.BulkInsert(bobTableBulkInsertRequest); return results; } [HttpPut(\"BulkUpdate\")] public BobTableBulkUpdateResponse BulkUpdate([FromBody] BobTableBulkUpdateRequest bobTableBulkUpdateRequest) { var results = _bobTableBobService.BulkUpdate(bobTableBulkUpdateRequest); return results; } [HttpDelete(\"Delete\")] public BobTableDeleteResponse Delete([FromBody] DeleteRequest deleteRequest) { var results = _bobTableBobService.Delete(deleteRequest); return results; } [HttpDelete(\"Destroy\")] public BobTableDestroyResponse Destroy([FromBody] DestroyRequest destroyRequest) { var results = _bobTableBobService.Destroy(destroyRequest); return results; } [HttpPost(\"Find\")] public BobTableFindResponse Find([FromBody] BobTableQuery bobTableQuery) { var results = _bobTableBobService.Find(bobTableQuery); return results; } [HttpGet(\"{id}\")] public BobTable Get(bool id) { var results = _bobTableBobService.Get(id); return results; } [HttpGet(\"GetActiveList\")] public BobTableGetActiveListResponse GetActiveList() { var results = _bobTableBobService.GetActiveList(); return results; } [HttpPost(\"Insert\")] public BobTableInsertResponse Insert([FromBody] BobTableInsertRequest bobTableInsertRequest) { var results = _bobTableBobService.Insert(bobTableInsertRequest); return results; } [HttpPut(\"Update\")] public BobTableUpdateResponse Update([FromBody] BobTableUpdateRequest bobTableUpdateRequest) { var results = _bobTableBobService.Update(bobTableUpdateRequest); return results; } } }"));
    }

    [Theory]
    [InlineData(null)]
    [InlineData(false)]
    public async void WebApiControllerService_CreateOutput_ShouldHaveConstructorOnly_GivenInput(bool? setting)
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.ActionLog = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.ActiveList = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkCreate = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkDelete = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkLogicalDelete = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkUpdate = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Delete = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Find = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.GetByPrimaryKeys = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Insert = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.LogicalDelete = setting;
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Update = setting;

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        var counter = 2;
        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.AddActive(counter++);
        _databaseTableColumn.AddCreateBy(counter++);
        _databaseTableColumn.AddCreateDate(counter++);
        _databaseTableColumn.AddModifyBy(counter++);
        _databaseTableColumn.AddModifyDate(counter++);
        _databaseTableColumn.AddPermanentName(counter++);
        _databaseTableColumn.AddTimeStamp(counter++);
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using Microsoft.AspNetCore.Authorization; using Microsoft.AspNetCore.Mvc; using System; using System.Collections.Generic; using System.Threading.Tasks; namespace Bob.Is.Here.Testing.BobController { public class BobTableController : Controller { private readonly IBobTableBobService _bobTableBobService ; public BobTableController(IBobTableBobService bobTableBobService) => _bobTableBobService = bobTableBobService; } }"));
    }

    [Fact]
    public async void WebApiControllerService_CreateOutput_ShouldNotContainGetActiveList_GivenBulkLogicalDeleteFalse()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData(false);

        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkLogicalDelete = false;

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        var counter = 2;
        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.AddActive(1);
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using Microsoft.AspNetCore.Authorization; using Microsoft.AspNetCore.Mvc; using System; using System.Collections.Generic; using System.Threading.Tasks; namespace Bob.Is.Here.Testing.BobController { public class BobTableController : Controller { private readonly IBobTableBobService _bobTableBobService ; public BobTableController(IBobTableBobService bobTableBobService) => _bobTableBobService = bobTableBobService; } }"));
    }

    [Fact]
    public async void WebApiControllerService_CreateOutput_ShouldNotContainGetActiveList_GivenNoActiveColumns()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData(false);

        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkLogicalDelete = true;

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        var counter = 2;
        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using Microsoft.AspNetCore.Authorization; using Microsoft.AspNetCore.Mvc; using System; using System.Collections.Generic; using System.Threading.Tasks; namespace Bob.Is.Here.Testing.BobController { public class BobTableController : Controller { private readonly IBobTableBobService _bobTableBobService ; public BobTableController(IBobTableBobService bobTableBobService) => _bobTableBobService = bobTableBobService; } }"));
    }

    [Fact]
    public async void WebApiControllerService_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void WebApiControllerService_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(null));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void WebApiControllerService_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(null);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }
}