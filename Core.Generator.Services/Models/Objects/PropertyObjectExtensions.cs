﻿namespace Core.Generator.Services.Models.Objects;

public static class PropertyObjectExtensions
{
    public static string ToPropertyObject(this IEnumerable<PropertyObject> properties) =>
        properties
            ?.OrderBy(x => (int)x.AccessorType)
            .ThenBy(x => x.Name)
            .Select(x => x.ToString()?.Trim())
            .Combine(Environment.NewLine + Environment.NewLine);
}