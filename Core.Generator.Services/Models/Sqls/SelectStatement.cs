﻿namespace Core.Generator.Services.Models.Sqls;

public class SelectStatement : ICloneable, IStatement
{
    public SelectStatement(string schema, string name)
    {
        Schema = schema.IsNullOrEmpty() ? "dbo" : schema;
        Name = name;
        TableAbbreviation = name.CreateAbbreviation();
    }

    private SelectStatement(
        string schema,
        string name,
        bool removeBrackets,
        ImmutableList<string> selectStatements,
        ImmutableList<StoredProcedureParameter> storedProcedureParameters)
        : this(schema, name)
    {
        RemoveBrackets = removeBrackets;
        SelectStatements = selectStatements.Clone().ToImmutableList();
        StoredProcedureParameters = storedProcedureParameters.Clone().ToImmutableList();
    }

    public string Name { get; }

    public bool RemoveBrackets { get; private set; }

    public string Schema { get; }

    public ImmutableList<string> SelectStatements { get; private set; } = new List<string>().ToImmutableList();

    public ImmutableList<StoredProcedureParameter> StoredProcedureParameters { get; private set; } = new List<StoredProcedureParameter>().ToImmutableList();

    public string TableAbbreviation { get; }

    public IStatement AddParameter(StoredProcedureParameter parameter)
    {
        if (parameter == null)
        {
            return this;
        }
        var clone = parameter.Clone() as StoredProcedureParameter;
        SetIsWhereStatement(clone);
        StoredProcedureParameters = StoredProcedureParameters.AddTo(clone).ToImmutableList();

        return this;
    }

    public IStatement AddParameter(IEnumerable<StoredProcedureParameter> parameter)
    {
        if (parameter.IsNullOrEmpty())
        {
            return this;
        }

        var clone = parameter.Clone().Select(SetIsWhereStatement).ToList();
        StoredProcedureParameters = StoredProcedureParameters.AddTo(clone).ToImmutableList();

        return this;
    }

    public SelectStatement AddSelectStatement(string statement)
    {
        if (statement.IsNullOrEmpty())
        {
            return this;
        }

        SelectStatements = SelectStatements.AddTo(statement?.Trim()).ToImmutableList();

        return this;
    }

    public SelectStatement AddSelectStatement(IEnumerable<string> statement)
    {
        if (statement.IsNullOrEmpty())
        {
            return this;
        }

        SelectStatements = SelectStatements.AddTo(statement.Clone()?.Select(x => x?.Trim())).ToImmutableList();

        return this;
    }

    public object Clone() => new SelectStatement(Schema, Name, RemoveBrackets, SelectStatements, StoredProcedureParameters);

    public SelectStatement SetRemoveBrackets(bool removeBrackets = true)
    {
        RemoveBrackets = removeBrackets;
        StoredProcedureParameters = StoredProcedureParameters
            .Clone()
            .Select(x=> x.SetRemoveBrackets(RemoveBrackets))
            .ToImmutableList();
        return this;
    }

    public override string ToString() => $"{Environment.NewLine}SELECT{Environment.NewLine}{ToSelectStatement()}{Environment.NewLine}{ToFromStatement()}{Environment.NewLine}{ToWhereStatement()}";

    private StoredProcedureParameter SetIsWhereStatement(StoredProcedureParameter parameter) => parameter.SetIsWhereStatement(true).SetRemoveBrackets(RemoveBrackets);

    private string AddBrackets(string value) => 
        value.FormatBracketedValue(RemoveBrackets);

    private string ToFromStatement() =>
        $"FROM {ToFullName()} as {TableAbbreviation}";

    private string ToFullName() =>
        $"{AddBrackets(Schema)}.{AddBrackets(Name)}";

    private string ToSelectStatement() =>
        SelectStatements.IsNullOrEmpty()
            ? "*"
            : SelectStatements
                ?.Where(x => !x.IsNullOrEmpty())
                .Select(x => $"{TableAbbreviation}.{AddBrackets(x)}")
                .Combine($",{Environment.NewLine}");

    private string ToWhereStatement() =>
        StoredProcedureParameters.ToWhereStatement();
}