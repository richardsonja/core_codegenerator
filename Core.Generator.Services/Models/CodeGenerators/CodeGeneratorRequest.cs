﻿namespace Core.Generator.Services.Models.CodeGenerators;

public class CodeGeneratorRequest : ICloneable
{
    public CodeGeneratorRequest(Table table) => Table = table?.Clone() as Table;

    public Table Table { get; }

    public object Clone() => new CodeGeneratorRequest(Table);
}