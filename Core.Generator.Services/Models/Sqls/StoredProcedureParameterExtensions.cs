﻿namespace Core.Generator.Services.Models.Sqls;

public static class StoredProcedureParameterExtensions
{
    public static string ToColumnNames(
        this IEnumerable<StoredProcedureParameter> storedProcedureParameters) =>
        storedProcedureParameters.IsNullOrEmpty()
            ? string.Empty
            : storedProcedureParameters
                ?.Where(x => x != null)
                .OrderBy(x => x.FormattedColumnName)
                .Select(x => x.FormattedColumnName)
                .Combine($",{Environment.NewLine}");

    public static string ToParameterNames(
        this IEnumerable<StoredProcedureParameter> storedProcedureParameters) =>
        storedProcedureParameters.IsNullOrEmpty()
            ? string.Empty
            : storedProcedureParameters
                ?.Where(x => x != null)
                .OrderBy(x => x.FormattedColumnName)
                .Select(x => x.FormattedParameterName)
                .Combine($",{Environment.NewLine}");

    public static string ToSetStatement(
        this IEnumerable<StoredProcedureParameter> storedProcedureParameters)
    {
        var whereStatement = storedProcedureParameters
            ?.Where(x => x != null)
            .Select(x => x.ToString())
            .Distinct()
            .OrderBy(x => x)
            .Combine($", {Environment.NewLine}");

        return whereStatement.IsNullOrEmpty()
            ? string.Empty
            : $"SET {whereStatement}";
    }

    public static string ToStoredProcedureParameter(
        this IEnumerable<StoredProcedureParameter> storedProcedureParameters) =>
        storedProcedureParameters.IsNullOrEmpty()
            ? string.Empty
            : storedProcedureParameters
                ?.Where(x => x != null)
                .Select(x => x.ToString())
                .Distinct()
                .OrderBy(x => x)
                .Combine($",{Environment.NewLine}");

    public static string ToWhereStatement(
        this IEnumerable<StoredProcedureParameter> storedProcedureParameters)
    {
        var whereStatement = storedProcedureParameters
            ?.Where(x => x != null)
            .Select(x => x.ToString())
            .Distinct()
            .OrderBy(x => x)
            .Combine($" AND {Environment.NewLine}");

        return whereStatement.IsNullOrEmpty()
            ? string.Empty
            : $"WHERE {whereStatement}";
    }
}