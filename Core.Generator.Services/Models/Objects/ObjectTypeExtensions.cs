﻿namespace Core.Generator.Services.Models.Objects;

public static class ObjectTypeExtensions
{
    public static string CreateOutput(this ObjectType objectType) => objectType switch
    {
        ObjectType.Enum => "enum",
        ObjectType.Interface => "interface",
        _ => "class"
    };
}