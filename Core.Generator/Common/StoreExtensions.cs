﻿using Core.Generator.Services.Models.Exceptions;

namespace Core.Generator.Common;

public static class StoreExtensions
{
    public static void ExceptionMessaging(this IActionException store)
    {
        if (store.HasErrors() && store.Exceptions is not null && store.Exceptions.Count > 0)
        {
            MessageBox.Show($"ERROR: {store.Exceptions?.Select(x => x.ToString()).Combine("|||||") ?? "EMPTY EXCEPTION LIST"}");
        }
    }

    public static bool HasErrors(this IActionException store) => store.Exceptions.IsNullOrEmpty();
}