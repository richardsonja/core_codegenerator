﻿using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace Core.Generator.Services.Models.Databases;

public static class QueryBaseExtensions
{
    public static SqlCommand AddParameters<T>(
        this QueryBase<T> queryBase, 
        SqlCommand command) 
        where T : IDbDataParameter
    {
        if (!queryBase.Parameters.IsNullOrEmpty())
        {
            command.Parameters.AddRange(queryBase.Parameters.ToArray());
        }

        return command;
    }

    public static SqlDataAdapter AddParameters<T>(
        this QueryBase<T> queryBase, 
        SqlDataAdapter command) 
        where T : IDbDataParameter
    {
        if (!queryBase.Parameters.IsNullOrEmpty())
        {
            command.SelectCommand.Parameters.AddRange(queryBase.Parameters.ToArray());
        }

        return command;
    }

    public static MySqlCommand AddParameters<T>(
        this QueryBase<T> queryBase,
        MySqlCommand command)
        where T : IDbDataParameter
    {
        if (!queryBase.Parameters.IsNullOrEmpty())
        {
            command.Parameters.AddRange(queryBase.Parameters.ToArray());
        }

        return command;
    }

    public static MySqlDataAdapter AddParameters<T>(this QueryBase<T> queryBase, MySqlDataAdapter command) where T : IDbDataParameter
    {
        if (!queryBase.Parameters.IsNullOrEmpty())
        {
            command.SelectCommand.Parameters.AddRange(queryBase.Parameters.ToArray());
        }

        return command;
    }

    public static async Task CloseConnection(this SqlConnection connection)
    {
        if (connection.State == ConnectionState.Open)
        {
            await connection.CloseAsync();
        }
    }

    public static async Task CloseConnection(this MySqlConnection connection)
    {
        if (connection.State == ConnectionState.Open)
        {
            await connection.CloseAsync();
        }
    }

    public static MySqlCommand CreateCommand<T>(this QueryBase<T> queryBase, MySqlConnection connection) where T : IDbDataParameter
    {
        var command = new MySqlCommand(queryBase.Sql, connection);
        return queryBase.AddParameters(command);
    }

    public static SqlCommand CreateCommand<T>(this QueryBase<T> queryBase, SqlConnection connection) where T : IDbDataParameter
    {
        var command = new SqlCommand(queryBase.Sql, connection);
        return queryBase.AddParameters(command);
    }

    public static MySqlDataAdapter CreateDataAdapter<T>(this QueryBase<T> queryBase, MySqlConnection connection) where T : IDbDataParameter
    {
        var command = new MySqlDataAdapter(queryBase.Sql, connection);
        return queryBase.AddParameters(command);
    }

    public static SqlDataAdapter CreateDataAdapter<T>(this QueryBase<T> queryBase, SqlConnection connection) where T : IDbDataParameter
    {
        var command = new SqlDataAdapter(queryBase.Sql, connection);
        return queryBase.AddParameters(command);
    }

    public static bool IsEmpty<T>(this QueryBase<T> queryBase) where T : IDbDataParameter => queryBase is null || queryBase.Sql.IsNullOrEmpty();
}