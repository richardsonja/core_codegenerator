﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Sqls;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Sqls;

public class SelectStatementExtensionsTests : IClassFixture<SelectStatementFixture>, IClassFixture<DatabaseTableColumnFixture>
{
    private readonly SelectStatementFixture _selectStatement;
    private readonly DatabaseTableColumnFixture _database;

    public SelectStatementExtensionsTests(SelectStatementFixture selectStatement, DatabaseTableColumnFixture database)
    {
        _selectStatement = selectStatement;
        _database = database;
    }

    [Fact]
    public void SelectStatement_AddSelectStatement_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _selectStatement.Reset();

        var results = _selectStatement.SelectStatement.AddSelectStatement("Select * from Nothing;").ToString();
        results.TrimInternalExtraSpaces().Should().Be("SELECT wh.[Select * from Nothing;] FROM [Bob].[Was Here] as wh");
    }

    [Fact]
    public void SelectStatement_AddSelectStatement_ToString_ShouldDisplayBasicMethodWithBody_GivenListInput()
    {
        _selectStatement.Reset();

        var tsil = new List<string>
        {
            "Select * from Nothing;"
        };

        var results = _selectStatement.SelectStatement.AddSelectStatement(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("SELECT wh.[Select * from Nothing;] FROM [Bob].[Was Here] as wh");
    }

    [Theory]
    [InlineData("WasHere", "SELECT wh.WasHere FROM Bob.[Was Here] as wh")]
    [InlineData("Was Here", "SELECT wh.[Was Here] FROM Bob.[Was Here] as wh")]
    public void SelectStatement_AddSelectStatement_ToString_ShouldMatchExpect_GivenInputAndRemoveBracketsTrue(
        string tableName,
        string expected)
    {
        _selectStatement.Reset();

        var results = _selectStatement.SelectStatement.AddSelectStatement(tableName).SetRemoveBrackets().ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void SelectStatement_AddSelectStatement_ToString_ShouldNotThrow_GivenEmptyList()
    {
        List<string> body = new List<string>();
        _selectStatement.Reset();

        Action results = () => _selectStatement.SelectStatement.AddSelectStatement(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void SelectStatement_AddSelectStatement_ToString_ShouldNotThrow_GivenNullList()
    {
        List<string> body = null;
        _selectStatement.Reset();

        Action results = () => _selectStatement.SelectStatement.AddSelectStatement(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void SelectStatement_AddSelectStatement_ToString_ShouldNotThrow_GivenNullSingle()
    {
        string body = null;
        _selectStatement.Reset();

        Action results = () => _selectStatement.SelectStatement.AddSelectStatement(body);
        results.Should().NotThrow();
    }

       

    [Fact]
    public void SelectStatement_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _selectStatement.Reset();
        var item = new StoredProcedureParameter(_database.CreateTableColumns("bob", "was here", "and here", 1));
        var results = _selectStatement.SelectStatement.AddParameter(item).ToString();
        results.TrimInternalExtraSpaces().Should().Be("SELECT * FROM [Bob].[Was Here] as wh WHERE b.[Column1] = @Column1");
    }

    [Fact]
    public void SelectStatement_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenListInput()
    {
        _selectStatement.Reset();

        var tsil = new List<StoredProcedureParameter>
        {
            new (_database.CreateTableColumns("bob", "was here", "and here", 2, "Bob1")),
            new (_database.CreateTableColumns("bob", "was here", "and here", 1, "Bob2"))
        };

        var results = _selectStatement.SelectStatement.AddParameter(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("SELECT * FROM [Bob].[Was Here] as wh WHERE b.[Bob1] = @Bob1 AND b.[Bob2] = @Bob2");
    }

    [Fact]
    public void SelectStatement_AddParameter_ToString_ShouldNotThrow_GivenEmptyList()
    {
        var body = new List<StoredProcedureParameter>();
        _selectStatement.Reset();

        Action results = () => _selectStatement.SelectStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void SelectStatement_AddParameter_ToString_ShouldNotThrow_GivenNullList()
    {
        List<StoredProcedureParameter> body = null;
        _selectStatement.Reset();

        Action results = () => _selectStatement.SelectStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void SelectStatement_AddParameter_ToString_ShouldNotThrow_GivenNullSingle()
    {
        StoredProcedureParameter body = null;
        _selectStatement.Reset();

        Action results = () => _selectStatement.SelectStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void SelectStatement_Clone_ShouldHaveAllAttributes_GivenSetup()
    {
        _selectStatement.Reset();
        var parameter = new StoredProcedureParameter(_database.CreateTableColumns("bob", "was here", "and here", 1));

        var results = (_selectStatement
                .SelectStatement
                .AddSelectStatement("Testing")
                .SetRemoveBrackets(true)
                .AddParameter(parameter) as SelectStatement)
            .Clone() as SelectStatement;

        results.Should().NotBeNull();
        results.Schema.Should().Be("Bob");
        results.Name.Should().Be("Was Here");
        results.TableAbbreviation.Should().Be("wh");
        results.SelectStatements.Should().Contain("Testing");
        results.StoredProcedureParameters.Should().HaveCount(1);
        results.StoredProcedureParameters.First().ToString().Should().Contain("b.Column1 = @Column1");
        results.RemoveBrackets.Should().BeTrue();

        results.ToString().TrimInternalExtraSpaces().Should().Be("SELECT wh.Testing FROM Bob.[Was Here] as wh WHERE b.Column1 = @Column1");
    }

    [Theory]
    [InlineData("Bob", "WasHere", "SELECT * FROM Bob.WasHere as wh")]
    [InlineData("Bob", "Was Here", "SELECT * FROM Bob.[Was Here] as wh")]
    [InlineData("Bob Was", "WasHere", "SELECT * FROM [Bob Was].WasHere as wh")]
    [InlineData("Bob Was", "Was Here", "SELECT * FROM [Bob Was].[Was Here] as wh")]
    public void SelectStatement_SetRemoveBrackets_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        string schemaName,
        string tableName,
        string expected)
    {
        _selectStatement.Reset();
        _selectStatement.SelectStatement = new SelectStatement(schemaName, tableName);

        var results = _selectStatement.SelectStatement.SetRemoveBrackets().ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void SelectStatement_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _selectStatement.Reset();

        var results = _selectStatement.SelectStatement.ToString();
        results.TrimInternalExtraSpaces().Should().Be("SELECT * FROM [Bob].[Was Here] as wh");
    }
}