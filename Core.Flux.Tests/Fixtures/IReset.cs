﻿namespace Core.Flux.Tests.Fixtures;

public interface IReset
{
    void Reset();
}