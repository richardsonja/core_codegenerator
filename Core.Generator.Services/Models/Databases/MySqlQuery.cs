﻿using MySql.Data.MySqlClient;

namespace Core.Generator.Services.Models.Databases;

public class MySqlQuery : QueryBase<MySqlParameter>
{
    public MySqlQuery(string sql) => (Sql, Parameters) = (sql, new List<MySqlParameter>().ToImmutableList());

    public MySqlQuery(string sql, IEnumerable<MySqlParameter> parameters) : this(sql) =>
        Parameters = parameters.Clone().ToImmutableList();
}