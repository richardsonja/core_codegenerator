﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.CodeGenerators;
using System.Text.RegularExpressions;

namespace Core.Generator.Common;

public static class TabPageExtensions
{
    private static readonly Regex AlphaNumericRegex = new(@"^[a-zA-Z0-9\s,]*$");

    public static TabPage CreateCurrentDataGridTabPage(this ImmutableList<TableColumn> results)
    {
        var columns = results.OrderByPosition();
        var tableDataGrid = new DataGridView
        {
            Name = "CurrentTableDataGrid",
            AllowUserToAddRows = false,
            AllowUserToDeleteRows = false,
            ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize,
            Dock = DockStyle.Fill,
            Location = new Point(0, 0),
            ReadOnly = true,
            RowHeadersWidth = 51,
            DataSource = columns
        };

        var tableDisplay = new TabPage("Column Data");
        tableDisplay.Controls.Add(tableDataGrid);
        return tableDisplay;
    }

    public static TabPage CreateTabPage(this DisplayResult result, int level = 0)
    {
        if (result.DisplayResults.IsNullOrEmpty())
        {
            return result.CreateTextFieldPageTab();
        }

        var tabControl = new TabControl
        {
            Name = AlphaNumericRegex.Replace(result.Name, string.Empty) + "TabControl",
            Dock = DockStyle.Fill
        };

        var pages = result.DisplayResults.Select(x => x.CreateTabPage(++level)).ToArray();
        tabControl.TabPages.AddRange(pages);

        var tabPage = new TabPage(result.Name);
        tabPage.Controls.Add(tabControl);

        return tabPage;
    }

    private static void CopyToClipboard(string textToCopy)
    {
        if (textToCopy.IsNullOrEmpty())
        {
            return;
        }

        try
        {
            Clipboard.Clear();
            Clipboard.SetText(textToCopy);
        }
        catch
        {
            MessageBox.Show(Constants.UnableToClearClipboard);
        }
    }

    private static TabPage CreateTextFieldPageTab(this DisplayResult result)
    {
        var textName = AlphaNumericRegex.Replace(result.Name, string.Empty) + "Text";
        var copyButtonName = AlphaNumericRegex.Replace(result.Name, string.Empty) + "Button";

        var textField = new TextBox
        {
            Name = textName,
            Dock = DockStyle.Fill,
            Location = new Point(3, 30),
            MaxLength = int.MaxValue,
            ScrollBars = ScrollBars.Both,
            Text = result.DisplayText,
            Multiline = true,
            Margin = new Padding(5, 30, 5, 5)
        };

        var copyButton = new Button
        {
            Name = copyButtonName,
            Text = Constants.CopyToClipBoard,
            Dock = DockStyle.Top,
            Height = 25
        };

        copyButton.Click += (_, _) => CopyToClipboard(textField.Text);

        var tabPage = new TabPage(result.Name);
        tabPage.Controls.Add(copyButton);
        tabPage.Controls.Add(textField);

        return tabPage;
    }
}