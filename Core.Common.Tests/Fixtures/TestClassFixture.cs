﻿namespace Core.Common.Tests.Fixtures;

public class TestClassFixture : IDisposable, IReset
{
    public List<TestClass> TestClasses;

    public TestClassFixture() => Reset();

    public void Dispose()
    {
        TestClasses = null;
        GC.SuppressFinalize(this);
    }

    public void Reset()
    {
        Reset(9);
    }

    public void Reset(int numberOfRecords)
    {
        TestClasses = new List<TestClass>();
        for (var i = 0; i < numberOfRecords; i++)
        {
            TestClasses.Add(new(i, Guid.NewGuid(), DateTime.Today.AddDays(i), $"Name {i}"));
        }
    }

    public void SetEmptyValues()
    {
        TestClasses = new List<TestClass>();
    }

    public void SetNullValues()
    {
        TestClasses = null;
    }
}