﻿namespace Core.Common.Tests;

public class ParserTests
{
    [Theory]
    [ClassData(typeof(BooleanData))]
    public void Parser_ToBoolean_ShouldMatchExpected_GivenInput(
        string testValue,
        bool defaultValue,
        bool expected) =>
        testValue.ToBoolean(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(BooleanNullableData))]
    public void Parser_ToBooleanNullable_ShouldMatchExpected_GivenInput(
        string testValue,
        bool? defaultValue,
        bool? expected) =>
        testValue.ToBooleanNullable(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(DateTimeData))]
    public void Parser_ToDateTime_ShouldMatchExpected_GivenInput(
        string testValue,
        DateTime defaultValue,
        DateTime expected) =>
        testValue.ToDateTime(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(DateTimeNullableData))]
    public void Parser_ToDateTimeNullable_ShouldMatchExpected_GivenInput(
        string testValue,
        DateTime? defaultValue,
        DateTime? expected) =>
        testValue.ToDateTimeNullable(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(DecimalData))]
    public void Parser_ToDecimal_ShouldMatchExpected_GivenInput(
        string testValue,
        decimal defaultValue,
        decimal expected) =>
        testValue.ToDecimal(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(DecimalNullableData))]
    public void Parser_ToDecimalNullable_ShouldMatchExpected_GivenInput(
        string testValue,
        decimal? defaultValue,
        decimal? expected) =>
        testValue.ToDecimalNullable(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(DoubleData))]
    public void Parser_ToDouble_ShouldMatchExpected_GivenInput(
        string testValue,
        double defaultValue,
        double expected) =>
        testValue.ToDouble(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(DoubleNullableData))]
    public void Parser_ToDoubleNullable_ShouldMatchExpected_GivenInput(
        string testValue,
        double? defaultValue,
        double? expected) =>
        testValue.ToDoubleNullable(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(FloatData))]
    public void Parser_ToFloat_ShouldMatchExpected_GivenInput(
        string testValue,
        float defaultValue,
        float expected) =>
        testValue.ToFloat(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(FloatNullableData))]
    public void Parser_ToFloatNullable_ShouldMatchExpected_GivenInput(
        string testValue,
        float? defaultValue,
        float? expected) =>
        testValue.ToFloatNullable(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(IntData))]
    public void Parser_ToInt_ShouldMatchExpected_GivenInput(
        string testValue,
        int defaultValue,
        int expected) =>
        testValue.ToInt(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(IntNullableData))]
    public void Parser_ToIntNullable_ShouldMatchExpected_GivenInput(
        string testValue,
        int? defaultValue,
        int? expected) =>
        testValue.ToIntNullable(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(LongData))]
    public void Parser_ToLong_ShouldMatchExpected_GivenInput(
        string testValue,
        long defaultValue,
        long expected) =>
        testValue.ToLong(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(LongNullableData))]
    public void Parser_ToLongNullable_ShouldMatchExpected_GivenInput(
        string testValue,
        long? defaultValue,
        long? expected) =>
        testValue.ToLongNullable(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(ShortData))]
    public void Parser_ToShort_ShouldMatchExpected_GivenInput(
        string testValue,
        short defaultValue,
        short expected) =>
        testValue.ToShort(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(ShortNullableData))]
    public void Parser_ToShortNullable_ShouldMatchExpected_GivenInput(
        string testValue,
        short? defaultValue,
        short? expected) =>
        testValue.ToShortNullable(defaultValue).Should().Be(expected);

    [Theory]
    [ClassData(typeof(TimeSpanData))]
    public void Parser_ToTimeSpan_ShouldMatchExpected_GivenInput(
        string testValue,
        TimeSpan defaultValue,
        TimeSpan expected) =>
        testValue.ToTimeSpan(defaultValue).Should().Be(expected);

    private class BooleanData : TheoryData<string, bool, bool>
    {
        public BooleanData()
        {
            Add("1", false, true);
            Add("true", false, true);
            Add("y", false, true);
            Add("yes", false, true);
            Add("0", false, false);
            Add("false", false, false);
            Add("n", false, false);
            Add("no", false, false);
            Add("", true, true);
            Add("", false, false);
        }
    }

    private class BooleanNullableData : TheoryData<string, bool?, bool?>
    {
        public BooleanNullableData()
        {
            Add("1", false, true);
            Add("true", false, true);
            Add("y", false, true);
            Add("yes", false, true);
            Add("0", false, false);
            Add("false", false, false);
            Add("n", false, false);
            Add("no", false, false);
            Add("", true, true);
            Add("", false, false);
            Add("", null, null);
        }
    }

    private class DateTimeData : TheoryData<string, DateTime, DateTime>
    {
        public DateTimeData()
        {
            Add(null, DateTime.MinValue, DateTime.MinValue);
            Add(string.Empty, DateTime.MaxValue, DateTime.MaxValue);
            Add("sillyString", DateTime.MinValue, DateTime.MinValue);
            Add("10/20/2014", DateTime.MinValue, new DateTime(2014, 10, 20));
            Add("10-20-2014", DateTime.MinValue, new DateTime(2014, 10, 20));
            Add("2014/10/20", DateTime.MinValue, new DateTime(2014, 10, 20));
        }
    }

    private class DateTimeNullableData : TheoryData<string, DateTime?, DateTime?>
    {
        public DateTimeNullableData()
        {
            Add(null, null, null);
            Add(string.Empty, null, null);
            Add("sillyString", DateTime.MinValue, DateTime.MinValue);
            Add("10/20/2014", DateTime.MinValue, new DateTime(2014, 10, 20));
            Add("10-20-2014", DateTime.MinValue, new DateTime(2014, 10, 20));
            Add("2014/10/20", DateTime.MinValue, new DateTime(2014, 10, 20));
        }
    }

    private class DecimalData : TheoryData<string, decimal, decimal>
    {
        public DecimalData()
        {
            Add(null, 0.00M, 0.00M);
            Add(string.Empty, 0.00M, 0.00M);
            Add("sillyString", 0.00M, 0.00M);
            Add("0", 0.00M, 0.00M);
            Add("10", 10.00M, 10.00M);
            Add("11.58", 11.58M, 11.58M);
        }
    }

    private class DecimalNullableData : TheoryData<string, decimal?, decimal?>
    {
        public DecimalNullableData()
        {
            Add(null, null, null);
            Add(string.Empty, null, null);
            Add("sillyString", null, null);
            Add("0", 0.00M, 0.00M);
            Add("10", 10.00M, 10.00M);
            Add("11.58", 11.58M, 11.58M);
        }
    }

    private class DoubleData : TheoryData<string, double, double>
    {
        public DoubleData()
        {
            Add(null, 0.00D, 0.00D);
            Add(string.Empty, 0.00D, 0.00D);
            Add("sillyString", 0.00D, 0.00D);
            Add("0", 0.00D, 0.00D);
            Add("10", 10.00D, 10.00D);
            Add("11.58", 11.58D, 11.58D);
        }
    }

    private class DoubleNullableData : TheoryData<string, double?, double?>
    {
        public DoubleNullableData()
        {
            Add(null, null, null);
            Add(string.Empty, null, null);
            Add("sillyString", null, null);
            Add("0", 0.00D, 0.00D);
            Add("10", 10.00D, 10.00D);
            Add("11.58", 11.58D, 11.58D);
        }
    }

    private class FloatData : TheoryData<string, float, float>
    {
        public FloatData()
        {
            Add(null, 0.00F, 0.00F);
            Add(string.Empty, 0.00F, 0.00F);
            Add("sillyString", 0.00F, 0.00F);
            Add("0", 0.00F, 0.00F);
            Add("10", 10.00F, 10.00F);
            Add("11.58", 11.58F, 11.58F);
        }
    }

    private class FloatNullableData : TheoryData<string, float?, float?>
    {
        public FloatNullableData()
        {
            Add(null, null, null);
            Add(string.Empty, null, null);
            Add("sillyString", null, null);
            Add("0", 0.00F, 0.00F);
            Add("10", 10.00F, 10.00F);
            Add("11.58", 11.58F, 11.58F);
        }
    }

    private class IntData : TheoryData<string, int, int>
    {
        public IntData()
        {
            Add(null, 0, 0);
            Add(string.Empty, 0, 0);
            Add("sillyString", 0, 0);
            Add("0", 0, 0);
            Add("10", 10, 10);
            Add("11.58", 11, 11);
        }
    }

    private class IntNullableData : TheoryData<string, int?, int?>
    {
        public IntNullableData()
        {
            Add(null, null, null);
            Add(string.Empty, null, null);
            Add("sillyString", null, null);
            Add("0", 0, 0);
            Add("10", 10, 10);
            Add("11.58", 11, 11);
        }
    }

    private class LongData : TheoryData<string, long, long>
    {
        public LongData()
        {
            Add(null, 0L, 0L);
            Add(string.Empty, 0L, 0L);
            Add("sillyString", 0L, 0L);
            Add("0", 0L, 0L);
            Add("10", 10L, 10L);
            Add("11.58", 11L, 11L);
        }
    }

    private class LongNullableData : TheoryData<string, long?, long?>
    {
        public LongNullableData()
        {
            Add(null, null, null);
            Add(string.Empty, null, null);
            Add("sillyString", null, null);
            Add("0", 0L, 0L);
            Add("10", 10L, 10L);
            Add("11.58", 11L, 11L);
        }
    }

    private class ShortData : TheoryData<string, short, short>
    {
        public ShortData()
        {
            Add(null, 0, 0);
            Add(string.Empty, 0, 0);
            Add("sillyString", 0, 0);
            Add("0", 0, 0);
            Add("10", 10, 10);
            Add("11.58", 11, 11);
        }
    }

    private class ShortNullableData : TheoryData<string, short?, short?>
    {
        public ShortNullableData()
        {
            Add(null, null, null);
            Add(string.Empty, null, null);
            Add("sillyString", null, null);
            Add("0", 0, 0);
            Add("10", 10, 10);
            Add("11.58", 11, 11);
        }
    }

    private class TimeSpanData : TheoryData<string, TimeSpan, TimeSpan>
    {
        public TimeSpanData()
        {
            Add(null, default, new TimeSpan(0, 0, 0));
            Add(string.Empty, default, new TimeSpan(0, 0, 0));
            Add("sillyString", default, new TimeSpan(0, 0, 0));
            Add("A:A", default, new TimeSpan(0, 0, 0));
            Add("10", default, new TimeSpan(10, 0, 0));
            Add("10:10", default, new TimeSpan(10, 10, 0));
            Add("10", default, new TimeSpan(10, 0, 0));
        }
    }
}