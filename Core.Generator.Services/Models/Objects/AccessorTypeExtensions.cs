﻿namespace Core.Generator.Services.Models.Objects;

public static class AccessorTypeExtensions
{
    public static string CreateOutput(this AccessorType accessorType) => accessorType switch
    {
        AccessorType.Protected => "protected",
        AccessorType.Private => "private",
        AccessorType.Internal => "internal",
        _ => "public"
    };
}