﻿namespace Core.Generator.Services.Models.Exceptions;

public interface IActionException
{
    List<ActionException> Exceptions { get; }
}