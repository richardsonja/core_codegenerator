﻿namespace Core.Generator.Services.Models.Databases;

public class CSharpType : ICloneable
{
    public CSharpType()
    {
    }

    public CSharpType(Type type, string typeString) => (Type, TypeString) = (type, typeString);

    public Type Type { get; set; }

    public string TypeString { get; set; }

    public object Clone() => new CSharpType(Type, TypeString);

    public override string ToString() => $"{Type} ({TypeString}) ";
}