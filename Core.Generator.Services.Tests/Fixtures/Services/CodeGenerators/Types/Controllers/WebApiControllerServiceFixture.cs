﻿using Core.Generator.Services.Services.CodeGenerators.Types.Controllers;
using Core.Generator.Services.Services.Configurations;
using Moq;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Controllers;

public class WebApiControllerServiceFixture : IDisposable, IReset
{
    public Mock<IConfigurationService> ConfigurationService = new();
    public WebApiControllerService Service;

    public WebApiControllerServiceFixture()
    {
        Reset();
        Service = new WebApiControllerService(ConfigurationService.Object);
    }

    public void Dispose()
    {
        Service = null;
        ConfigurationService = null;
    }

    public void Reset()
    {
        ConfigurationService.Reset();
    }
}