﻿namespace Core.Generator.Services.Models.Databases;

public static class DatabaseColumnConstants
{
    public const string ServerVersion = "ServerVersion";
    public const string DatabaseId = "ID";
    public const string DatabaseName = "Name";
    public const string TableDatabaseName = "TableDatabaseName";
    public const string TableName = "TableName";
    public const string TableSchema = "TableSchema";
    public const string ColumnName = "ColumnName";
    public const string OrdinalPosition = "OrdinalPosition";
    public const string DataType = "DataType";
    public const string NumericPrecision = "NumericPrecision";
    public const string NumericScale = "NumericScale";
    public const string IsNullable = "IsNullable";
    public const string CharacterMaximumLength = "CharacterMaximumLength";
    public const string IsIdentity = "IsIdentity";
    public const string IsIndex = "IsIndex";
    public const string ForeignTable = "ForeignTable";
    public const string ForeignColumn = "ForeignColumn";
    public const string ForeignSchema = "ForeignSchema";
}