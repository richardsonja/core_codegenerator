﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

public class SqlCodeGeneratorService : ISqlCodeGeneratorService
{
    private readonly IConfigurationService _configurationService;
    private readonly ISelectService _selectService;
    private readonly ISelectByPrimaryKeysService _selectByPrimaryKeysService;
    private readonly ISelectActiveListService _selectActiveListService;
    private readonly ISelectFindService _selectFindService;
    private readonly IDeleteService _deleteService;
    private readonly IUpdateService _updateService;

    public SqlCodeGeneratorService(
        IConfigurationService configurationService,
        ISelectService selectService,
        ISelectByPrimaryKeysService selectByPrimaryKeysService,
        ISelectActiveListService selectActiveListService,
        ISelectFindService selectFindService,
        IDeleteService deleteService,
        IUpdateService updateService)
    {
        _configurationService = configurationService;
        _selectService = selectService;
        _selectByPrimaryKeysService = selectByPrimaryKeysService;
        _selectActiveListService = selectActiveListService;
        _selectFindService = selectFindService;
        _deleteService = deleteService;
        _updateService = updateService;
    }

    public async Task<Option<ImmutableList<DisplayResult>>> CreateOutputAsync(CodeGeneratorRequest request)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<ImmutableList<DisplayResult>>.None;
        }

        var configOption = await _configurationService.GetAsync();
        var displayResults = await configOption
            .MatchAsync(
                x => CreateTabAsync(x, request),
                () => new List<DisplayResult>().ToImmutableList());

        return displayResults.IsNullOrEmpty()
            ? Option<ImmutableList<DisplayResult>>.None
            : displayResults.ToImmutableList();
    }

    private async Task<ImmutableList<DisplayResult>> CreateTabAsync(
        ConfigurationSettings configuration,
        CodeGeneratorRequest request)
    {
        if(configuration.ConfigurationDetail.OptionsConfiguration.GenerateStoredProcedures != true)
        {
            return new List<DisplayResult>().ToImmutableList();
        }

        var counter = 1;
        var selectAll = await _selectService
            .CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabSelectAll, x),
                () => new DisplayResult());

        var selectByPrimaryKeys = await _selectByPrimaryKeysService
            .CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabSelectByPrimaryKey, x),
                () => new DisplayResult());

        var selectActiveList = await _selectActiveListService
            .CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabSelectActiveList, x),
                () => new DisplayResult());

        var selectFind = await _selectFindService
            .CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabSelectFind, x),
                () => new DisplayResult());

        var delete = await _deleteService
            .CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabDestroy, x),
                () => new DisplayResult());

        var update = await _updateService
            .CreateOutputAsync(request)
            .Match(
                x => new DisplayResult(counter++, Constants.TabUpdate, x),
                () => new DisplayResult());
            
        return selectAll
            .ToFullList()
            .AddTo(selectByPrimaryKeys)
            .AddTo(selectActiveList)
            .AddTo(selectFind)
            .AddTo(delete)
            .AddTo(update)
            .ToImmutableList();
    }
}