﻿using Core.Common.Generics;

namespace Core.Common.Tests.Generics;

public class StringExtensionsTests
{
    [Theory]
    [InlineData(null, null)]
    [InlineData("", "")]
    [InlineData("LINK", "lINK")]
    [InlineData("link", "link")]
    [InlineData("Link", "link")]
    [InlineData("L", "l")]
    [InlineData("l", "l")]
    public void StringExtensions_CamelCase_ShouldBeExpect_GivenInput(
        string testValue,
        string expected) =>
        testValue.CamelCase().Should().Be(expected);

    // is it a string or object?
    // is null
    // is empty list
    // format
    // left and right formatting
    // no left but right formating, etc..
    // default
    // null as seperator
    [Theory]
    [InlineData("string", "string", "; ", null, null, "string; string")]
    [InlineData("string", "string", " ||||| ", null, null, "string ||||| string")]
    [InlineData("string", "string", null, null, null, "stringstring")]
    [InlineData("string", "string", "; ", "", "", "string; string")]
    [InlineData("string", "string", " ||||| ", "", "", "string ||||| string")]
    [InlineData("string", "string", null, "", "", "stringstring")]
    [InlineData("string", "string", "; ", "[", null, "[string; [string")]
    [InlineData("string", "string", "; ", null, "]", "string]; string]")]
    [InlineData("string", "string", "; ", "[", "]", "[string]; [string]")]
    public void StringExtensions_Combine_ShouldBeExpect_GivenInput(
        string testValue1,
        string testValue2,
        string separator,
        string leftSide,
        string rightSide,
        string expected) =>
        new List<string> { testValue1, testValue2 }
            .Combine(separator, leftSide, rightSide)
            .Should().Be(expected);

    [Fact]
    public void StringExtensions_Combine_ShouldBeNull_GivenEmptyList()
    {
        // arrange
        var list = new List<string>();

        // act
        var result  = list.Combine(null);

        // assert
        result.Should().BeNull();
    }
    [Fact]
    public void StringExtensions_Combine_ShouldBeNull_GivenNullList()
    {
        // arrange
        List<string> list = null;

        // act
        var result = list.Combine(null);

        // assert
        result.Should().BeNull();
    }

    [Theory]
    [InlineData(null, null)]
    [InlineData("", "")]
    [InlineData("null", "n")]
    [InlineData("A", "a")]
    [InlineData("aA", "aa")]
    [InlineData("aa", "a")]
    [InlineData("ID", "i")]
    [InlineData("IDID", "i")]
    [InlineData("ID_ID", "ii")]
    [InlineData("FieldID", "fi")]
    [InlineData("Field    ID", "fi")]
    [InlineData("Field_ID", "fi")]
    public void StringExtensions_CreateAbbreviation_ShouldMatchExpect_GivenInput(
        string testValue,
        string expected) =>
            testValue.CreateAbbreviation().Should().Be(expected);


    [Theory]
    [InlineData(null, null)]
    [InlineData("", "")]
    [InlineData("null", "Null")]
    [InlineData("a", "A")]
    [InlineData("aa", "Aa")]
    [InlineData("ID", "Id")]
    [InlineData("IDID", "Idid")]
    [InlineData("ID_ID", "Id Id")]
    [InlineData("FieldID", "Field Id")]
    [InlineData("Field_ID", "Field Id")]
    public void StringExtensions_FormatStringWithSpacesAndProperCapitalization_ShouldBeExpect_GivenInput(
        string testValue,
        string expected) =>
        testValue.FormatStringWithSpacesAndProperCapitalization().Should().Be(expected);

    [Theory]
    [InlineData(null, true)]
    [InlineData("", true)]
    [InlineData("null", false)]
    public void StringExtensions_IsNullOrEmpty_ShouldBeExpect_GivenInput(string testValue, bool expected) =>
        testValue.IsNullOrEmpty().Should().Be(expected);

    [Theory]
    [InlineData(null, null)]
    [InlineData("", "")]
    [InlineData("LINK", "LINK")]
    [InlineData("link", "Link")]
    [InlineData("Link", "Link")]
    [InlineData("L", "L")]
    [InlineData("l", "L")]
    public void StringExtensions_PascalCase_ShouldBeExpect_GivenInput(
        string testValue,
        string expected) =>
        testValue.PascalCase().Should().Be(expected);

    [Theory]
    [InlineData(null, 1, null)]
    [InlineData("", 1, "")]
    [InlineData("null", 1, "null")]
    [InlineData("a", 10, "aaaaaaaaaa")]
    public void StringExtensions_Repeat_ShouldBeExpect_GivenInput(string testValue, int repeat, string expected) =>
        testValue.Repeat(repeat).Should().Be(expected);

    [Theory]
    [InlineData(null, null)]
    [InlineData("", "")]
    [InlineData(
        "<a>link</a>. <b>bold</b>. <somethingrandom>No closing tag. <p>paragraph</p> <ul><li>bullet 1</li><li>bullet 2</li></ul>",
        "link. bold. No closing tag. paragraph bullet 1bullet 2")]
    public void StringExtensions_StripTag_NoTags_ShouldBeExpect_GivenInput(
        string testValue,
        string expected) =>
        testValue.StripTag().Should().Be(expected);

    [Theory]
    [InlineData(null, "<a>,b,<somethingrandom>, , p, <ul>, li", null)]
    [InlineData("", "<a>,b,<somethingrandom>, , p, <ul>, li", "")]
    [InlineData(
        "<a>link</a>. <b>bold</b>. <somethingrandom>No closing tag. <p>paragraph</p> <ul><li>bullet 1</li><li>bullet 2</li></ul>",
        "<a>,b,<somethingrandom>, , p, <ul>, li",
        "<a>link</a>. <b>bold</b>. <somethingrandom>No closing tag. <p>paragraph</p> <ul><li>bullet 1</li><li>bullet 2</li></ul>")]
    public void StringExtensions_StripTag_WithAllowTags_ShouldBeExpect_GivenInput(
        string testValue,
        string allowedTags,
        string expected) =>
        testValue.StripTag(allowedTags).Should().Be(expected);

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData("null")]
    public void StringExtensions_ToList_ShouldList_GivenInput(string testValue) => testValue.ToList().Should().NotBeEmpty();

    [Theory]
    [InlineData(null, null)]
    [InlineData("", "")]
    [InlineData("bob was here", "bob was here")]
    [InlineData("bob  was  here", "bob was here")]
    [InlineData("bob   was   here", "bob was here")]
    public void StringExtensions_TrimInternalExtraSpaces_ShouldBeExpect_GivenInput(
        string testValue,
        string expected) =>
        testValue.TrimInternalExtraSpaces().Should().Be(expected);
}