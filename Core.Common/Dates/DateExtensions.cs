﻿namespace Core.Common.Dates;

public static class DateExtensions
{
    public static bool IsNullOrEmpty(this DateTime? source) => !source.HasValue || source.Value.IsNullOrEmpty();
    // is null
    // format
    // default
    public static bool IsNullOrEmpty(this DateTime source) => source.IsDefault();
}