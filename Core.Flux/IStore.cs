﻿namespace Core.Flux;

public interface IStore
{
    bool IsLoaded { get; }

    void EmitChange();
}