﻿namespace Core.Flux;

public interface IStoreResolver
{
    T GetStore<T>() where T : Store;

    void Register<T>(T store) where T : Store;
}