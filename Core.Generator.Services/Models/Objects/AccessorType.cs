﻿namespace Core.Generator.Services.Models.Objects;

public enum AccessorType
{
    Public,
    Protected,
    Private,
    Internal
}