﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Tests.Fixtures.Models.Objects;
using FluentAssertions;
using System.Collections.Generic;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Objects;

public class GetterSetterObjectExtensionsTests : IClassFixture<GetterSetterObjectFixture>
{
    private readonly GetterSetterObjectFixture _getterSetterObject;

    public GetterSetterObjectExtensionsTests(GetterSetterObjectFixture getterSetterObject)
    {
        _getterSetterObject = getterSetterObject;
    }

    [Fact]
    public void GetterSetterObject_AddBody_ToString_ShouldDisplayBasicGetterSetterObjectWithBody_GivenInput()
    {
        _getterSetterObject.Reset();

        var results = _getterSetterObject.GetterSetterObject.AddBody("var test = new TestObject();").ToString();
        results.TrimInternalExtraSpaces().Should().Be("get => var test = new TestObject();");
    }

    [Fact]
    public void GetterSetterObject_AddBody_ToString_ShouldDisplayBasicGetterSetterObjectWithBody_GivenListInput()
    {
        _getterSetterObject.Reset();

        var tsil = new List<string>
        {
            "var test = new TestObject();",
            "var test = new TestObject();"
        };

        var results = _getterSetterObject.GetterSetterObject.AddBody(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("get { var test = new TestObject(); var test = new TestObject(); }");
    }

    [Fact]
    public void GetterSetterObject_Clone_ToString_ShouldClonedVersion_GivenInput()
    {
        _getterSetterObject.Reset();

        var results = _getterSetterObject
            .GetterSetterObject
            .AddBody("var test = new TestObject();")
            .SetAccessorType(AccessorType.Private)
            .Clone()
            .ToString();

        results.TrimInternalExtraSpaces().Should().Be("private get => var test = new TestObject();");
    }

    [Theory]
    [InlineData(GetterSetterType.Get, "get;")]
    [InlineData(GetterSetterType.Set, "set;")]
    [InlineData(GetterSetterType.Init, "init;")]
    public void GetterSetterObject_Constructor_ToString_ShouldMatchExpected_GivenInput(GetterSetterType value, string expected)
    {
        var results = new GetterSetterObject(value).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(AccessorType.Internal, "internal get;")]
    [InlineData(AccessorType.Public, "get;")]
    [InlineData(AccessorType.Private, "private get;")]
    [InlineData(AccessorType.Protected, "protected get;")]
    public void GetterSetterObject_SetAccessorType_ToString_ShouldMatchExpected_GivenInput(AccessorType value, string expected)
    {
        _getterSetterObject.Reset();

        var results = _getterSetterObject.GetterSetterObject.SetAccessorType(value).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }
}