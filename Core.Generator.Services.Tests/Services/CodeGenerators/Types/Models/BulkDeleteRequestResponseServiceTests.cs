﻿using Core.Common.Generics;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Models;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Models;

public class BulkDeleteRequestResponseServiceTests :
    IClassFixture<RequestResponseServicesFixture>,
    IClassFixture<DatabaseTableColumnFixture>,
    IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly RequestResponseServicesFixture _service;

    public BulkDeleteRequestResponseServiceTests(RequestResponseServicesFixture service, DatabaseTableColumnFixture databaseTableColumn, ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Request_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service
            .BulkDeleteRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Request_CreateOutput_ShouldBeNull_GivenMissingIsActiveField()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.BulkDeleteRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Request_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.AddActive(1);
        var results = await _service.BulkDeleteRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                true);
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using System; using System.Collections.Generic; namespace Bob.Is.Here.Testing.Models.bob { public class BulkDeleteRequest { public IEnumerable<bool> BobColumns { get; set; } } }"));
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Request_CreateOutput_ShouldNone_GivenOptionFalse()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkLogicalDelete = false;

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.BulkDeleteRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Request_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.BulkDeleteRequestResponseService.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")), true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Request_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.BulkDeleteRequestResponseService.CreateOutputAsync(new CodeGeneratorRequest(null), true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Request_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.BulkDeleteRequestResponseService.CreateOutputAsync(null, true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Response_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service
            .BulkDeleteRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Response_CreateOutput_ShouldBeNull_GivenMissingIsActiveField()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.BulkDeleteRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Response_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.AddActive(1);
        var results = await _service.BulkDeleteRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                false);
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using System; using System.Collections.Generic; namespace Bob.Is.Here.Testing.Models.bob { public class BulkDeleteResponse { public IEnumerable<string> Errors { get; set; } public bool IsSuccessful { get; set; } } }"));
    }

    public async void BulkDeleteRequestResponseService_Response_CreateOutput_ShouldNone_GivenOptionFalse()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkLogicalDelete = false;

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.BulkDeleteRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Response_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.BulkDeleteRequestResponseService.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")), false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Response_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.BulkDeleteRequestResponseService.CreateOutputAsync(new CodeGeneratorRequest(null), false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkDeleteRequestResponseService_Response_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.BulkDeleteRequestResponseService.CreateOutputAsync(null, false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }
}