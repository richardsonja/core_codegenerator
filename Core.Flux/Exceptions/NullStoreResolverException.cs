﻿using System;

namespace Core.Flux.Exceptions;

public class NullStoreResolverException : Exception
{
    public NullStoreResolverException()
        : base("Store resolver has not been configured")
    {
    }
}