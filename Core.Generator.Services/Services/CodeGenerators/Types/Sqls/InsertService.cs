﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Services.Configurations;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

public class InsertService : StatementBaseService, IInsertService
{
    public InsertService(IConfigurationService configurationService) : base(configurationService)
    {
    }
    protected override Option<string> ExecuteTableMessage() => Constants.NotFoundColumnMessage;

    protected override IStatement CreateBody(Table table, IEnumerable<StoredProcedureParameter> whereStatementParameters) =>
        new InsertStatement(table.Schema, table.Name)
            .AddParameter(whereStatementParameters);
        
    protected override ImmutableList<StoredProcedureParameter> StoredProcedureParameters(Table table) =>
        table.Columns
            .Except(table.Columns.GetPrimaryKeyColumns())
            .Select(x => new StoredProcedureParameter(x))
            .ToImmutableList();

    protected override string Prefix(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.CodeRelatedConfiguration.InsertMethodName;

    protected override bool ShouldExecute(ConfigurationSettings configuration) =>
        configuration.ConfigurationDetail.OptionsConfiguration.Insert == true;

    protected override bool ShouldExecute(Table table) =>
        !table.Columns
            .Except(table.Columns.GetPrimaryKeyColumns()).IsNullOrEmpty();
        
}