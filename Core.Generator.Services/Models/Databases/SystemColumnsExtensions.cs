﻿namespace Core.Generator.Services.Models.Databases;

public static class SystemColumnsExtensions
{
    public static ImmutableArray<string> ListOfActiveFieldTerminology { get; } =
        new List<string>
        {
            "active_ind",
            "activeind",
            "active",
            "isactive",
            "is_active",
            "void",
            "isvoid",
            "is_void",
            "isdeleted",
            "isdelete"
        }.ToImmutableArray();

    public static ImmutableArray<string> ListOfCreatedByTerminology { get; } =
        new List<string>
        {
            "create_by",
            "createby",
            "created_by",
            "createdby",
            "createdbyid"
        }.ToImmutableArray();

    public static ImmutableArray<string> ListOfCreatedDateTerminology { get; } =
        new List<string>
        {
            "create_date",
            "createdate",
            "created_date",
            "createddate",
            "createdon",
            "creationdate"
        }.ToImmutableArray();

    public static ImmutableArray<string> ListOfModifyByTerminology { get; } =
        new List<string>
        {
            "modify_by",
            "modifyby",
            "modifybyid",
            "modified_by",
            "modifiedby",
            "modifiedbyid",
            "modificationby",
            "modificationbyid",
            "updatedby",
            "updateby"
        }.ToImmutableArray();

    public static ImmutableArray<string> ListOfModifyDateTerminology { get; } =
        new List<string>
        {
            "modify_date",
            "modifydate",
            "modified_date",
            "modifieddate",
            "modificationdate",
            "modifiedon",
            "updateddate",
            "updatedate"
        }.ToImmutableArray();

    public static ImmutableArray<string> ListOfPermanentNameTerminology { get; } =
        new List<string>
        {
            "permanentname",
            "permname",
            "permanent"
        }.ToImmutableArray();

    public static ImmutableArray<string> ListOfTimeStampTerminology { get; } =
        new List<string>
        {
            "timestamp",
            "tstamp",
            "time_stamp"
        }.ToImmutableArray();

    public static bool IsActive(this object value) => DoesContain(value?.ToString(), ListOfActiveFieldTerminology);

    public static bool IsCreateBy(this object value) => DoesContain(value?.ToString(), ListOfCreatedByTerminology);

    public static bool IsCreateDate(this object value) => DoesContain(value?.ToString(), ListOfCreatedDateTerminology);

    public static bool IsModifyBy(this object value) => DoesContain(value?.ToString(), ListOfModifyByTerminology);

    public static bool IsModifyDate(this object value) => DoesContain(value?.ToString(), ListOfModifyDateTerminology);

    public static bool IsPermanentName(this object value) => DoesContain(value?.ToString(), ListOfPermanentNameTerminology);

    public static bool IsTimeStamp(this object value) => DoesContain(value?.ToString(), ListOfTimeStampTerminology);

    private static bool DoesContain(this string value, IEnumerable<string> stringArray)
    {
        return value is not null && stringArray.ToList().Any(x => value.Contains(x, StringComparison.InvariantCultureIgnoreCase));
    }
}