﻿namespace Core.Generator.Common;

public static class CursorBlock
{
    private static bool _waitCursorIsActive;

    public static IDisposable BeginWaitCursorBlock()
    {
        return ((!_waitCursorIsActive) ? new WaitCursorBlock() : null);
    }

    private class WaitCursorBlock : IDisposable
    {
        private readonly Cursor _oldCur;

        public WaitCursorBlock()
        {
            _waitCursorIsActive = true;
            _oldCur = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
        }

        public void Dispose()
        {
            Cursor.Current = _oldCur;
            _waitCursorIsActive = false;
        }
    }
}