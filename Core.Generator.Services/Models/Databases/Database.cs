﻿namespace Core.Generator.Services.Models.Databases;

public class Database : ICloneable
{
    public bool HasPermission { get; set; }

    public int Id { get; set; }

    public string Name { get; set; }

    public ImmutableList<Table> Tables { get; set; }

    public object Clone() =>
        new Database
        {
            HasPermission = HasPermission,
            Id = Id,
            Name = Name,
            Tables = Tables?.Clone().ToImmutableList() ?? new List<Table>().ToImmutableList()
        };
}