﻿namespace Core.Flux.Tests;

[Collection("Serial")]
public class AsyncDispatcherTests : IClassFixture<AsyncDispatcherFixture>
{
    private readonly AsyncDispatcherFixture _dispatcher;

    public AsyncDispatcherTests(AsyncDispatcherFixture dispatcher) => 
        _dispatcher = dispatcher;

    [Fact]
    public void AsyncDispatcher_Dispatch_ShouldInvokeRegisteredCallbacksOnDispatchedType()
    {
        _dispatcher.Reset();
        var callback1 = new Mock<Action<string>>();
        var callback2 = new Mock<Action<int>>();

        _dispatcher.Dispatcher.Register(callback1.Object);
        _dispatcher.Dispatcher.Register(callback2.Object);

        _dispatcher.Dispatcher.Dispatch("Test");

        callback1.Verify(x => x("Test"), Times.Once);
        callback2.Verify(x => x(It.IsAny<int>()), Times.Never);
    }

    [Fact]
    public void AsyncDispatcher_Register_ShouldAllowMultipleCallbacksWithTheSameType()
    {
        _dispatcher.Reset();
        var callback1 = new Action<string>(Console.Write);
        var callback2 = new Action<string>(Console.WriteLine);

        Action action = () =>
        {
            _dispatcher.Dispatcher.Register(callback1);
            _dispatcher.Dispatcher.Register(callback2);
        };
        action.Should().NotThrow();
    }

    [Fact]
    public void AsyncDispatcher_Register_ShouldNotThrowException()
    {
        _dispatcher.Reset();
        var callback = new Action<string>(Console.WriteLine);
        Action action = () => _dispatcher.Dispatcher.Register(callback);
        action.Should().NotThrow();
    }
}