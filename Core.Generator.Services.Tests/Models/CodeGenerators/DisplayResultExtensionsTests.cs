﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Models.CodeGenerators;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Models.CodeGenerators;

public class DisplayResultExtensionsTests : IClassFixture<DisplayResultFixture>
{
    private readonly DisplayResultFixture _displayResult;

    public DisplayResultExtensionsTests(DisplayResultFixture displayResult)
    {
        _displayResult = displayResult;
    }

    [Fact]
    public void DisplayResult_AddDisplayResult_ShouldMatchExpected_GivenListInput()
    {
        _displayResult.Reset();

        var list = new List<DisplayResult>
        {
            _displayResult.DisplayResult
        };

        var model = _displayResult.DisplayResult.AddDisplayResult(list);
        model.DisplayResults.Should().HaveCount(1);
    }

    [Fact]
    public void DisplayResult_AddDisplayResult_ShouldMatchExpected_GivenSingleInput()
    {
        _displayResult.Reset();
        var model = _displayResult.DisplayResult.AddDisplayResult(_displayResult.DisplayResult);
        model.DisplayResults.Should().HaveCount(1);
    }

    [Fact]
    public void DisplayResult_Clone_ShouldHaveTwoResultsWithoutTouchingOriginal_GivenListInput()
    {
        _displayResult.Reset();

        var list = new List<DisplayResult>
        {
            _displayResult.DisplayResult
        };

        var model = _displayResult.DisplayResult.AddDisplayResult(list);
        var newModel = (model.Clone() as DisplayResult).AddDisplayResult(list);

        model.DisplayResults.Should().HaveCount(1);
        newModel.DisplayResults.Should().HaveCount(2);
    }

    [Fact]
    public void DisplayResult_OrderList_ShouldNotTHrowException_GivenEmptyList()
    {
        _displayResult.Reset();
        var model = _displayResult.DisplayResult.OrderList();
        model.DisplayResults.Should().BeEmpty();
    }

    [Fact]
    public void DisplayResult_OrderList_ShouldSort_GivenEmptyList()
    {
        _displayResult.Reset();
        var list = new List<DisplayResult>
        {
            new (3, "Bob", "bob was here"),
            new (2, "Bob", "bob is here"),
            new (1, "Bob", "Bob is coming")
        };

        var list2 = new List<DisplayResult>
        {
            new (3, "Bob", list),
            new (2, "Bob", "bob is here"),
            new (1, "Bob", "Bob is coming")
        };

        var list3 = new List<DisplayResult>
        {
            new (3, "Bob", list2),
            new (2, "Bob", "bob is here"),
            new (1, "Bob", "Bob is coming")
        };

        var model = _displayResult.DisplayResult
            .AddDisplayResult(list3)
            .OrderList();

        // make sure it is order as well as its children
        model.DisplayResults.Should().HaveCount(3);
        model.DisplayResults.Should().BeInAscendingOrder(x => x.DisplayOrder);
        model.DisplayResults.Last().DisplayResults.Should().HaveCount(3);
        model.DisplayResults.Last().DisplayResults.Should().BeInAscendingOrder(x => x.DisplayOrder);
        model.DisplayResults.Last().DisplayResults.Last().DisplayResults.Should().HaveCount(3);
        model.DisplayResults.Last().DisplayResults.Last().DisplayResults.Should().BeInAscendingOrder(x => x.DisplayOrder);
    }

    [Fact]
    public void DisplayResult_ResetDisplayOrder_ShouldChangeOrder_GivenInput()
    {
        _displayResult.Reset();
        _displayResult.DisplayResult.DisplayOrder.Should().Be(1);
        var model = _displayResult.DisplayResult.ResetDisplayOrder(585);
        _displayResult.DisplayResult.DisplayOrder.Should().Be(585);
    }

    [Fact]
    public void DisplayResultExtensions_CreateDisplayResult_ParamVersion_ShouldReturnSingle_GivenListofOnePlusNull()
    {
        _displayResult.Reset();

        var model = "testing".CreateDisplayResult(187, new DisplayResult(3, "Bob", "bob was here"));
        model.Should().NotBeNull();
        model.DisplayResults.Should().BeEmpty();
        model.Name.Should().Be("testing");
        model.DisplayText.Should().Be("bob was here");
        model.DisplayOrder.Should().Be(187);
    }

    [Fact]
    public void DisplayResultExtensions_CreateDisplayResult_ShouldBeEmpty_GivenEmptyList()
    {
        _displayResult.Reset();
        var list = new List<DisplayResult>();
        var model = list.CreateDisplayResult(1, "testing");

        model.Should().NotBeNull();
        model.DisplayResults.Should().BeEmpty();
        model.DisplayText.Should().BeNullOrEmpty();
    }

    [Fact]
    public void DisplayResultExtensions_CreateDisplayResult_ShouldBeEmpty_GivenNullList()
    {
        _displayResult.Reset();
        List<DisplayResult> list = null;
        var model = list.CreateDisplayResult(1, "testing");

        model.Should().NotBeNull();
        model.DisplayResults.Should().BeEmpty();
        model.DisplayText.Should().BeNullOrEmpty();
    }

    [Fact]
    public void DisplayResultExtensions_CreateDisplayResult_ShouldReturnOneWithSubList_GivenListofOneWithSubList()
    {
        _displayResult.Reset();
        var subList = new List<DisplayResult>
        {
            new (3, "Bob", "bob was here"),
            new (2, "Bob", "bob is here"),
            null,
            new (1, "Bob", "Bob is coming")
        };

        var list = new List<DisplayResult>
        {
            new (3, "Bob", subList),
            null
        };

        var model = list.CreateDisplayResult(187, "testing");
        model.Should().NotBeNull();
        model.Name.Should().Be("testing");
        model.DisplayResults.Should().NotBeEmpty();
        model.DisplayResults.Should().HaveCount(3);
        model.DisplayResults.Should().BeInAscendingOrder(x => x.DisplayOrder);
        model.DisplayText.Should().BeNullOrEmpty();
        model.DisplayOrder.Should().Be(187);
    }

    [Fact]
    public void DisplayResultExtensions_CreateDisplayResult_ShouldReturnResultWithChildren_GivenListOfThree()
    {
        _displayResult.Reset();
        var list = new List<DisplayResult>
        {
            new (3, "Bob", "bob was here"),
            new (2, "Bob", "bob is here"),
            null,
            new (1, "Bob", "Bob is coming")
        };

        var model = list.CreateDisplayResult(187, "testing");
        model.Should().NotBeNull();
        model.DisplayResults.Should().NotBeEmpty();
        model.DisplayResults.Should().HaveCount(3);
        model.DisplayResults.Should().BeInAscendingOrder(x => x.DisplayOrder);
        model.Name.Should().Be("testing");
        model.DisplayText.Should().BeNullOrEmpty();
        model.DisplayOrder.Should().Be(187);
    }

    [Fact]
    public void DisplayResultExtensions_CreateDisplayResult_ShouldReturnSingle_GivenListofOnePlusNull()
    {
        _displayResult.Reset();
        var list = new List<DisplayResult>
        {
            new (3, "Bob", "bob was here"),
            null
        };

        var model = list.CreateDisplayResult(187, "testing");
        model.Should().NotBeNull();
        model.DisplayResults.Should().BeEmpty();
        model.Name.Should().Be("testing");
        model.DisplayText.Should().Be("bob was here");
        model.DisplayOrder.Should().Be(187);
    }

    [Fact]
    public void DisplayResultExtensions_OrderList_ShouldBeEmpty_GivenEmptyList()
    {
        _displayResult.Reset();
        var list = new List<DisplayResult>();
        var model = list.OrderList();

        model.Should().BeEmpty();
    }

    [Fact]
    public void DisplayResultExtensions_OrderList_ShouldBeEmpty_GivenNullList()
    {
        _displayResult.Reset();
        List<DisplayResult> list = null;
        var model = list.OrderList();

        model.Should().BeNull();
    }

    [Fact]
    public void DisplayResultExtensions_OrderList_ShouldSortButNotSubChildren_GivenEmptyList()
    {
        _displayResult.Reset();
        var list = new List<DisplayResult>
        {
            new (3, "Bob", "bob was here"),
            new (2, "Bob", "bob is here"),
            new (1, "Bob", "Bob is coming")
        };

        var list2 = new List<DisplayResult>
        {
            new (3, "Bob", list),
            new (2, "Bob", "bob is here"),
            new (1, "Bob", "Bob is coming")
        };

        var list3 = new List<DisplayResult>
        {
            new (3, "Bob", list2),
            new (2, "Bob", "bob is here"),
            new (1, "Bob", "Bob is coming")
        };

        var model = list3.OrderList();

        // make sure it is order as well as its children
        model.Should().HaveCount(3);
        model.Should().BeInAscendingOrder(x => x.DisplayOrder);
        model.Last().DisplayResults.Should().HaveCount(3);
        model.Last().DisplayResults.Should().BeInDescendingOrder(x => x.DisplayOrder);
        model.Last().DisplayResults.First().DisplayResults.Should().HaveCount(3);
        model.Last().DisplayResults.First().DisplayResults.Should().BeInDescendingOrder(x => x.DisplayOrder);
    }
}