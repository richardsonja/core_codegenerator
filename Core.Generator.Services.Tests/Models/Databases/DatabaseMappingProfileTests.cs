﻿using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using FluentAssertions;
using System.Data;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Databases;

public class DatabaseMappingProfileTests :
    IClassFixture<DatabaseMappingProfileFixture>,
    IClassFixture<DataTableFixture>
{
    private readonly DataTableFixture _dataTable;

    private readonly DatabaseMappingProfileFixture _mapper;

    public DatabaseMappingProfileTests(DatabaseMappingProfileFixture mapper, DataTableFixture dataTable)
    {
        _mapper = mapper;
        _dataTable = dataTable;
    }

    [Fact]
    public void DatabaseMappingProfile_Database_ShouldBeValid_GivenSetup()
    {
        _dataTable.SetDatabase();
        var results = _mapper.Mapper.Map<Database>(_dataTable.GetDataRow());

        results.Should().NotBeNull();
        results.Name.Should().Be("SomeTable");
        results.Id.Should().Be(11);
        results.HasPermission.Should().BeFalse();
        results.Tables.Should().BeNull();
    }

    [Fact]
    public void DatabaseMappingProfile_ServerVersion_ShouldBeValid_GivenSetup()
    {
        _dataTable.SetServerVersion();
        var results = _mapper.Mapper.Map<ServerVersion>(_dataTable.DataTable);

        results.Should().NotBeNull();
        results.FullVersion.Should().Be("1.1.1");
    }

    [Fact]
    public void DatabaseMappingProfile_ShouldBeValid_GivenSetup()
    {
        _mapper.Mapper.ConfigurationProvider.AssertConfigurationIsValid();
    }

    [Theory]
    [InlineData("bigint", SqlDbType.BigInt)]
    [InlineData("binary", SqlDbType.Binary)]
    [InlineData("bit", SqlDbType.Bit)]
    [InlineData("bool", SqlDbType.TinyInt)]
    [InlineData("variant", SqlDbType.Variant)]
    [InlineData("xml", SqlDbType.Xml)]
    [InlineData("bob", SqlDbType.Variant)]
    public void DatabaseMappingProfile_TableColumn_DataType_ShouldMatchExpect_GivenInput(
        string value,
        SqlDbType expected)
    {
        _dataTable.SetTableColumn();
        var dataRow = _dataTable.GetDataRow();
        dataRow[DatabaseColumnConstants.DataType] = value;
        var results = _mapper.Mapper.Map<TableColumn>(dataRow);

        results.Should().NotBeNull();
        results.DataType.Should().Be(expected);
    }

    [Fact]
    public void DatabaseMappingProfile_TableColumn_ShouldBeValid_GivenSetup()
    {
        _dataTable.SetTableColumn();
        var results = _mapper.Mapper.Map<TableColumn>(_dataTable.GetDataRow());

        results.Should().NotBeNull();
        results.CamelCaseName.Should().Be("testingSomething4");
        results.CharacterMaxLength.Should().Be(10);
        results.DatabaseName.Should().Be("Testing1");
        results.DataType.Should().Be(SqlDbType.NVarChar);
        results.ForeignColumn.Should().Be("Testing14");
        results.ForeignSchema.Should().Be("Testing15");
        results.ForeignTable.Should().Be("Testing13");
        results.HumanReadableFormattedName.Should().Be("Testing Something4");
        results.IsForeignKey.Should().BeTrue();
        results.IsIdentity.Should().BeTrue();
        results.IsIdentityOrIndex.Should().BeTrue();
        results.IsIndex.Should().BeTrue();
        results.IsNullable.Should().BeFalse();
        results.Name.Should().Be("TestingSomething4");
        results.OrdinalPosition.Should().Be(5);
        results.OriginalDataTypeString.Should().Be("NVarChar");
        results.PascalCaseName.Should().Be("TestingSomething4");
        results.Percision.Should().Be(7);
        results.Scale.Should().Be(8);
        results.TableName.Should().Be("Testing2");
        results.TableSchema.Should().Be("Testing3");

        results.CSharpType.Should().NotBeNull();
        results.CSharpType.Type.Should().Be(typeof(string));
        results.CSharpType.TypeString.Should().Be("string");

        results.SystemColumns.Should().NotBeNull();
        results.SystemColumns.IsActive.Should().BeFalse();
        results.SystemColumns.IsCreateBy.Should().BeFalse();
        results.SystemColumns.IsCreateDate.Should().BeFalse();
        results.SystemColumns.IsModifyBy.Should().BeFalse();
        results.SystemColumns.IsModifyDate.Should().BeFalse();
        results.SystemColumns.IsPermanentName.Should().BeFalse();
        results.SystemColumns.IsTimeStamp.Should().BeFalse();
    }
}