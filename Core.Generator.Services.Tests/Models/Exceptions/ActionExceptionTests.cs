﻿using Core.Generator.Services.Models.Exceptions;
using FluentAssertions;
using System;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Exceptions;

public class ActionExceptionTests
{
    [Fact]
    public void ActionException_ShouldBeValid_GivenInnerException()
    {
        var exception = new Exception("was here");
        var model = new ActionException("bob", exception);

        model.Message.Should().Be("Error During Processing Action");
        model.ActionName.Should().Be("bob");
        model.MemberName.Should().Be(nameof(ActionException_ShouldBeValid_GivenInnerException));
        model.SourceFilePath.Should().Contain($"{nameof(ActionExceptionTests)}.cs");
        model.SourceLineNumber.Should().Be(14);
        model.InnerException.Should().NotBeNull();
        model.InnerException.Message.Should().Be("was here");
    }

    [Fact]
    public void ActionException_ShouldBeValid_GivenNoInnerException()
    {
        var model = new ActionException("bob");

        model.Message.Should().Be("Error During Processing Action");
        model.ActionName.Should().Be("bob");
        model.MemberName.Should().Be(nameof(ActionException_ShouldBeValid_GivenNoInnerException));
        model.SourceFilePath.Should().Contain($"{nameof(ActionExceptionTests)}.cs");
        model.SourceLineNumber.Should().Be(28);
        model.InnerException.Should().BeNull();
    }
}