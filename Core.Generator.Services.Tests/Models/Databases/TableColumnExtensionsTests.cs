﻿using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Data;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Databases;

public class TableColumnExtensionsTests : IClassFixture<DatabaseTableColumnFixture>
{
    private readonly DatabaseTableColumnFixture _databaseTableColumn;

    public TableColumnExtensionsTests(DatabaseTableColumnFixture databaseTableColumn)
    {
        _databaseTableColumn = databaseTableColumn;
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_SqlDbType_ShouldEmpty_GivenEmptyListEmptyTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<SqlDbType> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_SqlDbType_ShouldEmpty_GivenEmptyListNullTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<SqlDbType> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_SqlDbType_ShouldEmpty_GivenEmptyListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        var dataTypes = new List<SqlDbType> { SqlDbType.VarChar };

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_SqlDbType_ShouldNull_GivenNullListEmptyTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<SqlDbType> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_SqlDbType_ShouldNull_GivenNullListNullTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<SqlDbType> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_SqlDbType_ShouldNull_GivenNullListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        var dataTypes = new List<SqlDbType> { SqlDbType.VarChar };

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_SqlDbType_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var dataTypes = new List<SqlDbType> { SqlDbType.VarChar };

        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.DataType = SqlDbType.VarChar;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.DataType = SqlDbType.NVarChar;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.DataType = SqlDbType.Char;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.DataType = SqlDbType.Bit;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.FilterColumns(dataTypes);
        results.Should().NotBeEmpty();
        results.Should().HaveCount(1);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
        results.Should().Contain(x => x.DataType == SqlDbType.VarChar);
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_string_ShouldEmpty_GivenEmptyListEmptyStrings()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<string> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_string_ShouldEmpty_GivenEmptyListNullStrings()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<string> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_string_ShouldEmpty_GivenEmptyListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        var dataTypes = new List<string> { "string" };

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_string_ShouldNull_GivenNullListEmptyStrings()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<string> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_string_ShouldNull_GivenNullListNullStrings()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<string> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_string_ShouldNull_GivenNullListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        var dataTypes = new List<string> { "string" };

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_string_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var dataTypes = new List<string> { "string" };

        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.CSharpType = new CSharpType(typeof(string), "string");

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.CSharpType = new CSharpType(typeof(bool), "bool");

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.CSharpType = new CSharpType(typeof(int), "int");

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.CSharpType = new CSharpType(typeof(Guid), "Guid");

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.FilterColumns(dataTypes);
        results.Should().NotBeEmpty();
        results.Should().HaveCount(1);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
        results.Should().Contain(x => x.CSharpType.TypeString == "string");
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_Type_ShouldEmpty_GivenEmptyListEmptyTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<Type> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_Type_ShouldEmpty_GivenEmptyListNullTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<Type> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_Type_ShouldEmpty_GivenEmptyListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        var dataTypes = new List<Type> { typeof(string) };

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_Type_ShouldNull_GivenNullListEmptyTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<Type> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_Type_ShouldNull_GivenNullListNullTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<Type> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_Type_ShouldNull_GivenNullListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        var dataTypes = new List<Type> { typeof(string) };

        _databaseTableColumn.TableColumns.FilterColumns(dataTypes).Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_FilterColumns_Type_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var dataTypes = new List<Type> { typeof(string) };

        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.CSharpType = new CSharpType(typeof(string), "string");

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.CSharpType = new CSharpType(typeof(bool), "bool");

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.CSharpType = new CSharpType(typeof(int), "int");

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.CSharpType = new CSharpType(typeof(Guid), "Guid");

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.FilterColumns(dataTypes);
        results.Should().NotBeEmpty();
        results.Should().HaveCount(1);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
        results.Should().Contain(x => x.CSharpType.Type == typeof(string));
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_SqlDbType_ShouldEmpty_GivenEmptyListEmptyTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<SqlDbType> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_SqlDbType_ShouldEmpty_GivenEmptyListNullTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<SqlDbType> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_SqlDbType_ShouldEmpty_GivenEmptyListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        var dataTypes = new List<SqlDbType> { SqlDbType.VarChar };

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_SqlDbType_ShouldNull_GivenNullListEmptyTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<SqlDbType> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_SqlDbType_ShouldNull_GivenNullListNullTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<SqlDbType> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_SqlDbType_ShouldNull_GivenNullListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        var dataTypes = new List<SqlDbType> { SqlDbType.VarChar };

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_SqlDbType_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var dataTypes = new List<SqlDbType> { SqlDbType.VarChar };

        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.DataType = SqlDbType.VarChar;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.DataType = SqlDbType.NVarChar;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.DataType = SqlDbType.Char;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.DataType = SqlDbType.Bit;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.FilterOutColumns(dataTypes);
        results.Should().NotBeEmpty();
        results.Should().HaveCount(3);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
        results.Should().NotContain(x => x.DataType == SqlDbType.VarChar);
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_string_ShouldEmpty_GivenEmptyListEmptyStrings()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<string> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_string_ShouldEmpty_GivenEmptyListNullStrings()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<string> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_string_ShouldEmpty_GivenEmptyListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        var dataTypes = new List<string> { "string" };

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_string_ShouldNull_GivenNullListEmptyStrings()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<string> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_string_ShouldNull_GivenNullListNullStrings()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<string> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_string_ShouldNull_GivenNullListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        var dataTypes = new List<string> { "string" };

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_string_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var dataTypes = new List<string> { "string" };

        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.CSharpType = new CSharpType(typeof(string), "string");

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.CSharpType = new CSharpType(typeof(bool), "bool");

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.CSharpType = new CSharpType(typeof(int), "int");

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.CSharpType = new CSharpType(typeof(Guid), "Guid");

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.FilterOutColumns(dataTypes);
        results.Should().NotBeEmpty();
        results.Should().HaveCount(3);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
        results.Should().NotContain(x => x.CSharpType.TypeString == "string");
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_Type_ShouldEmpty_GivenEmptyListEmptyTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<Type> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_Type_ShouldEmpty_GivenEmptyListNullTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        List<Type> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_Type_ShouldEmpty_GivenEmptyListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();
        var dataTypes = new List<Type> { typeof(string) };

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_Type_ShouldNull_GivenNullListEmptyTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<Type> dataTypes = new();

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_Type_ShouldNull_GivenNullListNullTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        List<Type> dataTypes = null;

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_Type_ShouldNull_GivenNullListValidTypes()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();
        var dataTypes = new List<Type> { typeof(string) };

        _databaseTableColumn.TableColumns.FilterOutColumns(dataTypes).Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_FilterOutColumns_Type_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var dataTypes = new List<Type> { typeof(string) };

        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.CSharpType = new CSharpType(typeof(string), "string");

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.CSharpType = new CSharpType(typeof(bool), "bool");

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.CSharpType = new CSharpType(typeof(int), "int");

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.CSharpType = new CSharpType(typeof(Guid), "Guid");

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.FilterOutColumns(dataTypes);
        results.Should().NotBeEmpty();
        results.Should().HaveCount(3);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);

        results.Should().NotContain(x => x.CSharpType.Type == typeof(string));
    }

    [Fact]
    public void TableColumnExtensions_GetActiveBooleanColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetActiveBooleanColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetActiveBooleanColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetActiveBooleanColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetActiveBooleanColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsActive = true;
        column1.CSharpType = new CSharpType(typeof(bool), "bool");

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsActive = false;
        column2.CSharpType = new CSharpType(typeof(bool), "bool");

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsActive = true;
        column3.CSharpType = new CSharpType(typeof(string), "string");

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsActive = false;
        column4.CSharpType = new CSharpType(typeof(bool), "bool");

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetActiveBooleanColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(1);
    }

    [Fact]
    public void TableColumnExtensions_GetDefaultSqlValue_ShouldFalse_GivenEmptyObject()
    {
        _databaseTableColumn.Reset();
        TableColumn model = new();

        model.GetDefaultSqlValue().Should().Be("\"TBD\"");
    }

    [Fact]
    public void TableColumnExtensions_GetDefaultSqlValue_ShouldFalse_GivenNull()
    {
        _databaseTableColumn.Reset();
        TableColumn model = null;

        model.GetDefaultSqlValue().Should().Be("\"TBD\"");
    }

    [Theory]
    [InlineData(typeof(bool), "bool", "1")]
    [InlineData(typeof(bool?), "bool?", "1")]
    [InlineData(typeof(string), "string", "\"TBD\"")]
    [InlineData(typeof(decimal), "decimal", "1")]
    [InlineData(typeof(decimal?), "decimal?", "1")]
    [InlineData(typeof(double), "double", "1")]
    [InlineData(typeof(double?), "double?", "1")]
    [InlineData(typeof(int), "int", "1")]
    [InlineData(typeof(int?), "int?", "1")]
    [InlineData(typeof(short), "short", "1")]
    [InlineData(typeof(short?), "short?", "1")]
    [InlineData(typeof(long), "long", "1")]
    [InlineData(typeof(long?), "long?", "1")]
    [InlineData(typeof(float), "float", "1")]
    [InlineData(typeof(float?), "float?", "1")]
    public void TableColumnExtensions_GetDefaultSqlValue_ShouldReturnExpected_GivenInput(
        Type type,
        string typeString,
        string expected)
    {
        _databaseTableColumn.Reset();
        var model = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        model.CSharpType = new CSharpType(type, typeString);

        model.GetDefaultSqlValue().Should().Be(expected);
    }

    [Fact]
    public void TableColumnExtensions_GetIsActiveColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetIsActiveColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetIsActiveColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetIsActiveColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetIsActiveColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsActive = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsActive = false;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsActive = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsActive = false;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetIsActiveColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_GetIsCreateByColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetIsCreateByColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetIsCreateByColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetIsCreateByColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetIsCreateByColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsCreateBy = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsCreateBy = false;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsCreateBy = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsCreateBy = false;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetIsCreateByColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_GetIsCreateDateColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetIsCreateDateColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetIsCreateDateColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetIsCreateDateColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetIsCreateDateColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsCreateDate = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsCreateDate = false;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsCreateDate = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsCreateDate = false;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetIsCreateDateColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_GetIsCreatedColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetIsCreatedColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetIsCreatedColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetIsCreatedColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetIsCreatedColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsCreateBy = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsCreateBy = false;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsCreateDate = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsCreateDate = false;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetIsCreatedColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_GetIsModifiedColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetIsModifiedColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetIsModifiedColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetIsModifiedColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetIsModifiedColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsModifyBy = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsModifyBy = false;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsModifyDate = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsModifyDate = false;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetIsModifiedColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_GetIsModifyByColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetIsModifyByColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetIsModifyByColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetIsModifyByColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetIsModifyByColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsModifyBy = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsModifyBy = false;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsModifyBy = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsModifyBy = false;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetIsModifyByColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_GetIsModifyDateColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetIsModifyDateColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetIsModifyDateColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetIsModifyDateColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetIsModifyDateColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsModifyDate = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsModifyDate = false;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsModifyDate = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsModifyDate = false;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetIsModifyDateColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_GetNonActiveBooleansOrActiveNonBooleanColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetNonActiveBooleansOrActiveNonBooleanColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetNonActiveBooleansOrActiveNonBooleanColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetNonActiveBooleansOrActiveNonBooleanColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetNonActiveBooleansOrActiveNonBooleanColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsActive = true;
        column1.CSharpType = new CSharpType(typeof(bool), "bool");

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsActive = false;
        column2.CSharpType = new CSharpType(typeof(bool), "bool");

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsActive = true;
        column3.CSharpType = new CSharpType(typeof(string), "string");

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsActive = false;
        column4.CSharpType = new CSharpType(typeof(Guid), "Guid");

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetNonActiveBooleansOrActiveNonBooleanColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
    }

    [Fact]
    public void TableColumnExtensions_GetNonPrimaryAndNonTimeStampColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetNonPrimaryAndNonTimeStampColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetNonPrimaryAndNonTimeStampColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetNonPrimaryAndNonTimeStampColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetNonPrimaryAndNonTimeStampColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.IsIndex = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.IsIdentity = true;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsTimeStamp = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetNonPrimaryAndNonTimeStampColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(1);
    }

    [Fact]
    public void TableColumnExtensions_GetNonSystemColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetNonSystemColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetNonSystemColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetNonSystemColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetNonSystemColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsCreateBy = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsCreateDate = true;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsModifyBy = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsModifyDate = true;

        var column5 = _databaseTableColumn.CreateTableColumns("Bob5", "WasHere5", "DB5", 5);
        column5.SystemColumns.IsTimeStamp = true;

        var column6 = _databaseTableColumn.CreateTableColumns("Bob6", "WasHere6", "DB6", 6);
        column6.SystemColumns.IsActive = true;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4,
            column5,
            column6
        };

        var results = tableColumns.GetNonSystemColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(1);
    }

    [Fact]
    public void TableColumnExtensions_GetPermanentNameColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetPermanentNameColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetPermanentNameColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetPermanentNameColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetPermanentNameColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.SystemColumns.IsPermanentName = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.SystemColumns.IsPermanentName = false;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.SystemColumns.IsPermanentName = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.SystemColumns.IsPermanentName = false;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetPermanentNameColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_GetPermanentNameOrNonActivePrimaryOrForeignColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetPermanentNameOrNonActivePrimaryOrForeignColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetPermanentNameOrNonActivePrimaryOrForeignColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetPermanentNameOrNonActivePrimaryOrForeignColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetPermanentNameOrNonActivePrimaryOrForeignColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var permanentName = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 5);
        permanentName.SystemColumns.IsPermanentName = true;
        permanentName.SystemColumns.IsActive = true;

        var inactiveIdentity = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 4);
        inactiveIdentity.IsIdentity = true;
        inactiveIdentity.SystemColumns.IsActive = false;

        var inactiveIndex = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        inactiveIndex.IsIndex = true;
        inactiveIndex.SystemColumns.IsActive = false;

        var inactiveForiegn = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 2);
        inactiveForiegn.ForeignColumn = "bob2000";
        inactiveForiegn.SystemColumns.IsActive = false;

        var active = _databaseTableColumn.CreateTableColumns("Bob5", "WasHere5", "DB5", 1);
        active.SystemColumns.IsActive = true;

        var tableColumns = new List<TableColumn>
        {
            permanentName,
            inactiveIdentity,
            inactiveIndex,
            inactiveForiegn,
            active
        };

        var results = tableColumns.GetPermanentNameOrNonActivePrimaryOrForeignColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(4);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_GetPrimaryKeyColumns_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.GetPrimaryKeyColumns().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_GetPrimaryKeyColumns_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.GetPrimaryKeyColumns().Should().BeNull();
    }

    [Fact]
    public void TableColumnExtensions_GetPrimaryKeyColumns_ShouldReturnExpected_GivenValidList()
    {
        _databaseTableColumn.Reset();
        var column1 = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        column1.IsIndex = true;

        var column2 = _databaseTableColumn.CreateTableColumns("Bob2", "WasHere2", "DB2", 2);
        column2.IsIndex = false;

        var column3 = _databaseTableColumn.CreateTableColumns("Bob3", "WasHere3", "DB3", 3);
        column3.IsIdentity = true;

        var column4 = _databaseTableColumn.CreateTableColumns("Bob4", "WasHere4", "DB4", 4);
        column4.IsIdentity = false;

        var tableColumns = new List<TableColumn>
        {
            column3,
            column1,
            column2,
            column4
        };

        var results = tableColumns.GetPrimaryKeyColumns();
        results.Should().NotBeEmpty();
        results.Should().HaveCount(2);
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_IsBoolean_ShouldFalse_GivenEmptyObject()
    {
        _databaseTableColumn.Reset();
        TableColumn model = new();

        model.IsBoolean().Should().BeFalse();
    }

    [Fact]
    public void TableColumnExtensions_IsBoolean_ShouldFalse_GivenNull()
    {
        _databaseTableColumn.Reset();
        TableColumn model = null;

        model.IsBoolean().Should().BeFalse();
    }

    [Theory]
    [InlineData(typeof(bool), "bool", true)]
    [InlineData(typeof(bool?), "bool?", true)]
    [InlineData(typeof(string), "string", false)]
    [InlineData(typeof(decimal), "decimal", false)]
    [InlineData(typeof(decimal?), "decimal?", false)]
    [InlineData(typeof(double), "double", false)]
    [InlineData(typeof(double?), "double?", false)]
    [InlineData(typeof(int), "int", false)]
    [InlineData(typeof(int?), "int?", false)]
    [InlineData(typeof(short), "short", false)]
    [InlineData(typeof(short?), "short?", false)]
    [InlineData(typeof(long), "long", false)]
    [InlineData(typeof(long?), "long?", false)]
    [InlineData(typeof(float), "float", false)]
    [InlineData(typeof(float?), "float?", false)]
    public void TableColumnExtensions_IsBoolean_ShouldReturnExpected_GivenInput(
        Type type,
        string typeString,
        bool expected)
    {
        _databaseTableColumn.Reset();
        var model = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        model.CSharpType = new CSharpType(type, typeString);

        model.IsBoolean().Should().Be(expected);
    }

    [Fact]
    public void TableColumnExtensions_IsNumeric_ShouldFalse_GivenEmptyObject()
    {
        _databaseTableColumn.Reset();
        TableColumn model = new();

        model.IsNumeric().Should().BeFalse();
    }

    [Fact]
    public void TableColumnExtensions_IsNumeric_ShouldFalse_GivenNull()
    {
        _databaseTableColumn.Reset();
        TableColumn model = null;

        model.IsNumeric().Should().BeFalse();
    }

    [Theory]
    [InlineData(typeof(bool), "bool", false)]
    [InlineData(typeof(bool?), "bool?", false)]
    [InlineData(typeof(string), "string", false)]
    [InlineData(typeof(decimal), "decimal", true)]
    [InlineData(typeof(decimal?), "decimal?", true)]
    [InlineData(typeof(double), "double", true)]
    [InlineData(typeof(double?), "double?", true)]
    [InlineData(typeof(int), "int", true)]
    [InlineData(typeof(int?), "int?", true)]
    [InlineData(typeof(short), "short", true)]
    [InlineData(typeof(short?), "short?", true)]
    [InlineData(typeof(long), "long", true)]
    [InlineData(typeof(long?), "long?", true)]
    [InlineData(typeof(float), "float", true)]
    [InlineData(typeof(float?), "float?", true)]
    public void TableColumnExtensions_IsNumeric_ShouldReturnExpected_GivenInput(
        Type type,
        string typeString,
        bool expected)
    {
        _databaseTableColumn.Reset();
        var model = _databaseTableColumn.CreateTableColumns("Bob1", "WasHere1", "DB1", 1);
        model.CSharpType = new CSharpType(type, typeString);

        model.IsNumeric().Should().Be(expected);
    }

    [Fact]
    public void TableColumnExtensions_OrderByPosition_ShouldBeAscending_GivenLIst()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetTableColumns("Bob", "Was Here", "Question");

        var results = _databaseTableColumn.TableColumns.OrderByPosition();
        results.Should().NotBeEmpty();
        results.Should().BeInAscendingOrder(x => x.OrdinalPosition);
    }

    [Fact]
    public void TableColumnExtensions_OrderByPosition_ShouldEmpty_GivenEmptyList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetEmptyTableColumns();

        _databaseTableColumn.TableColumns.OrderByPosition().Should().BeEmpty();
    }

    [Fact]
    public void TableColumnExtensions_OrderByPosition_ShouldNull_GivenNullList()
    {
        _databaseTableColumn.Reset();
        _databaseTableColumn.SetNullTableColumns();

        _databaseTableColumn.TableColumns.OrderByPosition().Should().BeNull();
    }
}