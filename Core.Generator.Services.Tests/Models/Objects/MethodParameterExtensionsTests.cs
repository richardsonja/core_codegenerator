﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Tests.Fixtures.Models.Objects;
using FluentAssertions;
using System.Collections.Generic;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Objects;

public class MethodParameterExtensionsTests : IClassFixture<MethodParameterFixture>, IClassFixture<MethodFixture>
{
    private readonly MethodFixture _method;
    private readonly MethodParameterFixture _methodParameter;

    public MethodParameterExtensionsTests(MethodParameterFixture methodParameter, MethodFixture method)
    {
        _methodParameter = methodParameter;
        _method = method;
    }

    [Fact]
    public void MethodParameter_AddAttributes_ToString_ShouldHaveAttribute_GivenAddedAttributes()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1),
            _methodParameter.Create("Bob", "Was Here", 2)
        };

        var attributes = new List<Method>
        {
            _method.CreateAttributeMethod("BobMethod", list),
            _method.CreateAttributeMethod("AbeMethod", null)
        };
        _method.Reset();

        var results = _methodParameter.MethodParameter.AddAttributes(attributes).ToString();
        results.TrimInternalExtraSpaces().Should().Be("[AbeMethod] [BobMethod(Bob Is Here, Bob Was Here)] Bob Was Here");
    }

    [Fact]
    public void MethodParameter_AddAttributes_ToString_ShouldHaveAttribute_GivenSingleAttribute()
    {
        var attribute = _method.CreateAttributeMethod("AbeMethod", null);
        _method.Reset();
        _methodParameter.Reset();

        var results = _methodParameter.MethodParameter.AddAttributes(attribute).ToString();
        results.TrimInternalExtraSpaces().Should().Be("[AbeMethod] Bob");
    }

    [Theory]
    [InlineData("was here", "was here")]
    [InlineData("", "")]
    [InlineData(null, "")]
    public void MethodParameter_ToString_ShouldMatchExpected_GivenName(string name, string expected)
    {
        _methodParameter.Create(name, 1);
        _methodParameter.MethodParameter.ToString().Should().Be(expected);
    }

    [Theory]
    [InlineData("bob", "was here", "bob was here")]
    [InlineData("", "was here", "was here")]
    [InlineData(null, "was here", "was here")]
    [InlineData("bob", "", "bob")]
    [InlineData("bob", null, "bob")]
    public void MethodParameter_ToString_ShouldMatchExpected_GivenTypeAndName(string type, string name, string expected)
    {
        _methodParameter.Create(type, name, 1);
        _methodParameter.MethodParameter.ToString().Should().Be(expected);
    }

    [Fact]
    public void MethodParameterExtensions_Clone_ShouldHaveAllAttributes_GivenSetup()
    {
        var attribute = _method.CreateAttributeMethod("AbeMethod", null);

        _method.Reset();
        _methodParameter.Reset();
        _methodParameter.Create("Bob", "Is Here", 1);

        var results = _methodParameter
            .MethodParameter
            .AddAttributes(attribute);

        results.Name.Should().Be("Is Here");
        results.Type.Should().Be("Bob");
        results.Order.Should().Be(1);

        results.ToString().TrimInternalExtraSpaces().Should().Be("[AbeMethod] Bob Is Here");
    }

    [Fact]
    public void MethodParameterExtensions_ToParameter_ShouldBeNull_GivenEmptyList()
    {
        var list = new List<MethodParameter>();

        var results = list.ToParameter();
        results.Should().BeNull();
    }

    [Fact]
    public void MethodParameterExtensions_ToParameter_ShouldBeNull_GivenNullList()
    {
        List<MethodParameter> list = null;

        var results = list.ToParameter();
        results.Should().BeNull();
    }

    [Fact]
    public void MethodParameterExtensions_ToParameter_ShouldCreateExpectedString_GivenAllItemsOrderOfZero()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Was Here", 0),
            _methodParameter.Create("Bob", "Is Here", 0),
            _methodParameter.Create("Abe", "Visited", 0)
        };

        var results = list.ToParameter();
        results.Should().Be("Bob Is Here, Abe Visited, Bob Was Here");
    }

    [Fact]
    public void MethodParameterExtensions_ToParameter_ShouldCreateExpectedString_GivenTwoItemsInList()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1),
            _methodParameter.Create("Bob", "Was Here", 2)
        };

        var results = list.ToParameter();
        results.Should().Be("Bob Is Here, Bob Was Here");
    }

    [Fact]
    public void MethodParameterExtensions_ToParameter_ShouldCreateExpectedString_GivenTwoItemsInListInDifferentORder()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 2),
            _methodParameter.Create("Bob", "Was Here", 1)
        };

        var results = list.ToParameter();
        results.Should().Be("Bob Was Here, Bob Is Here");
    }
}