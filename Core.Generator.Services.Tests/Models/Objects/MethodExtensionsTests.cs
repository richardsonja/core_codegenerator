﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Tests.Fixtures.Models.Objects;
using FluentAssertions;
using System;
using System.Collections.Generic;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Objects;

public class MethodExtensionsTests : IClassFixture<MethodFixture>, IClassFixture<MethodParameterFixture>
{
    private readonly MethodFixture _method;
    private readonly MethodParameterFixture _methodParameter;

    public MethodExtensionsTests(MethodFixture method, MethodParameterFixture methodParameter)
    {
        _method = method;
        _methodParameter = methodParameter;
    }

    [Fact]
    public void Method_AddAttributes_ToString_ShouldHaveAttribute_GivenAddedAttributes()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1),
            _methodParameter.Create("Bob", "Was Here", 2)
        };

        var attributes = new List<Method>
        {
            _method.CreateAttributeMethod("BobMethod", list),
            _method.CreateAttributeMethod("AbeMethod", null)
        };
        _method.Reset();

        var results = _method.Method.AddAttributes(attributes).ToString();
        results.TrimInternalExtraSpaces().Should().Be("[AbeMethod] [BobMethod(Bob Is Here, Bob Was Here)] public void Bob() { }");
    }

    [Fact]
    public void Method_AddAttributes_ToString_ShouldHaveAttribute_GivenSingle()
    {
        var attribute = _method.CreateAttributeMethod("AbeMethod", null);
        _method.Reset();

        var results = _method.Method.AddAttributes(attribute).ToString();
        results.TrimInternalExtraSpaces().Should().Be("[AbeMethod] public void Bob() { }");
    }

    [Fact]
    public void Method_AddAttributes_ToString_ShouldNotThrow_GivenEmptyList()
    {
        List<Method> attribute = new List<Method>();
        _method.Reset();

        Action results = () => _method.Method.AddAttributes(attribute);
        results.Should().NotThrow();
    }

    [Fact]
    public void Method_AddAttributes_ToString_ShouldNotThrow_GivenNullList()
    {
        List<Method> attribute = null;
        _method.Reset();

        Action results = () => _method.Method.AddAttributes(attribute);
        results.Should().NotThrow();
    }

    [Fact]
    public void Method_AddAttributes_ToString_ShouldNotThrow_GivenNullSingle()
    {
        Method attribute = null;
        _method.Reset();

        Action results = () => _method.Method.AddAttributes(attribute);
        results.Should().NotThrow();
    }

    [Fact]
    public void Method_AddBody_List_ShouldThrowException_GivenAddBodyButHasNoBodyIsSetToTrue()
    {
        _method.Reset();

        Action results = () => _method.Method.SetHasNoBody(true).AddBody(new List<string> { "This Will Throw Exception" });
        results.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void Method_AddBody_ShouldThrowException_GivenAddBodyButHasNoBodyIsSetToTrue()
    {
        _method.Reset();

        Action results = () => _method.Method.SetHasNoBody(true).AddBody("This Will Throw Exception");
        results.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void Method_AddBody_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _method.Reset();

        var results = _method.Method.AddBody("var test = new TestObject();").ToString();
        results.TrimInternalExtraSpaces().Should().Be("public void Bob() => var test = new TestObject();");
    }

    [Fact]
    public void Method_AddBody_ToString_ShouldDisplayBasicMethodWithBody_GivenListInput()
    {
        _method.Reset();

        var tsil = new List<string>
        {
            "var test = new TestObject();"
        };

        var results = _method.Method.AddBody(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("public void Bob() => var test = new TestObject();");
    }

    [Fact]
    public void Method_AddBody_ToString_ShouldNotThrow_GivenEmptyList()
    {
        List<string> body = new List<string>();
        _method.Reset();

        Action results = () => _method.Method.AddBody(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void Method_AddBody_ToString_ShouldNotThrow_GivenNullList()
    {
        List<string> body = null;
        _method.Reset();

        Action results = () => _method.Method.AddBody(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void Method_AddBody_ToString_ShouldNotThrow_GivenNullSingle()
    {
        string body = null;
        _method.Reset();

        Action results = () => _method.Method.AddBody(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void Method_AddParameters_ToString_ShouldHaveParameters_GivenAddedParameters()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1),
            _methodParameter.Create("Bob", "Was Here", 2)
        };
        _method.Reset();

        var results = _method.Method.AddParameters(list).ToString();
        results.TrimInternalExtraSpaces().Should().Be("public void Bob(Bob Is Here, Bob Was Here) { }");
    }

    [Fact]
    public void Method_AddParameters_ToString_ShouldHaveParameters_GivenSingleParameters()
    {
        var parameter = _methodParameter.Create("Bob", "Is Here", 1);
        _method.Reset();

        var results = _method.Method.AddParameters(parameter).ToString();
        results.TrimInternalExtraSpaces().Should().Be("public void Bob(Bob Is Here) { }");
    }

    [Fact]
    public void Method_AddParameters_ToString_ShouldNotThrow_GivenEmptyList()
    {
        List<MethodParameter> attribute = new List<MethodParameter>();
        _method.Reset();

        Action results = () => _method.Method.AddParameters(attribute);
        results.Should().NotThrow();
    }

    [Fact]
    public void Method_AddParameters_ToString_ShouldNotThrow_GivenNullList()
    {
        List<MethodParameter> attribute = null;
        _method.Reset();

        Action results = () => _method.Method.AddParameters(attribute);
        results.Should().NotThrow();
    }

    [Fact]
    public void Method_AddParameters_ToString_ShouldNotThrow_GivenNullSingle()
    {
        MethodParameter attribute = null;
        _method.Reset();

        Action results = () => _method.Method.AddParameters(attribute);
        results.Should().NotThrow();
    }

    [Fact]
    public void Method_AddReturnType_ShouldThrowException_GivenAddBodyButHasNoBodyIsSetToTrue()
    {
        _method.Reset();

        Action results = () => _method.Method.SetHasNoBody(true).AddReturnType("This Will Throw Exception");
        results.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void Method_AddReturnType_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _method.Reset();

        var results = _method.Method.AddReturnType("BLARP").ToString();
        results.TrimInternalExtraSpaces().Should().Be("public BLARP Bob() { }");
    }

    [Fact]
    public void Method_Clone_ShouldHaveAllAttributes_GivenSetup()
    {
        var attribute = _method.CreateAttributeMethod("AbeMethod", null);
        var parameter = _methodParameter.Create("Bob", "Is Here", 1);

        _method.Reset();

        var results = _method
            .Method
            .AddAttributes(attribute)
            .AddParameters(parameter)
            .AddReturnType("BLARP")
            .SetAccessorType(AccessorType.Public)
            .AddBody("var test = new TestObject();      ")
            .SetHasNoBody(false)
            .SetIsStatic(true)
            .SetIsAsync(true)
            .SetIsOverride(true)
            .SetIsShortHandMethod(false)
            .Clone() as Method;

        results.Name.Should().Be("Bob");
        results.AccessorType.Should().Be(AccessorType.Public);
        results.Parameters.Should().HaveCount(1);
        results.Attributes.Should().HaveCount(1);
        results.ReturnType.Should().Be("BLARP");
        results.Body.Should().Contain("var test = new TestObject();");
        results.HasNoBody.Should().BeFalse();
        results.IsStatic.Should().BeTrue();
        results.IsShortHandMethod.Should().BeFalse();
        results.IsAsync.Should().BeTrue();
        results.IsConstructor.Should().BeFalse();

        results.ToString().TrimInternalExtraSpaces().Should().Be("[AbeMethod] public static override async Task<BLARP> Bob(Bob Is Here) => var test = new TestObject();");
    }

    [Theory]
    [InlineData(AccessorType.Internal, "internal void Bob() { }")]
    [InlineData(AccessorType.Public, "public void Bob() { }")]
    [InlineData(AccessorType.Private, "private void Bob() { }")]
    [InlineData(AccessorType.Protected, "protected void Bob() { }")]
    public void Method_SetAccessorType_ToString_ShouldMatchExpected_GivenInput(AccessorType value, string expected)
    {
        _method.Reset();

        var results = _method.Method.SetAccessorType(value).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void Method_SetConstructorMethod_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _method.Reset();

        var results = _method.Method.SetConstructorMethod().ToString();
        results.TrimInternalExtraSpaces().Should().Be("public Bob() { }");
    }

    [Fact]
    public void Method_SetHasNoBody_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _method.Reset();

        var results = _method.Method.SetHasNoBody().ToString();
        results.TrimInternalExtraSpaces().Should().Be("public Bob()");
    }

    [Fact]
    public void Method_SetIsAsync_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _method.Reset();

        var results = _method.Method.SetIsAsync().ToString();
        results.TrimInternalExtraSpaces().Should().Be("public async Task Bob() { }");
    }

    [Fact]
    public void Method_SetIsOverride_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _method.Reset();

        var results = _method.Method.SetIsOverride().ToString();
        results.TrimInternalExtraSpaces().Should().Be("public override void Bob() { }");
    }

    [Fact]
    public void Method_SetIsShortHandMethod_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _method.Reset();

        var results = _method.Method.SetIsShortHandMethod().ToString();
        results.TrimInternalExtraSpaces().Should().Be("void Bob { }");
    }

    [Fact]
    public void Method_SetIsStatic_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _method.Reset();

        var results = _method.Method.SetIsStatic().ToString();
        results.TrimInternalExtraSpaces().Should().Be("public static void Bob() { }");
    }

    [Fact]
    public void Method_ToString_ShouldBeShortHandMethodNameOnly_GivenAttributeMethodSetupWithNoParameter()
    {
        _method.CreateAttributeMethod("Bob", null);

        var results = _method.Method.ToString();
        results.Should().Be("Bob");
    }

    [Fact]
    public void Method_ToString_ShouldBeShortHandMethodNameOnly_GivenAttributeMethodSetupWithParameter()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1),
            _methodParameter.Create("Bob", "Was Here", 2)
        };

        _method.CreateAttributeMethod("BobMethod", list);

        var results = _method.Method.ToString();
        results.Should().Be("BobMethod(Bob Is Here, Bob Was Here)");
    }

    [Fact]
    public void Method_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _method.Reset();

        var results = _method.Method.ToString();
        results.TrimInternalExtraSpaces().Should().Be("public void Bob() { }");
    }

    [Fact]
    public void MethodExtensions_CreateOutput_AccessorOrderThenName_ShouldProduceStringOfMethods_GivenListOfMethodObjects()
    {
        var methods = new List<Method>
        {
            new Method("Bobby").SetAccessorType(AccessorType.Private),
            new ("Bob"),
            new Method("Abe").SetAccessorType(AccessorType.Private)
        };

        var results = methods.CreateOutput();
        results.TrimInternalExtraSpaces().Should().Be("public void Bob() { } private void Abe() { } private void Bobby() { }");
    }

    [Fact]
    public void MethodExtensions_CreateOutput_NameOrder_ShouldProduceStringOfMethods_GivenListOfMethodObjects()
    {
        var methods = new List<Method>
        {
            new ("Bob"),
            new ("Abe")
        };

        var results = methods.CreateOutput();
        results.TrimInternalExtraSpaces().Should().Be("public void Abe() { } public void Bob() { }");
    }

    [Fact]
    public void MethodExtensions_CreateOutput_NameThenParameter_ShouldProduceStringOfMethods_GivenListOfMethodObjects()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1)
        };
        var methods = new List<Method>
        {
            new ("Bob"),
            new ("Bob", list),
        };

        var results = methods.CreateOutput();
        results.TrimInternalExtraSpaces().Should().Be("public void Bob() { } public void Bob(Bob Is Here) { }");
    }

    [Fact]
    public void MethodExtensions_ToAttribute_ShouldProduceAttributes_GivenListOfAttributes()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1),
            _methodParameter.Create("Bob", "Was Here", 2)
        };

        var attributes = new List<Method>
        {
            _method.CreateAttributeMethod("BobMethod", list),
            _method.CreateAttributeMethod("AbeMethod", null)
        };

        var results = attributes.ToAttribute();
        results.TrimInternalExtraSpaces().Should().Be("[AbeMethod] [BobMethod(Bob Is Here, Bob Was Here)]");
    }

    [Fact]
    public void MethodExtensions_ToMethod_ShouldProduceAttributes_GivenListOfAttributes()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1),
            _methodParameter.Create("Bob", "Was Here", 2)
        };

        var attributes = new List<Method>
        {
            _method.CreateMethod("BobMethod", list),
            _method.CreateMethod("AbeMethod", null),
            _method.CreateMethod("AllenMethod", null).SetAccessorType(AccessorType.Private),
            _method.CreateMethod("BobMethod", null),
        };

        var results = attributes.ToMethod();
        results.TrimInternalExtraSpaces().Should().Be("public void AbeMethod() { } public void BobMethod() { } public void BobMethod(Bob Is Here, Bob Was Here) { } private void AllenMethod() { }");
    }
}