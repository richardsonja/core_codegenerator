﻿using Core.Common.Generics;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Models;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Models;

public class BulkUpdateRequestResponseServiceTests :
    IClassFixture<RequestResponseServicesFixture>,
    IClassFixture<DatabaseTableColumnFixture>,
    IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly RequestResponseServicesFixture _service;

    public BulkUpdateRequestResponseServiceTests(RequestResponseServicesFixture service, DatabaseTableColumnFixture databaseTableColumn, ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Request_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service
            .BulkUpdateRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Request_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.AddActive(1);
        var results = await _service.BulkUpdateRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                true);
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using System; using System.Collections.Generic; namespace Bob.Is.Here.Testing.Models.bob { public class BulkUpdateRequest { public IEnumerable<BobTable> BobTables { get; set; } } }"));
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Request_CreateOutput_ShouldNone_GivenOptionFalse()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkUpdate = false;

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.BulkUpdateRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Request_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.BulkUpdateRequestResponseService.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")), true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Request_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.BulkUpdateRequestResponseService.CreateOutputAsync(new CodeGeneratorRequest(null), true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Request_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.BulkUpdateRequestResponseService.CreateOutputAsync(null, true);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Response_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service
            .BulkUpdateRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Response_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.AddActive(1);
        var results = await _service.BulkUpdateRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                false);
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using System; using System.Collections.Generic; namespace Bob.Is.Here.Testing.Models.bob { public class BulkUpdateResponse { public IEnumerable<string> Errors { get; set; } public bool IsSuccessful { get; set; } } }"));
    }

    public async void BulkUpdateRequestResponseService_Response_CreateOutput_ShouldNone_GivenOptionFalse()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();
        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.BulkUpdate = false;

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.BulkUpdateRequestResponseService
            .CreateOutputAsync(
                new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()),
                false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Response_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.BulkUpdateRequestResponseService.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")), false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Response_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.BulkUpdateRequestResponseService.CreateOutputAsync(new CodeGeneratorRequest(null), false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void BulkUpdateRequestResponseService_Response_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.BulkUpdateRequestResponseService.CreateOutputAsync(null, false);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }
}