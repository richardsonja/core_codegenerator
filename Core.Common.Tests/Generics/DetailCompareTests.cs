﻿using Core.Common.Generics;

namespace Core.Common.Tests.Generics;

public class DetailCompareTests : IClassFixture<TestClassFixture>
{
    private readonly TestClassFixture _testClass;

    public DetailCompareTests(TestClassFixture testClass) => _testClass = testClass;

    [Fact]
    public void DetailCompare_DetailedCompare_ShouldHaveNoMatch_GivenEmptyModels()
    {
        var testClass1 = new TestClass();
        var testClass2 = new TestClass();

        var results = testClass1.DetailedCompare(testClass2);
        results.Should().BeEmpty();
    }

    [Fact]
    public void DetailCompare_DetailedCompare_ShouldHaveOneDifference_GivenDifferentModels()
    {
        var testClass1 = _testClass.TestClasses.First();
        var testClass2 = _testClass.TestClasses.Last();

        var results = testClass1.DetailedCompare(testClass2);
        results.Should().NotBeEmpty();
        results.Should().HaveCount(4);
    }

    [Fact]
    public void DetailCompare_DetailedCompare_ShouldThrowException_GivenFirstModelBeingNull()
    {
        TestClass testClass1 = null;
        var testClass2 = new TestClass();

        Action results = () => testClass1.DetailedCompare(testClass2);
        results.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void DetailCompare_DetailedCompare_ShouldThrowException_GivenSecondModelBeingNull()
    {
        var testClass1 = new TestClass();
        TestClass testClass2 = null;

        Action results = () => testClass1.DetailedCompare(testClass2);
        results.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void DetailCompare_DetailedCompareWith_ShouldHaveNoMatch_GivenEmptyModels()
    {
        var testClass1 = new TestClass();
        var testClass2 = new TestClass();
        var propertiesToCompare = new List<string> { "Id" };

        var results = testClass1.DetailedCompareWith(testClass2, propertiesToCompare);
        results.Should().BeEmpty();
    }

    [Fact]
    public void DetailCompare_DetailedCompareWith_ShouldHaveOneDifference_GivenDifferentModels()
    {
        var testClass1 = _testClass.TestClasses.First();
        var testClass2 = _testClass.TestClasses.Last();
        var propertiesToCompare = new List<string> { "Id" };

        var results = testClass1.DetailedCompareWith(testClass2, propertiesToCompare);
        results.Should().NotBeEmpty();
        results.Should().HaveCount(1);
    }

    [Fact]
    public void DetailCompare_DetailedCompareWith_ShouldThrowException_GivenEmptyListOfPropertiesToExamine()
    {
        var testClass1 = _testClass.TestClasses.First();
        var testClass2 = _testClass.TestClasses.Last();
        var propertiesToCompare = new List<string>();

        Action results = () => testClass1.DetailedCompareWith(testClass2, propertiesToCompare);
        results.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void DetailCompare_DetailedCompareWith_ShouldThrowException_GivenFirstModelBeingNull()
    {
        TestClass testClass1 = null;
        var testClass2 = new TestClass();
        var propertiesToCompare = new List<string> { "Id" };

        Action results = () => testClass1.DetailedCompareWith(testClass2, propertiesToCompare);
        results.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void DetailCompare_DetailedCompareWith_ShouldThrowException_GivenSecondModelBeingNull()
    {
        var testClass1 = new TestClass();
        TestClass testClass2 = null;
        var propertiesToCompare = new List<string> { "Id" };

        Action results = () => testClass1.DetailedCompareWith(testClass2, propertiesToCompare);
        results.Should().Throw<ArgumentNullException>();
    }
}