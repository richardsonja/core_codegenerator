﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Services.CodeGenerators.Types.Models;
using Core.Generator.Services.Services.Configurations;
using Moq;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Models;

public class ModelCodeGeneratorServiceFixture : IDisposable, IReset
{
    public Mock<IBulkDeleteRequestResponseService> BulkDeleteRequestResponseService = new();
    public Mock<IBulkDestroyRequestResponseService> BulkDestroyRequestResponseService = new();
    public Mock<IBulkInsertRequestResponseService> BulkInsertRequestResponseService = new();
    public Mock<IBulkUpdateRequestResponseService> BulkUpdateRequestResponseService = new();
    public Mock<IDeleteRequestResponseService> DeleteRequestResponseService = new();
    public Mock<IDestroyRequestResponseService> DestroyRequestResponseService = new();
    public Mock<IInsertRequestResponseService> InsertRequestResponseService = new();
    public Mock<IUpdateRequestResponseService> UpdateRequestResponseService = new();
    public Mock<IGetRequestResponseService> GetRequestResponseService = new();
    public Mock<IConfigurationService> ConfigurationService = new();
    public Mock<IModelExtensionsService> ModelExtensionsService = new();
    public Mock<IModelQueryService> ModelQueryService = new();
    public Mock<IModelService> ModelService = new();
    public ModelCodeGeneratorService Service;

    public ModelCodeGeneratorServiceFixture()
    {
        Reset();
        Service = new ModelCodeGeneratorService(
            ConfigurationService.Object,
            ModelService.Object,
            ModelExtensionsService.Object,
            ModelQueryService.Object,
            BulkDeleteRequestResponseService.Object,
            BulkDestroyRequestResponseService.Object,
            BulkInsertRequestResponseService.Object,
            BulkUpdateRequestResponseService.Object,
            DeleteRequestResponseService.Object,
            DestroyRequestResponseService.Object,
            InsertRequestResponseService.Object,
            UpdateRequestResponseService.Object,
            GetRequestResponseService.Object);
    }

    public void Dispose()
    {
        Service = null;
        ConfigurationService = null;
        ModelService = null;
        ModelQueryService = null;
        ModelExtensionsService = null;
        BulkDeleteRequestResponseService = null;
        BulkDestroyRequestResponseService = null;
        BulkInsertRequestResponseService = null;
        BulkUpdateRequestResponseService = null;
        DeleteRequestResponseService = null;
        DestroyRequestResponseService = null;
        InsertRequestResponseService = null;
        UpdateRequestResponseService = null;
        GetRequestResponseService = null;
    }

    public void HardReset()
    {
        ConfigurationService.Reset();
        ModelService.Reset();
        ModelExtensionsService.Reset();
        ModelQueryService.Reset();
        BulkDeleteRequestResponseService.Reset();
        BulkDestroyRequestResponseService.Reset();
        BulkInsertRequestResponseService.Reset();
        BulkUpdateRequestResponseService.Reset();
        DeleteRequestResponseService.Reset();
        DestroyRequestResponseService.Reset();
        InsertRequestResponseService.Reset();
        UpdateRequestResponseService.Reset();
        GetRequestResponseService.Reset();
    }

    public void Reset()
    {
        HardReset();

        ModelService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("ModelService's BOB WAS HERE");

        ModelExtensionsService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("ModelExtensionsService's BOB WAS HERE");

        ModelQueryService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("ModelQueryService's BOB WAS HERE");

        BulkDeleteRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), false))
            .ReturnsAsync("BulkDeleteRequestResponseService's Response BOB WAS HERE");

        BulkDeleteRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), true))
            .ReturnsAsync("BulkDeleteRequestResponseService's Request BOB WAS HERE");

        BulkDestroyRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), false))
            .ReturnsAsync("BulkDestroyRequestResponseService's Response BOB WAS HERE");

        BulkDestroyRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), true))
            .ReturnsAsync("BulkDestroyRequestResponseService's Request BOB WAS HERE");

        BulkInsertRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), false))
            .ReturnsAsync("BulkInsertRequestResponseService's Response BOB WAS HERE");

        BulkInsertRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), true))
            .ReturnsAsync("BulkInsertRequestResponseService's Request BOB WAS HERE");

        BulkUpdateRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), false))
            .ReturnsAsync("BulkUpdateRequestResponseService's Response BOB WAS HERE");

        BulkUpdateRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), true))
            .ReturnsAsync("BulkUpdateRequestResponseService's Request BOB WAS HERE");
            

        DeleteRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), false))
            .ReturnsAsync("DeleteRequestResponseService's Response BOB WAS HERE");

        DeleteRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), true))
            .ReturnsAsync("DeleteRequestResponseService's Request BOB WAS HERE");

        DestroyRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), false))
            .ReturnsAsync("DestroyRequestResponseService's Response BOB WAS HERE");

        DestroyRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), true))
            .ReturnsAsync("DestroyRequestResponseService's Request BOB WAS HERE");

        InsertRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), false))
            .ReturnsAsync("InsertRequestResponseService's Response BOB WAS HERE");

        InsertRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), true))
            .ReturnsAsync("InsertRequestResponseService's Request BOB WAS HERE");

        UpdateRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), false))
            .ReturnsAsync("UpdateRequestResponseService's Response BOB WAS HERE");

        UpdateRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), true))
            .ReturnsAsync("UpdateRequestResponseService's Request BOB WAS HERE");

        GetRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), false))
            .ReturnsAsync("GetRequestResponseService's Response BOB WAS HERE");

        GetRequestResponseService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>(), true))
            .ReturnsAsync("GetRequestResponseService's Request BOB WAS HERE");
    }
}