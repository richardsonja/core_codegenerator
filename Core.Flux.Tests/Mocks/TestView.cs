﻿using Core.Flux.Extensions;

namespace Core.Flux.Tests.Mocks;

public class TestView : IFluxViewFor<TestStore>
{
    public TestView() => this.OnChange(UpdateTestResult);

    public string TestResult { get; private set; }

    private void UpdateTestResult(TestStore store) => TestResult = store.TestResult;
}