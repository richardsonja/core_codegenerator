﻿namespace Core.Generator.Services.Models.Databases;

public static class TypeConvertorExtensions
{
    private static readonly List<DbTypeMapEntry> DbTypeMapEntries = new();

    static TypeConvertorExtensions()
    {
        // <author>https://gist.github.com/abrahamjp/858392</author>
        var combinedList = new List<DbTypeMapEntry>
        {
            new ("Array","Array", typeof(Array), typeof(Array), DbType.Binary, SqlDbType.Binary),
            new ("bool", "bool?", typeof(bool),typeof(bool?), DbType.Boolean, SqlDbType.Bit),
            new ("byte", "byte?",typeof(byte), typeof(byte?), DbType.Double, SqlDbType.TinyInt),
            new ("byte[]","byte[]", typeof(byte[]),  typeof(byte[]), DbType.Binary, SqlDbType.Image),
            new ("byte[]","byte[]", typeof(byte[]), typeof(byte[]), DbType.Binary, SqlDbType.Timestamp),
            new ("byte[]","byte[]", typeof(byte[]), typeof(byte[]), DbType.Binary, SqlDbType.VarBinary, true),
            new ("char", "char?",typeof(char),typeof(char?),  DbType.String, SqlDbType.NChar),
            new ("char","char?",typeof(char), typeof(char?), DbType.String, SqlDbType.Char),
            new ("DateTime", "DateTime?", typeof(DateTime), typeof(DateTime?), DbType.DateTime, SqlDbType.SmallDateTime),
            new ("DateTime", "DateTime?", typeof(DateTime),typeof(DateTime?),  DbType.DateTime2, SqlDbType.DateTime2),
            new ("DateTime","DateTime?",  typeof(DateTime), typeof(DateTime?), DbType.DateTime, SqlDbType.DateTime, true),
            new ("DateTime","DateTime?", typeof(DateTime), typeof(DateTime?),  DbType.Date, SqlDbType.Date),
            new ("DateTimeOffset","DateTimeOffset?", typeof(DateTimeOffset),typeof(DateTimeOffset?),  DbType.DateTimeOffset, SqlDbType.DateTimeOffset),
            new ("decimal", "decimal?", typeof(decimal),typeof(decimal?), DbType.Decimal, SqlDbType.Money),
            new ("decimal","decimal?",  typeof(decimal), typeof(decimal?), DbType.Decimal, SqlDbType.SmallMoney),
            new ("decimal","decimal?", typeof(decimal),typeof(decimal?),  DbType.Decimal, SqlDbType.Decimal, true),
            new ("double","double?",  typeof(double),typeof(double?),  DbType.Double, SqlDbType.Float, true),
            new ("Guid","Guid?",  typeof(Guid),typeof(Guid?),  DbType.Guid, SqlDbType.UniqueIdentifier),
            new ("int","int?",  typeof(int),typeof(int?),  DbType.Int32, SqlDbType.Int),
            new ("Int16","Int16?",  typeof(short),typeof(short?), DbType.Int16, SqlDbType.SmallInt),
            new ("Int64","Int64?", typeof(long),typeof(long?) , DbType.Int64, SqlDbType.BigInt),
            new ("object", "object", typeof(object),typeof(object), DbType.Object, SqlDbType.Structured),
            new ("object", "object", typeof(object),typeof(object), DbType.Object, SqlDbType.Variant, true),
            new ("object","object", typeof(object), typeof(object), DbType.Object, SqlDbType.Udt),
            new ("Single","Single?",  typeof(float),typeof(float?), DbType.Single, SqlDbType.Real),
            new ("string", "string", typeof(string), typeof(string), DbType.String, SqlDbType.NVarChar, true),
            new ("string", "string", typeof(string), typeof(string), DbType.String, SqlDbType.Xml),
            new ("string", "string", typeof(string),typeof(string), DbType.String, SqlDbType.NText),
            new ("string", "string", typeof(string),typeof(string), DbType.String, SqlDbType.VarChar),
            new ("string","string", typeof(string), typeof(string), DbType.String, SqlDbType.Text),
            new ("TimeSpan","TimeSpan?", typeof(TimeSpan),typeof(TimeSpan?),  DbType.Time, SqlDbType.Time),
        };

        DbTypeMapEntries.AddRange(combinedList);
    }

    public static DbType ToDbType(this Type type, bool isNullable = false) => Find(type, isNullable).DbType;

    public static DbType ToDbType(this SqlDbType sqlDbType, bool isNullable = false) => Find(sqlDbType, isNullable).DbType;

    public static Type ToNetType(this DbType dbType, bool isNullable = false) => Find(dbType, isNullable).ToType(isNullable);

    public static Type ToNetType(this SqlDbType sqlDbType, bool isNullable = false) => Find(sqlDbType, isNullable).ToType(isNullable);

    public static SqlDbType ToSqlDbType(this Type type, bool isNullable = false) => Find(type, isNullable).SqlDbType;

    public static SqlDbType ToSqlDbType(this DbType dbType, bool isNullable = false) => Find(dbType, isNullable).SqlDbType;

    public static string ToStringFormat(this Type type, bool isNullable = false) => Find(type, isNullable).ToTypeString(isNullable);

    public static string ToStringFormat(this DbType dbType, bool isNullable = false) => Find(dbType, isNullable).ToTypeString(isNullable);

    public static string ToStringFormat(this SqlDbType sqlDbType, bool isNullable = false) => Find(sqlDbType, isNullable).ToTypeString(isNullable);

    private static DbTypeMapEntry Find(Type type, bool isNullable)
    {
        object retObj = null;
        var entriesToSearch = DbTypeMapEntries
            .GroupBy(x => x.Type)
            .Select(x => x.OrderByDescending(y => y.IsPrimary).ThenBy(x => x.TypeString).FirstOrDefault())
            .ToList();
        for (var i = 0; i < entriesToSearch.Count; i++)
        {
            var entry = (DbTypeMapEntry)entriesToSearch[i];
            if (entry.IsType(type))
            {
                retObj = entry;
                break;
            }
        }

        return ValidateAndFormat(retObj, nameof(Type));
    }

    private static DbTypeMapEntry Find(DbType dbType, bool isNullable)
    {
        object retObj = null;
        var entriesToSearch = DbTypeMapEntries
            .GroupBy(x => x.DbType)
            .Select(x => x.OrderByDescending(y => y.IsPrimary).ThenBy(x => x.TypeString).FirstOrDefault())
            .ToList();
        for (var i = 0; i < entriesToSearch.Count; i++)
        {
            var entry = (DbTypeMapEntry)entriesToSearch[i];
            if (entry.IsDbType(dbType))
            {
                retObj = entry;
                break;
            }
        }

        return ValidateAndFormat(retObj, nameof(DbType));
    }

    private static DbTypeMapEntry Find(SqlDbType sqlDbType, bool isNullable)
    {
        object retObj = null;
        var entriesToSearch = DbTypeMapEntries
            .GroupBy(x => x.SqlDbType)
            .Select(x => x.OrderByDescending(y => y.IsPrimary).ThenBy(x => x.TypeString).FirstOrDefault())
            .ToList();
        for (var i = 0; i < entriesToSearch.Count; i++)
        {
            var entry = (DbTypeMapEntry)entriesToSearch[i];
            if (entry.IsSqlDbType(sqlDbType))
            {
                retObj = entry;
                break;
            }
        }

        return ValidateAndFormat(retObj, nameof(SqlDbType));
    }

    private static DbTypeMapEntry ValidateAndFormat(object retObj, string type)
    {
        if (retObj == null)
        {
            throw
                new ApplicationException($"Referenced an unsupported {type}");
        }

        return (DbTypeMapEntry)retObj;
    }

    internal readonly struct DbTypeMapEntry
    {
        public readonly DbType DbType;
        public readonly bool IsPrimary;
        public readonly Type NullableType;
        public readonly string NullableTypeString;
        public readonly SqlDbType SqlDbType;
        public readonly Type Type;
        public readonly string TypeString;

        public DbTypeMapEntry(
            string typeString,
            string nullableTypeString,
            Type type,
            Type nullableType,
            DbType dbType,
            SqlDbType sqlDbType,
            bool isPrimary = false)
        {
            Type = type;
            NullableType = nullableType;
            DbType = dbType;
            SqlDbType = sqlDbType;
            TypeString = typeString;
            NullableTypeString = nullableTypeString;
            IsPrimary = isPrimary;
        }

        public bool IsDbType(DbType type) => type == DbType;

        public bool IsSqlDbType(SqlDbType type) => type == SqlDbType;

        public bool IsType(Type type) => type == Type || type == NullableType;

        public bool IsTypeString(string type) =>
            type.Equals(TypeString, StringComparison.InvariantCultureIgnoreCase)
            || type.Equals(NullableTypeString, StringComparison.InvariantCultureIgnoreCase);

        public Type ToType(bool isNullable) => isNullable ? NullableType : Type;

        public string ToTypeString(bool isNullable) => isNullable ? NullableTypeString : TypeString;
    }
}