﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Tests.Fixtures.Models.Objects;
using FluentAssertions;
using System;
using System.Collections.Generic;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Objects;

public class ClassObjectExtensionsTests :
    IClassFixture<ClassObjectFixture>,
    IClassFixture<MethodFixture>,
    IClassFixture<MethodParameterFixture>,
    IClassFixture<PropertyObjectFixture>
{
    private readonly ClassObjectFixture _classObject;
    private readonly MethodFixture _method;
    private readonly MethodParameterFixture _methodParameter;
    private readonly PropertyObjectFixture _propertyObject;

    public ClassObjectExtensionsTests(
        ClassObjectFixture classObject,
        MethodFixture method,
        MethodParameterFixture methodParameter,
        PropertyObjectFixture propertyObject)
    {
        _classObject = classObject;
        _method = method;
        _methodParameter = methodParameter;
        _propertyObject = propertyObject;
    }

    [Fact]
    public void ClassObject_AddAttributes_ToMethod_ShouldNotThrow_GivenEmptyList()
    {
        List<Method> body = new List<Method>();
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddAttributes(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddAttributes_ToMethod_ShouldNotThrow_GivenNullList()
    {
        List<Method> body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddAttributes(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddAttributes_ToMethod_ShouldNotThrow_GivenNullSingle()
    {
        Method body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddAttributes(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddAttributes_ToString_ShouldHaveAttribute_GivenAddedAttributes()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1),
            _methodParameter.Create("Bob", "Was Here", 2)
        };

        var attributes = new List<Method>
        {
            _method.CreateAttributeMethod("BobMethod", list),
            _method.CreateAttributeMethod("AbeMethod", null)
        };
        _classObject.Reset();

        var results = _classObject.ClassObject.AddAttributes(attributes).ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { [AbeMethod] [BobMethod(Bob Is Here, Bob Was Here)] public class Bob { } }");
    }

    [Fact]
    public void ClassObject_AddAttributes_ToString_ShouldHaveAttribute_GivenSingleAttribute()
    {
        var attribute = _method.CreateAttributeMethod("AbeMethod", null);
        _classObject.Reset();

        var results = _classObject.ClassObject.AddAttributes(attribute).ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { [AbeMethod] public class Bob { } }");
    }

    [Fact]
    public void ClassObject_AddInterfacesAndExtension_ToString_ShouldHaveUsingStatements_GivenAddedUsingStatements()
    {
        var statements = new List<string>
        {
            "Test.Testing",
            "Bob.Abe"
        };
        _classObject.Reset();

        var results = _classObject.ClassObject.AddInterfacesAndExtension(statements).ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { public class Bob : Test.Testing, Bob.Abe { } }");
    }

    [Fact]
    public void ClassObject_AddInterfacesAndExtension_ToString_ShouldHaveUsingStatements_GivenSingleUsingStatements()
    {
        _classObject.Reset();

        var results = _classObject.ClassObject.AddInterfacesAndExtension("Test").ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { public class Bob : Test { } }");
    }

    [Fact]
    public void ClassObject_AddInterfacesAndExtension_ToString_ShouldNotThrow_GivenEmptyList()
    {
        List<string> body = new List<string>();
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddInterfacesAndExtension(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddInterfacesAndExtension_ToString_ShouldNotThrow_GivenNullList()
    {
        List<string> body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddInterfacesAndExtension(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddInterfacesAndExtension_ToString_ShouldNotThrow_GivenNullSingle()
    {
        string body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddInterfacesAndExtension(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddMethods_ToMethod_ShouldNotThrow_GivenEmptyList()
    {
        List<Method> body = new List<Method>();
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddMethods(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddMethods_ToMethod_ShouldNotThrow_GivenNullList()
    {
        List<Method> body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddMethods(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddMethods_ToMethod_ShouldNotThrow_GivenNullSingle()
    {
        Method body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddMethods(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddMethods_ToString_ShouldHaveMethod_GivenAddedMethods()
    {
        var list = new List<MethodParameter>
        {
            _methodParameter.Create("Bob", "Is Here", 1),
            _methodParameter.Create("Bob", "Was Here", 2)
        };

        var attributes = new List<Method>
        {
            _method.CreateMethod("BobMethod", list),
            _method.CreateMethod("AbeMethod", null)
        };
        _classObject.Reset();

        var results = _classObject.ClassObject.AddMethods(attributes).ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { public class Bob { public void AbeMethod() { } public void BobMethod(Bob Is Here, Bob Was Here) { } } }");
    }

    [Fact]
    public void ClassObject_AddMethods_ToString_ShouldHaveMethod_GivenSingleMethod()
    {
        var attribute = _method.CreateMethod("AbeMethod", null);
        _classObject.Reset();

        var results = _classObject.ClassObject.AddMethods(attribute).ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { public class Bob { public void AbeMethod() { } } }");
    }

    [Fact]
    public void ClassObject_AddProperties_ToPropertyObject_ShouldNotThrow_GivenEmptyList()
    {
        List<PropertyObject> body = new List<PropertyObject>();
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddProperties(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddProperties_ToPropertyObject_ShouldNotThrow_GivenNullList()
    {
        List<PropertyObject> body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddProperties(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddProperties_ToPropertyObject_ShouldNotThrow_GivenNullSingle()
    {
        PropertyObject body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddProperties(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddProperties_ToString_ShouldHaveProperty_GivenAddedProperties()
    {
        var properties = new List<PropertyObject>
        {
            new ("BobMethod"),
            new ("AbeMethod")
        };
        _classObject.Reset();

        var results = _classObject.ClassObject.AddProperties(properties).ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { public class Bob { public string AbeMethod ; public string BobMethod ; } }");
    }

    [Fact]
    public void ClassObject_AddProperties_ToString_ShouldHaveProperty_GivenSingleProperty()
    {
        _propertyObject.Reset();
        var property = _propertyObject.PropertyObject;
        _classObject.Reset();

        var results = _classObject.ClassObject.AddProperties(property).ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { public class Bob { public string Bob ; } }");
    }

    [Fact]
    public void ClassObject_AddUsingStatements_ToString_ShouldHaveUsingStatements_GivenAddedUsingStatements()
    {
        var statements = new List<string>
        {
            "Test.Testing",
            "Bob.Abe"
        };
        _classObject.Reset();

        var results = _classObject.ClassObject.AddUsingStatements(statements).ToString();
        results.TrimInternalExtraSpaces().Should().Be("using Bob.Abe; using Test.Testing; namespace Bob.Was.Here.Testing { public class Bob { } }");
    }

    [Fact]
    public void ClassObject_AddUsingStatements_ToString_ShouldHaveUsingStatements_GivenSingleUsingStatements()
    {
        _classObject.Reset();

        var results = _classObject.ClassObject.AddUsingStatements("Test").ToString();
        results.TrimInternalExtraSpaces().Should().Be("using Test; namespace Bob.Was.Here.Testing { public class Bob { } }");
    }

    [Fact]
    public void ClassObject_AddUsingStatements_ToString_ShouldNotThrow_GivenEmptyList()
    {
        List<string> body = new List<string>();
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddUsingStatements(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddUsingStatements_ToString_ShouldNotThrow_GivenNullList()
    {
        List<string> body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddUsingStatements(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_AddUsingStatements_ToString_ShouldNotThrow_GivenNullSingle()
    {
        string body = null;
        _classObject.Reset();

        Action results = () => _classObject.ClassObject.AddUsingStatements(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void ClassObject_Clone_ToString_ShouldMatchExpected_GivenInputs()
    {
        var attribute = _method.CreateAttributeMethod("AbeMethod", null);
        var method = _method.CreateMethod("BobMethod", null);
        _propertyObject.Reset();
        _classObject.Reset();

        var results = _classObject.ClassObject
            .AddAttributes(attribute)
            .AddInterfacesAndExtension("IBob")
            .AddMethods(method)
            .AddProperties(_propertyObject.PropertyObject)
            .AddUsingStatements("Bob.Was.Here")
            .Clone()
            .ToString();
        results.TrimInternalExtraSpaces().Should().Be("using Bob.Was.Here; namespace Bob.Was.Here.Testing { [AbeMethod] public class Bob : IBob { public string Bob ; public void BobMethod() { } } }");
    }

    [Theory]
    [InlineData(AccessorType.Internal, "namespace Bob.Was.Here.Testing { internal class Bob { } }")]
    [InlineData(AccessorType.Public, "namespace Bob.Was.Here.Testing { public class Bob { } }")]
    [InlineData(AccessorType.Private, "namespace Bob.Was.Here.Testing { private class Bob { } }")]
    [InlineData(AccessorType.Protected, "namespace Bob.Was.Here.Testing { protected class Bob { } }")]
    public void ClassObject_SetAccessorType_ToString_ShouldMatchExpected_GivenInput(AccessorType value, string expected)
    {
        _classObject.Reset();

        var results = _classObject.ClassObject.SetAccessorType(value).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(ObjectType.Class, "namespace Bob.Was.Here.Testing { public class Bob { } }")]
    [InlineData(ObjectType.Interface, "namespace Bob.Was.Here.Testing { public interface Bob { } }")]
    [InlineData(ObjectType.Enum, "namespace Bob.Was.Here.Testing { public enum Bob { } }")]
    public void ClassObject_SetClassObject_ToString_ShouldMatchExpected_GivenInput(ObjectType value, string expected)
    {
        _classObject.Reset();

        var results = _classObject.ClassObject.SetClassObject(value).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void ClassObject_SetIsSealed_ToString_ShouldDisplayBasicClassObject_GivenNameOnly()
    {
        _classObject.Reset();

        var results = _classObject.ClassObject.SetIsSealed().ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { public sealed class Bob { } }");
    }

    [Fact]
    public void ClassObject_SetIsStatic_ToString_ShouldDisplayBasicClassObject_GivenNameOnly()
    {
        _classObject.Reset();

        var results = _classObject.ClassObject.SetIsStatic().ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { public static class Bob { } }");
    }

    [Fact]
    public void ClassObject_ToString_ShouldDisplayBasicClassObject_GivenNameOnly()
    {
        _classObject.Reset();

        var results = _classObject.ClassObject.ToString();
        results.TrimInternalExtraSpaces().Should().Be("namespace Bob.Was.Here.Testing { public class Bob { } }");
    }
}