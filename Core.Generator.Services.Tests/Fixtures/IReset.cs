﻿namespace Core.Generator.Services.Tests.Fixtures;

public interface IReset
{
    void Reset();
}