﻿namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public interface IUpdateRequestResponseService : ITypeService<bool>
{
}