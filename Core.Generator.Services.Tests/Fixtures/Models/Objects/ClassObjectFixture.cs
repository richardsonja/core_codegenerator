﻿using Core.Generator.Services.Models.Objects;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Objects;

public class ClassObjectFixture : IDisposable, IReset
{
    public ClassObject ClassObject;

    public ClassObjectFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        ClassObject = null;
    }

    public void Reset()
    {
        ClassObject = new ClassObject("Bob", "Bob.Was.Here.Testing");
    }
}