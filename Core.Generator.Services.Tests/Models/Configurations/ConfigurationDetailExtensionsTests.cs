﻿using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using FluentAssertions;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Configurations;

public class ConfigurationDetailExtensionsTests : IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;

    public ConfigurationDetailExtensionsTests(ConfigurationFixture configuration)
    {
        _configuration = configuration;
    }

    [Fact]
    public void ConfigurationDetailExtensions_DefaultSettings_ShouldMatchExpectedValues_GivenValidSetup()
    {
        _configuration.Reset();
        var results = ConfigurationDetailExtensions.DefaultSettings();
        results.Should().NotBeNull();
        results.ServerConfiguration.Should().NotBeNull();
        results.ServerConfiguration.AuthenticationType.Should().Be(AuthenticationType.MsSqlWindows);
        results.ServerConfiguration.DatabaseServer.Should().Be("localhost");
        results.ServerConfiguration.UserName.Should().BeNullOrEmpty();
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetCodeRelatedConfiguration_ShouldCloneAndSet_GivenNullInitialModel()
    {
        _configuration.Reset();
        CodeRelatedConfiguration initial = null;
        initial.Should().BeNull();

        var results = initial.SetCodeRelatedConfiguration(_configuration.ConfigurationDetail.CodeRelatedConfiguration);
        results.Should().NotBeNull();
        results.Namespace.Should().Be("CG3");
        results.QueryModelName.Should().Be("CG4");
        results.QueryNamespaceSuffix.Should().Be("CG5");
        results.ControllerLayerName.Should().Be("CG6");
        results.ServiceLayerName.Should().Be("CG7");
        results.DataLayerName.Should().Be("CG8");
        results.BulkDeleteMethodName.Should().Be("CG13");
        results.BulkDestroyMethodName.Should().Be("CG14");
        results.BulkUpdateMethodName.Should().Be("CG15");
        results.BulkInsertMethodName.Should().Be("CG16");
        results.DeleteMethodName.Should().Be("CG17");
        results.DestroyMethodName.Should().Be("CG18");
        results.UpdateMethodName.Should().Be("CG19");
        results.InsertMethodName.Should().Be("CG20");
        results.FindMethodName.Should().Be("CG21");
        results.GetMethodName.Should().Be("CG22");
        results.GetActiveListMethodName.Should().Be("CG23");
        results.RequestSuffixModelName.Should().Be("CG24");
        results.ResponseSuffixModelName.Should().Be("CG25");
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetCodeRelatedConfiguration_ShouldCloneAndSetWithDefaultValues_GivenEmptyInitialModel()
    {
        _configuration.Reset();
        var initial = new CodeRelatedConfiguration();
        initial.Should().NotBeNull();
        initial.Namespace.Should().BeNullOrEmpty();
        initial.QueryModelName.Should().BeNullOrEmpty();
        initial.QueryNamespaceSuffix.Should().BeNullOrEmpty();
        initial.ControllerLayerName.Should().BeNullOrEmpty();
        initial.ServiceLayerName.Should().BeNullOrEmpty();
        initial.DataLayerName.Should().BeNullOrEmpty();
        initial.BulkDeleteMethodName.Should().BeNullOrEmpty();
        initial.BulkDestroyMethodName.Should().BeNullOrEmpty();
        initial.BulkUpdateMethodName.Should().BeNullOrEmpty();
        initial.BulkInsertMethodName.Should().BeNullOrEmpty();
        initial.DeleteMethodName.Should().BeNullOrEmpty();
        initial.DestroyMethodName.Should().BeNullOrEmpty();
        initial.UpdateMethodName.Should().BeNullOrEmpty();
        initial.InsertMethodName.Should().BeNullOrEmpty();
        initial.FindMethodName.Should().BeNullOrEmpty();
        initial.GetMethodName.Should().BeNullOrEmpty();
        initial.GetActiveListMethodName.Should().BeNullOrEmpty();
        initial.RequestSuffixModelName.Should().BeNullOrEmpty();
        initial.ResponseSuffixModelName.Should().BeNullOrEmpty();

        var results = initial.SetCodeRelatedConfiguration(_configuration.ConfigurationDetail.CodeRelatedConfiguration);
        results.Should().NotBeNull();
        results.Namespace.Should().Be("CG3");
        results.QueryModelName.Should().Be("CG4");
        results.QueryNamespaceSuffix.Should().Be("CG5");
        results.ControllerLayerName.Should().Be("CG6");
        results.ServiceLayerName.Should().Be("CG7");
        results.DataLayerName.Should().Be("CG8");
        results.BulkDeleteMethodName.Should().Be("CG13");
        results.BulkDestroyMethodName.Should().Be("CG14");
        results.BulkUpdateMethodName.Should().Be("CG15");
        results.BulkInsertMethodName.Should().Be("CG16");
        results.DeleteMethodName.Should().Be("CG17");
        results.DestroyMethodName.Should().Be("CG18");
        results.UpdateMethodName.Should().Be("CG19");
        results.InsertMethodName.Should().Be("CG20");
        results.FindMethodName.Should().Be("CG21");
        results.GetMethodName.Should().Be("CG22");
        results.GetActiveListMethodName.Should().Be("CG23");
        results.RequestSuffixModelName.Should().Be("CG24");
        results.ResponseSuffixModelName.Should().Be("CG25");

        // make sure it is truly cloned
        initial.Namespace.Should().BeNullOrEmpty();
        initial.QueryModelName.Should().BeNullOrEmpty();
        initial.QueryNamespaceSuffix.Should().BeNullOrEmpty();
        initial.ControllerLayerName.Should().BeNullOrEmpty();
        initial.ServiceLayerName.Should().BeNullOrEmpty();
        initial.DataLayerName.Should().BeNullOrEmpty();
        initial.BulkDeleteMethodName.Should().BeNullOrEmpty();
        initial.BulkDestroyMethodName.Should().BeNullOrEmpty();
        initial.BulkUpdateMethodName.Should().BeNullOrEmpty();
        initial.BulkInsertMethodName.Should().BeNullOrEmpty();
        initial.DeleteMethodName.Should().BeNullOrEmpty();
        initial.DestroyMethodName.Should().BeNullOrEmpty();
        initial.UpdateMethodName.Should().BeNullOrEmpty();
        initial.InsertMethodName.Should().BeNullOrEmpty();
        initial.FindMethodName.Should().BeNullOrEmpty();
        initial.GetMethodName.Should().BeNullOrEmpty();
        initial.GetActiveListMethodName.Should().BeNullOrEmpty();
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetCodeRelatedConfiguration_ShouldSet_GivenValidModel()
    {
        _configuration.Reset();
        var initial =
            (_configuration.ConfigurationDetail.Clone() as ConfigurationDetail).CodeRelatedConfiguration.Clone() as CodeRelatedConfiguration;
        initial.ControllerLayerName = "TEST";
        initial.DestroyMethodName = "Made up";

        var results = initial.SetCodeRelatedConfiguration(_configuration.ConfigurationDetail.CodeRelatedConfiguration);
        results.Should().NotBeNull();
        results.Namespace.Should().Be("CG3");
        results.QueryModelName.Should().Be("CG4");
        results.QueryNamespaceSuffix.Should().Be("CG5");
        results.ControllerLayerName.Should().Be("TEST");
        results.ServiceLayerName.Should().Be("CG7");
        results.DataLayerName.Should().Be("CG8");
        results.BulkDeleteMethodName.Should().Be("CG13");
        results.BulkDestroyMethodName.Should().Be("CG14");
        results.BulkUpdateMethodName.Should().Be("CG15");
        results.BulkInsertMethodName.Should().Be("CG16");
        results.DeleteMethodName.Should().Be("CG17");
        results.DestroyMethodName.Should().Be("Made up");
        results.UpdateMethodName.Should().Be("CG19");
        results.InsertMethodName.Should().Be("CG20");
        results.FindMethodName.Should().Be("CG21");
        results.GetMethodName.Should().Be("CG22");
        results.GetActiveListMethodName.Should().Be("CG23");
        results.RequestSuffixModelName.Should().Be("CG24");
        results.ResponseSuffixModelName.Should().Be("CG25");
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetCodeRelatedConfiguration_ShouldSetMissingElementOnly_GivenValidModel()
    {
        _configuration.Reset();
        var initial =
            (_configuration.ConfigurationDetail.Clone() as ConfigurationDetail).CodeRelatedConfiguration.Clone() as CodeRelatedConfiguration;
        initial.ControllerLayerName = null;
        initial.DestroyMethodName = string.Empty;

        var results = initial.SetCodeRelatedConfiguration(_configuration.ConfigurationDetail.CodeRelatedConfiguration);
        results.Should().NotBeNull();
        results.Namespace.Should().Be("CG3");
        results.QueryModelName.Should().Be("CG4");
        results.QueryNamespaceSuffix.Should().Be("CG5");
        results.ControllerLayerName.Should().Be("CG6");
        results.ServiceLayerName.Should().Be("CG7");
        results.DataLayerName.Should().Be("CG8");
        results.BulkDeleteMethodName.Should().Be("CG13");
        results.BulkDestroyMethodName.Should().Be("CG14");
        results.BulkUpdateMethodName.Should().Be("CG15");
        results.BulkInsertMethodName.Should().Be("CG16");
        results.DeleteMethodName.Should().Be("CG17");
        results.DestroyMethodName.Should().Be("CG18");
        results.UpdateMethodName.Should().Be("CG19");
        results.InsertMethodName.Should().Be("CG20");
        results.FindMethodName.Should().Be("CG21");
        results.GetMethodName.Should().Be("CG22");
        results.GetActiveListMethodName.Should().Be("CG23");
        results.RequestSuffixModelName.Should().Be("CG24");
        results.ResponseSuffixModelName.Should().Be("CG25");
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetConfigurationDetail_ShouldCloneAndSet_GivenNullInitialModel()
    {
        _configuration.Reset();
        ConfigurationDetail initial = null;
        initial.Should().BeNull();

        var results = initial.SetConfigurationDetail(_configuration.ConfigurationDetail);
        results.Should().NotBeNull();
        results.ServerConfiguration.Should().NotBeNull();
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetConfigurationDetail_ShouldCloneAndSetWithDefaultValues_GivenEmptyInitialModel()
    {
        _configuration.Reset();
        var initial = new ConfigurationDetail();
        initial.Should().NotBeNull();
        initial.ServerConfiguration.Should().BeNull();

        var results = initial.SetConfigurationDetail(_configuration.ConfigurationDetail);
        results.Should().NotBeNull();
        results.ServerConfiguration.Should().NotBeNull();

        initial.ServerConfiguration.Should().BeNull();
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetConfigurationDetail_ShouldSetMissingElementOnly_GivenValidModel()
    {
        _configuration.Reset();
        var initial = _configuration.ConfigurationDetail.Clone() as ConfigurationDetail;
        initial.ServerConfiguration = new()
        {
            UserName = "CG5"
        };

        var results = initial.SetConfigurationDetail(_configuration.ConfigurationDetail);
        results.Should().NotBeNull();
        results.ServerConfiguration.UserName.Should().Be("CG5");
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetOptionsConfiguration_ShouldCloneAndSet_GivenNullInitialModel()
    {
        _configuration.Reset();
        OptionsConfiguration initial = null;
        initial.Should().BeNull();

        var results = initial.SetOptionsConfiguration(_configuration.ConfigurationDetail.OptionsConfiguration);
        results.Should().NotBeNull();
        results.ActionLog.Should().BeTrue();
        results.ActiveList.Should().BeTrue();
        results.BulkCreate.Should().BeTrue();
        results.BulkDelete.Should().BeTrue();
        results.BulkLogicalDelete.Should().BeTrue();
        results.BulkUpdate.Should().BeTrue();
        results.Delete.Should().BeTrue();
        results.Find.Should().BeTrue();
        results.GetByPrimaryKeys.Should().BeTrue();
        results.Insert.Should().BeTrue();
        results.LogicalDelete.Should().BeTrue();
        results.Update.Should().BeTrue();
        results.GenerateStoredProcedures.Should().BeTrue();
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetOptionsConfiguration_ShouldCloneAndSetWithDefaultValues_GivenEmptyInitialModel()
    {
        _configuration.Reset();
        var initial = new OptionsConfiguration();
        initial.Should().NotBeNull();
        initial.ActionLog.Should().BeNull();
        initial.ActiveList.Should().BeNull();
        initial.BulkCreate.Should().BeNull();
        initial.BulkDelete.Should().BeNull();
        initial.BulkLogicalDelete.Should().BeNull();
        initial.BulkUpdate.Should().BeNull();
        initial.Delete.Should().BeNull();
        initial.Find.Should().BeNull();
        initial.GetByPrimaryKeys.Should().BeNull();
        initial.Insert.Should().BeNull();
        initial.LogicalDelete.Should().BeNull();
        initial.Update.Should().BeNull();
        initial.GenerateStoredProcedures.Should().BeNull();

        var results = initial.SetOptionsConfiguration(_configuration.ConfigurationDetail.OptionsConfiguration);
        results.Should().NotBeNull();
        results.ActionLog.Should().BeTrue();
        results.ActiveList.Should().BeTrue();
        results.BulkCreate.Should().BeTrue();
        results.BulkDelete.Should().BeTrue();
        results.BulkLogicalDelete.Should().BeTrue();
        results.BulkUpdate.Should().BeTrue();
        results.Delete.Should().BeTrue();
        results.Find.Should().BeTrue();
        results.GetByPrimaryKeys.Should().BeTrue();
        results.Insert.Should().BeTrue();
        results.LogicalDelete.Should().BeTrue();
        results.Update.Should().BeTrue();
        results.GenerateStoredProcedures.Should().BeTrue();

        // make sure it is truly cloned
        initial.ActionLog.Should().BeNull();
        initial.ActiveList.Should().BeNull();
        initial.BulkCreate.Should().BeNull();
        initial.BulkDelete.Should().BeNull();
        initial.BulkLogicalDelete.Should().BeNull();
        initial.BulkUpdate.Should().BeNull();
        initial.Delete.Should().BeNull();
        initial.Find.Should().BeNull();
        initial.GetByPrimaryKeys.Should().BeNull();
        initial.Insert.Should().BeNull();
        initial.LogicalDelete.Should().BeNull();
        initial.Update.Should().BeNull();
        initial.GenerateStoredProcedures.Should().BeNull();
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetOptionsConfiguration_ShouldSet_GivenValidModel()
    {
        _configuration.Reset();
        var initial =
            (_configuration.ConfigurationDetail.Clone() as ConfigurationDetail).OptionsConfiguration.Clone() as OptionsConfiguration;
        initial.ActiveList = false;
        initial.BulkCreate = false;

        var results = initial.SetOptionsConfiguration(_configuration.ConfigurationDetail.OptionsConfiguration);
        results.Should().NotBeNull();
        results.ActionLog.Should().BeTrue();
        results.ActiveList.Should().BeFalse();
        results.BulkCreate.Should().BeFalse();
        results.BulkDelete.Should().BeTrue();
        results.BulkLogicalDelete.Should().BeTrue();
        results.BulkUpdate.Should().BeTrue();
        results.Delete.Should().BeTrue();
        results.Find.Should().BeTrue();
        results.GetByPrimaryKeys.Should().BeTrue();
        results.Insert.Should().BeTrue();
        results.LogicalDelete.Should().BeTrue();
        results.Update.Should().BeTrue();
        results.GenerateStoredProcedures.Should().BeTrue();
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetOptionsConfiguration_ShouldSetMissingElementOnly_GivenValidModel()
    {
        _configuration.Reset();
        var initial =
            (_configuration.ConfigurationDetail.Clone() as ConfigurationDetail).OptionsConfiguration.Clone() as OptionsConfiguration;
        initial.ActiveList = null;
        initial.BulkCreate = null;

        var results = initial.SetOptionsConfiguration(_configuration.ConfigurationDetail.OptionsConfiguration);
        results.Should().NotBeNull();
        results.ActionLog.Should().BeTrue();
        results.ActiveList.Should().BeTrue();
        results.BulkCreate.Should().BeTrue();
        results.BulkDelete.Should().BeTrue();
        results.BulkLogicalDelete.Should().BeTrue();
        results.BulkUpdate.Should().BeTrue();
        results.Delete.Should().BeTrue();
        results.Find.Should().BeTrue();
        results.GetByPrimaryKeys.Should().BeTrue();
        results.Insert.Should().BeTrue();
        results.LogicalDelete.Should().BeTrue();
        results.Update.Should().BeTrue();
        results.GenerateStoredProcedures.Should().BeTrue();
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetServerConfiguration_ShouldCloneAndSet_GivenNullInitialModel()
    {
        _configuration.Reset();
        ServerConfiguration initial = null;
        initial.Should().BeNull();

        var results = initial.SetServerConfiguration(_configuration.ConfigurationDetail.ServerConfiguration);
        results.Should().NotBeNull();
        results.DatabaseServer.Should().Be("CG1");
        results.UserName.Should().Be("CG2");
        results.AuthenticationType.Should().Be(AuthenticationType.MsSqlServerAccount);
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetServerConfiguration_ShouldCloneAndSetWithDefaultValues_GivenEmptyInitialModel()
    {
        _configuration.Reset();
        var initial = new ServerConfiguration();
        initial.Should().NotBeNull();
        initial.DatabaseServer.Should().BeNullOrEmpty();
        initial.UserName.Should().BeNullOrEmpty();
        initial.AuthenticationType.Should().Be(AuthenticationType.NotSelectedYet);

        var results = initial.SetServerConfiguration(_configuration.ConfigurationDetail.ServerConfiguration);
        results.Should().NotBeNull();
        results.DatabaseServer.Should().Be("CG1");
        results.UserName.Should().BeNullOrEmpty();
        results.AuthenticationType.Should().Be(AuthenticationType.MsSqlServerAccount);

        initial.DatabaseServer.Should().BeNullOrEmpty();
        initial.UserName.Should().BeNullOrEmpty();
        initial.AuthenticationType.Should().Be(AuthenticationType.NotSelectedYet);
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetServerConfiguration_ShouldSet_GivenValidModel()
    {
        _configuration.Reset();
        var initial =
            (_configuration.ConfigurationDetail.Clone() as ConfigurationDetail).ServerConfiguration.Clone() as ServerConfiguration;
        initial.DatabaseServer = "CG6";
        initial.UserName = "CG5";

        var results = initial.SetServerConfiguration(_configuration.ConfigurationDetail.ServerConfiguration);
        results.Should().NotBeNull();
        results.DatabaseServer.Should().Be("CG6");
        results.UserName.Should().Be("CG5");
        results.AuthenticationType.Should().Be(AuthenticationType.MsSqlServerAccount);
    }

    [Fact]
    public void ConfigurationDetailExtensions_SetServerConfiguration_ShouldSetMissingElementOnly_GivenValidModel()
    {
        _configuration.Reset();
        var initial =
            (_configuration.ConfigurationDetail.Clone() as ConfigurationDetail).ServerConfiguration.Clone() as ServerConfiguration;
        initial.DatabaseServer = string.Empty;
        initial.UserName = "CG5";

        var results = initial.SetServerConfiguration(_configuration.ConfigurationDetail.ServerConfiguration);
        results.Should().NotBeNull();
        results.DatabaseServer.Should().Be("CG1");
        results.UserName.Should().Be("CG5");
        results.AuthenticationType.Should().Be(AuthenticationType.MsSqlServerAccount);
    }
}