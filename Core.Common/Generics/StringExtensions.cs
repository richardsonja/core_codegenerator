﻿using System.Text;
using System.Text.RegularExpressions;

namespace Core.Common.Generics;

public static class StringExtensions
{
    public static string CamelCase(this string text) => FormatCaseString(text, true);

    public static string Combine<T>(this IEnumerable<T> source, string separator = "; ", string leftSide = null, string rightSide = null)
    {
        var renderedValues = source?.Select(x => $"{leftSide}{x.ToString()}{rightSide}").ToList();
        return renderedValues.IsNullOrEmpty()
            ? null
            : string.Join(separator, renderedValues);
    }

    // is it a string or object?
    // is null
    // is empty list
    // format
    // left and right formatting
    // no left but right formating, etc..
    // default
    // null as seperator

    public static string CreateAbbreviation(this string text) =>
        string.IsNullOrWhiteSpace(text)
            ? text
            : text
                .FormatStringWithSpacesAndProperCapitalization()
                .Split(" ", StringSplitOptions.RemoveEmptyEntries)
                .Where(x => !x.IsNullOrEmpty())
                .Select(x => x[0])
                .Combine(string.Empty)
                .ToLower();

    public static string FormatStringWithSpacesAndProperCapitalization(this string text)
    {
        if (string.IsNullOrWhiteSpace(text))
        {
            return text;
        }

        text = text.Replace("_", " ");
        var newText = new StringBuilder(text.Length * 2);

        // use the first letter and make it uppercase
        // loop thru all the rest of the characters.
        // if is uppercase, the previous character was not a space, and the previous character is not capitalized,
        // then put that space in there and make it capitalized
        // else if the previous character was a space, then capitalized the character (first character of new word)
        // else make it lower!
        newText.Append(text[0].ToString().ToUpper());
        for (var i = 1; i < text.Length; i++)
        {
            // if it is upper and the previous character is not a space and is not capitalized
            if (char.IsUpper(text[i])
                && text[i - 1] != ' '
                && !char.IsUpper(text[i - 1]))
            {
                newText.Append(' ');
                newText.Append(text[i].ToString().ToUpper());
            }
            else if (text[i - 1] == ' ')
            {
                newText.Append(text[i].ToString().ToUpper());
            }
            else
            {
                newText.Append(text[i].ToString().ToLower());
            }
        }

        return newText.ToString();
    }

    public static bool IsNullOrEmpty(this string source) => string.IsNullOrEmpty(source);

    public static bool IsNullOrWhiteSpace(this string source) => source.IsNullOrEmpty() || string.IsNullOrWhiteSpace(source);

    public static string PascalCase(this string text) => FormatCaseString(text, false);

    public static string Repeat(this string stringToRepeat, int repeat) =>
        stringToRepeat.IsNullOrEmpty()
            ? stringToRepeat
            : new(Enumerable.Range(0, repeat).SelectMany(_ => stringToRepeat).ToArray());

    public static string StripTag(this string htmlString) => StripTag(htmlString, null);

    public static string StripTag(this string htmlString, string listOfAllowedTags)
    {
        if (string.IsNullOrEmpty(htmlString))
        {
            return htmlString;
        }

        // this is the reg pattern that will retrieve all tags
        var patternThatGetsAllTags = "</?[^><]+>";

        // Create the Regex for all of the Allowed Tags
        //string patternForTagsThatAreAllowed = string.Empty;
        var patternForTagsThatAreAllowed = "<p></p><table></table><tbody></tbody><tr></tr><th></th>";
        if (!string.IsNullOrEmpty(listOfAllowedTags))
        {
            // get the HTML starting tag, such as p,i,b from an example string of <p>,<i>,<b>
            var remove = new Regex("[<>\\/ ]+");

            // now strip out /\<> and spaces
            listOfAllowedTags = remove.Replace(listOfAllowedTags, string.Empty);

            // split at the commas
            var listOfAllowedTagsArray = listOfAllowedTags.Split(',');

            foreach (var allowedTag in listOfAllowedTagsArray)
            {
                if (string.IsNullOrEmpty(allowedTag))
                {
                    // jump to next element of array.
                    continue;
                }

                var patternVersion1 = "<" + allowedTag + ">"; // <p>
                var patternVersion2 = "<" + allowedTag + " [^><]*>$";

                // <img src=stuff  or <hr style="width:50%;" />
                var patternVersion3 = "</" + allowedTag + ">"; // closing tag

                // if it is not the first time, then add the pipe | to the end of the string
                if (!string.IsNullOrEmpty(patternForTagsThatAreAllowed))
                {
                    patternForTagsThatAreAllowed += "|";
                }

                patternForTagsThatAreAllowed += patternVersion1 + "|" + patternVersion2 + "|" + patternVersion3;
            }
        }

        // Get all html tags included in the string
        var regexHtmlTag = new Regex(patternThatGetsAllTags);

        if (!string.IsNullOrEmpty(patternForTagsThatAreAllowed))
        {
            var allTagsThatMatched = regexHtmlTag.Matches(htmlString);

            foreach (Match theTag in allTagsThatMatched)
            {
                var regOfAllowedTag = new Regex(patternForTagsThatAreAllowed);
                var matchOfTag = regOfAllowedTag.Match(theTag.Value);

                if (!matchOfTag.Success)
                {
                    // if not allowed replace it with nothing
                    htmlString = htmlString.Replace(theTag.Value, string.Empty);
                }
            }
        }
        else
        {
            // else strip out all tags
            htmlString = regexHtmlTag.Replace(htmlString, string.Empty);
        }

        return htmlString;
    }

    public static List<string> ToList(this string source) => new() { source };

    public static string TrimInternalExtraSpaces(this string text) => text.IsNullOrEmpty() ? text : Regex.Replace(text.Trim(), @"\s+", " ");

    private static string FormatCaseString(this string text, bool camelCase)
    {
        if (string.IsNullOrEmpty(text))
        {
            return text;
        }

        var firstLetter = text.Substring(0, 1);
        var restOfString = text.Substring(1);
        if (camelCase)
        {
            return firstLetter.ToLowerInvariant() + restOfString;
        }

        return firstLetter.ToUpper() + restOfString;
    }
}