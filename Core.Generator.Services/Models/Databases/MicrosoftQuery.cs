﻿using System.Data.SqlClient;
using System.Globalization;

namespace Core.Generator.Services.Models.Databases;

public class MicrosoftQuery : QueryBase<SqlParameter>
{
    public MicrosoftQuery(string sqlOrStoredProcedureName, bool isStoredProcedure = false) => 
        (Sql, Parameters) = (
            FormatSql(sqlOrStoredProcedureName, isStoredProcedure), 
            new List<SqlParameter>().ToImmutableList());

    public MicrosoftQuery(
        string sqlOrStoredProcedureName,
        IEnumerable<SqlParameter> parameters,
        bool isStoredProcedure = false) : this(
        sqlOrStoredProcedureName,
        isStoredProcedure) =>
        Parameters = parameters.Clone().ToImmutableList();

    private static string FormatSql(string sqlOrStoredProcedureName, bool isStoredProcedure) =>
        !sqlOrStoredProcedureName.IsNullOrEmpty() && isStoredProcedure && !sqlOrStoredProcedureName.Contains(".")
            ? string.Format(CultureInfo.CurrentCulture, "{0}.{1}", Constants.MicrosoftSqlDefaultSchema, sqlOrStoredProcedureName)
            : sqlOrStoredProcedureName;
}