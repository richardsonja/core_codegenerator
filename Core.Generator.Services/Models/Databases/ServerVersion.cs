﻿namespace Core.Generator.Services.Models.Databases;

public class ServerVersion
{
    public string FullVersion { get; set; }
}