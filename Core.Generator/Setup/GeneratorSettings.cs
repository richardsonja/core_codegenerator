﻿using Core.Generator.Services.DependencyInjection;
using Core.Generator.Services.Services.CodeGenerators;
using Core.Generator.Services.Services.Configurations;
using Core.Generator.Services.Services.Databases;
using Core.Generator.Stores.CodeGenerator;
using Core.Generator.Stores.Database;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Generator.Setup;

public static class GeneratorSettings
{
    public static IServiceProvider Initialize()
    {
        var serviceProvider = InitializeServiceProvider();
        InitializeStores(serviceProvider);

        return serviceProvider;
    }

    private static IServiceProvider InitializeServiceProvider() =>
        new ServiceCollection()
            .BuildGeneratorServices()
            .BuildServiceProvider();

    private static void InitializeStores(IServiceProvider serviceProvider)
    {
        var configurationService = serviceProvider.GetService<IConfigurationService>();
        var sessionService = serviceProvider.GetService<ISessionService>();
        var databaseService = serviceProvider.GetService<IDatabaseService>();
        var codeGeneratorService = serviceProvider.GetService<ICodeGeneratorService>();
        Fluxs.Dispatcher = new AsyncDispatcher();
        Fluxs.StoreResolver.Register(new StorageStore(configurationService, sessionService));
        Fluxs.StoreResolver.Register(new DatabaseStore(databaseService));
        Fluxs.StoreResolver.Register(new CodeStore(codeGeneratorService));
    }
}