﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services;
using FluentAssertions;
using LanguageExt;
using Moq;
using System.Collections.Immutable;
using Xunit;

namespace Core.Generator.Services.Tests.Services.Databases;

public class DatabaseServiceTests : IClassFixture<DatabaseServiceFixture>, IClassFixture<DatabaseTableColumnFixture>, IClassFixture<ConfigurationFixture>, IClassFixture<ServerVersionFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly ServerVersionFixture _serverVersion;
    private readonly DatabaseServiceFixture _service;

    public DatabaseServiceTests(DatabaseServiceFixture service, DatabaseTableColumnFixture databaseTableColumn, ConfigurationFixture configuration, ServerVersionFixture serverVersion)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
        _serverVersion = serverVersion;
    }

    [Fact]
    public async void DatabaseService_GetAsync_ReturnNone_GivenServerVersionNOne()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();
        _serverVersion.Reset();

        _configuration.ConfigurationSettings.ConfigurationDetail.ServerConfiguration.AuthenticationType = AuthenticationType.MsSqlServerAccount;

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _service.MicrosoftSqlRepository.Setup(x => x.GetServerVersionAsync()).ReturnsAsync(Option<ServerVersion>.None);

        var result = await _service.Service.GetAsync();
        result.IsNone.Should().BeTrue();
        result.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void DatabaseService_GetAsync_ReturnSomething_GivenGetTableNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();
        _serverVersion.Reset();

        _configuration.ConfigurationSettings.ConfigurationDetail.ServerConfiguration.AuthenticationType = AuthenticationType.MsSqlServerAccount;
        _serverVersion.SetServerVersion("10.0.0.1");

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _service.MicrosoftSqlRepository.Setup(x => x.GetServerVersionAsync()).ReturnsAsync(_serverVersion.ServerVersion);
        _service.MicrosoftSqlRepository.Setup(x => x.GetDatabaseListFromServerVersionGreaterThan2008Async()).ReturnsAsync(_databaseTableColumn.Database.ToFullImmutableList());
        _service.MicrosoftSqlRepository.Setup(x => x.GetDatabaseListFromServerVersionLessThan2008Async()).ReturnsAsync(_databaseTableColumn.Database.ToFullImmutableList());
        _service.MicrosoftSqlRepository.Setup(x => x.HasAccessToDatabaseAsync(It.IsAny<string>())).ReturnsAsync(true);
        _service.MicrosoftSqlRepository.Setup(x => x.GetTableAndColumnsAsync(It.IsAny<IEnumerable<string>>())).ReturnsAsync(Option<ImmutableList<TableColumn>>.None);

        var result = await _service.Service.GetAsync();
        result.IsNone.Should().BeFalse();
        result.IsSome.Should().BeTrue();
    }

    [Theory]
    [InlineData(AuthenticationType.MsSqlServerAccount, "1.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "1.0.0.0")]
    [InlineData(AuthenticationType.MsSqlServerAccount, "2.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "2.0.0.0")]
    [InlineData(AuthenticationType.MsSqlServerAccount, "3.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "3.0.0.0")]
    [InlineData(AuthenticationType.MsSqlServerAccount, "4.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "4.0.0.0")]
    [InlineData(AuthenticationType.MsSqlServerAccount, "5.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "5.0.0.0")]
    [InlineData(AuthenticationType.MsSqlServerAccount, "6.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "6.0.0.0")]
    [InlineData(AuthenticationType.MsSqlServerAccount, "7.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "7.0.0.0")]
    [InlineData(AuthenticationType.MsSqlServerAccount, "8.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "8.0.0.0")]
    [InlineData(AuthenticationType.MsSqlServerAccount, "9.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "9.0.0.0")]
    [InlineData(AuthenticationType.MsSqlServerAccount, "50.0.0.0")]
    [InlineData(AuthenticationType.MsSqlWindows, "50.0.0.0")]
    [InlineData(AuthenticationType.MySqlServerAccount, null)]
    public async void DatabaseService_GetAsync_ShouldMatchExpected_GivenInputs(AuthenticationType authenticationType, string sqlVersion)
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();
        _serverVersion.Reset();

        _configuration.ConfigurationSettings.ConfigurationDetail.ServerConfiguration.AuthenticationType = authenticationType;
        _serverVersion.SetServerVersion(sqlVersion);

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _service.MicrosoftSqlRepository.Setup(x => x.GetServerVersionAsync()).ReturnsAsync(_serverVersion.ServerVersion);
        _service.MicrosoftSqlRepository.Setup(x => x.GetDatabaseListFromServerVersionGreaterThan2008Async()).ReturnsAsync(_databaseTableColumn.Database.ToFullImmutableList());
        _service.MicrosoftSqlRepository.Setup(x => x.GetDatabaseListFromServerVersionLessThan2008Async()).ReturnsAsync(_databaseTableColumn.Database.ToFullImmutableList());
        _service.MicrosoftSqlRepository.Setup(x => x.HasAccessToDatabaseAsync(It.IsAny<string>())).ReturnsAsync(true);
        _service.MicrosoftSqlRepository.Setup(x => x.GetTableAndColumnsAsync(It.IsAny<IEnumerable<string>>())).ReturnsAsync(_databaseTableColumn.TableColumns);

        _service.MySqlRepository.Setup(x => x.GetDatabaseListFromServerAsync()).ReturnsAsync(_databaseTableColumn.Database.ToFullImmutableList());
        _service.MySqlRepository.Setup(x => x.GetTableAndColumnsAsync(It.IsAny<IEnumerable<string>>())).ReturnsAsync(_databaseTableColumn.TableColumns);

        var result = await _service.Service.GetAsync();
        result.IsNone.Should().BeFalse();
        result.IsSome.Should().BeTrue();

        if (authenticationType == AuthenticationType.MySqlServerAccount)
        {
            _service.MySqlRepository.Verify(x => x.GetDatabaseListFromServerAsync(), Times.Once);
            _service.MySqlRepository.Verify(x => x.GetTableAndColumnsAsync(It.IsAny<IEnumerable<string>>()), Times.Once);

            _service.MicrosoftSqlRepository.Verify(x => x.HasAccessToDatabaseAsync(It.IsAny<string>()), Times.Never);
            _service.MicrosoftSqlRepository.Verify(x => x.GetTableAndColumnsAsync(It.IsAny<IEnumerable<string>>()), Times.Never);
            _service.MicrosoftSqlRepository.Verify(x => x.GetDatabaseListFromServerVersionLessThan2008Async(), Times.Never);
            _service.MicrosoftSqlRepository.Verify(x => x.GetDatabaseListFromServerVersionGreaterThan2008Async(), Times.Never);
        }
        else
        {
            _service.MySqlRepository.Verify(x => x.GetDatabaseListFromServerAsync(), Times.Never);
            _service.MySqlRepository.Verify(x => x.GetTableAndColumnsAsync(It.IsAny<IEnumerable<string>>()), Times.Never);

            _service.MicrosoftSqlRepository.Verify(x => x.HasAccessToDatabaseAsync(It.IsAny<string>()), Times.AtLeastOnce());
            _service.MicrosoftSqlRepository.Verify(x => x.GetTableAndColumnsAsync(It.IsAny<IEnumerable<string>>()), Times.Once);
            if (_serverVersion.ServerVersion.GetMajorVersion() <= 8)
            {
                _service.MicrosoftSqlRepository.Verify(x => x.GetDatabaseListFromServerVersionLessThan2008Async(), Times.Once);
                _service.MicrosoftSqlRepository.Verify(x => x.GetDatabaseListFromServerVersionGreaterThan2008Async(), Times.Never);
            }
            else
            {
                _service.MicrosoftSqlRepository.Verify(x => x.GetDatabaseListFromServerVersionLessThan2008Async(), Times.Never);
                _service.MicrosoftSqlRepository.Verify(x => x.GetDatabaseListFromServerVersionGreaterThan2008Async(), Times.Once);
            }
        }
    }

    [Fact]
    public async void DatabaseService_GetAsync_ShouldReturnNone_GivenConfigurationReturningNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        var result = await _service.Service.GetAsync();
        result.IsNone.Should().BeTrue();
        result.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void DatabaseService_GetAsync_ShouldReturnNone_GivenGetDatabaseNOne()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();
        _serverVersion.Reset();

        _configuration.ConfigurationSettings.ConfigurationDetail.ServerConfiguration.AuthenticationType = AuthenticationType.MsSqlServerAccount;
        _serverVersion.SetServerVersion("10.0.0.1");

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _service.MicrosoftSqlRepository.Setup(x => x.GetServerVersionAsync()).ReturnsAsync(_serverVersion.ServerVersion);
        _service.MicrosoftSqlRepository.Setup(x => x.GetDatabaseListFromServerVersionGreaterThan2008Async()).ReturnsAsync(Option<ImmutableList<Database>>.None);
        _service.MicrosoftSqlRepository.Setup(x => x.GetDatabaseListFromServerVersionLessThan2008Async()).ReturnsAsync(_databaseTableColumn.Database.ToFullImmutableList());
        _service.MicrosoftSqlRepository.Setup(x => x.HasAccessToDatabaseAsync(It.IsAny<string>())).ReturnsAsync(true);
        _service.MicrosoftSqlRepository.Setup(x => x.GetTableAndColumnsAsync(It.IsAny<IEnumerable<string>>())).ReturnsAsync(_databaseTableColumn.TableColumns);

        var result = await _service.Service.GetAsync();
        result.IsNone.Should().BeTrue();
        result.IsSome.Should().BeFalse();
    }
}