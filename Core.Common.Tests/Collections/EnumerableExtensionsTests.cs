﻿using Core.Common.Collections;

namespace Core.Common.Tests.Collections;

public class EnumerableExtensionsTests : IClassFixture<TestClassFixture>
{
    private readonly TestClassFixture _testClass;

    public EnumerableExtensionsTests(TestClassFixture testClass) => _testClass = testClass;

    [Fact]
    public void EnumerableExtension_AddTo_Range_ShouldNotBeSameList_GivenList()
    {
        _testClass.Reset(2);
        var results = _testClass.TestClasses.AddTo(_testClass.TestClasses).ToList();
        results.Should().NotBeNullOrEmpty();
        results.Should().HaveCount(4);

        results.First().Should().NotBe(_testClass.TestClasses.First());
    }

    [Fact]
    public void EnumerableExtension_AddTo_ShouldNotBeSameList_GivenList()
    {
        _testClass.Reset(1);
        var newClass = new TestClass();
        var results = _testClass.TestClasses.AddTo(newClass).ToList();
        results.Should().NotBeNullOrEmpty();
        results.Should().HaveCount(2);

        results.First().Should().NotBe(_testClass.TestClasses.First());
    }

    [Fact]
    public void EnumerableExtension_Clone_ShouldNotBeSameList_GivenList()
    {
        _testClass.Reset();

        var results = _testClass.TestClasses.Clone().ToList();
        results.Should().NotBeNullOrEmpty();
        results.SequenceEqual(_testClass.TestClasses).Should().BeFalse();
    }

    [Fact]
    public void EnumerableExtension_IsNullOrEmpty_ShouldFalse_GivenList()
    {
        _testClass.Reset();

        var results = _testClass.TestClasses.IsNullOrEmpty();
        results.Should().BeFalse();
    }

    [Fact]
    public void EnumerableExtension_IsNullOrEmpty_ShouldTrue_GivenEmptyList()
    {
        _testClass.Reset();
        _testClass.SetEmptyValues();

        var results = _testClass.TestClasses.IsNullOrEmpty();
        results.Should().BeTrue();
    }

    [Fact]
    public void EnumerableExtension_IsNullOrEmpty_ShouldTrue_GivenNullList()
    {
        _testClass.Reset();
        _testClass.SetNullValues();

        var results = _testClass.TestClasses.IsNullOrEmpty();
        results.Should().BeTrue();
    }

    [Fact]
    public void EnumerableExtension_MaxObject_ShouldReturnValid_GivenDescendingList()
    {
        _testClass.Reset(1000);
        _testClass.TestClasses = _testClass.TestClasses.OrderByDescending(x => x.DateTime).ToList();

        var results = _testClass.TestClasses.MaxObject(x => x.DateTime);
        results.Should().NotBeNull();
        results.Value.Should().Be(_testClass.TestClasses.Max(x => x.Value));
    }

    [Fact]
    public void EnumerableExtension_MaxObject_ShouldReturnValid_GivenList()
    {
        _testClass.Reset(1000);
        _testClass.TestClasses = _testClass.TestClasses.OrderBy(x => x.DateTime).ToList();

        var results = _testClass.TestClasses.MaxObject(x => x.DateTime);
        results.Should().NotBeNull();
        results.Value.Should().Be(_testClass.TestClasses.Max(x => x.Value));
    }

    [Fact]
    public void EnumerableExtension_MaxObject_ShouldThrowException_GivenEmptyList()
    {
        _testClass.Reset();
        _testClass.SetEmptyValues();

        Action results = () => _testClass.TestClasses.MaxObject(x => x.Id);
        results.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void EnumerableExtension_MaxObject_ShouldThrowException_GivenNullList()
    {
        _testClass.Reset();
        _testClass.SetNullValues();

        Action results = () => _testClass.TestClasses.MaxObject(x => x.Id);
        results.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void EnumerableExtension_MinObject_ShouldReturnValid_GivenDescendingList()
    {
        _testClass.Reset(1000);
        _testClass.TestClasses = _testClass.TestClasses.OrderByDescending(x => x.DateTime).ToList();

        var results = _testClass.TestClasses.MinObject(x => x.DateTime);
        results.Should().NotBeNull();
        results.Value.Should().Be(_testClass.TestClasses.Min(x => x.Value));
    }

    [Fact]
    public void EnumerableExtension_MinObject_ShouldReturnValid_GivenList()
    {
        _testClass.Reset(1000);
        _testClass.TestClasses = _testClass.TestClasses.OrderBy(x => x.DateTime).ToList();

        var results = _testClass.TestClasses.MinObject(x => x.DateTime);
        results.Should().NotBeNull();
        results.Value.Should().Be(_testClass.TestClasses.Min(x => x.Value));
    }

    [Fact]
    public void EnumerableExtension_MinObject_ShouldThrowException_GivenEmptyList()
    {
        _testClass.Reset();
        _testClass.SetEmptyValues();

        Action results = () => _testClass.TestClasses.MinObject(x => x.Id);
        results.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void EnumerableExtension_MinObject_ShouldThrowException_GivenNullList()
    {
        _testClass.Reset();
        _testClass.SetNullValues();

        Action results = () => _testClass.TestClasses.MinObject(x => x.Id);
        results.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void EnumerableExtension_Shuffle_ShouldNotBeSameList_GivenListOfCards()
    {
        _testClass.Reset();

        var results = _testClass.TestClasses.Shuffle().ToList();
        results.Should().NotBeNullOrEmpty();
        results.SequenceEqual(_testClass.TestClasses).Should().BeFalse();
    }

    [Fact]
    public void EnumerableExtension_Shuffle_ShouldReturnEmptyLIst_GivenNullList()
    {
        _testClass.Reset();
        _testClass.SetNullValues();

        var results = _testClass.TestClasses.Shuffle().ToList();
        results.Should().BeEmpty();
    }
}