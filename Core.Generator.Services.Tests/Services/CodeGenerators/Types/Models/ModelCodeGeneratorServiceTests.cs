﻿using Core.Common.Generics;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Models;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Models;

public class ModelCodeGeneratorServiceTests :
    IClassFixture<ModelCodeGeneratorServiceFixture>,
    IClassFixture<DatabaseTableColumnFixture>,
    IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly ModelCodeGeneratorServiceFixture _service;

    public ModelCodeGeneratorServiceTests(
        ModelCodeGeneratorServiceFixture service,
        DatabaseTableColumnFixture databaseTableColumn,
        ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }

    [Fact]
    public async void ModelCodeGeneratorService_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelCodeGeneratorService_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.CreateComplexTableColumns();
        var table = new Table("BobTable", "Bob")
        {
            Columns = _databaseTableColumn.TableColumns.ToImmutableList()
        };
        var resultOption = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(table));
        resultOption.IsNone.Should().BeFalse();
        resultOption.IsSome.Should().BeTrue();

        List<DisplayResult> result = null;
        resultOption.ShouldBeSome(some => result = some.ToList());

        result.Should().NotBeNull();
        result.Should().HaveCount(12);

        // first tab model should several tabs

        AssertDisplayResults(
            result,
            "ModelService's BOB WAS HERE",
            "ModelExtensionsService's BOB WAS HERE",
            "ModelQueryService's BOB WAS HERE");

        var counter = 3;
        AssertDisplayResults(
            result[counter++].DisplayResults.ToList(),
            "BulkDeleteRequestResponseService's Request BOB WAS HERE",
            "BulkDeleteRequestResponseService's Response BOB WAS HERE");

        AssertDisplayResults(
            result[counter++].DisplayResults.ToList(),
            "BulkDestroyRequestResponseService's Request BOB WAS HERE",
            "BulkDestroyRequestResponseService's Response BOB WAS HERE");

        AssertDisplayResults(
            result[counter++].DisplayResults.ToList(),
            "BulkInsertRequestResponseService's Request BOB WAS HERE",
            "BulkInsertRequestResponseService's Response BOB WAS HERE");

        AssertDisplayResults(
            result[counter++].DisplayResults.ToList(),
            "BulkUpdateRequestResponseService's Request BOB WAS HERE",
            "BulkUpdateRequestResponseService's Response BOB WAS HERE");

        AssertDisplayResults(
            result[counter++].DisplayResults.ToList(),
            "DeleteRequestResponseService's Request BOB WAS HERE",
            "DeleteRequestResponseService's Response BOB WAS HERE");

        AssertDisplayResults(
            result[counter++].DisplayResults.ToList(),
            "DestroyRequestResponseService's Request BOB WAS HERE",
            "DestroyRequestResponseService's Response BOB WAS HERE");

        AssertDisplayResults(
            result[counter++].DisplayResults.ToList(),
            "InsertRequestResponseService's Request BOB WAS HERE",
            "InsertRequestResponseService's Response BOB WAS HERE");

        AssertDisplayResults(
            result[counter++].DisplayResults.ToList(),
            "UpdateRequestResponseService's Request BOB WAS HERE",
            "UpdateRequestResponseService's Response BOB WAS HERE");
            
        AssertDisplayResults(
            result[counter++].DisplayResults.ToList(),
            "GetRequestResponseService's Request BOB WAS HERE",
            "GetRequestResponseService's Response BOB WAS HERE");
    }

    [Fact]
    public async void ModelCodeGeneratorService_CreateOutput_ShouldCreateOutputWithOnly1Result_GivenValidInput()
    {
        _service.HardReset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _service.ModelService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync("ModelService's BOB WAS HERE");

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.First().DisplayText.Should().Be("ModelService's BOB WAS HERE"));
    }

    [Fact]
    public async void ModelCodeGeneratorService_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelCodeGeneratorService_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(null));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelCodeGeneratorService_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(null);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    private static void AssertDisplayResults(List<DisplayResult> displayResults, params string[] messages)
    {
        displayResults.Should().HaveCountGreaterOrEqualTo(messages.Length);
        for (var i = 0; i < messages.Length; i++)
        {
            var message = messages[i];
            displayResults[i].Should().NotBeNull();
            displayResults[i].DisplayText.Should().Be(message);
        }
    }
}