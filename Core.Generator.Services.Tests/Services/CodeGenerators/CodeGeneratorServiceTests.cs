﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators;

public class CodeGeneratorServiceTests : 
    IClassFixture<CodeGeneratorServiceFixture>, 
    IClassFixture<DatabaseTableColumnFixture>
{
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly CodeGeneratorServiceFixture _service;

    public CodeGeneratorServiceTests(CodeGeneratorServiceFixture service, DatabaseTableColumnFixture databaseTableColumn)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
    }
        
    [Fact]
    public async void CodeGeneratorService_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
            
            
        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        _databaseTableColumn.CreateComplexTableColumns();
        var table = new Table("BobTable", "Bob")
        {
            Columns = _databaseTableColumn.TableColumns.ToImmutableList()
        };
        var resultOption = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(table));
        resultOption.IsNone.Should().BeFalse();
        resultOption.IsSome.Should().BeTrue();

        CodeGeneratorResult result = null;
        resultOption.ShouldBeSome(some => result = some);

        result.Should().NotBeNull();
        result.DisplayResults.Should().HaveCount(3);

        // first tab model should several tabs
        result.DisplayResults[0]
            .DisplayText.Should().Be("ModelCodeGeneratorService's BOB WAS HERE");

        result.DisplayResults[1]
            .DisplayText.Should().Be("ControllerCodeGeneratorService's BOB WAS HERE");

        result.DisplayResults[2]
            .DisplayText.Should().Be("SqlCodeGeneratorService's BOB WAS HERE");
    }

    [Fact]
    public async void CodeGeneratorService_CreateOutput_ShouldCreateOutputWithOnly1Result_GivenValidInput()
    {
        _service.HardReset();
        _databaseTableColumn.Reset();
            
            

        _service.ModelCodeGeneratorService
            .Setup(x => x.CreateOutputAsync(It.IsAny<CodeGeneratorRequest>()))
            .ReturnsAsync(new List<DisplayResult>{new (1, "WAS HERE", "ModelCodeGeneratorService's BOB WAS HERE")}.ToImmutableList());

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.DisplayResults.First().DisplayText.Should().Be("ModelCodeGeneratorService's BOB WAS HERE"));
    }

    [Fact]
    public async void CodeGeneratorService_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void CodeGeneratorService_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(null));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void CodeGeneratorService_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(null);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    private static void AssertDisplayResults(DisplayResult displayResult, params string[] messages)
    {
        if (messages.Length == 1)
        {
            displayResult.DisplayText.Should().Be(messages.First());
            displayResult.DisplayResults.Should().BeEmpty();
            return;
        }

        displayResult.DisplayText.Should().BeNullOrEmpty();
        AssertDisplayResults(displayResult.DisplayResults, messages);
    }

    private static void AssertDisplayResults(ImmutableList<DisplayResult> displayResults, params string[] messages)
    {
        displayResults.Should().HaveCount(messages.Length);
        for (var i = 0; i < messages.Length; i++)
        {
            var message = messages[i];
            displayResults[i].Should().NotBeNull();
            displayResults[i].DisplayText.Should().Be(message);
        }
    }
}