﻿using Core.Generator.Services.Models.Sqls;
using FluentAssertions;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Sqls;

public class StatementExtensionsTests
{
    [Theory]
    [InlineData("", false, "")]
    [InlineData(null, false, null)]
    [InlineData("WasHere", false, "[WasHere]")]
    [InlineData("Was Here", false, "[Was Here]")]
    [InlineData("", true, "")]
    [InlineData(null, true, null)]
    [InlineData("WasHere", true, "WasHere")]
    [InlineData("Was Here", true, "[Was Here]")]
    public void String_FormatBracketedValue_ShouldMatchExpected_GivenInput(
        string value,
        bool removeBrackets,
        string expected) =>
        value.FormatBracketedValue(removeBrackets).Should().Be(expected);
}