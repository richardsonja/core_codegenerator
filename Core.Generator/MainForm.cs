﻿using Core.Generator.Services.Models;
using Core.Generator.Setup;
using Core.Generator.Stores.CodeGenerator;
using Core.Generator.Stores.CodeGenerator.Actions;
using Core.Generator.Stores.Database;
using Core.Generator.Stores.Database.Actions;
using System.Text.RegularExpressions;

namespace Core.Generator;

public partial class MainForm : Form, IFluxViewFor<StorageStore>, IFluxViewFor<DatabaseStore>, IFluxViewFor<CodeStore>
{
    public IServiceProvider ServiceProvider;
    private static readonly Regex AlphaNumericRegex = new(@"^[a-zA-Z0-9\s,]*$");
    private readonly FormConnect _formConnect;
    private readonly FormSettings _formSettings;
    private IImmutableList<Database> _databases;
    private volatile bool _isUpdating;

    public MainForm()
    {
        InitializeComponent();
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();

        ServiceProvider = GeneratorSettings.Initialize();
        _databases = new List<Database>().ToImmutableList();

        this.OnChange<StorageStore>(OnStorageStoreChange);
        this.OnChange<DatabaseStore>(OnDatabaseStoreChange);
        this.OnChange<CodeStore>(OnCodeStoreChange);
        this.Dispatch<StorageStore, LoadConfigurationAction>(new LoadConfigurationAction());

        _formConnect = new FormConnect();
        _formSettings = new FormSettings();

        btnGenerate.Enabled = false;
        btnDBSettings.Click += (_, _) => OpenDbSettingsForm();
        btnSettings.Click += (_, _) => OpenSettingsForm();
        btnConnect.Click += (_, _) => LoadDatabases();
        btnGenerate.Click += (_, _) => GenerateCode();
    }

    private static TabPage CreateCurrentDataGridTabPage(ImmutableList<TableColumn> results)
    {
        var columns = results.OrderByPosition();
        var tableDataGrid = new DataGridView
        {
            Name = Constants.ColumnDataGridName,
            AllowUserToAddRows = false,
            AllowUserToDeleteRows = false,
            ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize,
            Dock = DockStyle.Fill,
            Location = new Point(0, 0),
            ReadOnly = true,
            RowHeadersWidth = 51,
            DataSource = columns
        };

        var tableDisplay = new TabPage(Constants.ColumnData);
        tableDisplay.Controls.Add(tableDataGrid);
        return tableDisplay;
    }

    private void GenerateCode()
    {
        if (_isUpdating)
        {
            return;
        }

        if (_databases.IsNullOrEmpty())
        {
            MessageBox.Show(Constants.NoDatabaseMessage);
            return;
        }

        if (TvEntities.SelectedNode is null || TvEntities.SelectedNode.Level != 1)
        {
            return;
        }

        var databaseName = TvEntities.SelectedNode.Parent.Text;
        var tableAndSchemaName = TvEntities.SelectedNode.Text;
        var schemaName = tableAndSchemaName.Split('.')[0];
        var tableName = tableAndSchemaName.Split('.')[1];

        var table = _databases
            .FirstOrDefault(x => x.Name == databaseName)
            ?.Tables
            .FirstOrDefault(x => x.Schema == schemaName && x.Name == tableName)
            ?.Clone() as Table;

        this.Dispatch<CodeStore, LoadGeneratorCodeAction>(new LoadGeneratorCodeAction(table));
    }

    private void LoadDatabases()
    {
        if (_isUpdating)
        {
            return;
        }

        btnConnect.Enabled = false;
        this.Dispatch<DatabaseStore, LoadDatabasesAction>(new LoadDatabasesAction());
    }

    private void OnCodeStoreChange(CodeStore store)
    {
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();
        if (!store.IsLoaded)
        {
            return;
        }
        _isUpdating = true;
        store.ExceptionMessaging();

        TabControlMain.TabPages.Clear();
        var results = store.CodeGeneratorResult;
        if (results is null || results.Columns.IsNullOrEmpty())
        {
            // MessageBox.Show(Constants.NoColumnsFoundErrorMessage);
            _isUpdating = false;
            return;
        }

        var pages = results.DisplayResults?.Select(x => x.CreateTabPage()).ToList() ?? new List<TabPage>();
        pages.Add(results.Columns.CreateCurrentDataGridTabPage());

        TabControlMain.TabPages.AddRange(pages.ToArray());

        _isUpdating = false;
    }

    private void OnDatabaseStoreChange(DatabaseStore store)
    {
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();
        if (!store.IsLoaded)
        {
            return;
        }

        _isUpdating = true;
        store.ExceptionMessaging();

        btnGenerate.Enabled = !store.Databases.IsNullOrEmpty();
        var treeNodes = store.Databases.ConvertToTreeNodes();
        TvEntities.Nodes.Clear();
        TvEntities.Nodes.AddRange(treeNodes.ToArray());
        TvEntities.ExpandAll();

        _databases = store.Databases ?? new List<Database>().ToImmutableList();
        _isUpdating = false;

        btnConnect.Enabled = true;
    }

    private void OnStorageStoreChange(StorageStore store)
    {
        using var cursorBlock = CursorBlock.BeginWaitCursorBlock();
        if (!store.IsLoaded)
        {
            return;
        }

        _isUpdating = true;
        var isDatabasePasswordSet = store.ConfigurationSettings.IsDatabasePasswordSet();
        var hasAnyExceptions = store.Exceptions.Any();

        store.ExceptionMessaging();

        var databaseName = !isDatabasePasswordSet
            ? Constants.SetupNeeded
            : hasAnyExceptions
                ? store.Exceptions.Select(x => x.ToString()).Combine()
                : store.ConfigurationSettings.ConfigurationDetail.ServerConfiguration.DatabaseServer;
        var connectButtonLabel = !isDatabasePasswordSet || hasAnyExceptions
            ? Constants.SetupButton
            : Constants.SetupCompletedButton;

        var isButtonClickable = isDatabasePasswordSet && !hasAnyExceptions;

        txtDatabaseName.Text = databaseName;
        btnConnect.Text = connectButtonLabel;
        btnConnect.Enabled = isButtonClickable;
        _isUpdating = false;
    }

    private void OpenDbSettingsForm()
    {
        OpenForm(_formConnect);
    }

    private void OpenForm(Form form)
    {
        if (_isUpdating)
        {
            return;
        }

        form.ShowDialog();

        this.Dispatch<StorageStore, LoadConfigurationAction>(new LoadConfigurationAction());
    }

    private void OpenSettingsForm()
    {
        OpenForm(_formSettings);
    }
}