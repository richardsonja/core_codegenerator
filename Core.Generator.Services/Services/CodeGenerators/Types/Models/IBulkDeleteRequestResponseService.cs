﻿namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public interface IBulkDeleteRequestResponseService : ITypeService<bool>
{
}