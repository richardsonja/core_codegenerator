﻿namespace Core.Flux;

public class Dispatcher : IDispatcher
{
    private IDictionary<Type, IList<object>> _callbacks = new Dictionary<Type, IList<object>>();

    public void Clear()
    {
        _callbacks = new Dictionary<Type, IList<object>>();
    }

    public void Dispatch<T>(T payload) =>
            GetCallbacks<T>()
            .ToList()
            .ForEach(callback => callback(payload));

    public void Register<T>(Action<T> callback)
    {
        var key = typeof(T);

        if (!_callbacks.ContainsKey(key))
        {
            _callbacks.Add(key, new List<object>());
        }

        _callbacks[key].Add(callback);
    }

    protected IEnumerable<Action<T>> GetCallbacks<T>()
    {
        var key = typeof(T);

        return !_callbacks.ContainsKey(key)
            ? Enumerable.Empty<Action<T>>()
            : _callbacks[key].OfType<Action<T>>();
    }
}