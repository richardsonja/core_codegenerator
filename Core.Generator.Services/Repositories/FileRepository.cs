﻿using Core.Generator.Services.Models.Files;
using System.Text;

namespace Core.Generator.Services.Repositories;

public class FileRepository : IFileRepository
{
    public async Task<Option<FileDetails>> GetAsync(FileDetails fileDetails)
    {
        if (!File.Exists(fileDetails.FilePathAndName))
        {
            return Option<FileDetails>.None;
        }

        await using var stream = File.Open(fileDetails.FilePathAndName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        using var reader = new StreamReader(stream, Encoding.UTF8);
        var jsonString = await reader.ReadToEndAsync();

        return new FileDetails(jsonString, fileDetails.FileName, fileDetails.DirectoryPath);
    }

    public async Task<Option<FileDetails>> SaveAsync(FileDetails fileDetails)
    {
        if (!Directory.Exists(fileDetails.DirectoryPath))
        {
            Directory.CreateDirectory(fileDetails.DirectoryPath);
        }

        if (File.Exists(fileDetails.FilePathAndName))
        {
            File.Delete(fileDetails.FilePathAndName);
        }

        await using var stream = File.Open(fileDetails.FilePathAndName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
        await using var writer = new StreamWriter(stream);
        await writer.WriteLineAsync(fileDetails.FileString);

        return (FileDetails)fileDetails.Clone();
    }
}