﻿using Core.Generator.Services.Services.CodeGenerators.Types.Models;
using Core.Generator.Services.Services.Configurations;
using Moq;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Models;

public class RequestResponseServicesFixture : IDisposable, IReset
{
    public Mock<IConfigurationService> ConfigurationService = new();
    public BulkDeleteRequestResponseService BulkDeleteRequestResponseService;
    public BulkDestroyRequestResponseService BulkDestroyRequestResponseService;
    public BulkInsertRequestResponseService BulkInsertRequestResponseService;
    public BulkUpdateRequestResponseService BulkUpdateRequestResponseService;
    public DeleteRequestResponseService DeleteRequestResponseService;
    public DestroyRequestResponseService DestroyRequestResponseService;
    public InsertRequestResponseService InsertRequestResponseService;
    public UpdateRequestResponseService UpdateRequestResponseService;
    public GetRequestResponseService GetRequestResponseService;

    public RequestResponseServicesFixture()
    {
        Reset();
        BulkDeleteRequestResponseService = new BulkDeleteRequestResponseService(ConfigurationService.Object);
        BulkDestroyRequestResponseService = new BulkDestroyRequestResponseService(ConfigurationService.Object);
        BulkInsertRequestResponseService = new BulkInsertRequestResponseService(ConfigurationService.Object);
        BulkUpdateRequestResponseService = new BulkUpdateRequestResponseService(ConfigurationService.Object);
        DeleteRequestResponseService = new DeleteRequestResponseService(ConfigurationService.Object);
        DestroyRequestResponseService = new DestroyRequestResponseService(ConfigurationService.Object);
        InsertRequestResponseService = new InsertRequestResponseService(ConfigurationService.Object);
        UpdateRequestResponseService = new UpdateRequestResponseService(ConfigurationService.Object);
        GetRequestResponseService = new GetRequestResponseService(ConfigurationService.Object);
    }

    public void Dispose()
    {
        BulkDeleteRequestResponseService = null;
        BulkDestroyRequestResponseService = null;
        BulkInsertRequestResponseService = null;
        BulkUpdateRequestResponseService = null;
        DeleteRequestResponseService = null;
        DestroyRequestResponseService = null;
        InsertRequestResponseService = null;
        UpdateRequestResponseService = null;
        GetRequestResponseService = null;
        ConfigurationService = null;
    }

    public void Reset()
    {
        ConfigurationService.Reset();
    }
}