﻿using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using System;
using System.Data;

namespace Core.Generator.Services.Tests.Fixtures.Models.Sqls;

public class StoredProcedureParameterFixture : DatabaseTableColumnFixture, IDisposable, IReset
{
    public StoredProcedureParameter StoredProcedureParameter;

    public StoredProcedureParameterFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        StoredProcedureParameter = null;
        base.Dispose();
    }

    public void Reset()
    {
        StoredProcedureParameter = new StoredProcedureParameter(CreateTableColumns("Bob", "Was Here", "bobDB", 1, "bobColumn"));
        base.Reset();
    }
}