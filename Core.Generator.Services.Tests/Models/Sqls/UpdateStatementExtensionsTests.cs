﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Sqls;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Sqls;

public class UpdateStatementExtensionsTests : IClassFixture<UpdateStatementFixture>, IClassFixture<DatabaseTableColumnFixture>
{
    private readonly UpdateStatementFixture _updateStatement;
    private readonly DatabaseTableColumnFixture _database;

    public UpdateStatementExtensionsTests(UpdateStatementFixture updateStatement, DatabaseTableColumnFixture database)
    {
        _updateStatement = updateStatement;
        _database = database;
    }

    [Fact]
    public void UpdateStatement_AddSetValue_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _updateStatement.Reset();
        var item = new StoredProcedureParameter(_database.CreateTableColumns("Bob", "was here", "and here", 1));

        var results = _updateStatement.UpdateStatement.AddSetValue(item).ToString();
        results.TrimInternalExtraSpaces().Should().Be("UPDATE [Bob].[Was Here] SET [Column1] = @Column1");
    }

    [Fact]
    public void UpdateStatement_AddSetValue_ToString_ShouldDisplayBasicMethodWithBody_GivenListInput()
    {
        _updateStatement.Reset();

        var tsil = new List<StoredProcedureParameter>
        {
            new StoredProcedureParameter(_database.CreateTableColumns("Bob", "was here", "and here", 1))
        };

        var results = _updateStatement.UpdateStatement.AddSetValue(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("UPDATE [Bob].[Was Here] SET [Column1] = @Column1");
    }

    [Theory]
    [InlineData("WasHere", "UPDATE WasHere.WasHere SET Column1 = @Column1")]
    [InlineData("Was Here", "UPDATE [Was Here].[Was Here] SET Column1 = @Column1")]
    public void UpdateStatement_AddSetValue_ToString_ShouldMatchExpect_GivenInputAndRemoveBracketsTrue(
        string tableName,
        string expected)
    {
        _updateStatement.Reset();
        var item = new StoredProcedureParameter(_database.CreateTableColumns(tableName, tableName, "and here", 1));
        _updateStatement.UpdateStatement = new UpdateStatement(tableName, tableName);
        var results = _updateStatement.UpdateStatement.AddSetValue(item).SetRemoveBrackets().ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void UpdateStatement_AddSetValue_ToString_ShouldNotThrow_GivenEmptyList()
    {
        var body = new List<StoredProcedureParameter>();
        _updateStatement.Reset();

        Action results = () => _updateStatement.UpdateStatement.AddSetValue(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void UpdateStatement_AddSetValue_ToString_ShouldNotThrow_GivenNullList()
    {
        List<StoredProcedureParameter> body = null;
        _updateStatement.Reset();

        Action results = () => _updateStatement.UpdateStatement.AddSetValue(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void UpdateStatement_AddSetValue_ToString_ShouldNotThrow_GivenNullSingle()
    {
        StoredProcedureParameter body = null;
        _updateStatement.Reset();

        Action results = () => _updateStatement.UpdateStatement.AddSetValue(body);
        results.Should().NotThrow();
    }

       

    [Fact]
    public void UpdateStatement_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _updateStatement.Reset();
        var item = new StoredProcedureParameter(_database.CreateTableColumns("bob", "was here", "and here", 1));
        var results = _updateStatement.UpdateStatement.AddParameter(item).ToString();
        results.TrimInternalExtraSpaces().Should().Be("UPDATE [Bob].[Was Here] WHERE [Column1] = @Column1");
    }

    [Fact]
    public void UpdateStatement_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenListInput()
    {
        _updateStatement.Reset();

        var tsil = new List<StoredProcedureParameter>
        {
            new (_database.CreateTableColumns("bob", "was here", "and here", 2, "Bob1")),
            new (_database.CreateTableColumns("bob", "was here", "and here", 1, "Bob2"))
        };

        var results = _updateStatement.UpdateStatement.AddParameter(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("UPDATE [Bob].[Was Here] WHERE [Bob1] = @Bob1 AND [Bob2] = @Bob2");
    }

    [Fact]
    public void UpdateStatement_AddParameter_ToString_ShouldNotThrow_GivenEmptyList()
    {
        var body = new List<StoredProcedureParameter>();
        _updateStatement.Reset();

        Action results = () => _updateStatement.UpdateStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void UpdateStatement_AddParameter_ToString_ShouldNotThrow_GivenNullList()
    {
        List<StoredProcedureParameter> body = null;
        _updateStatement.Reset();

        Action results = () => _updateStatement.UpdateStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void UpdateStatement_AddParameter_ToString_ShouldNotThrow_GivenNullSingle()
    {
        StoredProcedureParameter body = null;
        _updateStatement.Reset();

        Action results = () => _updateStatement.UpdateStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void UpdateStatement_Clone_ShouldHaveAllAttributes_GivenSetup()
    {
        _updateStatement.Reset();
        var parameter = new StoredProcedureParameter(_database.CreateTableColumns("bob", "was here", "and here", 1));

        var results = (_updateStatement
                .UpdateStatement
                .AddSetValue(parameter)
                .AddParameter(parameter) as UpdateStatement)
            .SetRemoveBrackets(true)
            .Clone() as UpdateStatement;

        results.Should().NotBeNull();
        results.Schema.Should().Be("Bob");
        results.Name.Should().Be("Was Here");
        results.SetValues.Should().HaveCount(1);
        results.SetValues.First().ToString().Should().Contain("Column1 = @Column1");
        results.WhereParameters.Should().HaveCount(1);
        results.WhereParameters.First().ToString().Should().Contain("Column1 = @Column1");
        results.RemoveBrackets.Should().BeTrue();

        results.ToString().TrimInternalExtraSpaces().Should().Be("UPDATE Bob.[Was Here] SET Column1 = @Column1 WHERE Column1 = @Column1");
    }

    [Theory]
    [InlineData("Bob", "WasHere", "UPDATE Bob.WasHere")]
    [InlineData("Bob", "Was Here", "UPDATE Bob.[Was Here]")]
    [InlineData("Bob Was", "WasHere", "UPDATE [Bob Was].WasHere")]
    [InlineData("Bob Was", "Was Here", "UPDATE [Bob Was].[Was Here]")]
    public void UpdateStatement_SetRemoveBrackets_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        string schemaName,
        string tableName,
        string expected)
    {
        _updateStatement.Reset();
        _updateStatement.UpdateStatement = new UpdateStatement(schemaName, tableName);

        var results = _updateStatement.UpdateStatement.SetRemoveBrackets().ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void UpdateStatement_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _updateStatement.Reset();

        var results = _updateStatement.UpdateStatement.ToString();
        results.TrimInternalExtraSpaces().Should().Be("UPDATE [Bob].[Was Here]");
    }
}