﻿using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Services.Configurations;
using Method = Core.Generator.Services.Models.Objects.Method;

namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public class ModelService : IModelService
{
    private readonly IConfigurationService _configurationService;

    public ModelService(IConfigurationService configurationService)
    {
        _configurationService = configurationService;
    }

    public async Task<Option<string>> CreateOutputAsync(CodeGeneratorRequest request)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<string>.None;
        }

        var configOption = await _configurationService.GetAsync();
        return configOption.Match(
            x => CreateClass(x, request.Table),
            () => Option<string>.None);
    }

    private static Option<string> CreateClass(ConfigurationSettings configuration, Table table)
    {
        var classObject = new ClassObject(GetClassName(configuration, table), GetNamespace(configuration, table))
            .AddUsingStatements(GetUsingStatements())
            .AddInterfacesAndExtension(GetInterfacesAndExtension(table))
            .AddMethods(GetIEquatableMethods(table))
            .AddProperties(GetProperties(table));

        return classObject.ToString();
    }

    private static string GetClassName(ConfigurationSettings configuration, Table table) =>
        $"{table.FormattedName}";

    private static ImmutableList<Method> GetIEquatableMethods(Table table)
    {
        var getHashCode = new Method("GetHashCode")
            .SetIsShortHandMethod()
            .AddReturnType(typeof(int))
            .SetIsOverride()
            .AddBody($"HashCode.Combine({table.Columns.Select(x => x.PascalCaseName).Combine(", ")})");

        var equalsSelf = new Method("Equals", new List<MethodParameter> { new(table.FormattedName, "other", 1) })
            .AddReturnType(typeof(bool))
            .AddBody("if (ReferenceEquals(null, other))")
            .AddBody("{")
            .AddBody("return false;")
            .AddBody("}")
            .AddBody(Environment.NewLine)
            .AddBody("if (ReferenceEquals(this, other))")
            .AddBody("{")
            .AddBody("return true;")
            .AddBody("}")
            .AddBody(Environment.NewLine)
            .AddBody($"return {table.Columns.Select(x => $"{x.PascalCaseName} = other.{x.PascalCaseName}").Combine(" && ")};");

        var equalsObject = new Method("Equals", new List<MethodParameter> { new(typeof(object).ToStringFormat(), "obj", 1) })
            .AddReturnType(typeof(bool))
            .SetIsOverride()
            .AddBody("if (ReferenceEquals(null, obj))")
            .AddBody("{")
            .AddBody("return false;")
            .AddBody("}")
            .AddBody(Environment.NewLine)
            .AddBody("if (ReferenceEquals(this, obj))")
            .AddBody("{")
            .AddBody("return true;")
            .AddBody("}")
            .AddBody(Environment.NewLine)
            .AddBody("return obj.GetType() == this.GetType() && Equals(({table.FormattedName})obj);");

        return getHashCode.ToFullList()
            .AddTo(equalsObject)
            .AddTo(equalsSelf)
            .ToImmutableList();
    }

    private static ImmutableList<string> GetInterfacesAndExtension(Table table) =>
        new List<string>
        {
            $"IEquatable<{table.FormattedName}>"
        }.ToImmutableList();

    private static string GetNamespace(ConfigurationSettings configuration, Table table) => 
        $"{configuration.ConfigurationDetail.CodeRelatedConfiguration.Namespace}.Models.{table.Schema}";

    private static ImmutableList<PropertyObject> GetProperties(Table table) =>
        table.Columns
            .Select(x => new PropertyObject(x.PascalCaseName)
                .SetType(x.CSharpType)
                .AddGetterSetter(new GetterSetterObject(GetterSetterType.Get))
                .AddGetterSetter(new GetterSetterObject(GetterSetterType.Set)))
            .ToImmutableList();

    private static ImmutableList<string> GetUsingStatements() =>
        new List<string>
        {
            "System",
            "System.Collections.Generic"
        }.ToImmutableList();
}