﻿namespace Core.Generator.Services.Repositories.Database;

public interface IMicrosoftSqlRepository
{
    Task<Option<ImmutableList<Models.Databases.Database>>> GetDatabaseListFromServerVersionGreaterThan2008Async();
    Task<Option<ImmutableList<Models.Databases.Database>>> GetDatabaseListFromServerVersionLessThan2008Async();
    Task<Option<ServerVersion>> GetServerVersionAsync();
    Task<Option<ImmutableList<TableColumn>>> GetTableAndColumnsAsync(IEnumerable<string> listOfDatabases);
    Task<bool> HasAccessToDatabaseAsync(string databaseName);
}