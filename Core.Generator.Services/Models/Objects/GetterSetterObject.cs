﻿namespace Core.Generator.Services.Models.Objects;

public class GetterSetterObject : ICloneable
{
    public GetterSetterObject(GetterSetterType getterSetterType) => 
        GetterSetterType = getterSetterType;

    private GetterSetterObject(
        GetterSetterType getterSetterType,
        AccessorType accessorType,
        ImmutableList<string> body)
        : this(getterSetterType) =>
        (Body, AccessorType) = (body.Clone().ToImmutableList(), accessorType);

    public AccessorType AccessorType { get; private set; } = AccessorType.Public;

    public ImmutableList<string> Body { get; private set; } = new List<string>().ToImmutableList();

    public GetterSetterType GetterSetterType { get; }

    public GetterSetterObject AddBody(string body)
    {
        Body = Body.AddTo(body?.Trim()).ToImmutableList();

        return this;
    }

    public GetterSetterObject AddBody(IEnumerable<string> body)
    {
        Body = Body.AddTo(body.Clone()?.Select(x => x?.Trim())).ToImmutableList();

        return this;
    }

    public object Clone() => new GetterSetterObject(GetterSetterType, AccessorType, Body);

    public GetterSetterObject SetAccessorType(AccessorType accessorType)
    {
        AccessorType = accessorType;

        return this;
    }

    public override string ToString() => $"{ToAccessorType()} {ToGetterSetter()}{ToBody()}".Trim();

    private string ToAccessorType() =>
        AccessorType == AccessorType.Public
            ? string.Empty
            : AccessorType.CreateOutput();

    private string ToBody() =>
        Body
            .Where(x => !x.IsNullOrEmpty())
            .IsNullOrEmpty()
            ? ";"
            : Body.Count() == 1
                ? $" => {Body.First().Replace(";", string.Empty)};"
                : $@"
{{
    {Body.Combine(Environment.NewLine)}
}}";

    private string ToGetterSetter() => GetterSetterType.CreateOutput();
}