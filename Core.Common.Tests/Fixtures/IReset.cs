﻿namespace Core.Common.Tests.Fixtures;

public interface IReset
{
    void Reset();
}