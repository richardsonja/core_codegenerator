﻿namespace Core.Common.Tests.Fixtures;

public class DataTableFixture : IDisposable, IReset
{
    public DataTable DataTable;

    public DataTableFixture() => Reset();

    public void Dispose()
    {
        DataTable = null;
    }

    public void Reset()
    {
        DataTable = new DataTable();
        DataTable.Clear();
    }

    public void SetValidLoaded()
    {
        DataTable = new DataTable();
        DataTable.Clear();
        DataTable.Columns.Add("bob");
        var dataRow = DataTable.NewRow();
        dataRow["bob"] = "said hello";
        DataTable.Rows.Add(dataRow);
    }

    public void SetValidRowNoContent()
    {
        DataTable = new DataTable();
        DataTable.Clear();
        DataTable.Columns.Add("bob");
    }
}