﻿using Core.Generator.Services.Models.Files;

namespace Core.Generator.Services.Repositories;

public interface IFileRepository
{
    Task<Option<FileDetails>> GetAsync(FileDetails fileDetails);

    Task<Option<FileDetails>> SaveAsync(FileDetails fileDetails);
}