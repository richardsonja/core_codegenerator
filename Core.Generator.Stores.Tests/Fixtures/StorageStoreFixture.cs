﻿using Core.Flux;
using Core.Generator.Services.Services.Configurations;
using Core.Generator.Stores.Storage;
using Moq;
using System;

namespace Core.Generator.Stores.Tests.Fixtures;

public class StorageStoreFixture : IDisposable, IReset
{
    public Mock<IConfigurationService> ConfigurationService;
    public Mock<ISessionService> SessionService;
    public StorageStore Store;

    public StorageStoreFixture()
    {
        ConfigurationService = new Mock<IConfigurationService>();
        SessionService = new Mock<ISessionService>();
            
        Fluxs.Dispatcher = new AsyncDispatcher();
        Reset();
    }

    public void Dispose()
    {
        Store = null;
        ConfigurationService = null;
        SessionService = null;
    }

    public void Reset()
    {
        ConfigurationService.Reset();
        SessionService.Reset();
        Store = new StorageStore(ConfigurationService.Object, SessionService.Object);
        Fluxs.StoreResolver.Register(Store);
    }
}