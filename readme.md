# Introduction
* The Code Generator is a learning project that I am continuingly improving. 
* It allows you to do database first development and generate code and sql to pull the data out.
*  provides T4 platform to do the same thing and is typically the best choice, but this project is about learning and exploring coding styles.
* The first generation was an [open source project](http://spgen.codeplex.com/license). 
* This new version is built to be more loosely coupled and allow rapid develop new code without having to manually change the UI (tabs are created based on the data, not what it was told to do)

## Architecture
* This project is using a NON production ready flux system to learn about uni-directional data flow and isolating mutable data in one place. 
* the UI is seperated from the services by the flux store and is rerenders the data when a change is emitted
* the flux store corridates all communication to the services and retains the resulting data/results
* Services are seperated from the repository (standard pattern)
** using LanguageExt's Option pattern to encourage functional programming mindset.
* Repository is seperated from the database connector code to allow different databases to be connected to