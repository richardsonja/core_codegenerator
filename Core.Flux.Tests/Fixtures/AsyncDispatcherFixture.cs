﻿namespace Core.Flux.Tests.Fixtures;

public class AsyncDispatcherFixture : IDisposable, IReset
{
    public IAsyncDispatcher Dispatcher;

    public AsyncDispatcherFixture() => Reset();

    public void Dispose() => Dispatcher = null;

    public void Reset()
    {
        Dispatcher = new AsyncDispatcher();
        Dispatcher.Clear();
    }
}