﻿using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using FluentAssertions;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Databases;

public class QueryBaseExtensionsTests : IClassFixture<QueryBaseFixture>
{
    private readonly QueryBaseFixture _queryBase;

    public QueryBaseExtensionsTests(QueryBaseFixture queryBase)
    {
        _queryBase = queryBase;
    }

    [Fact]
    public void QueryBaseExtensions_CreateCommand_MySqlConnection_ShouldBeValid_GivenValidModel()
    {
        _queryBase.Reset();
        _queryBase.SetParameters();

        var results = _queryBase.MySqlQueryBase.CreateCommand(new MySqlConnection());
        results.Should().NotBeNull();
        results.Parameters.Should().HaveCount(2);
    }

    [Fact]
    public void QueryBaseExtensions_CreateCommand_SqlConnection_ShouldBeValid_GivenValidModel()
    {
        _queryBase.Reset();
        _queryBase.SetParameters();

        var results = _queryBase.MicrosoftQueryBase.CreateCommand(new SqlConnection());
        results.Should().NotBeNull();
        results.Parameters.Should().HaveCount(2);
    }

    [Fact]
    public void QueryBaseExtensions_CreateDataAdapter_MySqlConnection_ShouldBeValid_GivenValidModel()
    {
        _queryBase.Reset();
        _queryBase.SetParameters();

        var results = _queryBase.MySqlQueryBase.CreateDataAdapter(new MySqlConnection());
        results.Should().NotBeNull();
        results.SelectCommand.Parameters.Should().HaveCount(2);
    }

    [Fact]
    public void QueryBaseExtensions_CreateDataAdapter_SqlConnection_ShouldBeValid_GivenValidModel()
    {
        _queryBase.Reset();
        _queryBase.SetParameters();

        var results = _queryBase.MicrosoftQueryBase.CreateDataAdapter(new SqlConnection());
        results.Should().NotBeNull();
        results.SelectCommand.Parameters.Should().HaveCount(2);
    }

    [Fact]
    public void QueryBaseExtensions_IsEmpty_ShouldBeFalse_GivenValidModel()
    {
        _queryBase.Reset();

        _queryBase.MicrosoftQueryBase.IsEmpty().Should().BeFalse();
    }

    [Fact]
    public void QueryBaseExtensions_IsEmpty_ShouldBeTrue_GivenEmptyModel()
    {
        _queryBase.Reset();
        _queryBase.SetEmpty();

        _queryBase.MicrosoftQueryBase.IsEmpty().Should().BeTrue();
    }

    [Fact]
    public void QueryBaseExtensions_IsEmpty_ShouldBeTrue_GivenMissingSqlButHasParameterModel()
    {
        _queryBase.Reset();
        _queryBase.MicrosoftQueryBase = new QueryBase<SqlParameter>(null, new List<SqlParameter> { new("string", SqlDbType.NVarChar) });

        _queryBase.MicrosoftQueryBase.IsEmpty().Should().BeTrue();
    }

    [Fact]
    public void QueryBaseExtensions_IsEmpty_ShouldBeTrue_GivenNullModel()
    {
        _queryBase.Reset();
        _queryBase.SetNull();

        _queryBase.MicrosoftQueryBase.IsEmpty().Should().BeTrue();
    }
}