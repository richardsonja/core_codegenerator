﻿using Core.Generator.Services.Services.Configurations;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Services;

public class SessionServiceFixture : IDisposable, IReset
{
    public SessionService Service;

    public SessionServiceFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        Service = null;
    }

    public void Reset()
    {
        Service = new SessionService();
    }
}