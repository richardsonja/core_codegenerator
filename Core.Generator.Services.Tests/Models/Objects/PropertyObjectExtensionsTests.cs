﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Objects;
using Core.Generator.Services.Tests.Fixtures.Models.Objects;
using FluentAssertions;
using System;
using System.Collections.Generic;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Objects;

public class PropertyObjectExtensionsTests : IClassFixture<PropertyObjectFixture>, IClassFixture<GetterSetterObjectFixture>
{
    private readonly GetterSetterObjectFixture _getterSetterObject;
    private readonly PropertyObjectFixture _propertyObject;

    public PropertyObjectExtensionsTests(PropertyObjectFixture propertyObject, GetterSetterObjectFixture getterSetterObject)
    {
        _propertyObject = propertyObject;
        _getterSetterObject = getterSetterObject;
    }

    [Fact]
    public void PropertyObject_AddGetterSetter_List_Duplicates_ShouldMatchIfThrowException_GivenList()
    {
        _getterSetterObject.Reset();
        _propertyObject.Reset();

        Action action = () => _propertyObject.PropertyObject
            .AddGetterSetter(new List<GetterSetterObject> { new(GetterSetterType.Get) })
            .AddGetterSetter(new List<GetterSetterObject> { new(GetterSetterType.Get), new(GetterSetterType.Set) });

        action.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void PropertyObject_AddGetterSetter_List_Duplicates_ShouldMatchIfThrowException_GivenPreviouslyListed()
    {
        _getterSetterObject.Reset();
        _propertyObject.Reset();

        Action action = () => _propertyObject.PropertyObject
            .AddGetterSetter(new List<GetterSetterObject> { new(GetterSetterType.Get) })
            .AddGetterSetter(new List<GetterSetterObject> { new(GetterSetterType.Get) });

        action.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void PropertyObject_AddGetterSetter_List_Duplicates_ShouldMatchIfThrowException_GivenSameList()
    {
        _getterSetterObject.Reset();
        _propertyObject.Reset();

        Action action = () => _propertyObject.PropertyObject
            .AddGetterSetter(new List<GetterSetterObject> { new(GetterSetterType.Get), new(GetterSetterType.Get) });

        action.Should().Throw<InvalidOperationException>();
    }

    [Theory]
    [InlineData(GetterSetterType.Init, GetterSetterType.Set, true)]
    [InlineData(GetterSetterType.Get, GetterSetterType.Init, false)]
    [InlineData(GetterSetterType.Get, GetterSetterType.Set, false)]
    public void PropertyObject_AddGetterSetter_List_InvalidCombo_ShouldMatchIfThrowException_GivenInputs(
        GetterSetterType type1,
        GetterSetterType type2,
        bool shouldThrowException)
    {
        _getterSetterObject.Reset();
        _propertyObject.Reset();

        Action action = () => _propertyObject.PropertyObject
            .AddGetterSetter(new GetterSetterObject(type1))
            .AddGetterSetter(new List<GetterSetterObject> { new(type2) });

        if (shouldThrowException)
        {
            action.Should().Throw<IndexOutOfRangeException>();
        }
        else
        {
            action.Should().NotThrow<IndexOutOfRangeException>();
        }
    }

    [Fact]
    public void PropertyObject_AddGetterSetter_OneAtTime_Duplicates_ShouldMatchIfThrowException_GivenInputs()
    {
        _getterSetterObject.Reset();
        _propertyObject.Reset();

        Action action = () => _propertyObject.PropertyObject
            .AddGetterSetter(new GetterSetterObject(GetterSetterType.Get))
            .AddGetterSetter(new GetterSetterObject(GetterSetterType.Get));

        action.Should().Throw<InvalidOperationException>();
    }

    [Theory]
    [InlineData(GetterSetterType.Init, GetterSetterType.Set, true)]
    [InlineData(GetterSetterType.Get, GetterSetterType.Init, false)]
    [InlineData(GetterSetterType.Get, GetterSetterType.Set, false)]
    public void PropertyObject_AddGetterSetter_OneAtTime_InvalidCombo_ShouldMatchIfThrowException_GivenInputs(
        GetterSetterType type1,
        GetterSetterType type2,
        bool shouldThrowException)
    {
        _getterSetterObject.Reset();
        _propertyObject.Reset();

        Action action = () => _propertyObject.PropertyObject
            .AddGetterSetter(new GetterSetterObject(type1))
            .AddGetterSetter(new GetterSetterObject(type2));

        if (shouldThrowException)
        {
            action.Should().Throw<IndexOutOfRangeException>();
        }
        else
        {
            action.Should().NotThrow<IndexOutOfRangeException>();
        }
    }

    [Fact]
    public void PropertyObject_AddGetterSetter_ToString_ShouldHaveGetterSetter_GivenAddedGetAndSet()
    {
        var getterSetterObjects = new List<GetterSetterObject>
        {
            new (GetterSetterType.Get)
        };
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.AddGetterSetter(new GetterSetterObject(GetterSetterType.Set)).AddGetterSetter(new GetterSetterObject(GetterSetterType.Get)).ToString();
        results.TrimInternalExtraSpaces().Should().Be("public string Bob { get; set; }");
    }

    [Fact]
    public void PropertyObject_AddGetterSetter_ToString_ShouldHaveGetterSetter_GivenAddedGetOnly()
    {
        var getterSetterObjects = new List<GetterSetterObject>
        {
            new (GetterSetterType.Get)
        };
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.AddGetterSetter(getterSetterObjects).ToString();
        results.TrimInternalExtraSpaces().Should().Be("public string Bob { get; }");
    }

    [Fact]
    public void PropertyObject_AddGetterSetter_ToString_ShouldHaveGetterSetter_GivenSingleGetterSetter()
    {
        _getterSetterObject.Reset();
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.AddGetterSetter(_getterSetterObject.GetterSetterObject).ToString();
        results.TrimInternalExtraSpaces().Should().Be("public string Bob { get; }");
    }

    [Fact]
    public void PropertyObject_Clone_ToString_ShouldHaveAllElements_GivenSetup()
    {
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject
            .AddGetterSetter(new GetterSetterObject(GetterSetterType.Get))
            .SetAccessorType(AccessorType.Public)
            .SetPropertyInitiator("Bob", false)
            .SetType(typeof(decimal))
            .Clone()
            .ToString();
        results.TrimInternalExtraSpaces().Should().Be("public decimal Bob { get; } = Bob;");
    }

    [Theory]
    [InlineData(AccessorType.Internal, "internal string Bob ;")]
    [InlineData(AccessorType.Public, "public string Bob ;")]
    [InlineData(AccessorType.Private, "private string Bob ;")]
    [InlineData(AccessorType.Protected, "protected string Bob ;")]
    public void PropertyObject_SetAccessorType_ToString_ShouldMatchExpected_GivenInput(AccessorType value, string expected)
    {
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.SetAccessorType(value).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void PropertyObject_SetIsConstant_ToString_ShouldDisplayPropertyObject_GivenSetup()
    {
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.SetIsConstant().ToString();
        results.TrimInternalExtraSpaces().Should().Be("public const string Bob ;");
    }

    [Fact]
    public void PropertyObject_SetIsConstant_ToString_ShouldThrowException_GivenInvalidCombos()
    {
        _propertyObject.Reset();

        Action results = () => _propertyObject.PropertyObject.SetIsReadOnly().SetIsConstant();
        results.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void PropertyObject_SetIsReadOnly_ToString_ShouldDisplayPropertyObject_GivenSetup()
    {
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.SetIsReadOnly().ToString();
        results.TrimInternalExtraSpaces().Should().Be("public readonly string Bob ;");
    }

    [Fact]
    public void PropertyObject_SetIsReadOnly_ToString_ShouldThrowException_GivenInvalidCombos()
    {
        _propertyObject.Reset();

        Action results = () => _propertyObject.PropertyObject.SetIsConstant().SetIsReadOnly();
        results.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void PropertyObject_SetIsStatic_ToString_ShouldDisplayPropertyObject_GivenSetup()
    {
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.SetIsStatic().ToString();
        results.TrimInternalExtraSpaces().Should().Be("public static string Bob ;");
    }

    [Fact]
    public void PropertyObject_SetIsStatic_ToString_ShouldThrowException_GivenInvalidCombos()
    {
        _propertyObject.Reset();

        Action results = () => _propertyObject.PropertyObject.SetIsConstant().SetIsStatic();
        results.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void PropertyObject_SetPropertyInitiator_ToString_ShouldDisplayPropertyObject_GivenSetupWithoutQuotes()
    {
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.SetPropertyInitiator("Bob", false).ToString();
        results.TrimInternalExtraSpaces().Should().Be("public string Bob = Bob;");
    }

    [Fact]
    public void PropertyObject_SetPropertyInitiator_ToString_ShouldDisplayPropertyObject_GivenSetupWithQuotes()
    {
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.SetPropertyInitiator("Bob", true).ToString();
        results.TrimInternalExtraSpaces().Should().Be("public string Bob = \"Bob\";");
    }

    [Fact]
    public void PropertyObject_SetType_ToString_ShouldDisplayPropertyObject_GivenSetup()
    {
        _propertyObject.Reset();

        var results = _propertyObject.PropertyObject.SetType(typeof(Guid), false).ToString();
        results.TrimInternalExtraSpaces().Should().Be("public Guid Bob ;");
    }

    [Fact]
    public void PropertyObjectExtensions_ToPropertyObject_ShouldProducePropertyObject_GivenListOfPropertyObject()
    {
        var properties = new List<PropertyObject>
        {
            new ("BobMethod"),
            new("AbeMethod"),
            new PropertyObject("AllenMethod").SetAccessorType(AccessorType.Private),
            new("BobMethod"),
        };

        var results = properties.ToPropertyObject();
        results.TrimInternalExtraSpaces().Should().Be("public string AbeMethod ; public string BobMethod ; public string BobMethod ; private string AllenMethod ;");
    }
}