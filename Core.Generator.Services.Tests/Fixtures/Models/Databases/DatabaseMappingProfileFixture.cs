﻿using AutoMapper;
using Core.Generator.Services.Models.Databases;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Models.Databases;

public class DatabaseMappingProfileFixture : IDisposable, IReset
{
    public IMapper Mapper;

    public DatabaseMappingProfileFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        Mapper = null;
    }

    public void Reset()
    {
        Mapper = new MapperConfiguration(cfg => { cfg.AddProfile<DatabaseMappingProfile>(); }).CreateMapper();
    }
}