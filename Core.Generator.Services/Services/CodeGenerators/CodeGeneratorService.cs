﻿using Core.Generator.Services.Models;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Services.CodeGenerators.Types.Controllers;
using Core.Generator.Services.Services.CodeGenerators.Types.Models;
using Core.Generator.Services.Services.CodeGenerators.Types.Sqls;

namespace Core.Generator.Services.Services.CodeGenerators;

public class CodeGeneratorService : ICodeGeneratorService
{
    private readonly IModelCodeGeneratorService _modelCodeGeneratorService;
    private readonly IControllerCodeGeneratorService _controllerCodeGeneratorService;
    private readonly ISqlCodeGeneratorService _sqlCodeGeneratorService;

    public CodeGeneratorService(
        IModelCodeGeneratorService modelCodeGeneratorService,
        IControllerCodeGeneratorService controllerCodeGeneratorService,
        ISqlCodeGeneratorService sqlCodeGeneratorService)
    {
        _modelCodeGeneratorService = modelCodeGeneratorService;
        _controllerCodeGeneratorService = controllerCodeGeneratorService;
        _sqlCodeGeneratorService = sqlCodeGeneratorService;
    }

    public async Task<Option<CodeGeneratorResult>> CreateOutputAsync(CodeGeneratorRequest request)
    {
        if (request?.Table?.Columns == null)
        {
            return Option<CodeGeneratorResult>.None;
        }

        var result = new CodeGeneratorResult(request.Table?.Columns.OrderByPosition());
            
        return await CreateResultsAsync(result, request);
    }

    private async Task<DisplayResult> CreateMethodTabAsync(
        int displayOrder, 
        CodeGeneratorRequest request)
    {
        var modelResult = await _modelCodeGeneratorService
            .CreateOutputAsync(request)
            .Match(x => x, () => new List<DisplayResult>().ToImmutableList());
            
        return Constants.TabModels
            .CreateDisplayResult(
                displayOrder, 
                modelResult
                    .FilterNullOrEmpty()
                    .ToArray());
    }
        
    private async Task<DisplayResult> CreateControllerTabAsync(
        int displayOrder, 
        CodeGeneratorRequest request)
    {
        var controllerResult = await _controllerCodeGeneratorService
            .CreateOutputAsync(request)
            .Match(x => x,() => new List<DisplayResult>().ToImmutableList());
            
        return Constants.TabController.CreateDisplayResult(
            displayOrder, 
            controllerResult
                .FilterNullOrEmpty()
                .ToArray());
    }
        
    private async Task<DisplayResult> CreateSqlTabAsync(
        int displayOrder, 
        CodeGeneratorRequest request)
    {
        var controllerResult = await _sqlCodeGeneratorService
            .CreateOutputAsync(request)
            .Match(x => x,() => new List<DisplayResult>().ToImmutableList());
            
        return Constants.TabSql.CreateDisplayResult(
            displayOrder, 
            controllerResult
                .FilterNullOrEmpty()
                .ToArray());
    }

    private async Task<Option<CodeGeneratorResult>> CreateResultsAsync(
        CodeGeneratorResult result, 
        CodeGeneratorRequest request)
    {
        var counter = 1;
        var methodResults = await CreateMethodTabAsync(counter++,  request);
        var controllerResults = await CreateControllerTabAsync(counter++,  request);
        var sqlResults = await CreateSqlTabAsync(counter++,  request);

        var finalResults = methodResults
            .ToFullList()
            .AddTo(controllerResults)
            .AddTo(sqlResults)
            .FilterNullOrEmpty();

        result = result.AddDisplayResult(finalResults);
        return result.OrderList();
    }
}