﻿namespace Core.Flux.Tests.Fixtures;

public class DefaultStoreResolverFixture : IDisposable, IReset
{
    public IStoreResolver StoreResolver;

    public DefaultStoreResolverFixture() => Reset();

    public void Dispose() => StoreResolver = null;

    public void Reset() => StoreResolver = new DefaultStoreResolver();
}