﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Sqls;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Sqls;

public class DeleteStatementExtensionsTests : IClassFixture<DeleteStatementFixture>, IClassFixture<DatabaseTableColumnFixture>
{
    private readonly DatabaseTableColumnFixture _database;
    private readonly DeleteStatementFixture _deleteStatement;

    public DeleteStatementExtensionsTests(DeleteStatementFixture deleteStatement, DatabaseTableColumnFixture database)
    {
        _deleteStatement = deleteStatement;
        _database = database;
    }

    [Fact]
    public void DeleteStatement_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenInput()
    {
        _deleteStatement.Reset();
        var item = new StoredProcedureParameter(_database.CreateTableColumns("bob", "was here", "and here", 1));
        var results = _deleteStatement.DeleteStatement.AddParameter(item).ToString();
        results.TrimInternalExtraSpaces().Should().Be("DELETE FROM [Bob].[Was Here] as wh WHERE b.[Column1] = @Column1");
    }

    [Fact]
    public void DeleteStatement_AddParameter_ToString_ShouldDisplayBasicMethodWithBody_GivenListInput()
    {
        _deleteStatement.Reset();

        var tsil = new List<StoredProcedureParameter>
        {
            new (_database.CreateTableColumns("bob", "was here", "and here", 2, "Bob1")),
            new (_database.CreateTableColumns("bob", "was here", "and here", 1, "Bob2"))
        };

        var results = _deleteStatement.DeleteStatement.AddParameter(tsil).ToString();
        results.TrimInternalExtraSpaces().Should().Be("DELETE FROM [Bob].[Was Here] as wh WHERE b.[Bob1] = @Bob1 AND b.[Bob2] = @Bob2");
    }

    [Fact]
    public void DeleteStatement_AddParameter_ToString_ShouldNotThrow_GivenEmptyList()
    {
        var body = new List<StoredProcedureParameter>();
        _deleteStatement.Reset();

        Action results = () => _deleteStatement.DeleteStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void DeleteStatement_AddParameter_ToString_ShouldNotThrow_GivenNullList()
    {
        List<StoredProcedureParameter> body = null;
        _deleteStatement.Reset();

        Action results = () => _deleteStatement.DeleteStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void DeleteStatement_AddParameter_ToString_ShouldNotThrow_GivenNullSingle()
    {
        StoredProcedureParameter body = null;
        _deleteStatement.Reset();

        Action results = () => _deleteStatement.DeleteStatement.AddParameter(body);
        results.Should().NotThrow();
    }

    [Fact]
    public void DeleteStatement_Clone_ShouldHaveAllAttributes_GivenSetup()
    {
        _deleteStatement.Reset();
        var parameter = new StoredProcedureParameter(_database.CreateTableColumns("bob", "was here", "and here", 1));

        var results = (_deleteStatement
                .DeleteStatement
                .SetRemoveBrackets(true)
                .AddParameter(parameter) as DeleteStatement)
            .Clone() as DeleteStatement;

        results.Should().NotBeNull();
        results.Schema.Should().Be("Bob");
        results.Name.Should().Be("Was Here");
        results.TableAbbreviation.Should().Be("wh");
        results.WhereStatementParameters.Should().HaveCount(1);
        results.WhereStatementParameters.First().ToString().Should().Contain("b.[Column1] = @Column1");
        results.RemoveBrackets.Should().BeTrue();

        results.ToString().TrimInternalExtraSpaces().Should().Be("DELETE FROM Bob.[Was Here] as wh WHERE b.[Column1] = @Column1");
    }

    [Theory]
    [InlineData("Bob", "WasHere", "DELETE FROM Bob.WasHere as wh")]
    [InlineData("Bob", "Was Here", "DELETE FROM Bob.[Was Here] as wh")]
    [InlineData("Bob Was", "WasHere", "DELETE FROM [Bob Was].WasHere as wh")]
    [InlineData("Bob Was", "Was Here", "DELETE FROM [Bob Was].[Was Here] as wh")]
    public void DeleteStatement_SetRemoveBrackets_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        string schemaName,
        string tableName,
        string expected)
    {
        _deleteStatement.Reset();
        _deleteStatement.DeleteStatement = new DeleteStatement(schemaName, tableName);

        var results = _deleteStatement.DeleteStatement.SetRemoveBrackets().ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void DeleteStatement_ToString_ShouldDisplayBasicMethod_GivenNameOnly()
    {
        _deleteStatement.Reset();

        var results = _deleteStatement.DeleteStatement.ToString();
        results.TrimInternalExtraSpaces().Should().Be("DELETE FROM [Bob].[Was Here] as wh");
    }
}