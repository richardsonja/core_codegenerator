﻿using Core.Common.Generics;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Models;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Models;

public class ModelServiceTests : IClassFixture<ModelServiceFixture>, IClassFixture<DatabaseTableColumnFixture>, IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly ModelServiceFixture _service;

    public ModelServiceTests(ModelServiceFixture service, DatabaseTableColumnFixture databaseTableColumn, ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }

    [Fact]
    public async void ModelService_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelService_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("using System; using System.Collections.Generic; namespace Bob.Is.Here.Testing.Models.bob { public class BobTable : IEquatable<BobTable> { public bool BobColumn { get; set; } public override bool Equals(object obj) { if (ReferenceEquals(null, obj)) { return false; } if (ReferenceEquals(this, obj)) { return true; } return obj.GetType() == this.GetType() && Equals(({table.FormattedName})obj); } public bool Equals(BobTable other) { if (ReferenceEquals(null, other)) { return false; } if (ReferenceEquals(this, other)) { return true; } return BobColumn = other.BobColumn; } override int GetHashCode => HashCode.Combine(BobColumn); } }"));
    }

    [Fact]
    public async void ModelService_CreateOutput_ShouldReturnNone_GivenEmptyModelEmptyTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelService_CreateOutput_ShouldReturnNone_GivenEmptyModelNullTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(null));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void ModelService_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(null);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }
}