﻿using Core.Generator.Services.Repositories;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Repositories;

public class FileRepositoryFixture : IDisposable, IReset
{
    public FileRepository Repository;

    public FileRepositoryFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        Repository = null;
    }

    public void Reset()
    {
        Repository = new FileRepository();
    }
}