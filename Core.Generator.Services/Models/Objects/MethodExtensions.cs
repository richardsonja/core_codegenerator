﻿namespace Core.Generator.Services.Models.Objects;

public static class MethodExtensions
{
    public static string CreateOutput(this IEnumerable<Method> methods) =>
        methods
            ?.OrderBy(x => x.AccessorType)
            .ThenBy(x => x.Name)
            .ThenByDescending(x => x.Parameters.IsNullOrEmpty())
            .ThenBy(x => x.Parameters.FirstOrDefault()?.Name)
            .Select(x => x.ToString()?.Trim())
            .Combine($"{Environment.NewLine}{Environment.NewLine}");

    public static string ToAttribute(this IEnumerable<Method> attributes) =>
        attributes
            ?.OrderBy(x => x.Name)
            .Select(x => x.ToString()?.Trim())
            .Combine(Environment.NewLine, "[", $"]");

    public static string ToMethod(this IEnumerable<Method> methods) =>
        methods
            ?.OrderBy(x => (int)x.AccessorType)
            .ThenBy(x => x.Name)
            .ThenBy(x => x.Parameters?.Count() ?? 0)
            .Select(x => x.ToString()?.Trim())
            .Combine(Environment.NewLine + Environment.NewLine);
}