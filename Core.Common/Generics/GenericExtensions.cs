﻿namespace Core.Common.Generics;

public static class GenericExtensions
{
    public static bool IsDefault<T>(this T source) where T : struct => source.Equals(default(T));

    public static bool IsNumeric(this object source)
    {
        var stringToCheck = source?.ToString();
        return !stringToCheck.IsNullOrEmpty()
               && stringToCheck.ToCharArray().All(char.IsDigit);
    }

    public static ImmutableList<T> ToFullImmutableList<T>(this T source) =>
        source.ToFullList().ToImmutableList();

    public static List<T> ToFullList<T>(this T source) =>
        source == null
            ? new()
            : new() { source };
}