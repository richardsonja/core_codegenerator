﻿using Core.Generator.Services.Models.Objects;
using System;
using System.Collections.Generic;

namespace Core.Generator.Services.Tests.Fixtures.Models.Objects;

public class MethodFixture : IDisposable, IReset
{
    public Method Method;

    public MethodFixture()
    {
        Reset();
    }

    public void Dispose()
    {
        Method = null;
    }

    public void Reset()
    {
        Method = new Method("Bob");
    }

    public Method CreateAttributeMethod(string name, IEnumerable<MethodParameter> parameters)
    {
        Method = new Method(name, parameters).SetAttributeMethod();

        return Method;
    }

    public Method CreateMethod(string name, IEnumerable<MethodParameter> parameters)
    {
        Method = new Method(name, parameters);

        return Method;
    }
}