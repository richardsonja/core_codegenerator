﻿using Core.Common.Generics;

namespace Core.Common.Tests.Generics;

public class GenericExtensionsTests
{
    [Theory]
    [InlineData(0, true)]
    [InlineData(-1, false)]
    [InlineData(1, false)]
    public void GenericExtensions_IsDefault_ShouldBeExpected_GivenInput(int testValue, bool expected) =>
        testValue.IsDefault().Should().Be(expected);

    [Theory]
    [InlineData("0", true)]
    [InlineData("-1", false)]
    [InlineData("1.0", false)]
    [InlineData("silly", false)]
    public void GenericExtensions_IsNumeric_ShouldBeExpected_GivenInput(string testValue, bool expected) =>
        testValue.IsNumeric().Should().Be(expected);
}