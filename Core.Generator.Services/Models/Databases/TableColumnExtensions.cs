﻿namespace Core.Generator.Services.Models.Databases;

public static class TableColumnExtensions
{
    public static ImmutableList<TableColumn> FilterColumns(
        this IEnumerable<TableColumn> tableColumns,
        IEnumerable<SqlDbType> dataTypes) =>
        dataTypes.IsNullOrEmpty()
            ? new List<TableColumn>().ToImmutableList()
            : tableColumns
                ?.Where(x => dataTypes.Contains(x.DataType))
                .OrderByPosition();

    public static ImmutableList<TableColumn> FilterColumns(
        this IEnumerable<TableColumn> tableColumns,
        IEnumerable<Type> dataTypes) =>
        dataTypes.IsNullOrEmpty()
            ? new List<TableColumn>().ToImmutableList()
            : tableColumns
                ?.Where(x => dataTypes.Contains(x.CSharpType.Type))
                .OrderByPosition();

    public static ImmutableList<TableColumn> FilterColumns(
        this IEnumerable<TableColumn> tableColumns,
        IEnumerable<string> dataTypes) =>
        dataTypes.IsNullOrEmpty()
            ? new List<TableColumn>().ToImmutableList()
            : tableColumns
                ?.Where(x => dataTypes.Contains(x?.CSharpType?.TypeString))
                .OrderByPosition();

    public static ImmutableList<TableColumn> FilterOutColumns(
        this IEnumerable<TableColumn> tableColumns,
        IEnumerable<SqlDbType> dataTypes) =>
        dataTypes.IsNullOrEmpty()
            ? new List<TableColumn>().ToImmutableList()
            : tableColumns
                ?.Where(x => !dataTypes.Contains(x.DataType))
                .OrderByPosition();

    public static ImmutableList<TableColumn> FilterOutColumns(
        this IEnumerable<TableColumn> tableColumns,
        IEnumerable<Type> dataTypes) =>
        dataTypes.IsNullOrEmpty()
            ? new List<TableColumn>().ToImmutableList()
            : tableColumns
                ?.Where(x => !dataTypes.Contains(x.CSharpType.Type))
                .OrderByPosition();

    public static ImmutableList<TableColumn> FilterOutColumns(
        this IEnumerable<TableColumn> tableColumns,
        IEnumerable<string> dataTypes) =>
        dataTypes.IsNullOrEmpty()
            ? new List<TableColumn>().ToImmutableList()
            : tableColumns
                ?.Where(x => !dataTypes.Contains(x?.CSharpType?.TypeString))
                .OrderByPosition();

    public static ImmutableList<TableColumn> GetActiveBooleanColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.SystemColumns.IsActive && x.IsBoolean())
            .OrderByPosition();

    public static string GetDefaultSqlValue(this TableColumn x) =>
        x.IsBoolean() || x.IsNumeric()
            ? "1"
            : "\"TBD\"";

    public static ImmutableList<TableColumn> GetIsActiveColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.SystemColumns.IsActive)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetIsCreateByColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.SystemColumns.IsCreateBy)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetIsCreateDateColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.SystemColumns.IsCreateDate)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetIsCreatedColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.SystemColumns.IsCreateDate || x.SystemColumns.IsCreateBy)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetIsModifiedColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.SystemColumns.IsModifyDate || x.SystemColumns.IsModifyBy)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetIsModifyByColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.SystemColumns.IsModifyBy)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetIsModifyDateColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.SystemColumns.IsModifyDate)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetNonActiveBooleansOrActiveNonBooleanColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => (!x.SystemColumns.IsActive && x.IsBoolean()) || (x.SystemColumns.IsActive && !x.IsBoolean()))
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetNonPrimaryAndNonTimeStampColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => !x.IsIdentityOrIndex && !x.SystemColumns.IsTimeStamp)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetNonSystemColumns(this IEnumerable<TableColumn> tableColumns) =>
                                            tableColumns
            ?.Where(x =>
                !x.SystemColumns.IsTimeStamp
                && !x.SystemColumns.IsCreateDate
                && !x.SystemColumns.IsCreateBy
                && !x.SystemColumns.IsModifyDate
                && !x.SystemColumns.IsModifyBy)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetPermanentNameColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.SystemColumns.IsPermanentName)
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetPermanentNameOrNonActivePrimaryOrForeignColumns(
        this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x =>
                x.SystemColumns.IsPermanentName
                || (!x.SystemColumns.IsActive && (x.IsIdentityOrIndex || x.IsForeignKey)))
            .Clone()
            .OrderByPosition();

    public static ImmutableList<TableColumn> GetPrimaryKeyColumns(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.Where(x => x.IsIdentityOrIndex)
            .OrderByPosition();

    public static bool IsBoolean(this TableColumn x) =>
        x?.CSharpType?.Type != null
        && (x.CSharpType.Type == typeof(bool)
            || x.CSharpType.Type == typeof(bool?));

    public static bool IsNumeric(this TableColumn x) =>
        x?.CSharpType?.Type != null
        && (x.CSharpType.Type == typeof(decimal)
            || x.CSharpType.Type == typeof(decimal?)
            || x.CSharpType.Type == typeof(double)
            || x.CSharpType.Type == typeof(double?)
            || x.CSharpType.Type == typeof(int)
            || x.CSharpType.Type == typeof(int?)
            || x.CSharpType.Type == typeof(short)
            || x.CSharpType.Type == typeof(short?)
            || x.CSharpType.Type == typeof(long)
            || x.CSharpType.Type == typeof(long?)
            || x.CSharpType.Type == typeof(float)
            || x.CSharpType.Type == typeof(float?));

    public static ImmutableList<TableColumn> OrderByPosition(this IEnumerable<TableColumn> tableColumns) =>
        tableColumns
            ?.OrderBy(x => x.OrdinalPosition)
            .ToImmutableList();
}