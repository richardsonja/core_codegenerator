﻿namespace Core.Generator.Services.Models.Configurations;

public class CodeRelatedConfiguration : ICloneable
{
    public string BulkDeleteMethodName { get; set; }

    public string BulkDestroyMethodName { get; set; }

    public string BulkInsertMethodName { get; set; }

    public string BulkUpdateMethodName { get; set; }

    public string ControllerLayerName { get; set; }

    public string DataLayerName { get; set; }

    public string DeleteMethodName { get; set; }

    public string DestroyMethodName { get; set; }

    public string FindMethodName { get; set; }

    public string GetActiveListMethodName { get; set; }

    public string GetMethodName { get; set; }

    public string InsertMethodName { get; set; }

    public string Namespace { get; set; }

    public string QueryModelName { get; set; }

    public string QueryNamespaceSuffix { get; set; }

    public string RequestSuffixModelName { get; set; }

    public string ResponseSuffixModelName { get; set; }

    public string ServiceLayerName { get; set; }

    public string UpdateMethodName { get; set; }

    public object Clone() =>
        new CodeRelatedConfiguration
        {
            BulkDeleteMethodName = BulkDeleteMethodName,
            BulkDestroyMethodName = BulkDestroyMethodName,
            BulkInsertMethodName = BulkInsertMethodName,
            BulkUpdateMethodName = BulkUpdateMethodName,
            ControllerLayerName = ControllerLayerName,
            DataLayerName = DataLayerName,
            DeleteMethodName = DeleteMethodName,
            DestroyMethodName = DestroyMethodName,
            FindMethodName = FindMethodName,
            GetActiveListMethodName = GetActiveListMethodName,
            GetMethodName = GetMethodName,
            InsertMethodName = InsertMethodName,
            Namespace = Namespace,
            QueryModelName = QueryModelName,
            QueryNamespaceSuffix = QueryNamespaceSuffix,
            RequestSuffixModelName = RequestSuffixModelName,
            ResponseSuffixModelName = ResponseSuffixModelName,
            ServiceLayerName = ServiceLayerName,
            UpdateMethodName = UpdateMethodName,
        };
}