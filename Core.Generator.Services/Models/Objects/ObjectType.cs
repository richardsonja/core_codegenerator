﻿namespace Core.Generator.Services.Models.Objects;

public enum ObjectType
{
    Class,
    Enum,
    Interface
}