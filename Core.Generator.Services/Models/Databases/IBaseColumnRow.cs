﻿namespace Core.Generator.Services.Models.Databases;

public interface IBaseColumnRow
{
    int? CharacterMaxLength { get; }

    SqlDbType DataType { get; }

    bool IsNullable { get; }

    string Name { get; }

    string OriginalDataTypeString { get; }

    int? Percision { get; }

    int? Scale { get; }

    string TableName { get; }

    string TableSchema { get; }
}