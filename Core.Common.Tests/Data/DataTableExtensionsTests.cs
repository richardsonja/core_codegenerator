﻿using Core.Common.Data;

namespace Core.Common.Tests.Data;

public class DataTableExtensionsTests : IClassFixture<DataTableFixture>
{
    private readonly DataTableFixture _dataTable;

    public DataTableExtensionsTests(DataTableFixture dataTable) => _dataTable = dataTable;

    [Fact]
    public void DataTableExtension_IsNullOrEmpty_ShouldFalse_GivenLoadedDataTable()
    {
        _dataTable.SetValidLoaded();
        var results = _dataTable.DataTable.IsNullOrEmpty();
        results.Should().BeFalse();
    }

    [Fact]
    public void DataTableExtension_IsNullOrEmpty_ShouldTrue_GivenEmptyDataTable()
    {
        _dataTable.Reset();
        _dataTable.DataTable = new DataTable();
        var results = _dataTable.DataTable.IsNullOrEmpty();
        results.Should().BeTrue();
    }

    [Fact]
    public void DataTableExtension_IsNullOrEmpty_ShouldTrue_GivenNull()
    {
        _dataTable.Reset();
        _dataTable.DataTable = null;
        var results = _dataTable.DataTable.IsNullOrEmpty();
        results.Should().BeTrue();
    }

    [Fact]
    public void DataTableExtension_IsNullOrEmpty_ShouldTrue_GivenSetupButNoDataDataTable()
    {
        _dataTable.SetValidRowNoContent();
        var results = _dataTable.DataTable.IsNullOrEmpty();
        results.Should().BeTrue();
    }
}