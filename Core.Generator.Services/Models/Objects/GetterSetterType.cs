﻿namespace Core.Generator.Services.Models.Objects;

public enum GetterSetterType
{
    Get,
    Set,
    Init
}