﻿using Core.Generator.Services.Repositories.Database;
using Core.Generator.Services.Services.Configurations;
using Core.Generator.Services.Services.Databases;
using Moq;
using System;

namespace Core.Generator.Services.Tests.Fixtures.Services;

public class DatabaseServiceFixture : IDisposable, IReset
{
    public Mock<IConfigurationService> ConfigurationService;
    public Mock<IMicrosoftSqlRepository> MicrosoftSqlRepository;
    public Mock<IMySqlRepository> MySqlRepository;
    public DatabaseService Service;

    public DatabaseServiceFixture()
    {
        ConfigurationService = new Mock<IConfigurationService>();
        MicrosoftSqlRepository = new Mock<IMicrosoftSqlRepository>();
        MySqlRepository = new Mock<IMySqlRepository>();

        Service = new DatabaseService(ConfigurationService.Object, MicrosoftSqlRepository.Object, MySqlRepository.Object);
        Reset();
    }

    public void Dispose()
    {
        Service = null;
        ConfigurationService = null;
        MicrosoftSqlRepository = null;
        MySqlRepository = null;
    }

    public void Reset()
    {
        ConfigurationService.Reset();
        MicrosoftSqlRepository.Reset();
        MySqlRepository.Reset();
    }
}