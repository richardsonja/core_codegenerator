﻿using AutoMapper;
using Core.Common.Data;
using Core.Generator.Services.Repositories.Database.Connectors;

namespace Core.Generator.Services.Repositories.Database;

public class MicrosoftSqlRepository : IMicrosoftSqlRepository
{
    private readonly ImmutableList<string> _databaseTablesToExcludeFromResults = 
        new List<string> { "tempdb", "msdb", "model", "master" }.ToImmutableList();
    private readonly IMapper _mapper;
    private readonly IMicrosoftSqlConnector _sqlConnector;

    public MicrosoftSqlRepository(IMicrosoftSqlConnector sqlConnector, IMapper mapper)
    {
        _sqlConnector = sqlConnector;
        _mapper = mapper;
    }

    public async Task<Option<ImmutableList<Models.Databases.Database>>> GetDatabaseListFromServerVersionGreaterThan2008Async()
    {
        var sql = @$"SELECT
                    database_id AS {DatabaseColumnConstants.DatabaseId},
                    name AS {DatabaseColumnConstants.DatabaseName}
                    FROM sys.databases
                    WHERE name not in ({GetDatabaseExcludeList()})
                    ORDER BY [name]";

        return await GetDatabaseList(sql);
    }

    public async Task<Option<ImmutableList<Models.Databases.Database>>> GetDatabaseListFromServerVersionLessThan2008Async()
    {
        var sql = @$"SELECT
                    dbid AS {DatabaseColumnConstants.DatabaseId},
                    name AS {DatabaseColumnConstants.DatabaseName}
                    FROM [master].[dbo].[sysdatabases]
                    WHERE name not in ({GetDatabaseExcludeList()})
                    ORDER BY [name]";

        return await GetDatabaseList(sql);
    }

    public async Task<Option<ServerVersion>> GetServerVersionAsync()
    {
        var sql = $"SELECT SERVERPROPERTY('ProductVersion') as {DatabaseColumnConstants.ServerVersion}";
        var results = await _sqlConnector.ExecuteSqlAsync(new MicrosoftQuery(sql));
        if (results.IsNone)
        {
            return Option<ServerVersion>.None;
        }

        var converted = results.Match(
            s => s.IsNullOrEmpty()
                ? null
                : _mapper.Map<ServerVersion>(results.AsEnumerable().First()),
            () => null);
        return converted is null || converted.FullVersion.IsNullOrEmpty()
            ? Option<ServerVersion>.None
            : converted;
    }

    public async Task<Option<ImmutableList<TableColumn>>> GetTableAndColumnsAsync(IEnumerable<string> listOfDatabases)
    {
        var listOfTableAndColumnSql = listOfDatabases.Select(TableAndColumnSql).ToList();
        var sql = listOfTableAndColumnSql.Combine(" UNION ");
        var results = await _sqlConnector.ExecuteSqlAsync(new MicrosoftQuery(sql));
        if (results.IsNone)
        {
            return Option<ImmutableList<TableColumn>>.None;
        }

        var converted = results.Match(
            s => s.IsNullOrEmpty()
                ? null
                // Lesson learn is to use select so the mapping error will show up correctly in error logs
                : results.AsEnumerable()
                    .SelectMany(x => x.AsEnumerable().Select(y=> _mapper.Map<TableColumn>(y)))
                    .ToList(),
            () => null);
        return converted.IsNullOrEmpty() || converted.FirstOrDefault().Name.IsNullOrEmpty()
            ? Option<ImmutableList<TableColumn>>.None
            : converted.ToImmutableList();
    }

    public async Task<bool> HasAccessToDatabaseAsync(string databaseName)
    {
        try
        {
            var sql = $"SELECT 1 FROM [{databaseName}].INFORMATION_SCHEMA.TABLES";
            var result = await _sqlConnector.ExecuteSqlAsync(new MicrosoftQuery(sql));
            return result.Match(_ => true, () => false);
        }
        catch
        {
            return false;
        }
    }

    private string GetDatabaseExcludeList() => $"'{_databaseTablesToExcludeFromResults.Combine("','")}'";

    private async Task<Option<ImmutableList<Models.Databases.Database>>> GetDatabaseList(string sql)
    {
        var results = await _sqlConnector.ExecuteSqlAsync(new MicrosoftQuery(sql));
        if (results.IsNone)
        {
            return Option<ImmutableList<Models.Databases.Database>>.None;
        }

        var converted = results.Match(
            s => s.IsNullOrEmpty()
                ? null
                // Lesson learn is to use select so the mapping error will show up correctly in error logs
                : results.AsEnumerable()
                    .SelectMany(x => x.AsEnumerable().Select(y=> _mapper.Map<Models.Databases.Database>(y)))
                    .ToList(),
            () => null);
        return converted.IsNullOrEmpty() || converted.FirstOrDefault().Name.IsNullOrEmpty()
            ? Option<ImmutableList<Models.Databases.Database>>.None
            : converted.ToImmutableList();
    }

    private string TableAndColumnSql(string databaseName) => 
        $@"
SELECT '{databaseName}' AS {DatabaseColumnConstants.TableDatabaseName},
       Cols.TABLE_NAME AS {DatabaseColumnConstants.TableName},
       Cols.TABLE_SCHEMA AS {DatabaseColumnConstants.TableSchema},
       Cols.COLUMN_NAME AS {DatabaseColumnConstants.ColumnName},
       Cols.ORDINAL_POSITION AS {DatabaseColumnConstants.OrdinalPosition},
       Cols.DATA_TYPE AS {DatabaseColumnConstants.DataType},
       Cols.NUMERIC_PRECISION AS {DatabaseColumnConstants.NumericPrecision},
       Cols.NUMERIC_SCALE AS {DatabaseColumnConstants.NumericScale},
       CASE
            WHEN Cols.IS_NULLABLE = 'YES' THEN '1'
            WHEN Cols.IS_NULLABLE = 'NO' THEN '0'
            ELSE Cols.IS_NULLABLE
       END  AS {DatabaseColumnConstants.IsNullable},
       Cols.CHARACTER_MAXIMUM_LENGTH AS {DatabaseColumnConstants.CharacterMaximumLength},
(
    SELECT COUNT(KCU.COLUMN_NAME)
    FROM {databaseName}.INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU
         INNER JOIN {databaseName}.INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC ON KCU.TABLE_NAME = TC.TABLE_NAME
                                                                              AND KCU.CONSTRAINT_NAME = TC.CONSTRAINT_NAME
                                                                              AND TC.CONSTRAINT_TYPE = 'PRIMARY KEY'
    WHERE KCU.TABLE_NAME = Cols.TABLE_NAME
          AND KCU.COLUMN_NAME = Cols.COLUMN_NAME
) AS {DatabaseColumnConstants.IsIdentity},
(
    SELECT COUNT(KCU.COLUMN_NAME)
    FROM {databaseName}.INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU
         INNER JOIN {databaseName}.INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC ON KCU.TABLE_NAME = TC.TABLE_NAME
                                                                              AND KCU.CONSTRAINT_NAME = TC.CONSTRAINT_NAME
                                                                              AND TC.CONSTRAINT_TYPE = 'PRIMARY KEY'
    WHERE KCU.TABLE_NAME = Cols.TABLE_NAME
          AND KCU.COLUMN_NAME = Cols.COLUMN_NAME
) AS {DatabaseColumnConstants.IsIndex},
       KCU2.TABLE_NAME AS {DatabaseColumnConstants.ForeignTable},
       KCU2.COLUMN_NAME AS {DatabaseColumnConstants.ForeignColumn},
       KCU2.CONSTRAINT_SCHEMA AS {DatabaseColumnConstants.ForeignSchema}
FROM {databaseName}.INFORMATION_SCHEMA.COLUMNS Cols
     LEFT JOIN
(
    SELECT KCU.TABLE_NAME AS OriginalTableName,
           KCU.COLUMN_NAME AS OriginalColumnName,
           KCU2.TABLE_NAME,
           KCU2.COLUMN_NAME,
           KCU2.CONSTRAINT_SCHEMA
    FROM {databaseName}.INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC
         INNER JOIN {databaseName}.INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU ON KCU.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG
                                                                  AND KCU.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA
                                                                  AND KCU.CONSTRAINT_NAME = RC.CONSTRAINT_NAME
         INNER JOIN {databaseName}.INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU2 ON KCU2.CONSTRAINT_CATALOG = RC.UNIQUE_CONSTRAINT_CATALOG
                                                                   AND KCU2.CONSTRAINT_SCHEMA = RC.UNIQUE_CONSTRAINT_SCHEMA
                                                                   AND KCU2.CONSTRAINT_NAME = RC.UNIQUE_CONSTRAINT_NAME
                                                                   AND KCU2.ORDINAL_POSITION = KCU.ORDINAL_POSITION
    WHERE KCU.CONSTRAINT_CATALOG = '{databaseName}'
) AS KCU2 ON KCU2.OriginalTableName = Cols.TABLE_NAME
             AND KCU2.OriginalColumnName = Cols.COLUMN_NAME
WHERE Cols.TABLE_NAME <> 'dtproperties'
      AND Cols.TABLE_NAME <> 'sysdiagrams'";
}