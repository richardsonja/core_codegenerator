﻿
namespace Core.Generator
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDatabaseName = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDBSettings = new System.Windows.Forms.Button();
            this.TvEntities = new System.Windows.Forms.TreeView();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.TabControlMain = new System.Windows.Forms.TabControl();
            this.btnSettings = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtDatabaseName
            // 
            this.txtDatabaseName.Location = new System.Drawing.Point(12, 12);
            this.txtDatabaseName.Name = "txtDatabaseName";
            this.txtDatabaseName.Size = new System.Drawing.Size(159, 27);
            this.txtDatabaseName.TabIndex = 0;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(178, 13);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(94, 29);
            this.btnConnect.TabIndex = 1;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // btnDBSettings
            // 
            this.btnDBSettings.Location = new System.Drawing.Point(278, 13);
            this.btnDBSettings.Name = "btnDBSettings";
            this.btnDBSettings.Size = new System.Drawing.Size(94, 29);
            this.btnDBSettings.TabIndex = 2;
            this.btnDBSettings.Text = "DB Settings";
            this.btnDBSettings.UseVisualStyleBackColor = true;
            // 
            // TvEntities
            // 
            this.TvEntities.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TvEntities.Location = new System.Drawing.Point(13, 100);
            this.TvEntities.Name = "TvEntities";
            this.TvEntities.Size = new System.Drawing.Size(359, 640);
            this.TvEntities.TabIndex = 3;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(13, 52);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(359, 42);
            this.btnGenerate.TabIndex = 4;
            this.btnGenerate.Text = "Generate Code";
            this.btnGenerate.UseVisualStyleBackColor = true;
            // 
            // TabControlMain
            // 
            this.TabControlMain.Location = new System.Drawing.Point(379, 52);
            this.TabControlMain.Name = "TabControlMain";
            this.TabControlMain.SelectedIndex = 0;
            this.TabControlMain.Size = new System.Drawing.Size(974, 688);
            this.TabControlMain.TabIndex = 5;
            // 
            // btnSettings
            // 
            this.btnSettings.Location = new System.Drawing.Point(379, 13);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(94, 29);
            this.btnSettings.TabIndex = 6;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1365, 752);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.TabControlMain);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.TvEntities);
            this.Controls.Add(this.btnDBSettings);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtDatabaseName);
            this.Name = "MainForm";
            this.Text = "Main Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDatabaseName;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnDBSettings;
        private System.Windows.Forms.TreeView TvEntities;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TabControl TabControlMain;
        private System.Windows.Forms.Button btnSettings;
    }
}

