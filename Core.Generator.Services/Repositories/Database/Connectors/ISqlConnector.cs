﻿namespace Core.Generator.Services.Repositories.Database.Connectors;

public interface ISqlConnector
{
    Task<Option<int>> ExecuteNonQuerySqlAsync<T>(QueryBase<T> query) where T : IDbDataParameter;

    Task<Option<DataTable>> ExecuteSqlAsync<T>(QueryBase<T> query) where T : IDbDataParameter;
}