﻿using Core.Common.Generics;
using Core.Generator.Services.Models.CodeGenerators;
using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Databases;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators;
using Core.Generator.Services.Tests.Fixtures.Services.CodeGenerators.Types.Sqls;
using FluentAssertions;
using LanguageExt;
using LanguageExt.UnitTesting;
using Moq;
using System.Collections.Immutable;
using System.Linq;
using Xunit;

namespace Core.Generator.Services.Tests.Services.CodeGenerators.Types.Sqls;

public class SelectFindServiceTests :
    IClassFixture<SelectFindServiceFixture>,
    IClassFixture<DatabaseTableColumnFixture>,
    IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;
    private readonly DatabaseTableColumnFixture _databaseTableColumn;
    private readonly SelectFindServiceFixture _service;

    public SelectFindServiceTests(SelectFindServiceFixture service, DatabaseTableColumnFixture databaseTableColumn, ConfigurationFixture configuration)
    {
        _service = service;
        _databaseTableColumn = databaseTableColumn;
        _configuration = configuration;
    }

    [Fact]
    public async void SelectFindService_CreateOutput_ShouldBeNone_GivenConfigReturnsNone()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(Option<ConfigurationSettings>.None);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void SelectFindService_CreateOutput_ShouldCreateOutput_GivenValidInput()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var counter = _databaseTableColumn.Database.Tables.First().Columns.Count() + 1;
        _databaseTableColumn.AddActive(counter++);
        _databaseTableColumn.AddCreateBy(counter++);
        _databaseTableColumn.AddCreateDate(counter++);
        _databaseTableColumn.AddModifyBy(counter++);
        _databaseTableColumn.AddModifyDate(counter++);
        _databaseTableColumn.AddPermanentName(counter++);
        _databaseTableColumn.AddPrimary(counter++);
        _databaseTableColumn.AddTimeStamp(counter++);

        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeFalse();
        results.IsSome.Should().BeTrue();

        results.ShouldBeSome(some => some.TrimInternalExtraSpaces().Should().Be("IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Find_BobTable]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) DROP PROCEDURE [bob].[Find_BobTable] CREATE PROCEDURE [bob].[Find_BobTable] @Bob2 NVARCHAR(MAX) = NULL, @Bob7 NVARCHAR(MAX) = NULL, @Bob8 NVARCHAR(MAX) = NULL, @BobColumn NVARCHAR(MAX) = NULL AS BEGIN SELECT bt.[BobColumn], bt.[Bob2], bt.[Bob3], bt.[Bob4], bt.[Bob5], bt.[Bob6], bt.[Bob7], bt.[Bob8], bt.[Bob9] FROM [bob].[BobTable] as bt WHERE b.[Bob2] = ISNULL(@Bob2, b.[Bob2]) AND b.[Bob7] = ISNULL(@Bob7, b.[Bob7]) AND b.[Bob8] = ISNULL(@Bob8, b.[Bob8]) AND bt.[BobColumn] = ISNULL(@BobColumn, bt.[BobColumn]) END GO"));
    }

    [Fact]
    public async void SelectFindService_CreateOutput_ShouldReturnNone_GivenColumn()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var model = _databaseTableColumn.Database.Tables.First();
        model.Columns = model.Columns
            .Select(x => { x.SystemColumns.IsModifyBy = true; return x; })
            .ToImmutableList();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(model));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void SelectFindService_CreateOutput_ShouldReturnNone_GivenConfigIsFalse()
    {
        _service.Reset();
        _databaseTableColumn.Reset();
        _configuration.Reset();

        _configuration.SetData();

        _configuration.ConfigurationSettings.ConfigurationDetail.OptionsConfiguration.Find = false;
        _service.ConfigurationService.Setup(x => x.GetAsync()).ReturnsAsync(_configuration.ConfigurationSettings);

        _databaseTableColumn.SetDatabase("BobDB", "BobTable", "bob", "BobColumn");
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(_databaseTableColumn.Database.Tables.First()));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void SelectFindService_CreateOutput_ShouldReturnNone_GivenEmptySelectEmptyTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(new Table("was here", "bob")));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void SelectFindService_CreateOutput_ShouldReturnNone_GivenEmptySelectNullTable()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(new CodeGeneratorRequest(null));
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }

    [Fact]
    public async void SelectFindService_CreateOutput_ShouldReturnNone_GivenNullObject()
    {
        _service.Reset();
        var results = await _service.Service.CreateOutputAsync(null);
        results.IsNone.Should().BeTrue();
        results.IsSome.Should().BeFalse();
    }
}