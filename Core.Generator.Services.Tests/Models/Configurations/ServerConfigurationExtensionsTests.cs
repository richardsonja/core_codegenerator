﻿using Core.Generator.Services.Models.Configurations;
using Core.Generator.Services.Tests.Fixtures.Models.Configurations;
using FluentAssertions;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Configurations;

public class ServerConfigurationExtensionsTests : IClassFixture<ConfigurationFixture>
{
    private readonly ConfigurationFixture _configuration;

    public ServerConfigurationExtensionsTests(ConfigurationFixture configuration)
    {
        _configuration = configuration;
    }

    [Theory]
    [InlineData(AuthenticationType.MsSqlWindows, ServerType.MicrosoftSql)]
    [InlineData(AuthenticationType.NotSelectedYet, ServerType.Unknown)]
    [InlineData(AuthenticationType.MsSqlServerAccount, ServerType.MicrosoftSql)]
    [InlineData(AuthenticationType.MySqlServerAccount, ServerType.MySqlSql)]
    public void ServerConfigurationExtensions_GetServerType_ShouldMatchExpected_GivenInput(
        AuthenticationType authenticationType,
        ServerType expected)
    {
        _configuration.Reset();
        _configuration.ConfigurationDetail.ServerConfiguration.AuthenticationType = authenticationType;

        var results = _configuration.ConfigurationDetail.ServerConfiguration.GetServerType();
        results.Should().Be(expected);
    }

    [Theory]
    [InlineData(AuthenticationType.MsSqlWindows, false)]
    [InlineData(AuthenticationType.NotSelectedYet, true)]
    [InlineData(AuthenticationType.MsSqlServerAccount, true)]
    [InlineData(AuthenticationType.MySqlServerAccount, true)]
    public void ServerConfigurationExtensions_IsLoginDataRequired_ShouldMatchExpected_GivenInput(
        AuthenticationType authenticationType,
        bool expected)
    {
        _configuration.Reset();
        _configuration.ConfigurationDetail.ServerConfiguration.AuthenticationType = authenticationType;

        var results = _configuration.ConfigurationDetail.ServerConfiguration.IsLoginBasedConnection();
        results.Should().Be(expected);
    }

    [Fact]
    public void ServerConfigurationExtensions_IsLoginDataRequired_ShouldReturnTrue_GivenEmptyInput()
    {
        ServerConfiguration settings = new();

        var results = settings.IsLoginBasedConnection();
        results.Should().BeTrue();
    }

    [Fact]
    public void ServerConfigurationExtensions_IsLoginDataRequired_ShouldReturnTrue_GivenNullInput()
    {
        ServerConfiguration settings = null;

        var results = settings.IsLoginBasedConnection();
        results.Should().BeTrue();
    }
}