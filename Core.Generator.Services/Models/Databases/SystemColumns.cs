﻿namespace Core.Generator.Services.Models.Databases;

public class SystemColumns : ICloneable
{
    public bool IsActive { get; set; }

    public bool IsCreateBy { get; set; }

    public bool IsCreateDate { get; set; }

    public bool IsModifyBy { get; set; }

    public bool IsModifyDate { get; set; }

    public bool IsPermanentName { get; set; }

    public bool IsTimeStamp { get; set; }

    public object Clone() =>
        new SystemColumns
        {
            IsActive = IsActive,
            IsCreateBy = IsCreateBy,
            IsCreateDate = IsCreateDate,
            IsModifyBy = IsModifyBy,
            IsModifyDate = IsModifyDate,
            IsPermanentName = IsPermanentName,
            IsTimeStamp = IsTimeStamp
        };
}