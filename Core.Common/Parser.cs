﻿using System.Globalization;

namespace Core.Common;

public static class Parser
{
    private delegate bool TryParseHandler<T>(string value, out T result);

    public static bool ToBoolean(this object value, bool defaultValue = default) =>
        value.ToBooleanNullable(defaultValue) ?? defaultValue;

    public static bool? ToBooleanNullable(this object value, bool? defaultValue = default) =>
        value == null || value.ToString().IsNullOrEmpty()
            ? defaultValue
            : value.ToString()?.ToLowerInvariant() switch
            {
                "1" => true,
                "true" => true,
                "y" => true,
                "yes" => true,
                "0" => false,
                "false" => false,
                "n" => false,
                "no" => false,
                _ => value.TryParseNullable(bool.TryParse, defaultValue)
            };

    public static DateTime ToDateTime(this object value, DateTime defaultValue = default) =>
        value.TryParse(DateTime.TryParse, defaultValue);

    public static DateTime? ToDateTimeNullable(this object value, DateTime? defaultValue = default) =>
        value.TryParseNullable(DateTime.TryParse, defaultValue);

    public static decimal ToDecimal(this object value, decimal defaultValue = default) =>
        value.TryParse(decimal.TryParse, defaultValue);

    public static decimal? ToDecimalNullable(this object value, decimal? defaultValue = default) =>
        value.TryParseNullable(decimal.TryParse, defaultValue);

    public static double ToDouble(this object value, double defaultValue = default) => value.TryParse(double.TryParse, defaultValue);

    public static double? ToDoubleNullable(this object value, double? defaultValue = default) =>
        value.TryParseNullable(double.TryParse, defaultValue);

    public static T ToEnum<T>(this object value, T defaultValue = default)
    {
        if (string.IsNullOrEmpty(value?.ToString()))
        {
            return defaultValue;
        }

        try
        {
            return (T)Enum.Parse(typeof(T), value.ToString(), true);
        }
        catch
        {
            return defaultValue;
        }
    }

    public static float ToFloat(this object value, float defaultValue = default) => value.TryParse(float.TryParse, defaultValue);

    public static float? ToFloatNullable(this object value, float? defaultValue = default) =>
        value.TryParseNullable(float.TryParse, defaultValue);

    public static Guid ToGuid(this object value, Guid defaultValue = default) => value.TryParse(Guid.TryParse, defaultValue);

    public static Guid? ToGuidNullable(this object value, Guid? defaultValue = default) =>
        value.TryParseNullable(Guid.TryParse, defaultValue);

    public static int ToInt(this object value, int defaultValue = default) => value.TryParse(int.TryParse, defaultValue);

    public static int? ToIntNullable(this object value, int? defaultValue = default) =>
        value.TryParseNullable(int.TryParse, defaultValue);

    public static long ToLong(this object value, long defaultValue = default) => value.TryParse(long.TryParse, defaultValue);

    public static long? ToLongNullable(this object value, long? defaultValue = default) =>
        value.TryParseNullable(long.TryParse, defaultValue);

    public static short ToShort(this object value, short defaultValue = default) => value.TryParse(short.TryParse, defaultValue);

    public static short? ToShortNullable(this object value, short? defaultValue = default) =>
        value.TryParseNullable(short.TryParse, defaultValue);

    public static TimeSpan ToTimeSpan(this object value, TimeSpan defaultValue = default)
    {
        string[] formats =
        {
            "hh", "hhtt", "hh tt", "%h", "%htt", "%h tt",
            @"h\:mm", @"h\:mmtt", @"h\:mm tt", @"hh\:mm", @"hh\:mmtt", @"h\:mm tt",
            @"d\.hh\:mm\:ss", @"d\.hh\:mm\:sstt", @"d\.hh\:mm\:ss tt",
            "fffff", "hhmm", "hhmmtt", "hhmm tt"
        };

        return value.ToTimeSpan(formats, defaultValue);
    }

    public static TimeSpan ToTimeSpan(this object value, string format, TimeSpan defaultValue = default) =>
        value.ToTimeSpan(new[] { format }, defaultValue);

    public static TimeSpan ToTimeSpan(this object value, string[] formats, TimeSpan defaultValue = default) =>
        value == null || value.ToString().IsNullOrEmpty()
            ? defaultValue
            : DateTime.TryParseExact(value.ToString(), formats, null, DateTimeStyles.None, out var newDate)
                ? newDate.TimeOfDay
                : defaultValue;

    private static T TryParse<T>(this object value, TryParseHandler<T> handler, T defaultValue = default) where T : struct =>
        value?.ToString()?.TryParseNullable(handler) ?? defaultValue;

    private static T? TryParseNullable<T>(this object value, TryParseHandler<T> handler, T? defaultValue = default) where T : struct =>
        (value?.ToString()).IsNullOrEmpty()
            ? defaultValue
            : handler(value.ToString(), out var result)
                ? result
                : defaultValue;
}