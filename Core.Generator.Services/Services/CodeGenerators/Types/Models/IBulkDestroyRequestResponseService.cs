﻿namespace Core.Generator.Services.Services.CodeGenerators.Types.Models;

public interface IBulkDestroyRequestResponseService : ITypeService<bool>
{
}