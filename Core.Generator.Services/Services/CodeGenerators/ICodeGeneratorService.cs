﻿using Core.Generator.Services.Models.CodeGenerators;

namespace Core.Generator.Services.Services.CodeGenerators;

public interface ICodeGeneratorService : ICodeGeneratorServiceType<CodeGeneratorResult>
{
}