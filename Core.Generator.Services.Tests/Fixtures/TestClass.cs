﻿namespace Core.Generator.Services.Tests.Fixtures;

public class TestClass
{
    public TestClass(string name)
    {
        Name = name;
    }

    public string Name { get; }
}