﻿using Core.Common.Generics;
using Core.Generator.Services.Models.Sqls;
using Core.Generator.Services.Tests.Fixtures.Models.Sqls;
using FluentAssertions;
using System.Collections.Generic;
using System.Data;
using Xunit;

namespace Core.Generator.Services.Tests.Models.Sqls;

public class StoredProcedureParameterExtensionsTests : IClassFixture<StoredProcedureParameterFixture>
{
    private readonly StoredProcedureParameterFixture _storedProcedureParameter;

    public StoredProcedureParameterExtensionsTests(StoredProcedureParameterFixture storedProcedureParameter)
    {
        _storedProcedureParameter = storedProcedureParameter;
    }

    [Theory]
    [InlineData(false, false, "@bobColumn NVARCHAR(MAX) = NULL")]
    [InlineData(true, false, "wh.bobColumn = ISNULL(@bobColumn, wh.bobColumn)")]
    [InlineData(false, true, "@bobColumn NVARCHAR(MAX) = NULL")]
    [InlineData(true, true, "bobColumn = ISNULL(@bobColumn, bobColumn)")]
    public void StoredProcedureParameter_Clone_ShouldHaveAllAttributes_GivenSetup(
        bool isWhereStatement,
        bool isTableAbbreviationDisabled,
        string expected
    )
    {
        _storedProcedureParameter.Reset();

        var results = _storedProcedureParameter
            .StoredProcedureParameter
            .SetIsWhereStatement(isWhereStatement)
            .SetIsTableAbbreviationDisabled(isTableAbbreviationDisabled)
            .SetRemoveBrackets(true)
            .SetIsNullable(true)
            .SetIsNullableWhereFormat(true)
            .SetTableAbbreviation("wh")
            .Clone() as StoredProcedureParameter;

        results.Should().NotBeNull();
        results.Name.Should().Be("bobColumn");
        results.TableAbbreviation.Should().Be("wh");
        results.IsWhereStatement.Should().Be(isWhereStatement);
        results.IsNullable.Should().BeTrue();
        results.IsNullableWhereFormat.Should().BeTrue();
        results.RemoveBrackets.Should().BeTrue();
        results.TableName.Should().Be("Bob");
        results.TableSchema.Should().Be("Was Here");
        results.DataType.Should().Be(SqlDbType.NVarChar);
        results.OriginalDataTypeString.Should().Be("nvarchar");
        results.CharacterMaxLength.Should().Be(-1);
        results.Percision.Should().Be(10);
        results.Scale.Should().Be(11);

        results.ToString().TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(false, SqlDbType.Char, "char", null, null, null, "@bobColumn CHAR(MAX)")]
    [InlineData(false, SqlDbType.Char, "char", -1, null, null, "@bobColumn CHAR(MAX)")]
    [InlineData(false, SqlDbType.Char, "char", 50, null, null, "@bobColumn CHAR(50)")]
    [InlineData(false, SqlDbType.NChar, "NChar", null, null, null, "@bobColumn NCHAR(MAX)")]
    [InlineData(false, SqlDbType.NChar, "NChar", -1, null, null, "@bobColumn NCHAR(MAX)")]
    [InlineData(false, SqlDbType.NChar, "NChar", 50, null, null, "@bobColumn NCHAR(50)")]
    [InlineData(false, SqlDbType.NVarChar, "NVarChar", null, null, null, "@bobColumn NVARCHAR(MAX)")]
    [InlineData(false, SqlDbType.NVarChar, "NVarChar", -1, null, null, "@bobColumn NVARCHAR(MAX)")]
    [InlineData(false, SqlDbType.NVarChar, "NVarChar", 50, null, null, "@bobColumn NVARCHAR(50)")]
    [InlineData(false, SqlDbType.VarChar, "VarChar", null, null, null, "@bobColumn VARCHAR(MAX)")]
    [InlineData(false, SqlDbType.VarChar, "VarChar", -1, null, null, "@bobColumn VARCHAR(MAX)")]
    [InlineData(false, SqlDbType.VarChar, "VarChar", 50, null, null, "@bobColumn VARCHAR(50)")]
    [InlineData(false, SqlDbType.Decimal, "Decimal", null, null, null, "@bobColumn DECIMAL(, )")]
    [InlineData(false, SqlDbType.Decimal, "Decimal", -1, 5, null, "@bobColumn DECIMAL(5, )")]
    [InlineData(false, SqlDbType.Decimal, "Decimal", 50, null, 5, "@bobColumn DECIMAL(, 5)")]
    [InlineData(false, SqlDbType.Decimal, "Decimal", 50, 10, 5, "@bobColumn DECIMAL(10, 5)")]
    [InlineData(false, SqlDbType.BigInt, "BigInt", null, null, null, "@bobColumn BIGINT")]
    [InlineData(false, SqlDbType.Binary, "Binary", null, null, null, "@bobColumn BINARY")]
    [InlineData(false, SqlDbType.Bit, "Bit", null, null, null, "@bobColumn BIT")]
    [InlineData(false, SqlDbType.Date, "Date", null, null, null, "@bobColumn DATE")]
    [InlineData(false, SqlDbType.DateTime, "DateTime", null, null, null, "@bobColumn DATETIME")]
    [InlineData(false, SqlDbType.DateTime2, "DateTime2", null, null, null, "@bobColumn DATETIME2")]
    [InlineData(false, SqlDbType.DateTimeOffset, "DateTimeOffset", null, null, null, "@bobColumn DATETIMEOFFSET")]
    [InlineData(false, SqlDbType.Float, "Float", null, null, null, "@bobColumn FLOAT")]
    [InlineData(false, SqlDbType.Image, "Image", null, null, null, "@bobColumn IMAGE")]
    [InlineData(false, SqlDbType.Int, "Int", null, null, null, "@bobColumn INT")]
    [InlineData(false, SqlDbType.Money, "Money", null, null, null, "@bobColumn MONEY")]
    [InlineData(false, SqlDbType.NText, "NText", null, null, null, "@bobColumn NTEXT")]
    [InlineData(false, SqlDbType.Real, "Real", null, null, null, "@bobColumn REAL")]
    [InlineData(false, SqlDbType.SmallDateTime, "SmallDateTime", null, null, null, "@bobColumn SMALLDATETIME")]
    [InlineData(false, SqlDbType.SmallInt, "SmallInt", null, null, null, "@bobColumn SMALLINT")]
    [InlineData(false, SqlDbType.SmallMoney, "SmallMoney", null, null, null, "@bobColumn SMALLMONEY")]
    [InlineData(false, SqlDbType.Structured, "Structured", null, null, null, "@bobColumn STRUCTURED")]
    [InlineData(false, SqlDbType.Text, "Text", null, null, null, "@bobColumn TEXT")]
    [InlineData(false, SqlDbType.Time, "Time", null, null, null, "@bobColumn TIME")]
    [InlineData(false, SqlDbType.Timestamp, "Timestamp", null, null, null, "@bobColumn TIMESTAMP")]
    [InlineData(false, SqlDbType.TinyInt, "TinyInt", null, null, null, "@bobColumn TINYINT")]
    [InlineData(false, SqlDbType.Udt, "Udt", null, null, null, "@bobColumn UDT")]
    [InlineData(false, SqlDbType.UniqueIdentifier, "UniqueIdentifier", null, null, null, "@bobColumn UNIQUEIDENTIFIER")]
    [InlineData(false, SqlDbType.VarBinary, "VarBinary", null, null, null, "@bobColumn VARBINARY")]
    [InlineData(false, SqlDbType.Variant, "Variant", null, null, null, "@bobColumn VARIANT")]
    [InlineData(false, SqlDbType.Xml, "Xml", null, null, null, "@bobColumn XML")]
    [InlineData(true, SqlDbType.Char, "char", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Char, "char", -1, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Char, "char", 50, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.NChar, "NChar", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.NChar, "NChar", -1, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.NChar, "NChar", 50, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.NVarChar, "NVarChar", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.NVarChar, "NVarChar", -1, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.NVarChar, "NVarChar", 50, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.VarChar, "VarChar", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.VarChar, "VarChar", -1, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.VarChar, "VarChar", 50, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Decimal, "Decimal", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Decimal, "Decimal", -1, 5, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Decimal, "Decimal", 50, null, 5, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Decimal, "Decimal", 50, 10, 5, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.BigInt, "BigInt", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Binary, "Binary", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Bit, "Bit", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Date, "Date", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.DateTime, "DateTime", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.DateTime2, "DateTime2", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.DateTimeOffset, "DateTimeOffset", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Float, "Float", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Image, "Image", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Int, "Int", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Money, "Money", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.NText, "NText", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Real, "Real", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.SmallDateTime, "SmallDateTime", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.SmallInt, "SmallInt", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.SmallMoney, "SmallMoney", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Structured, "Structured", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Text, "Text", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Time, "Time", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Timestamp, "Timestamp", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.TinyInt, "TinyInt", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Udt, "Udt", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.UniqueIdentifier, "UniqueIdentifier", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.VarBinary, "VarBinary", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Variant, "Variant", null, null, null, "b.bobColumn = @bobColumn")]
    [InlineData(true, SqlDbType.Xml, "Xml", null, null, null, "b.bobColumn = @bobColumn")]
    public void StoredProcedureParameter_DataTypes_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        bool isWhereStatement,
        SqlDbType dbType,
        string originalDataTypeString,
        int? characterMaxLength,
        int? percision,
        int? scale,
        string expected)
    {
        _storedProcedureParameter.Reset();
        var tableColumn = _storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 1, "bobColumn");
        tableColumn.DataType = dbType;
        tableColumn.OriginalDataTypeString = originalDataTypeString;
        tableColumn.CharacterMaxLength = characterMaxLength;
        tableColumn.Percision = percision;
        tableColumn.Scale = scale;
        _storedProcedureParameter.StoredProcedureParameter = new StoredProcedureParameter(tableColumn);

        var results = _storedProcedureParameter.StoredProcedureParameter.SetRemoveBrackets().SetIsWhereStatement(isWhereStatement).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(false, "@bobColumn NVARCHAR(MAX) = NULL")]
    [InlineData(true, "b.[bobColumn] = @bobColumn")]
    public void StoredProcedureParameter_SetIsNullable_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        bool isWhereStatement,
        string expected)
    {
        _storedProcedureParameter.Reset();

        var results = _storedProcedureParameter.StoredProcedureParameter.SetIsNullable().SetIsWhereStatement(isWhereStatement).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(false, "@bobColumn NVARCHAR(MAX)")]
    [InlineData(true, "b.[bobColumn] = ISNULL(@bobColumn, b.[bobColumn])")]
    public void StoredProcedureParameter_SetIsNullableWhereFormat_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        bool isTableAbbreviationDisabled,
        string expected)
    {
        _storedProcedureParameter.Reset();

        var results = _storedProcedureParameter.StoredProcedureParameter.SetIsNullableWhereFormat().SetIsWhereStatement(isTableAbbreviationDisabled).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(false, false, "@bobColumn NVARCHAR(MAX)")]
    [InlineData(true, false, "@bobColumn NVARCHAR(MAX)")]
    [InlineData(false, true, "b.[bobColumn] = @bobColumn")]
    [InlineData(true, true, "[bobColumn] = @bobColumn")]
    public void StoredProcedureParameter_SetIsTableAbbreviationDisabled_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        bool isTableAbbreviationDisabled,
        bool isWhereStatement,
        string expected)
    {
        _storedProcedureParameter.Reset();

        var results = _storedProcedureParameter.StoredProcedureParameter.SetIsWhereStatement(isWhereStatement).SetIsTableAbbreviationDisabled(isTableAbbreviationDisabled).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(false, "@bobColumn NVARCHAR(MAX)")]
    [InlineData(true, "b.[bobColumn] = \"bobdroppedby\"")]
    public void StoredProcedureParameter_SetOverriddenWhereStatementValue_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        bool isWhereStatement,
        string expected)
    {
        _storedProcedureParameter.Reset();

        var results = _storedProcedureParameter.StoredProcedureParameter
            .SetOverriddenWhereStatementValue("\"bobdroppedby\"").SetIsWhereStatement(isWhereStatement).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(false, "Bob", "IsHere", "WasHere", "@WasHere NVARCHAR(MAX)")]
    [InlineData(false, "Bob", "IsHere", "Was Here", "@WasHere NVARCHAR(MAX)")]
    [InlineData(false, "Bob Was", "Is Here", "WasHere", "@WasHere NVARCHAR(MAX)")]
    [InlineData(false, "Bob Was", "IsHere", "Was Here", "@WasHere NVARCHAR(MAX)")]
    [InlineData(true, "Bob", "IsHere", "WasHere", "ih.WasHere = @WasHere")]
    [InlineData(true, "Bob", "IsHere", "Was Here", "ih.[Was Here] = @WasHere")]
    [InlineData(true, "Bob Was", "Is Here", "WasHere", "ih.WasHere = @WasHere")]
    [InlineData(true, "Bob Was", "IsHere", "Was Here", "ih.[Was Here] = @WasHere")]
    public void StoredProcedureParameter_SetRemoveBrackets_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        bool isWhereStatement,
        string schemaName,
        string tableName,
        string columnName,
        string expected)
    {
        _storedProcedureParameter.Reset();
        var tableColumn = _storedProcedureParameter.CreateTableColumns(tableName, schemaName, "bobDB", 1, columnName);
        _storedProcedureParameter.StoredProcedureParameter = new StoredProcedureParameter(tableColumn);

        var results = _storedProcedureParameter.StoredProcedureParameter.SetRemoveBrackets().SetIsWhereStatement(isWhereStatement).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(false, "@bobColumn NVARCHAR(MAX)")]
    [InlineData(true, "bobdroppedby.[bobColumn] = @bobColumn")]
    public void StoredProcedureParameter_SetTableAbbreviation_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        bool isWhereStatement,
        string expected)
    {
        _storedProcedureParameter.Reset();

        var results = _storedProcedureParameter.StoredProcedureParameter.SetTableAbbreviation("bobdroppedby").SetIsWhereStatement(isWhereStatement).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Theory]
    [InlineData(false, "@bobColumn NVARCHAR(MAX)")]
    [InlineData(true, "b.[bobColumn] = @bobColumn")]
    public void StoredProcedureParameter_ToString_ShouldDisplayBasicMethod_GivenNameOnly(
        bool isWhereStatement,
        string expected)
    {
        _storedProcedureParameter.Reset();

        var results = _storedProcedureParameter.StoredProcedureParameter.SetIsWhereStatement(isWhereStatement).ToString();
        results.TrimInternalExtraSpaces().Should().Be(expected);
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToColumnNames_ShouldProduceString_GivenEmptyList()
    {
        var list = new List<StoredProcedureParameter>();
        list.ForEach(x => x.SetIsWhereStatement(true));

        var results = list.ToColumnNames();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToColumnNames_ShouldProduceString_GivenList()
    {
        var list = new List<StoredProcedureParameter>
        {
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 1, "bobColumn2")),
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 2, "bobColumn1"))
        };
        list.ForEach(x => x.SetIsWhereStatement(true));

        var results = list.ToColumnNames();
        results.TrimInternalExtraSpaces().Should().Be("[bobColumn1], [bobColumn2]");
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToColumnNames_ShouldProduceString_GivenNullList()
    {
        List<StoredProcedureParameter> list = null;

        var results = list.ToColumnNames();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToParameterNames_ShouldProduceString_GivenEmptyList()
    {
        var list = new List<StoredProcedureParameter>();
        list.ForEach(x => x.SetIsWhereStatement(true));

        var results = list.ToParameterNames();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToParameterNames_ShouldProduceString_GivenList()
    {
        var list = new List<StoredProcedureParameter>
        {
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 1, "bobColumn2")),
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 2, "bobColumn1"))
        };
        list.ForEach(x => x.SetIsWhereStatement(true));

        var results = list.ToParameterNames();
        results.TrimInternalExtraSpaces().Should().Be("@bobColumn1, @bobColumn2");
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToParameterNames_ShouldProduceString_GivenNullList()
    {
        List<StoredProcedureParameter> list = null;

        var results = list.ToParameterNames();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToSetStatement_ShouldProduceString_GivenEmptyList()
    {
        var list = new List<StoredProcedureParameter>();
        list.ForEach(x => x.SetIsWhereStatement(true));

        var results = list.ToSetStatement();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToSetStatement_ShouldProduceString_GivenList()
    {
        var list = new List<StoredProcedureParameter>
        {
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 1, "bobColumn2")),
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 2, "bobColumn1"))
        };
        list.ForEach(x => x.SetIsWhereStatement(true));

        var results = list.ToSetStatement();
        results.TrimInternalExtraSpaces().Should().Be("SET b.[bobColumn1] = @bobColumn1, b.[bobColumn2] = @bobColumn2");
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToSetStatement_ShouldProduceString_GivenNullList()
    {
        List<StoredProcedureParameter> list = null;

        var results = list.ToSetStatement();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToStoredProcedureParameter_ShouldProduceString_GivenEmptyList()
    {
        var list = new List<StoredProcedureParameter>();
        list.ForEach(x => x.SetIsWhereStatement(false));

        var results = list.ToStoredProcedureParameter();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToStoredProcedureParameter_ShouldProduceString_GivenList()
    {
        var list = new List<StoredProcedureParameter>
        {
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 1, "bobColumn2")),
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 2, "bobColumn1"))
        };
        list.ForEach(x => x.SetIsWhereStatement(false));

        var results = list.ToStoredProcedureParameter();
        results.TrimInternalExtraSpaces().Should().Be("@bobColumn1 NVARCHAR(MAX), @bobColumn2 NVARCHAR(MAX)");
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToStoredProcedureParameter_ShouldProduceString_GivenNullList()
    {
        List<StoredProcedureParameter> list = null;

        var results = list.ToStoredProcedureParameter();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToWhereStatement_ShouldProduceString_GivenEmptyList()
    {
        var list = new List<StoredProcedureParameter>();
        list.ForEach(x => x.SetIsWhereStatement(true));

        var results = list.ToWhereStatement();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToWhereStatement_ShouldProduceString_GivenList()
    {
        var list = new List<StoredProcedureParameter>
        {
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 1, "bobColumn2")),
            new (_storedProcedureParameter.CreateTableColumns("Bob", "Was Here", "bobDB", 2, "bobColumn1"))
        };
        list.ForEach(x => x.SetIsWhereStatement(true));

        var results = list.ToWhereStatement();
        results.TrimInternalExtraSpaces().Should().Be("WHERE b.[bobColumn1] = @bobColumn1 AND b.[bobColumn2] = @bobColumn2");
    }

    [Fact]
    public void StoredProcedureParameterExtensions_ToWhereStatement_ShouldProduceString_GivenNullList()
    {
        List<StoredProcedureParameter> list = null;

        var results = list.ToWhereStatement();
        results.TrimInternalExtraSpaces().Should().BeEmpty();
    }
}